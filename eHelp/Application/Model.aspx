﻿<%@ Page Language="C#" MasterPageFile="Main.Master" AutoEventWireup="true" CodeFile="Model.aspx.cs" Inherits="eFrameWork.Application.Model" %>
<%@ Import Namespace="EKETEAM.FrameWork" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%if (!Ajax){ %>
<div class="nav">您当前位置：<a href="<%=eBase.getApplicationHomeURL() %>">首页</a><%= model.ModelLink%>
<%if (eRegisterInfo.Base == 0 && eRegisterInfo.Loaded){ %><a href="http://frame.eketeam.com\" class="help" target="_blank" title="eFrameWork开发框架">&nbsp;</a><%}%>
<%=model.ActionBottons %></div>
<%}%>
<%= model.StartHTML   %>
<%= model.Tip  %>
<asp:Literal ID="LitBody" runat="server" />
</asp:Content>
