﻿<%@ Page Language="C#" MasterPageFile="Main.Master" AutoEventWireup="true" CodeFile="ModifyPass.aspx.cs" Inherits="eFrameWork.Application.ModifyPass" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="nav">您当前位置：<a href="<%=eBase.getApplicationHomeURL() %>">首页</a>&nbsp;->&nbsp;修改密码</div>
<script>
    function modifyPass(frm) {
        var _back = eCheckform(frm);
        if (!_back) { return; }
        $.ajax({
            type: 'post',
            url: $(frm).attr("action"),
            data: "ajax=true&" + $(frm).serialize(),
            dataType: "json",
            success: function (data) {
                if (parseBool(data.success)) {
                    layer.msg(data.message);

                }
                else {
                    layer.msg(data.message);
                }
                $(document).find("input[type='password']").each(function (i, elem) {
                    elem.value = "";
                });
            }
        });
    };
</script>
<div style="margin:10px;">
<form name="frmaddoredit" id="frmaddoredit" method="post" enctype="multipart/form-data" action="" onsubmit="return checkfrm(this);">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="eDataView">
<colgroup>
<col width="120" />
<col />
</colgroup>
    <tr>
      <td width="120" class="title"><font color="#FF0000"> *</font>&nbsp;旧密码：</td>
      <td class="content"><span class="eform"><input name="f1" type="password" class="text" id="f1" value="" fieldname="旧密码" notnull="true" style="width:150px;" /></span></td>
    </tr>
    <tr>
      <td class="title"><font color="#FF0000"> *</font>&nbsp;新密码：</td>
      <td class="content"><span class="eform"><input name="f2" type="password" class="text" id="f2" value="" fieldname="新密码" notnull="true" style="width:150px;" /></span></td>
    </tr>
    <tr>
      <td class="title"><font color="#FF0000"> *</font>&nbsp;确认密码：</td>
      <td class="content"><span class="eform"><input name="f3" type="password" class="text" id="f3" value="" fieldname="确认密码" notnull="true" equal="f2" style="width:150px;" /></span></td>
    </tr>
	<tr>
		<td colspan="2" class="title" style="text-align:left;padding-left:125px;">
		<a class="button" href="javascript:;" onclick="modifyPass(frmaddoredit);" _onclick="submitForm(frmaddoredit);"><span><i class="save">修改</i></span></a>
		<a class="button" href="javascript:;" style="margin-left:30px;" onclick="history.back();"><span><i class="back">返回</i></span></a>
		</td>
	</tr>
</table>
</form>
</div>
</asp:Content>
