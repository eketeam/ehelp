﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using EKETEAM.UserControl;

namespace eFrameWork.Application
{
    public partial class Model : System.Web.UI.Page
    {
        public string UserArea = "Application";
        public eModel model;
        public bool Ajax = false;
        public string mode = eParameters.Request("mode").ToLower();
        public eUser user;
        protected void Page_Load(object sender, EventArgs e)
        {
            //Menu Menu1 = (Menu)Master.FindControl("Menu1");if (Menu1 != null){Menu1.aaa = "BBB"; }
            user = new eUser(UserArea);
            user.Check();

            model = new eModel();
            model.clientMode = "pc";
            LitBody.Text = model.autoHandle();

           
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Request.QueryString["ajax"] != null) Ajax = Convert.ToBoolean(Request.QueryString["ajax"]);
            if (Master == null) return;
            if (!Ajax)
            {
                MasterPageFile = "Main.Master";
            }
            else
            {
                MasterPageFile = "MainNone.Master";
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (Master == null) return;
            Literal lit = (Literal)Master.FindControl("LitTitle");
            if (lit != null) lit.Text = model.ModelInfo["mc"].ToString() + "-" + eConfig.ApplicationTitle(user["SiteID"].ToString()); // model.ModelInfo["mc"].ToString() + " - " +

               

            lit = (Literal)Master.FindControl("LitJavascript");
            if (lit != null && model.Javasctipt.Length>0) lit.Text = model.Javasctipt;


            lit = (Literal)Master.FindControl("LitStyle");
            if (lit != null && model.cssText.Length > 0) lit.Text = model.cssText;

        }
    }
}