﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CheckupRecords.aspx.cs" Inherits="eFrameWork.Customs.Base.CheckupRecords" %>
<ev:eListControl ID="eListControl1" showBodyAlways="true" LineHeight="40" runat="server" >
<ev:eListColumn FieldName="序号" width="80" runat="server">{row:index}</ev:eListColumn>
<ev:eListColumn FieldName="审核流程" Field="CheckupText" runat="server" />
<ev:eListColumn FieldName="审核状态" Field="CheckupState" Options="[{text:<font color='#00CC00'>通过</font>,value:1},{text:<font color='#FF0000'>不通过</font>,value:2},{text:<font color='#FF0000'>作废</font>,value:3}]" ReplaceString="[{text:不详,value:0}]" runat="server" />
<ev:eListColumn FieldName="审核意见" Field="CheckupIdea"  runat="server" />
<ev:eListColumn FieldName="操作人" Field="UserID" BindSQL="select xm from a_eke_sysUsers where UserId='{data:UserID}'" runat="server" />
<ev:eListColumn FieldName="审核时间" Field="addTime" Width="150" FormatString="{0:yyyy-MM-dd HH:mm}" runat="server" />
</ev:eListControl>

<asp:Repeater id="Rep" runat="server">
<HeaderTemplate>
<table id="Table1" class="eDataTable" width="100%" cellspacing="0" cellpadding="0" border="0">
<thead>
<tr>
<td width="80">序号</td>
<td width="356">审核流程</td>
<td width="356">审核状态</td>
<td width="356">审核意见</td>
<td width="355">操作人</td>
<td width="150">审核时间</td>
</tr>
</thead>
<tbody>
</HeaderTemplate>
<ItemTemplate>
<tr>
<td height="40"><%# Container.ItemIndex + 1%></td>
<td><%# Eval("CheckupText").ToString()%></td>
<td><%# Eval("CheckupState").ToString().Replace("1","<font color='#00CC00'>通过</font>").Replace("2","<font color='#FF0000'>不通过</font>").Replace("3","<font color='#FF0000'>作废</font>") %></td>
<td><%# Eval("SignFile").ToString().Length==0 ? Eval("CheckupIdea").ToString() : "<img border=\"0\" style=\"width:260px;height:40px;\" src=\"../" + Eval("SignFile").ToString() + "\">" %></td>
<td><asp:Literal id="LitUser" runat="server" /></td>
<td><%# Eval("addTime","{0:yyyy-MM-dd HH:mm}").ToString() %></td>
</tr>
</ItemTemplate>
<FooterTemplate>
</tbody>
</table>
</FooterTemplate>
</asp:Repeater>

<asp:Repeater id="RepMobile" runat="server">
<ItemTemplate>
<dl class="eCard">
<dt><span><%# Eval("CheckupState").ToString().Replace("1","<font color='#00CC00'>通过</font>").Replace("2","<font color='#FF0000'>不通过</font>").Replace("3","<font color='#FF0000'>作废</font>") %></span><div><%# Eval("CheckupText").ToString() %></div></dt>
<dd>
<table>
<tr>
<td width="<%=ColumnTitleWidth %>" class="title">审核人：</td>
<td><asp:Literal id="LitUser" runat="server" /></td>
</tr>
<tr>
<td class="title">审核意见：</td>
<td><%# Eval("SignFile").ToString().Length==0 ? Eval("CheckupIdea").ToString() : "<img border=\"0\" style=\"width:260px;height:60px;\" src=\"../" + Eval("SignFile").ToString() + "\">" %></td>
</tr>
<tr>
<td class="title">审核时间：</td>
<td><%# Eval("addTime","{0:yyyy-MM-dd HH:mm}").ToString() %></td>
</tr>
</table>
</dd>
</dl>
</ItemTemplate>
</asp:Repeater>