﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExcelImport.aspx.cs" Inherits="eFrameWork.Customs.Base.ExcelImport" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<script src="../../Scripts/init.js?ver=<%=Common.Version %>"></script>
<script src="../../Plugins/layui226/layui.all.js"></script>
<link href="../../Plugins/eControls/default/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />   
<link href="../../Plugins/Theme/manage/st33yle.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />   
  <style>
    select {width:85%;
        }
    body {font-size:13px;margin:8px 8px 20px 8px;width:auto;min-width:80%; height:auto;
    }
    div.itemtitle {
    padding:6px 0px 10px 0px; color:#66666; font-weight:bold;font-size:13px;
    }
    input {vertical-align:middle;}

.eDataTable thead td,.eDataTable thead th{white-space:normal;word-break:break-all;he3ight:50px;line-height:21px;}
.eDataTable thead div{white-space:normal;word-break:break-all;min-height:40px;text-align:center;}
.eDataTable thead div,.eDataTable tbody td div{display: -webkit-box;display: -webkit-flex;display: flex;-webkit-box-align: center;-webkit-align-items: center;align-items: center;}
.eDataTable tbody td div p{ white-space: nowrap;overflow:hidden;-o-text-overflow:ellipsis;text-overflow:ellipsis;}
.btn {display: inline-block;width:120px;border-radius: 4px;background-color: #499BFE;color: #fff;line-height:35px;text-align:center;}
 </style>
<script>
    function submitform()
    {
        if ($("input:checked").length == 0) {
            layer.msg("请选择要导入的数据!");
            return;
        }
        form1.submit();
    };
    function show(obj,idx)
    {
        if (obj.checked)
        {
            $(".box" + idx).show();
        }
        else
        {
            $(".box" + idx).hide();
        }
    };
    function checkfrm(frm) {
        if (frm.imgFile.value.length == 0) {
            top.layer.msg("请选择要上传的文件!");
            frm.imgFile.focus();
            return false;
        }
        return true;
    }
</script>
<body>
<asp:Literal id="litBody" runat="server" />
 <%if(file.Length ==0){ %>
<form method="post" enctype="multipart/form-data" id="Form1" onSubmit="return checkfrm(this);">
    <p style="margin-top:8px;">
    <INPUT type="file" id="imgFile" name="imgFile" style="width:160px;overflow:hidden;vertical-align:middle;" onchange="autoupload_check(this);" allowexts="<%=allowExts.ToString()%>" preventexts="<%=eConfig.PreventExtensions.ToString()%>" accept="<%=allowExts.ToString(",")%>" />&nbsp;<INPUT name="button" type="submit" id="button" value=" 导 入 ">
	<input name="act" type="hidden" id="act" value="save">
    </p>
</form>
<%}else{ %>

<%} %>
</body>
</html>