﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using EKETEAM.Data;
using EKETEAM.FrameWork;

namespace eFrameWork.Customs.Base
{
    public partial class FileSelect : System.Web.UI.Page
    {
        private string basePath = "";//当前目录
        public string path = eParameters.QueryString("path");
        string aspxFile = eBase.getAspxFileName();
        public string obj = eParameters.QueryString("obj");
        public string siteid = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            siteid = eConfig.SiteID();
            basePath = Server.MapPath("~/") + "upload\\" + (siteid.Length > 0 ? siteid + "\\" : "") + "filemanage\\";
            if (path.Length > 0) basePath += path.Replace("/", "\\") + "\\";
            if (!Directory.Exists(basePath)) Directory.CreateDirectory(basePath);

            //eBase.Writeln(basePath );
            //eBase.End();

            StringBuilder sb = new StringBuilder();
            sb.Append("<div class=\"filemanage_local\">");
            if (path.Length > 0)
            {
                sb.Append("<a href=\"" + aspxFile + "?obj=" + obj + "\">根目录</a>");
                string[] arr = path.Split("/".ToCharArray());
                string tmp = "";
                for (int i = 0; i < arr.Length; i++)
                {
                    if (i > 0) tmp += "/";
                    tmp += arr[i];
                    if (i == arr.Length - 1)
                    {
                        sb.Append(" / " + arr[i]);
                    }
                    else
                    {
                        sb.Append(" / <a href=\"" + aspxFile + "?obj=" + obj + "&path=" + tmp + "\">" + arr[i] + "</a>");
                    }

                }
            }
            else
            {
                sb.Append("根目录");
            }
            sb.Append("</div>");

            sb.Append("<div class=\"filemanage_files\">");
            System.IO.DirectoryInfo dinfo = new DirectoryInfo(basePath);
            foreach (DirectoryInfo a in dinfo.GetDirectories())
            {
                sb.Append("<a href=\"" + aspxFile + "?obj=" + obj + "&path=" + (path.Length > 0 ? path + "/" : path) + a.Name.ToString() + "\" title=\"" + a.Name + "\">\r\n");

                sb.Append("<dl>\r\n");
                sb.Append("<dt class=\"folder\"></dt>\r\n");
                sb.Append("<dd>" + a.Name + "</dd>\r\n");
                sb.Append("</dl>\r\n");
                sb.Append("</a>\r\n");
            }

            foreach (FileInfo a in dinfo.GetFiles())
             {
                 getFileHTML(sb, a.Name);
     
             }

            sb.Append("</div>");

            litBody.Text = sb.ToString();
        }
        private void getFileHTML(StringBuilder sb, string name)
        {
            string _path = "../../upload/" + (siteid.Length > 0 ? siteid + "/" : "")  + "filemanage" + (path.Length > 0 ? "/" + path : "") + "/" + name;
            FileInfo info = new FileInfo(Server.MapPath(_path));


            eFileInfo fi = new eFileInfo(name);
            sb.Append("<a href=\"javascript:;\" onclick=\"parent.$('#" + obj + "').val('upload/" + (siteid.Length > 0 ? siteid + "/" : "") + "filemanage" + (path.Length > 0 ? "/" + path : "") + "/" + name + "');parent.layer.close(parent.arrLayerIndex.pop());\"  title=\"" + name + "&#10;创建时间：" + string.Format("{0:yyyy-MM-dd HH:mm:ss}", info.CreationTime) + "&#10;修改时间：" + string.Format("{0:yyyy-MM-dd HH:mm:ss}", info.LastWriteTime) + "&#10;大小：" + eBase.getFileSize(info.Length) + "\">\r\n");
            sb.Append("<dl>\r\n");
            if (".jpg.jpeg.gif.bmp.png.tif".IndexOf("." + fi.Extension.ToLower()) > -1)
            {
                sb.Append("<dt><img src=\"../../upload/" + (siteid.Length > 0 ? siteid + "/" : "") + "filemanage" + (path.Length > 0 ? "/" + path : "") + "/" + name + "\" /></dt>\r\n");
            }
            else
            {

                switch (fi.Extension.ToLower())
                {
                    case "zip":
                        sb.Append("<dt class=\"zip\"></dt>\r\n");
                        break;
                    default:
                        sb.Append("<dt class=\"file\"></dt>\r\n");
                        break;
                }
            }
            sb.Append("<dd>" + name + "</dd>\r\n");
            sb.Append("</dl>\r\n");
            sb.Append("</a>\r\n");
        }
    }
}