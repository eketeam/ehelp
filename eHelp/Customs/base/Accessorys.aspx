﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Accessorys.aspx.cs" Inherits="eFrameWork.Customs.Base.Accessorys" %>
<%@ Import Namespace="System.Data" %>
<style>
.eDataTable td a.btnupload{
position: relative;
display:inline-block;width:16px;height:16px;font-size:1px;background: url(../Plugins/eControls/default/images/eDataTable_btn_add.png) no-repeat center center;_background: none;
_filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true, sizingMethod=scale,src="../Plugins/eControls/default/images/eDataTable_btn_add.png");
-moz-opacity:0.7;-khtml-opacity: 0.7;opacity: 0.7;filter:alpha(opacity=70);
cursor:pointer;
}
.eDataTable td a.btnupload:hover{-moz-opacity:1;-khtml-opacity: 1;opacity: 1;filter:alpha(opacity=100);}
.eDataTable td a.btnupload .file
{
position:absolute;
left:0px;top:0px;
width: 16px;
height: 16px;
filter:alpha(opacity=0);-moz-opacity:0;-khtml-opacity: 0; opacity: 0;
cursor:pointer;
}

</style>

<script>
//文件上传回调
    function Accessorys_callback(files) {
        for (var i = 0; i < files.length; i++) {
            Accessorys_append(files[i]);
        }
    };
function Accessorys_delete(obj)
{
	var modelid="<%=modelid%>";
	if(!confirm("确认删除吗？")){return;}
	var tr = getParent(obj, "tr");
	if (tr.getAttribute("eRowID") == null)
	{
	    tr = tr.parentNode;
	    tr = getParent(tr, "tr");
	}
	var tbody = getParent(tr, "tbody");
	var index=tr.Index();
	var input=getobj("eformdata_" + modelid);
	var model = input.value.toJson();
	
	var _model=model.get("eformdata_" + modelid);
	_model = _model[index];
	var src = _model["m1_f5"];
	if (src.toLowerCase().indexOf("/temp/") > -1) {
	    var url = "<%=eBase.getVirtualPath()%>Plugins/ProUpload.aspx?act=del&file=" + src;
	    $.ajax({
	        type: "GET", async: true,
	        url: url,
	        dataType: "html",
	        success: function (data) {
	           // alert("ok:" + data);
	        },
	        error: function (XMLHttpRequest, textStatus, errorThrown) {
	            //alert("error:" + XMLHttpRequest.responseText + "::" + XMLHttpRequest.status + "::" + XMLHttpRequest.readyState + "::" + XMLHttpRequest.statusText);
	        }
	    });
	}


	if(_model["ID"].length==0)
	{
		_model=model.get("eformdata_" + modelid)
		_model.remove(index);	
		tbody.removeChild(tr);
	}
	else
	{
		_model["Delete"]="true";
		tr.style.display="none";
	}
	
	//alert(index + "::" + _model["ID"]);
	//alert(_model.tostring());
	//alert(model.tostring());
	input.value = model.toJson();
};
function Accessorys_append(data)
{
	var modelid="<%=modelid%>";
	var input=getobj("eformdata_" + modelid);
	var model=input.value.toJson();	

	
	var _data=model.get("eformdata_" + modelid);	
	var json = new eJson2();
	
	json["ID"]="";
	json["Delete"]="false";
	json["m1_f1"]=data.name;
	json["m1_f2"]=data.ext;
	json["m1_f3"]=data.size;
	json["m1_f4"]=data.time;
	json["m1_f5"]=data.url;
	
	_data.append(json);
	
	//alert(model.tostring());	
	//alert(_data.tostring());
	input.value = model.toJson();

	
	var table=getobj("eformlist_" + modelid);
	var tbody=table.getElementsByTagName("tbody")[0];
	var tfoot=table.getElementsByTagName("tfoot")[0];	
	var tr = tfoot.getElementsByTagName("tr")[0].cloneNode(true);

	
	//tr.getElementsByTagName("td")[1].innerHTML=data.name;
	//tr.getElementsByTagName("td")[2].innerHTML=data.ext;
	//tr.getElementsByTagName("td")[3].innerHTML=data.size;
	//tr.getElementsByTagName("td")[4].innerHTML = data.time;
	$(tr).find("[field='m1_f1']").html(data.name);
	$(tr).find("[field='m1_f2']").html(data.ext);
	$(tr).find("[field='m1_f3']").html(data.size);
	$(tr).find("[field='m1_f4']").html(data.time);
	var html = '';
	var url = data.url;
	if (url.toLowerCase().indexOf("upload/") > 0) { url = url.substring(url.toLowerCase().indexOf("upload/")); }

	html += '<a class="btn" href="javascript:;" onclick="top.viewFile(\'' + url + '\');">查看</a>';
    //html += '<a href="' + data.url + '" target="_blank">下载</a>';

	html += '<a class="btn" href="<%=aspxfile%>?act=down&path=' + url + '&name=' + data.name + '" target="_blank">下载</a>\n';
    <%if(IsMobile){ %>html += '<a class="btn" href="javascript:;" onclick="Accessorys_delete(this);">删除</a>';<%}%>
    //tr.getElementsByTagName("td")[5].innerHTML=html;
    $(tr).find("[field='m1_f5']").html(html);
	tbody.appendChild(tr);	
};
</script>
<%if(1==12){ %>
<textarea id="eformdata_<%=modelid %>" style="width:600px;height:120px;" name="eformdata_<%=modelid %>"><%=HttpUtility.HtmlEncode(getJson) %></textarea>
<%}else{%>
<input type="hidden" id="eformdata_<%=modelid %>" name="eformdata_<%=modelid %>" value="<%=HttpUtility.HtmlEncode(getJson) %>">
<%}%>
<%if(IsMobile){ %>

<table id="eformlist_<%=modelid %>" width="100%" cellpadding="0" cellspacing="0">
<tbody>
<%foreach(DataRow dr in Data.Rows){%>
<tr eRowID="" >
<td>
    <dl class="eCard">
    <dt field="m1_f1"><%=dr["MC"].ToString()%></dt>
     <dd>
       <table width="100%">
           <tr>
               <td width="70" class="title">文件类型：</td>
               <td field="m1_f2"><%=dr["ext"].ToString()%></td>             
           </tr>
           <tr>
               <td class="title">文件大小：</td>
               <td field="m1_f3"><%=dr["size"].ToString()%></td>
           </tr>
           <tr>
               <td class="title">上传时间：</td>
               <td field="m1_f4"><%=string.Format("{0:yyyy-MM-dd HH:mm:ss}",dr["addTime"])%></td>
           </tr>
           <tr>
               <td class="title">操作：</td>
               <td field="m1_f5">
                    <a class="btn" href="javascript:;" onclick="top.viewFile('<%=dr["path"].ToString() %>');">查看</a>
                   <a class="btn" href="<%=aspxfile%>?act=down&path=<%=  dr["path"].ToString() %>" href_bak="<%=  dr["path"].ToString()%>" target="_blank">下载</a>
                   <%if(act != "view"){%><a class="btn" href="javascript:;" onclick="Accessorys_delete(this);">删除</a><%}%>
               </td>
           </tr>
       </table>
     </dd>
    </dl>
</td>
</tr>
<%}%>
</tbody>
<tfoot style="display:none;">
<tr eRowID="" >
<td>
<dl class="eCard">
    <dt field="m1_f1">&nbsp;</dt>
     <dd>
       <table>
           <tr>
               <td class="title">文件类型：</td>
               <td field="m1_f2">&nbsp;</td>             
           </tr>
           <tr>
               <td class="title">文件大小：</td>
               <td field="m1_f3">&nbsp;</td>
           </tr>
           <tr>
               <td class="title">上传时间：</td>
               <td field="m1_f4">&nbsp;</td>
           </tr>
           <tr>
               <td class="title">操作：</td>
               <td field="m1_f5">&nbsp;</td>
           </tr>
       </table>
     </dd>
    </dl>
</td>



</tr>
</tfoot>
</table>
<%if(act=="add" || act=="edit" || act=="copy"){ %>
<a href="javascript:;" class="subbtnadd">
<input class="file" type="file" id="file1" name="file" exclude="true" vpath="../" callback="Accessorys_callback" onchange="autoupload_change(this);" preventexts="<%=eConfig.PreventExtensions.ToString()%>" multiple="">
</a>
<%}%>
<%}else{ %>
<table id="eformlist_<%=modelid %>" class="eDataTable" cellpadding="0" cellspacing="0" width="100%">
    <colgroup>
        <%if(act != "view"){%><col width="40" /><%}%>
		<col width="150" />
        <col width="150" />
        <col width="150" />
        <col width="160" />
        <col width="90" />
    </colgroup>
<thead>
<tr>
<%if(act != "view"){%><td width="40"><a href="javascript:;" class="btnupload"><input class="file" type="file" id="file" name="file" exclude="true" vpath="../" callback="Accessorys_callback" onchange="autoupload_change(this);" preventexts="<%=eConfig.PreventExtensions.ToString()%>" multiple=""></a></td><%}%>
<td>附件名称</td>
<td>文件类型</td>
<td>文件大小</td>
<td>上传时间</td>
<td width="90">操作</td>
</tr>
</thead>
<tbody>
<%foreach(DataRow dr in Data.Rows){%>
<tr eRowID="" >
<%if(act != "view"){%><td name=""><a href="javascript:;" onclick="Accessorys_delete(this);" class="btndel">&nbsp;</a></td><%}%>
<td field="m1_f1" height="35"><%=dr["MC"].ToString()%></td>
<td field="m1_f2"><%=dr["ext"].ToString()%></td>
<td field="m1_f3"><%=dr["size"].ToString()%></td>
<td field="m1_f4"><%=string.Format("{0:yyyy-MM-dd HH:mm:ss}",dr["addTime"])%></td>
<td field="m1_f5">
    <a class="btn" href="javascript:;" onclick="top.viewFile('<%=dr["path"].ToString() %>');">查看</a>
    <a href="<%=aspxfile%>?act=down&path=<%= dr["path"].ToString() %>&name=<%=dr["MC"].ToString() %>" href_bak="<%= dr["path"].ToString()%>" target="_blank">下载</a>
</td>
</tr>
<%}%>
</tbody>
<tfoot style="display:none;">
<tr eRowID="" >
<%if(act != "view"){%><td name=""><a href="javascript:;" onclick="Accessorys_delete(this);" class="btndel">&nbsp;</a></td><%}%>
<td field="m1_f1" height="35">&nbsp;</td>
<td field="m1_f2">&nbsp;</td>
<td field="m1_f3">&nbsp;</td>
<td field="m1_f4">&nbsp;</td>
<td field="m1_f5">&nbsp;</td>
</tr>
</tfoot>
</table>
<%}%>