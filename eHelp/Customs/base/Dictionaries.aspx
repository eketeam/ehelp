﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Dictionaries.aspx.cs" Inherits="eFrameWork.Customs.Base.Dictionaries" %>
<%@ Import Namespace="EKETEAM.FrameWork" %>
<%if (!Ajax)
{ %>
<script>
var menu_a=null;
var win_width = "650px";
var win_height = "360px";
if (IsMobile())
{
    win_width = "95%";
    win_height = "60%";
}
function postback()
{
    var url = document.location.href;
	url = url.addquerystring("ajax", "true");	
	$.ajax({
		   type: "get",
		   url:url,
		   dataType: "json",
		   success: function(data)
		   {
		   		if(parent.arrLayerIndex.length>0)
				{
					parent.layer.close(parent.arrLayerIndex.pop());
		   		}
		   		else
		   		{
		   		    if (arrLayerIndex.length > 0)
		   		    {
		   		        layer.close(arrLayerIndex.pop());
		   		    }
		   		}
				
				var etreebody=getobj("etreebody");
				if(etreebody)
				{
					$(etreebody).html(data.body.decode());					
					bindTree();
				}
				
		   }
	});	
};
function view()
{
    var url = "<%=aspxfile%>?";
    if (typeof (AppItem) == "string" && AppItem.length > 0) {
        url += "AppItem=" + AppItem;
    }
    if (typeof (ModelID) == "string" && ModelID.length > 0) {
        url += "ModelID=" + ModelID;
    }
    url += "&ajax=true&frame=true&act=view";

    var dataid = menu_a.getAttribute("dataid");
    url += "&id=" + dataid;
    layer.open({
        type: 2,
        //skin: 'layui-layer-rim', //加上边框
        title: "查看",
        maxmin: false,
        shadeClose: true, //点击遮罩关闭层
        area: [win_width, win_height],
        //scrollbar: false, 
        //move: false,		
        content: [url, 'no'],
        //content: url,
        success: function (layero, index) {
            arrLayerIndex.push(index);
        }
    });
};
function edit()
{

    var url = "<%=aspxfile%>?";  
    if (typeof (AppItem) == "string" && AppItem.length > 0) {
        url += "AppItem=" + AppItem;
    }
    if (typeof (ModelID) == "string" && ModelID.length > 0) {
        url += "ModelID=" + ModelID;
    }
    url += "&ajax=true&frame=true&act=edit";
	var dataid=menu_a.getAttribute("dataid");
	url+="&id=" + dataid;
	layer.open({
      type: 2,
	  //skin: 'layui-layer-rim', //加上边框
      title: "编辑",
      maxmin: false,
      shadeClose: true, //点击遮罩关闭层
      //area: win_width,
     // shade: [0.8, '#393D49'],
      area : [win_width , win_height],
	  //scrollbar: false, 
	  //move: false,		
	  content: [url,'no'], 
	  //content: url,
	  success: function(layero, index)
	  {
	      
	     // layer.iframeAuto(index);
	      arrLayerIndex.push(index);
  	  }
    });	
};
function add(obj)
{
	if(obj){menu_a=obj;}
	var url = "<%=aspxfile%>?";
	if (typeof (AppItem) == "string" && AppItem.length > 0) {
	    url += "AppItem=" + AppItem;
	}
	if (typeof (ModelID) == "string" && ModelID.length > 0) {
	    url += "ModelID=" + ModelID;
	}

	url += "&ajax=true&frame=true&act=add";
	if(menu_a)
	{
		var dataid=menu_a.getAttribute("dataid");
		
		if(dataid.length>0)
		{
			url+="&pid=" + dataid;
		}
	}
	layer.open({
      type: 2,
	  //skin: 'layui-layer-rim', //加上边框
      title: "添加",
      maxmin: false,
      shadeClose: true, //点击遮罩关闭层
      area : [win_width , win_height],
	  //scrollbar: false, 
	  //move: false,		
	  content: [url,'no'], 
	  //content: url,
	  success: function(layero, index)
	  {
		arrLayerIndex.push(index);
  	  }
    });	
};
function del()
{
	var _back=confirm('确认要删除吗？');
	if(!_back){return;};
	var url = "<%=aspxfile%>?";  
	if (typeof (AppItem) == "string" && AppItem.length > 0) {
	    url += "AppItem=" + AppItem;
	}
	if (typeof (ModelID) == "string" && ModelID.length > 0) {
	    url += "ModelID=" + ModelID;
	}
	url += "&ajax=true&act=del&id=" + menu_a.getAttribute("dataid");
	$.ajax({
		   type: "get",
		   url:url,
		   dataType: "json",
		   success: function(data)
		   {
		       if (data.errcode == "0") {
		           //$(menu_a).parent().remove();
		           menu_a = null;
		           postback();
		       }
		       else if (data.errcode == "101")
		       {
		           layer.msg(data.message);
		           setTimeout(function () { document.location.assign(document.location.href) }, 2000);
		       }
		       else {
		           layer.msg(data.message);
		       }
		   }
	});
};
function Export()
{
    var url = "../Customs/Base/Dictionaries.aspx?";
    if (typeof (AppItem) == "string" && AppItem.length > 0) {
        url += "AppItem=" + AppItem;
    }
    if (typeof (ModelID) == "string" && ModelID.length > 0) {
        url += "ModelID=" + ModelID;
    }
    url += "&act=export";
    var dataid = menu_a.getAttribute("dataid");
    url += "&id=" + dataid;
    window.open(url);
}
function bodyck(e)
{
	hide_etreeMenu();
};
function bodykd(e)
{
	e=window.event||e; 
	if(e.keyCode==27){hide_etreeMenu();}
};
function show_etreeMenu()
{
	var menu=getobj("etreeMenu");
	menu.style.display="";
	if(document.body.addEventListener){document.body.addEventListener("keydown",bodykd, false);}else{document.body.attachEvent("onkeydown",bodykd);}
	if(document.body.addEventListener){document.body.addEventListener("click",bodyck, false);}else{document.body.attachEvent("onclick",bodyck);}
};
function hide_etreeMenu()
{
	var menu=getobj("etreeMenu");
	menu.style.display="none";
	if (document.body.addEventListener){document.body.removeEventListener('keydown', bodykd, false);}else{document.body.detachEvent("onkeydown",bodykd);}
	if (document.body.addEventListener){document.body.removeEventListener('click', bodyck, false);}else{document.body.detachEvent("onclick",bodyck);}
};
function div_contextmenu(evt,div)
{
    //alert(IsMobile() + "::" + evt.button);
	if(!IsMobile() && evt.button!=2){return;}	
	var obj=div.getElementsByTagName("a")[0];	
	
	menu_a=obj;	
	var oRect = obj.getBoundingClientRect();	
	var top = eScroll().top + oRect.top + obj.offsetHeight;
	var left = eScroll().left + oRect.right - obj.offsetWidth;
	
	top= eScroll().top + evt.clientY;
	left= eScroll().left + evt.clientX;
	var menu=getobj("etreeMenu");
	menu.style.top=top + "px";
	menu.style.left=left + "px";
	show_etreeMenu();
};
function contextmenu(evt,obj)
{
   // alert(IsMobile());
    if (!IsMobile() && evt.button != 2) { return; }
	menu_a=obj;
	var oRect = obj.getBoundingClientRect();	
	var top = eScroll().top + oRect.top + obj.offsetHeight;
	var left = eScroll().left + oRect.right - obj.offsetWidth;	
	top= eScroll().top + evt.clientY;
	left= eScroll().left + evt.clientX;
	var menu=getobj("etreeMenu");
	menu.style.top=top + "px";
	menu.style.left = left + "px";
	
	show_etreeMenu();	
};
function bindTree()
{
    var tree = new eTree("etree");
    tree.onrightmenu = function (evt, li) {
        //var d = new Date();
        //	if(typeof(parent.moveout)=="function"){parent.moveout(oul,nul,li);}
        //document.title= typeof(parent.onrightmenu) + "::" + d.getMinutes() + ":" + d.getMilliseconds();
      //  document.title = evt.clientY; //d.getMinutes() + ":" + d.getMilliseconds();
        var obj = li.getElementsByTagName("a")[0];

        menu_a = obj;
        var oRect = obj.getBoundingClientRect();
        var top = eScroll().top + oRect.top + obj.offsetHeight;
        var left = eScroll().left + oRect.right - obj.offsetWidth;
     

        top = 0; //eScroll().top;
        left = 0;// eScroll().left;
        if (evt.touches) {
            top += evt.touches[0].clientY;
            left += evt.touches[0].clientX;           
        }
        else {
            if (evt.clientY) {
                top += evt.clientY;
                left += evt.clientX;
            }
            else {
                if (evt.pageY) {
                    top += evt.pageY;
                    left += evt.pageX;
                }
            }
        }
       
        var menu = getobj("etreeMenu");
        //menu.style.top = (oRect.bottom -$( menu).height()) + "px";
        menu.style.top = top + "px";
        menu.style.left = left + "px";
        show_etreeMenu();
    };
    tree.moveout = function (oul, nul, li) {
        var url = "<%=aspxfile%>?";
        if (typeof (AppItem) == "string" && AppItem.length > 0) {
            url += "AppItem=" + AppItem;
        }
        if (typeof (ModelID) == "string" && ModelID.length > 0) {
            url += "ModelID=" + ModelID;
        }

        url += "&act=setsort&id=" + li.attr("dataid") + "&pid=" + nul.attr("PID") + "&index=" + (li.index() + 1) + "&t=" + now();


        $.ajax({
            type: "GET",
            async: false,
            url: url,
            dataType: "html",
            success: function (data) {
                // alert(data);
            }
        });
    };


	var etree = getobj("etree");
	etree.oncontextmenu = function () { return false; };	
};
function setBindItem(obj,id)
{
    var evt = event || window.event;
    if (evt.stopPropagation) { evt.stopPropagation(); } else { evt.cancelBubble = true; }
    if (evt.preventDefault) { evt.preventDefault(); } else { evt.cancelBubble = true; }
    var url = "<%=aspxfile%>?";
    if (typeof (AppItem) == "string" && AppItem.length > 0) {
        url += "AppItem=" + AppItem;
    }
    if (typeof (ModelID) == "string" && ModelID.length > 0) {
        url += "ModelID=" + ModelID;
    }
    url += "&act=setbinditem&id=" + id;
    if ($(obj).hasClass("true"))
    {
        url += "&value=0";
        $(obj).removeClass("true");
        $(obj).addClass("false");
    }
    else
    {
        url += "&value=1";
        $(obj).removeClass("false");
        $(obj).addClass("true");
    }
    $.ajax({
        type: "GET",
        async: false,
        url: url,
        dataType: "html",
        success: function (data) {
            // alert(data);
        }
    });
};
$(document).ready(function()
{
    document.body.oncontextmenu = function () {
        var src = getEventObject();
        /*
        if (typeof(src.oncontextmenu) == "function")
        {
            return src.oncontextmenu();
        }
        */
        if (src.tagName == "A" || src.tagName == "DIV" || src.tagName == "LI")
        {
            return false;
        }
        return true;
    };
    bindTree();	
});
</script>

<%if (!Ajax && model.clientMode != "mobile")
{ %>
<div class="nav">您当前位置：<a href="<%=eBase.getApplicationHomeURL() %>" target="_top">首页</a>&nbsp;->&nbsp;<%=model.ModelInfo["MC"].ToString()%>
<%if (eRegisterInfo.Base == 0 && eRegisterInfo.Loaded){ %><a href="http://frame.eketeam.com\" class="help" target="_blank" title="eFrameWork开发框架">&nbsp;</a><%}%>
<%
if(model.Action=="")
{
    if (model.Power["Add"])
{
%>
<a id="btn_add" class="button btnprimary" style="<%=( (Action.Value == "" || Action.Value == "view" ) && model.Power["Add"] ? "" : "display:none;" )%>" href="javascript:;" dataid="<%=pid%>" onclick="add(this);"><span><i class="add">添加</i></span></a>
<%
}
}
%></div>
<%}%>
<div id="etreeMenu" class="etreeMenu" style="display:none;">
<a href="javascript:;" onclick="view();">查看</a>
<a href="javascript:;" onclick="add();">添加</a>
<a href="javascript:;" onclick="edit();">修改</a>
<a href="javascript:;" onclick="del();">删除</a>
<a href="javascript:;" onclick="Export();">导出</a>
</div>
<%}%>
<div style="margin:10px;">
<%
if(Action.Value.Length>0)
{ 
%>
<asp:PlaceHolder ID="eFormControlGroup" runat="server">
<form id="frmaddoredit" name="frmaddoredit" method="post" tar_get="mainform" action="<%=eform.getSaveURL()%>">
<input type="hidden" id="act" name="act" value="save"> 
<input type="hidden" id="fromurl" name="fromurl" value="<%=eform.FromURL%>">
<input type="hidden" id="ID" name="ID" value="<%=eform.ID%>">
<input type="hidden" name="eformdata_1" id="eformdata_1" value="<%=HttpUtility.HtmlEncode(eform.getJson())%>">
</form>
<form id="form_1" name="form_1" method="post" tar_get="mainform" action="">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="eDataView">
<colgroup>
<col width="120" />
<col />
</colgroup>
<tr>
<td class="title"><%if(Action.Value == "add" || Action.Value == "edit"){Response.Write("<ins>*</ins>");}%>名称：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="M1_F1" Name="M1_F1" ControlType="textarea" Field="MC" Width="300px" FieldName="名称" NotNull="True" runat="server" /></span></td>
</tr>
<tr>
<td class="title">上级：</td>
<td class="content" id="padd"><span class="eform"><ev:eFormControl ID="M1_F2" Name="M1_F2" ControlType="autoselect" Field="ParentID" FieldName="上级" FieldType="uniqueidentifier" BindAuto="True" BindObject="Dictionaries" BindText="MC" BindValue="DictionarieID" BindCondition="delTag=0 and (ParentID is Null)" BindOrderBy="PX,AddTime" NotNull="False" DataType="string" runat="server" /></span></td>
</tr>
<tr>
<td class="title">值(编码)</td>
<td class="content"><span class="eform"><ev:eFormControl ID="M1_F4" Name="M1_F4" ControlType="text" Field="Value" FieldName="值(编码)" NotNull="False" DataType="string" runat="server" /></span></td>
</tr>
<tr>
<td class="title">绑定项</td>
<td class="content"><span class="eform"><ev:eFormControl ID="M1_F5" Name="M1_F5" ControlType="radio" Field="BindItem" FieldName="绑定项" NotNull="False" DefaultValue="0" Options="[{text:是,value:1},{text:否,value:0}]" DataType="string" runat="server" /></span></td>
</tr>
<tr>
<td class="title">显示顺序：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="M1_F3" Name="M1_F3" ControlType="sort" Field="PX" FieldName="显示顺序" NotNull="False" ReplaceString="[{text:&quot;&quot;,value:0}]" DefaultValue="0" runat="server" /></span></td>
</tr>
<tr>
<td colspan="2" class="title"  style="text-align:left;padding-left:100px;padding-top:10px;padding-bottom:10px;">
<%
if(Action.Value == "add" || Action.Value == "edit")
{
%>

<a class="button btnprimary" href="javascript:;" onclick="packform('1',true);"><span><i class="save"><%=(Action.Value == "add" ? "添加" : "保存")%></i></span></a>

<%}%>
<a class="button" href="javascript:;" style="margin-left:30px;" onclick="goBack();"><span><i class="back">返回</i></span></a>
</td>
</tr>
</table>
</form>
</asp:PlaceHolder>
<%
}
else
{%>
<dl id="eSearchBox" class="ePanel">
<dt><h1 onclick="showPanel(this);" class="search"><a href="javascript:;" class="cur" onfocus="this.blur();"></a>搜索</h1></dt>
<dd style="display:none;">
<asp:PlaceHolder ID="eSearchControlGroup" runat="server">
<form id="frmsearch" name="frmsearch" method="post" onsubmit="return goSearch(this);" action="<%=aspxfile + "?" + (AppItem.Length > 0 ? "AppItem=" + AppItem : "ModelID=" + ModelID) %>">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="eDataView">
<colgroup>
<col width="120" />
<col />
</colgroup>
<tr>
<td class="title">名称：</td>
<td class="content"><span class="eform"><ev:eSearchControl ID="s1" Name="s1" ControlType="text" Field="MC" Operator="like" FieldName="名称" Width="200" DataType="string" runat="server" /></span></td>
</tr>
<tr>
<td colspan="2" class="title" style="text-align:left;padding-left:125px;"><a class="button" href="javascript:;" onclick="if(frmsearch.onsubmit()!=false){frmsearch.submit();}"><span><i class="search">搜索</i></span></a></td>
</tr>
</table>
</form>
</asp:PlaceHolder>
</dd>
</dl>
 <div id="etreebody" oncontextmenu="return false;">   
<%
	Response.Write(eTree);
}
%>
</div>
     <div style="height:80px;"></div>
</div>