﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using EKETEAM.FrameWork;
using EKETEAM.Data;
using LitJson;

namespace eFrameWork.Customs
{
    public partial class Persons : System.Web.UI.Page
    {
        public string UserArea = "Application"; //用户登录域-默认值
        //public eParentModel model;//取父模块动作及表单数据 
        public eModelInfo model;
        eUser user;//当前操作用户

        public string modelid = "2"; //本模块编号
        public string frmwidth = "450px";
        public string frmheight = "280px";
        public string modelName = "人员信息";
        public string aspxFile = "../Customs/Persons.aspx";

        private DataTable _data;
        public DataTable Data
        {
            get
            {
                if (_data == null)
                {
                    _data = eBase.DataBase.getDataTable("SELECT PartMoreID,XM,XB,DH FROM Demo_Customs_PartMore where CustomID='" + model.ID + "' and deltag=0 order by addTime");
                }
                return _data;
            }
        }
        private string _json = null;
        public string getJson
        {
            get
            {
                if (_json == null)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("{\"eformdata_" + modelid + "\":[");
                    int i = 0;
                    foreach (DataRow dr in Data.Rows)
                    {
                        if (i > 0) sb.Append(",");
                        sb.Append("{\"ID\":\"" + dr["PartMoreID"].ToString() + "\",\"Delete\":\"false\",\"m2_f1\":\"" + eBase.encode(dr["xm"].ToString()) + "\",\"m2_f2\":\"" + eBase.encode(dr["xb"].ToString()) + "\",\"m2_f3\":\"" + eBase.encode(dr["dh"].ToString()) + "\"}");
                        i++;
                    }
                    sb.Append("]}");
                    _json = sb.ToString();
                }
                return _json;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser(eBase.getUserArea(UserArea));
            model = new eModelInfo(user);
            eTable etb;
            JsonData jd;


            switch (model.Action.ToLower())
            {               
                case "insert":     
                    //jd = model.JSON();//获取主模块数据，1V1数据跟主模块数据同级
                    jd = model.postJson;
                    if (jd.IsArray && jd.Count == 1) jd = jd[0];
                    jd = jd["eformdata_" + modelid]; //获取本模块数据
                    foreach (JsonData row in jd)
                    {
                        etb = new eTable("Demo_Customs_PartMore", user);
                        etb.Fields.Add("CustomID", model.ID);
                        etb.Fields.Add("XM", row.getValue("m2_f1"));
                        etb.Fields.Add("XB", row.getValue("m2_f2"));
                        etb.Fields.Add("DH", row.getValue("m2_f3"));
                        etb.Add();
                    }

                    break;
                case "update":
                    //jd = model.JSON();//获取主模块数据，1V1数据跟主模块数据同级
                    jd = model.postJson;
                    if (jd.IsArray && jd.Count == 1) jd = jd[0];
                    jd = jd["eformdata_" + modelid]; //获取本模块数据
                    foreach (JsonData row in jd)
                    {
                        string _ID = row.getValue("ID");
                        string _Delete = row.getValue("Delete");
                        etb = new eTable("Demo_Customs_PartMore", user);
                        if (_Delete.ToLower() == "true")
                        {
                            etb.Where.Add("PartMoreID='" + _ID + "'");
                            etb.Delete();
                        }
                        else
                        {
                            etb.Fields.Add("XM", row.getValue("m2_f1"));
                            etb.Fields.Add("XB", row.getValue("m2_f2"));
                            etb.Fields.Add("DH", row.getValue("m2_f3"));
                            if (_ID.Length == 0) //添加
                            {
                                etb.Fields.Add("CustomID", model.ID);
                                etb.Add();
                            }
                            else //修改
                            {
                                etb.Where.Add("PartMoreID='" + _ID + "'");
                                etb.Update();
                            }
                        }
                    }
                    break;
                case "del":
                    etb = new eTable("Demo_Customs_PartMore", user);
                    etb.Where.Add("CustomID='" + model.ID + "'");
                    etb.Delete();
                    break;
            }


        }
    }
}