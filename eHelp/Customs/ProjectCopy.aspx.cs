﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;

public partial class Customs_ProjectCopy : System.Web.UI.Page
{
    public string projectid = eParameters.QueryString("projectid");
    public string newprojectid = "";
    eUser user;
    protected void Page_Load(object sender, EventArgs e)
    {
        user = new eUser("Application");
        if (!user.Logined)
        {
            litBody.Text = "没有权限!";
            return;
        }
        eModelInfo customModel = new eModelInfo(user);
        eModel model = customModel.Model;
        if (!model.Power["copy"])
        {
            litBody.Text = "没有权限!";
            return;
        }
        copy();
    }
    private void copy()
    {
        DataTable tb = eBase.DataBase.getDataTable("select * from Help_Projects where HelpProjectID='" + projectid + "' and deltag=0");
        if (tb.Rows.Count == 0) return;

        eTable etb = new eTable("Help_Projects", user);
        etb.Fields.Add("SiteID", tb.Rows[0]["SiteID"].ToString());
        etb.Fields.Add("Name", tb.Rows[0]["Name"].ToString()+"-复件");
        etb.Fields.Add("Type", tb.Rows[0]["Type"].ToString());
        etb.Fields.Add("KeyWords", tb.Rows[0]["KeyWords"].ToString());
        etb.Fields.Add("Description", tb.Rows[0]["Description"].ToString());
        etb.Fields.Add("Icon", tb.Rows[0]["Icon"].ToString());
        etb.Fields.Add("show", tb.Rows[0]["show"].ToString());
        etb.Fields.Add("simpleInteract", tb.Rows[0]["simpleInteract"].ToString());
        etb.Fields.Add("showComment", tb.Rows[0]["showComment"].ToString());
        etb.Fields.Add("anonymousComment", tb.Rows[0]["anonymousComment"].ToString());

        etb.Fields.Add("webCode", tb.Rows[0]["webCode"].ToString());
        etb.Fields.Add("Remarks", tb.Rows[0]["Remarks"].ToString());
        etb.Add();

        newprojectid =  etb.ID;
        if (newprojectid.Length == 0) return;


        tb = eBase.DataBase.getDataTable("select * from Help_Items where HelpProjectID='" + projectid + "' and deltag=0");
        tb.Columns.Add("NewHelpItemID", typeof(string));
        tb.Columns["NewHelpItemID"].SetOrdinal(0);

        copyitem(tb, "");
        litBody.Text = "复制完成!";
    }
    private void copyitem(DataTable tb, string pid)
    {
        DataRow[] rs;
        string parid = "";
        if (pid.Length > 0)
        {
            rs = tb.Select("HelpItemID='" + pid + "'");
            if (rs.Length > 0) parid = rs[0]["NewHelpItemID"].ToString();
        }
        rs = tb.Select((pid.Length == 0 ? "ParentID is null" : "ParentID='" + pid + "'"), "px");
        if (rs.Length == 0) return;
        foreach (DataRow dr in rs)
        {
            eTable etb = new eTable("Help_Items", user);
            etb.Fields.Add("HelpProjectID", newprojectid);            
            etb.Fields.Add("SiteID", dr["SiteID"].ToString());
            etb.Fields.Add("Name", dr["Name"].ToString());

            if (parid.Length > 0) etb.Fields.Add("ParentID", parid);
            etb.Fields.Add("Body", dr["Body"].ToString());
            etb.Fields.Add("PX", dr["PX"].ToString());
            etb.Fields.Add("URL", dr["URL"].ToString());
            etb.Add();
            string _id = etb.ID;
            dr["NewHelpItemID"] = _id;

            copyitem(tb, dr["HelpItemID"].ToString());
        }
    }
}