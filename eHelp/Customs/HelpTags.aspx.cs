﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using System.Text;

public partial class Customs_HelpTags : System.Web.UI.Page
{
    public string UserArea = "Application";
    public eUser user;
    public eModel model;
    public string proid = eParameters.QueryString("proid");
    private string pubCond
    {
        get
        {
            return "HelpProjectID='" + proid + "' and ";
        }
    }
    private string joinCond
    {
        get
        {
            return "a.HelpProjectID='" + proid + "' and ";
        }
    }
    private string TalbeName
    {
        get
        {
            return "Help_Tags";
        }
    }
    public string PK
    {
        get
        {
            return "TagID";
        }
    }
    private string Name
    {
        get
        {
            return "Name";
        }
    }
    private eDictionary _otherfields;
    private eDictionary OtherFields
    {
        get
        {
            if (_otherfields == null)
            {
                _otherfields = new eDictionary();
                _otherfields.Add("HelpProjectID", proid);
            }
            return _otherfields;
        }
    }
    private DataTable _projects;
    private DataTable Projects
    {
        get
        {
            if (_projects == null)
            {
                string sql = "select HelpProjectID,Name from Help_Projects where SiteID='" + user["SiteID"] + "' and deltag=0 order by px,addtime";
                _projects = eBase.DataBase.getDataTable(sql);
            }
            return _projects;
        }
    }
    public string ProjectList
    {
        get
        {
            StringBuilder sb = new StringBuilder();
            if (Projects.Rows.Count > 0)
            {
                sb.Append("<div class=\"esearchgroup\">\r\n");
                foreach (DataRow dr in Projects.Rows)
                {
                    sb.Append("<a href=\"Custom.aspx?AppItem=" + eParameters.QueryString("appitem") + "&proid=" + dr["HelpProjectID"].ToString() + "\" onfocus=\"this.blur();\"" + (proid == dr["HelpProjectID"].ToString() ? " class=\"cur\"" : "") + ">" + dr["Name"].ToString() + "</a>");
                }
                sb.Append("</div>\r\n");    
            }
            return sb.ToString();
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        user = new eUser(eBase.getUserArea(UserArea));
        user.Check();
      
      
        eModelInfo customModel = new eModelInfo(user);
        model = customModel.Model;

        if (Projects.Rows.Count == 0) 
        {
            LitBody.Text = "<div style=\"margin:10px;color:#666;\">请先建立项目!</div>";
            return;
        }
        if (proid.Length == 0) proid = Projects.Rows[0]["HelpProjectID"].ToString();




        string act = eParameters.Request("act");
        if (act == "")
        {
            #region 列表
            eList list = new eList(TalbeName);
            list.Fields.Add(PK);
            list.Fields.Add(Name);
            list.Where.Add(pubCond + "delTag=0");
            list.OrderBy.Add("px,addTime");
            list.Bind(Rep);
            #endregion
        }
        if (act == "load")
        {
            #region 重新加载
            eList list = new eList(TalbeName);
            list.Fields.Add(PK);
            list.Fields.Add(Name);
            list.Where.Add(pubCond + "delTag=0");
            list.OrderBy.Add("px,addTime");
            list.Bind(Rep);
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            Rep.RenderControl(htw);
            Rep.Visible = false;
            Response.Write(sw.ToString());
            Response.End();
            #endregion
        }
        if (act == "del")
        {
            #region 删除标签
            string id = eParameters.Form("id");
            eTable etb = new eTable(TalbeName, user);
            etb.Where.Add(pubCond  + PK + "='" + id + "'");
            etb.Delete();

            string sql = "update a set a.PX=b.PX from " + TalbeName + " a";
            sql += " inner join (select ROW_NUMBER() over(order by px,addtime) as PX," + PK + " from " + TalbeName + " where " + pubCond + "delTag=0) as b on a." + PK + "=b." + PK;
            sql += " where " + joinCond + "a.delTag=0"; ;
            eBase.DataBase.Execute(sql);

            sql = "update " + TalbeName + " set px=999999 where " + pubCond + PK + "='" + id + "'";
            eBase.DataBase.Execute(sql);
            #endregion
            eResult.Success("删除成功!");
        }
        if (act == "sort")
        {
            #region 排序
            string ids = eParameters.Form("ids");
            int i = 1;
            foreach (string id in ids.Split(",".ToCharArray()))
            {
                eBase.DataBase.Execute("update " + TalbeName + " set PX=" + i.ToString() + " where " + pubCond + PK + "='" + id + "'");
                i++;
            }
            #endregion
            eResult.Success("排序成功!");
        }
        if (act == "save")
        {         
            #region 保存
            eTable etb;
            string id = eParameters.Form("id");
            string value = eParameters.Form("value");
            value = value.Replace("，", ",");
            if (id.Length == 0)
            {
               
                #region 添加
                int maxpx = Convert.ToInt32(eBase.DataBase.getValue("select isnull(max(px),0) + 1 from " + TalbeName + " where " + pubCond + "px<999999 and deltag=0"));
                if (value.IndexOf("	") > -1 || value.IndexOf("\n") > -1 || value.IndexOf(",") > -1)
                {
                    #region 批量添加
                    DateTime time = DateTime.Now;
                    foreach (string str in value.Replace("\r", "").Split("\n".ToCharArray()))
                    {
                        if (str.Trim().Length > 0)
                        {
                            foreach (string _str in str.Split("	".ToCharArray()))
                            {
                                if (_str.Trim().Length > 0)
                                {
                                    foreach (string key in _str.Replace("，", ",").Split(",".ToCharArray()))
                                    {
                                        if (key.Trim().Length > 0)
                                        {
                                            etb = new eTable(TalbeName, user);
                                            //etb.Fields.Add("HelpProjectID", proid);
                                            foreach (string _key in OtherFields.Keys)
                                            {
                                                //eBase.Writeln(_key + "::" + OtherFields[_key].ToString());
                                                etb.Fields.Add(_key, OtherFields[_key].ToString());
                                            }                                            
                                            etb.Fields.Add(Name, key.Trim());
                                            etb.Fields.Add("px", maxpx.ToString());
                                            etb.Fields.Add("addTime", time.ToString());
                                            etb.Add();
                                            maxpx++;
                                            time = time.AddSeconds(1);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    etb = new eTable(TalbeName, user);
                    //etb.Fields.Add("HelpProjectID", proid);
                    foreach (string _key in OtherFields.Keys)
                    {
                        etb.Fields.Add(_key, OtherFields[_key].ToString());
                    }
                    etb.Fields.Add(Name, value);
                    etb.Fields.Add("px", maxpx.ToString());
                    etb.Add();
                }
                #endregion
                eResult.Success("添加成功!");
            }
            else
            {
                #region 修改
                etb = new eTable(TalbeName, user);
                etb.Fields.Add(Name, value);
                etb.Where.Add(PK + "='" + id + "'");
                etb.Update();
                #endregion
                eResult.Success("修改成功!");
            }
            #endregion
        }
    }
}