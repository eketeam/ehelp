﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HelpItems.aspx.cs" Inherits="eFrameWork.Customs.HelpItems" %>
<%@ Import Namespace="EKETEAM.FrameWork" %>
<%if (!Ajax)
{ %>
<script>
var menu_a=null;
var win_width = "90%";
var win_height = "90%";
function postback()
{
    var url = document.location.href;
	url = url.addquerystring("ajax", "true");	
	$.ajax({
		   type: "get",
		   url:url,
		   dataType: "json",
		   success: function(data)
		   {
		   		if(parent.arrLayerIndex.length>0)
				{
					parent.layer.close(parent.arrLayerIndex.pop());
		   		}
		   		else
		   		{
		   		    if (arrLayerIndex.length > 0)
		   		    {
		   		        layer.close(arrLayerIndex.pop());
		   		    }
		   		}
				
				var etreebody=getobj("etreebody");
				if(etreebody)
				{
					$(etreebody).html(data.body.decode());					
					bindTree();
				}
				
		   }
	});	
};
function view()
{
    var url = "?";
    if (typeof (AppItem) == "string" && AppItem.length > 0) {
        url += "AppItem=" + AppItem;
    }
    url += "&proid=<%=proid%>";
    url += "&act=view";
    //url += "&ajax=true&act=view";

    var dataid = menu_a.getAttribute("dataid");
    url += "&pid=" + dataid;
    url += "&id=" + dataid;
    document.location.href = url;
    return;
    layer.open({
        type: 2,
        //skin: 'layui-layer-rim', //加上边框
        title: "查看",
        maxmin: false,
        shadeClose: true, //点击遮罩关闭层
        area: [win_width, win_height],
        //scrollbar: false, 
        //move: false,		
        content: [url, 'no'],
        //content: url,
        success: function (layero, index) {
            arrLayerIndex.push(index);
        }
    });
};
function edit()
{
    var url = "?";  
    if (typeof (AppItem) == "string" && AppItem.length > 0) {
        url += "AppItem=" + AppItem;
    }
    url += "&proid=<%=proid%>";
    //url+="&ajax=true&act=edit";
    url += "&act=edit";
	var dataid=menu_a.getAttribute("dataid");
	url += "&pid=" + dataid;
	url += "&id=" + dataid;
	document.location.href = url;
	return;
	layer.open({
      type: 2,
	  //skin: 'layui-layer-rim', //加上边框
      title: "编辑",
      maxmin: false,
      shadeClose: true, //点击遮罩关闭层
      area : [win_width , win_height],
	  //scrollbar: false, 
	  //move: false,		
	  content: [url,'no'], 
	  //content: url,
	  success: function(layero, index)
	  {
		arrLayerIndex.push(index);
  	  }
    });	
};
function add(obj)
{
	if(obj){menu_a=obj;}
	var url = "?";
	if (typeof (AppItem) == "string" && AppItem.length > 0) {
	    url += "AppItem=" + AppItem;
	}
	url += "&proid=<%=proid%>";
    //url+="&ajax=true&act=add";
    url += "&act=add";
	if(menu_a)
	{
		var dataid=menu_a.getAttribute("dataid");
		
		if(dataid.length>0)
		{
			url+="&pid=" + dataid;
		}
	}
	document.location.href = url;
	return;
	layer.open({
      type: 2,
	  //skin: 'layui-layer-rim', //加上边框
      title: "添加",
      maxmin: false,
      shadeClose: true, //点击遮罩关闭层
      area : [win_width , win_height],
	  //scrollbar: false, 
	  //move: false,		
	  content: [url,'no'], 
	  //content: url,
	  success: function(layero, index)
	  {
		arrLayerIndex.push(index);
  	  }
    });	
};
function delitem(id)
{
    var _back = confirm('确认要删除吗？');
    if (!_back) { return; };
    var url = "?";
    if (typeof (AppItem) == "string" && AppItem.length > 0) {
        url += "AppItem=" + AppItem;
    }
    url += "&proid=<%=proid%>";
    url += "&act=del&id=" + id;//&ajax=true
    document.location.href = url;
    return;
};
function del()
{
	var _back=confirm('确认要删除吗？');
	if(!_back){return;};
	var url = "?";  
	if (typeof (AppItem) == "string" && AppItem.length > 0) {
	    url += "AppItem=" + AppItem;
	}
	url += "&proid=<%=proid%>";
    url += "&act=del&id=" + menu_a.getAttribute("dataid");//&ajax=true
    url += "&pid=" + menu_a.getAttribute("dataid");
    document.location.href = url;
    return;
	$.ajax({
		   type: "get",
		   url:url,
		   dataType: "json",
		   success: function(data)
		   {
		   		//$(menu_a).parent().remove();
		        menu_a = null;
			    postback();
		   }
	});
};

function bodyck(e)
{
	hide_etreeMenu();
};
function bodykd(e)
{
	e=window.event||e; 
	if(e.keyCode==27){hide_etreeMenu();}
};
function show_etreeMenu()
{
    var menu = getobj("etreeMenu");
    var type = $(menu_a).attr("itemtype");
    if (type == "2") {
        //$(menu).find(".add").hide();
    }
    else {
        //$(menu).find(".add").show();
    }
	menu.style.display="";
	if(document.body.addEventListener){document.body.addEventListener("keydown",bodykd, false);}else{document.body.attachEvent("onkeydown",bodykd);}
	if(document.body.addEventListener){document.body.addEventListener("click",bodyck, false);}else{document.body.attachEvent("onclick",bodyck);}
};
function hide_etreeMenu()
{
	var menu=getobj("etreeMenu");
	menu.style.display="none";
	if (document.body.addEventListener){document.body.removeEventListener('keydown', bodykd, false);}else{document.body.detachEvent("onkeydown",bodykd);}
	if (document.body.addEventListener){document.body.removeEventListener('click', bodyck, false);}else{document.body.detachEvent("onclick",bodyck);}
};
function div_contextmenu(evt,div)
{
    //alert(IsMobile() + "::" + evt.button);
	if(!IsMobile() && evt.button!=2){return;}	
	var obj=div.getElementsByTagName("a")[0];	
	
	menu_a=obj;	
	var oRect = obj.getBoundingClientRect();	
	var top = eScroll().top + oRect.top + obj.offsetHeight;
	var left = eScroll().left + oRect.right - obj.offsetWidth;
	
	top= eScroll().top + evt.clientY;
	left= eScroll().left + evt.clientX;
	var menu=getobj("etreeMenu");
	menu.style.top=top + "px";
	menu.style.left=left + "px";
	show_etreeMenu();
};
function contextmenu(evt,obj)
{
   // alert(IsMobile());
    if (!IsMobile() && evt.button != 2) { return; }
    menu_a = obj;
	var oRect = obj.getBoundingClientRect();	
	var top = eScroll().top + oRect.top + obj.offsetHeight;
	var left = eScroll().left + oRect.right - obj.offsetWidth;	
	top= eScroll().top + evt.clientY;
	left= eScroll().left + evt.clientX;
	var menu=getobj("etreeMenu");
	menu.style.top=top + "px";
	menu.style.left = left + "px";
	
	show_etreeMenu();	
};
function bindTree()
{
    var tree = new eTree("etree");
    tree.onrightmenu = function (evt, li) {
        //var d = new Date();
        //	if(typeof(parent.moveout)=="function"){parent.moveout(oul,nul,li);}
        //document.title= typeof(parent.onrightmenu) + "::" + d.getMinutes() + ":" + d.getMilliseconds();
      //  document.title = evt.clientY; //d.getMinutes() + ":" + d.getMilliseconds();
        var obj = li.getElementsByTagName("a")[0];

        menu_a = obj;
        var oRect = obj.getBoundingClientRect();
        var top = eScroll().top + oRect.top + obj.offsetHeight;
        var left = eScroll().left + oRect.right - obj.offsetWidth;
     

        top = 0; //eScroll().top;
        left = 0;// eScroll().left;
        if (evt.touches) {
            top += evt.touches[0].clientY;
            left += evt.touches[0].clientX;           
        }
        else {
            if (evt.clientY) {
                top += evt.clientY;
                left += evt.clientX;
            }
            else {
                if (evt.pageY) {
                    top += evt.pageY;
                    left += evt.pageX;
                }
            }
        }
       
        var menu = getobj("etreeMenu");
        //menu.style.top = (oRect.bottom -$( menu).height()) + "px";
        menu.style.top = top + "px";
        menu.style.left = left + "px";
        show_etreeMenu();
    };
    tree.moveout = function (oul, nul, li) {
        var url = "?";
        if (typeof (AppItem) == "string" && AppItem.length > 0) {
            url += "AppItem=" + AppItem;
        }
        url += "&proid=<%=proid%>";
        url += "&act=setsort&id=" + li.attr("dataid") + "&pid=" + nul.attr("PID") + "&index=" + (li.index() + 1) + "&t=" + now();


        $.ajax({
            type: "GET",
            async: false,
            url: url,
            dataType: "html",
            success: function (data) {
                // alert(data);
            }
        });
    };


	var etree = getobj("etree");
	etree.oncontextmenu = function () { return false; };	
};
$(document).ready(function()
{
    //document.body.oncontextmenu=function(){return false;};	
    document.body.oncontextmenu = function () {
        var src = getEventObject();
        if (src.tagName == "A" || src.tagName == "DIV" || src.tagName == "LI") {
            return false;
        }
        return true;
    };
    bindTree();	
});

function setShow(obj, id) {
    var evt = event || window.event;
    if (evt.stopPropagation) { evt.stopPropagation(); } else { evt.cancelBubble = true; }
    if (evt.preventDefault) { evt.preventDefault(); } else { evt.cancelBubble = true; }
    var url = "?";
    if (typeof (AppItem) == "string" && AppItem.length > 0) {
        url += "AppItem=" + AppItem;
    }
    url += "&act=setshow&id=" + id;
    if ($(obj).hasClass("play")) {
        url += "&value=0";
        $(obj).removeClass("play");
        $(obj).addClass("pause");
    }
    else {
        url += "&value=1";
        $(obj).removeClass("pause");
        $(obj).addClass("play");
    }
    $.ajax({
        type: "GET",
        async: false,
        url: url,
        dataType: "html",
        success: function (data) {
            // alert(data);
        }
    });
};
</script>
<style>

    .etree li div {
      white-space:nowrap; overflow:hidden;
    }
    .etree li a {
display:inline-block; white-space:nowrap; overflow:hidden;-o-text-overflow:ellipsis;text-overflow:ellipsis;

    }
.etree li span.show {
  display: inline-block;
  width: 20px;
  height: 20px;
  float:right;margin-right:10px;margin-top:5px;cursor:pointer;

}
.etree li span.pause{ background: url("data:image/gif;base64,R0lGODlhEQARAKIAAP////T09Nzc3NDQ0P+ZAJSUlHBwcAAAACH5BAEHAAAALAAAAAARABEAAANBCLrc/jDCQGoFtgYmMsGZwAweaA1j6aELmZkVq7gWTMgAfa3py7eqDE73+c2CJ07BwCwAlk3RIiAYWEXV60YCSQAAOw==") no-repeat center center; }
.etree li span.play{ background: url("data:image/gif;base64,R0lGODlhEQARANUAAN735Nr34dz34tn24NT13Nb13dH02c/02M301srz1Mzz1brvxrHuwK7tvaTrtZ/qsZvorZfoqpnorIjlnnrhknbhj3Dfim7fiWvehmnehWjdg2TdgWHcfmPcf17be1zbelnad1vaeVTZc0rXazfSWzPSWS7RVS3QVC3OUy3MUizLUizJUSvGUCrDTirBTiq/TSm8TCm6Syi3Sie0SCavRiSnQySlQiSjQiKfQP///wAAAAAAAAAAAAAAAAAAAAAAACH5BAEHADkALAAAAAARABEAAAZ4wJxwSCwaj0jJaIEsBjInGOnRFCY+qGxMBWkmQKiT+MR6TY6IT2rMjl2KBs+azZ51hgbOnC6OlQhCBRsqfCc1OC8UA0IDGoRsNjgyIhEHQwIYK2ItkS4hEgpGFmQ3NyoaDgZIFTQ3JhcNAVUQHBUMAFVCAQi5vUdBADs=") no-repeat center center; }


</style>
  <script src="../Scripts/clipboard.min.js?beacon=7"></script>
<%if (!Ajax && model.Mode != "mobile")
{ %>
<div class="nav">您当前位置：<a href="<%=eBase.getApplicationHomeURL() %>" target="_top">首页</a>&nbsp;->&nbsp;<%=model.ModelInfo["MC"].ToString()%>
<%if (eRegisterInfo.Base == 0 && eRegisterInfo.Loaded){ %><a href="http://frame.eketeam.com\" class="help" target="_blank" title="eFrameWork开发框架">&nbsp;</a><%}%>
<%
if(model.Action=="")
{%>
    
    <%
    if (model.Power["Add"])
{
%>
<a id="btn_add" class="button btnprimary" style="<%=( (model.Action == "" || model.Action == "view" ) && model.Power["Add"] ? "" : "display:none;" )%>" href="javascript:;" dataid="<%=eParameters.QueryString("pid")%>" onclick="add(this);"><span><i class="add">添加</i></span></a>
<%}%>

    <a class="cop3y" data-clipboard-action="copy" data-clipboard-text="<%=eBase.getAbsoluteUrl() + proid + "/" + pid + ".html" %>" href="<%=eBase.getAbsoluteUrl() + proid + "/" + pid + ".html" %>" href2="javascript:;" target="_blank" title="复制查看地址" style="float:right;"><img src="../images/copyurl.gif" border="0" /></a>
<%}%>
</div>
<%}%>
<div id="etreeMenu" class="etreeMenu" style="display:none;">
<!--
<a href="javascript:;" onclick="view();">查看</a>
 -->
<a href="javascript:;" class="add" onclick="add();">添加</a>
<a href="javascript:;" onclick="edit();">修改</a>
<a href="javascript:;" onclick="del();">删除</a>
</div>
<%}%>
<%=ProjectList %>
<asp:Literal id="LitBody" runat="server" />
<div style="margin:10px;">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout: fixed;">
  <tr valign="top">
    <td width="300"><div id="etreebody"><%=eTree%></div></td>
    <td>
<%
    //if (Type=="1" && (pid.Length > 0 || model.Action == "add"))
  if(model.Action.Length>0 || Type=="1")
  {%>
<asp:PlaceHolder ID="eFormControlGroup" runat="server">
<form id="frmaddoredit" name="frmaddoredit" method="post" tar_get="mainform" action="<%="?AppItem=" + AppItem + "&proid=" + proid + (Request.QueryString["debug"]!=null ? "&debug=1" : "") %>">
<input type="hidden" id="act" name="act" value="save"> 
<input type="hidden" id="fromurl" name="fromurl" value="<%=eform.FromURL%>">
<input type="hidden" id="ID" name="ID" value="<%=eform.ID%>">
<ev:eFormControl ID="M1_F10" Name="M1_F10" Field="PX" DefaultValue="0" ControlType="hidden" runat="server" />
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="eDataView">
<colgroup>
<col width="120" />
<col />
</colgroup>
<tr>
<td class="title"><%if (model.Action == "add" || model.Action == "edit") { Response.Write("<ins>*</ins>"); }%>名称：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="M1_F1" Name="M1_F1" ControlType="text" Field="Name" Width="300px" FieldName="名称" NotNull="True" runat="server" /></span></td>
</tr>
<%if ((model.Action == "add" && Type != "2") || (model.Action == "edit" && Type != "0"))
  { %>
<tr>
<td class="title"><%if (model.Action == "add" || model.Action == "edit") { Response.Write("<ins>*</ins>"); }%>类型：</td>
<td class="content" id="Td2"><span class="eform"><ev:eFormControl ID="M1_F2" Name="M1_F2" ControlType="select" Field="Type" FieldName="类型"  NotNull="True" DefaultValue="1" Options="[{text:单页,value:1},{text:列表,value:2}]" DataType="string" runat="server" /></span></td>
</tr>
<%} %>
<tr>
<td class="title">上级：</td>
<td class="content" id="padd"><span class="eform"><ev:eFormControl ID="M1_F3" Name="M1_F3" ControlType="autoselect" Field="ParentID" FieldName="上级" FieldType="uniqueidentifier" BindAuto="True" BindObject="Help_Items" BindText="Name" BindValue="HelpItemID" BindCondition="HelpProjectID='{querystring:proid}' and Type>0 and Type<3 and delTag=0 and (ParentID is Null)" BindOrderBy="PX,AddTime" NotNull="False" DataType="string" runat="server" /></span></td>
</tr>
<tr>
<td class="title">标签：</td>
<td class="content" id="Td1"><span class="eform"><ev:eFormControl ID="M1_F4" Name="M1_F4" ControlType="checkbox" Field="Tags" FieldName="标签" FieldType="string" BindAuto="True" BindObject="Help_Tags" BindText="Name" BindValue="Name" BindCondition="(HelpProjectID={querystring:proid} or HelpProjectID is null) and delTag=0" BindOrderBy="PX,AddTime" NotNull="False" DataType="string" runat="server" /></span></td>
</tr>
<tr>
<td class="title">SEO关键词：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="M1_F5" Name="M1_F5" ControlType="text" Field="KeyWords" FieldName="SEO关键词" Width="600px" NotNull="False" runat="server" /></span></td>
</tr>
<tr>
<td class="title">SEO描述：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="M1_F6" Name="M1_F6" ControlType="textarea" Field="Description" FieldName="SEO描述" Width="600px" Height="70px" NotNull="False" runat="server" /></span></td>
</tr>
<tr>
<td class="title">外连地址：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="M1_F7" Name="M1_F7" ControlType="text" Field="URL" FieldName="外连地址" Width="300px" NotNull="False" runat="server" /></span></td>
</tr>
<tr>
<td class="title">内容：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="M1_F8" Name="M1_F8" PictureMaxWidth="1600" ControlType="html" Field="Body" FieldName="内容" NotNull="False" runat="server" /></span></td>
</tr>
<tr>
<td class="title">是否显示：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="M1_F9" Name="M1_F9" ControlType="radio" Field="Show" FieldName="是否显示" NotNull="False" DefaultValue="True" Options="[{text:是,value:True},{text:否,value:False}]" DataType="string" runat="server" /></span></td>
</tr>
    <%
        if (eform.Action == "view")
{
%>
<tr>
<td class="title">有用：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="useful" Name="useful" ControlType="label" Field="Useful" FieldName="有用" NotNull="False" DataType="string" runat="server" /></span></td>
</tr>
<tr>
<td class="title">无用：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="useless" Name="useless" ControlType="label" Field="Useless" FieldName="无用" NotNull="False" DataType="string" runat="server" /></span></td>
</tr>
<%}%>
<tr>
<td colspan="2" class="title"  style="text-align:left;padding-left:100px;padding-top:10px;padding-bottom:10px;">
<%
    if (model.Action == "add" || model.Action == "edit")
{
%>
<a class="button btnprimary" href="javascript:;" onclick="if(frmaddoredit.onsubmit()!=false){frmaddoredit.submit();}" onclick2="packform('1',true);"><span><i class="save"><%=(model.Action == "add" ? "添加" : "保存")%></i></span></a>

<a class="button" href="javascript:;" style="margin-left:30px;" onclick="goBack();"><span><i class="back">返回</i></span></a>
<%}else{%>

<%if(Type=="2" && model.Action=="view"){ %>
<a class="button" style="margin-left:30px;" href="?AppItem=<%=AppItem %>&proid=<%=proid %>&pid=<%=pid %>"><span><i class="back">返回</i></span></a>
<%}else{ %>
<a class="button" style="margin-left:30px;" href="?AppItem=<%=AppItem %>&proid=<%=proid %>&act=edit&pid=<%=pid %>&id=<%=pid %>"><span><i class="edit">编辑</i></span></a>
<a class="button" style="margin-left:30px;" href="?AppItem=<%=AppItem %>&proid=<%=proid %>&act=del&pid=<%=pid %>&id=<%=pid %>"><span><i class="del">删除</i></span></a>
<%}%>


<%}%>
</td>
</tr>
</table>
</form>
</asp:PlaceHolder>
        <%}else{%>
<ev:eListControl ID="eDataTable" LineHeight="35" runat="server" >
<ev:eListColumn FieldName="序号" Width="60" runat="server">{row:index}</ev:eListColumn>
<ev:eListColumn Field="Name" FieldName="名称" OrderBy="true" runat="server" />
<ev:eListColumn FieldName="是否显示" Width="90" runat="server">
<a href="{base:url}act=show&id={data:id}&value={data:showvalue}"><img src="{base:VirtualPath}{data:ShowPIC}" border="0"></a>
</ev:eListColumn>
<ev:eListColumn Field="Useful" FieldName="有用" OrderBy="true" Width="90" runat="server" />
<ev:eListColumn Field="Useless" FieldName="无用" OrderBy="true" Width="90" runat="server" />
<ev:eListColumn Field="addTime" FieldName="添加时间" Width="120" FormatString="{0:yyyy-MM-dd HH:mm}" OrderBy="true" runat="server" />
<ev:eListColumn FieldName="操作" Width="160" runat="server">
      <a href="{base:url}act=view&id={data:ID}">查看</a>
    <a href="{base:url}act=edit&id={data:ID}">修改</a>  
    <a href="{base:url}act=del&id={data:ID}" onclick="javascript:return confirm('确认要删除吗？');">删除</a>
</ev:eListColumn>
</ev:eListControl>
<div style="margin:10px;">
<ev:ePageControl ID="ePageControl1" PageSize="15" PageNum="9" runat="server" />
</div>
 <%}%>
	</td>
  </tr>
</table>
<div style="height:80px;"></div>
</div>

<script type="text/javascript">
    var clip = new Clipboard(".copy");
    clip.on('success', function (e) {
        layer.msg("复制成功!");
    });

    clip.on('error', function (e) {
        //alert("复制失败")
    });
</script>