﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.FrameWork;

namespace EKECMS.Customs
{
    public partial class eHelpSiteInfo : System.Web.UI.Page
    {
        public eUser user;
        public string act = eParameters.Request("act");
        public string AppItem = eParameters.Request("AppItem");
        public eModel model;
        protected void Page_Load(object sender, EventArgs e)
        {

            user = new eUser("Application");
            user.Check();
          

            string siteid = user["siteid"].ToString();
            if (siteid.Length == 0) return;
            if (eBase.a_eke_sysWebSites.Columns.Contains("siteid"))
            {
                DataRow[] rows = eBase.a_eke_sysWebSites.Select("siteid=" + siteid);
                if (rows.Length > 0)
                {
                    string id = rows[0]["WebID"].ToString();
                    //eBase.Writeln(id);

                    string modelid = "c1bcf75f-de84-43da-ad04-c7ba2f4ab3ba";
                    model = new eModel(modelid, user);

                    eModel cusmodel = new eModel();//取得自定义模块的权限
                    model.Power = cusmodel.Power;
                    model.ID = id;//修改数据的主键
                    switch (act)
                    {
                        case "":
                            model.Action = "view";
                            litBody.Text = model.getActionHTML();
                            break;
                        case "edit":
                            model.Action = "edit";
                            litBody.Text = model.getActionHTML();
                            break;
                        case "save":
                            model.autoHandle();
                            break;
                    }
                }
            }
        }
    }
}