﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;

public partial class Customs_Links : System.Web.UI.Page
{
    public string UserArea = "Application";
    public eUser user;
    public eModel model;
    private string TalbeName = "Help_Links";
    public string PK = "LinkID";
    public string Name = "MC";
    public eForm eform;
    public bool Ajax = false;
    protected void Page_Load(object sender, EventArgs e)
    {       
        user = new eUser(eBase.getUserArea(UserArea));
        eModelInfo customModel = new eModelInfo(user);
        model = customModel.Model;

        eform = new eForm(TalbeName, user);
        string act = eParameters.Request("act");
        if (act == "")
        {
            #region 列表
            eList list = new eList(TalbeName);
            list.Where.Add("delTag=0");
            list.Where.Add("siteid=" + user["Siteid"]);
            list.OrderBy.Add("px,addTime");
            list.Bind(Rep);
            #endregion
        }
        if (act == "load")
        {
            #region 重新加载
            eList list = new eList(TalbeName);
            list.Where.Add("delTag=0");
            list.Where.Add("siteid=" + user["Siteid"]);
            list.OrderBy.Add("px,addTime");
            list.Bind(Rep);
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            Rep.RenderControl(htw);
            Rep.Visible = false;
            Response.Write(sw.ToString());
            Response.End();
            #endregion
        }
        if (act == "sort" && model.Power["move"])
        {
            #region 排序
            string ids = eParameters.Form("ids");
            int i = 1;
            foreach (string id in ids.Split(",".ToCharArray()))
            {
                eBase.DataBase.Execute("update " + TalbeName + " set PX=" + i.ToString() + " where " +  PK + "='" + id + "'");
                i++;
            }
            #endregion
            eResult.Success("排序成功!");
        }
        if (act.Length > 0)
        {
            eform.AddControl(eFormControlGroup);
            eform.Handle();
        }

    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Request.QueryString["ajax"] != null) Ajax = Convert.ToBoolean(Request.QueryString["ajax"]);
    }
}