﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Links.aspx.cs" Inherits="Customs_Links" %>
<%@ Import Namespace="EKETEAM.FrameWork" %>
<script>
    var win_width = "500px";
    var win_height = "320px";
    function binddrag()
    {
        var drag = "<%=model.Power["move"].ToString().ToLower()%>";
        if (drag != "true") {return;}
        $(".draglist li").arrangeable({
            dragEnd: function () {
                sort();
            }
        });
    };
    function loadlist()
    {
        $.ajax({
            type: 'post',
            url: document.location.href,
            data: { act: "load" },
            dataType: "html",
            success: function (data) {
                $("#listbody").html(data);
                binddrag();
            }
        });
    };
    function sort()
    {
        var ids = "";
        $(".draglist li").each(function (i, elem) {
            ids += (i>0 ? "," : "") + $(elem).attr("eid");
        });
        $.ajax({
            type: 'post',
            url: document.location.href,
            data: { act: "sort", ids: ids },
            dataType: "json",
            success: function (data) {
                layer.msg(data.message);
            }
        });
    };
    function post(frm)
    {
        //alert(doc.frmaddoredit);
        var _back = eCheckform(frm);
        if (!_back) { return; }
        $.ajax({
            type: 'post',
            url: $(frm).attr("action"),
            data: $(frm).serialize(),
            dataType: "json",
            success: function (data) {
                if (parseBool(data.success)) {
                    //layer.msg(data.message);
                    layer.close(arrLayerIndex.pop());
                    layer.msg(data.message);
                    loadlist();
                }
                else {
                    //layer.msg(data.message);
                }
            }
        });
    };
    function add()
    {
        var url = document.location.href;
        url = url.addquerystring("act", "add");
        url = url.addquerystring("ajax", "true");
        layer.open({
            type: 2,
            //skin: 'layui-layer-rim', //加上边框
            title: "添加",
            maxmin: false,
            shadeClose: true, //点击遮罩关闭层
            area: [win_width, win_height],
            //scrollbar: false, 
            //move: false,		
            content: [url, 'no'],
            //content: url,
            success: function (layero, index) {
                arrLayerIndex.push(index);
            }
        });
    };
    function edit(obj, id)
    {
        var url = document.location.href;
        url = url.addquerystring("act", "edit");
        url = url.addquerystring("ajax", "true");
        url = url.addquerystring("id", id);
        //alert(url);
        layer.open({
            type: 2,
            //skin: 'layui-layer-rim', //加上边框
            title: "编辑",
            maxmin: false,
            shadeClose: true, //点击遮罩关闭层
            area: [win_width, win_height],
            //scrollbar: false, 
            //move: false,		
            content: [url, 'no'],
            //content: url,
            success: function (layero, index) {
                arrLayerIndex.push(index);
            }
        });
    };
    function view(obj, id) {
        var url = document.location.href;
        url = url.addquerystring("act", "view");
        url = url.addquerystring("ajax", "true");
        url = url.addquerystring("id", id);
        //alert(url);
        layer.open({
            type: 2,
            //skin: 'layui-layer-rim', //加上边框
            title: "查看",
            maxmin: false,
            shadeClose: true, //点击遮罩关闭层
            area: [win_width, win_height],
            //scrollbar: false, 
            //move: false,		
            content: [url, 'no'],
            //content: url,
            success: function (layero, index) {
                arrLayerIndex.push(index);
            }
        });
    };
    function remove(obj,id) {
        layer.confirm('确认要删除吗?', {shadeClose: true, btn: ['确定', '取消'], title: "删除提示" }, function (index) {
            //$(obj).parent().remove();
            //layer.close(index);
            var url = document.location.href;
            url = url.addquerystring("act", "del");
            url = url.addquerystring("ajax", "true");
            url = url.addquerystring("id", id);
            $.ajax({
                type: 'get',
                url: url,
                dataType: "json",
                success: function (data) {
                    $(obj).parent().remove();
                    layer.close(index);
                    layer.msg(data.message);
                }
            });
        });
    };

</script>
<%if (!Ajax)
{ %>
<div class="nav">您当前位置：<a href="<%=eBase.getApplicationHomeURL() %>" target="_top">首页</a>&nbsp;->&nbsp;<%=model.ModelInfo["MC"].ToString()%>
<%} %>
<%
if(model.Action=="")
{
    if (model.Power["Add"])
{
%>
<a id="btn_add" class="button btnprimary" style="<%=( (model.Action == "" || model.Action == "view" ) && model.Power["Add"] ? "" : "display:none;" )%>" href="javascript:;" dataid="<%=""%>" onclick="add();"><span><i class="add">添加</i></span></a>
<%
}
}
%>

</div>

<style>
.draglist li {float:left; 
    border:1px solid #ccc;margin:10px; position:relative;wid3th:120px;height:40px;line-height:40px;text-align:center; vertical-align:middle;
    background-color:#fff;
    box-shadow: 0 0 6px #ccc;
     cursor: move;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        border-radius: 5px;
    }
    .draglist li span {
    cursor:pointer;margin-left:25px;margin-right:25px; 
    }
    .draglist li i {
        display:none;width:20px;height:20px;position:absolute;top:0px;right:1px; border:0px solid #ff0000;cursor:pointer;
        background:url(../Plugins/eControls/default/images/eFrameTab_close_cur.gif) no-repeat center center;
    }
    .draglist li:hover i {
    display:inline-block;
    }
</style>
<div style="margin:10px;">
<%
if(model.Action.Length>0)
{ 
%>
<asp:PlaceHolder ID="eFormControlGroup" runat="server">
<form id="frmaddoredit" name="frmaddoredit" method="post" action="<%=eform.getSaveURL()%>">
<input type="hidden" id="act" name="act" value="save">
<input type="hidden" id="fromurl" name="fromurl" value="<%=eform.FromURL%>">
<input type="hidden" id="ID" name="ID" value="<%=eform.ID%>">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="eDataView">
<colgroup>
<col width="120" />
<col />
</colgroup>
<tr>
<td class="title"><%=(model.Action == "add" || model.Action == "edit" ? "<ins>*</ins>" : "")%>名称：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F1" ControlType="text" Field="MC" Width="300px" FieldName="名称" NotNull="True" runat="server" /></span></td>
</tr>
<tr>
<td class="title"><%=(model.Action == "add" || model.Action == "edit" ? "<ins>*</ins>" : "")%>地址：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F2" ControlType="text" Field="URL" Width="300px"  FieldName="地址" DefaultValue="javascript:;" NotNull="True" runat="server" /></span></td>
</tr>
<tr>
<td class="title"><%=(model.Action == "add" || model.Action == "edit" ? "" : "")%>目标：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F3" ControlType="select" Field="Target" FieldName="目标" Options="[{text:无,value:&quot;&quot;},{text:新窗口,value:_blank}]" NotNull="False" runat="server" /></span></td>
</tr>
<tr>
<td class="title"><%=(model.Action == "add" || model.Action == "edit" ? "" : "")%>当前项：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F4" ControlType="radio" Field="Cur" FieldName="当前项" Options="[{text:是,value:1},{text:否,value:0}]" NotNull="False" DefaultValue="0" runat="server" /></span></td>
</tr>
<tr>
<td class="title"><%=(model.Action == "add" || model.Action == "edit" ? "" : "")%>是否显示：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F5" ControlType="radio" Field="Show" FieldName="是否显示" Options="[{text:是,value:1},{text:否,value:0}]" NotNull="False" DefaultValue="1" runat="server" /></span></td>
</tr>
<tr>
<tr>
<td colspan="2" class="title"  style="text-align:left;padding-left:100px;padding-top:10px;padding-bottom:10px;">
<%
    if (model.Action == "add" || model.Action == "edit")
{
%>
<a class="button" href="javascript:;" onclick="parent.post(frmaddoredit);" onclick1="if(frmaddoredit.onsubmit()!=false){frmaddoredit.submit();}"><span><i class="save"><%=(model.Action == "add" ? "添加" : "保存")%></i></span></a><%}%>
<a class="button" href="javascript:;" style="margin-left:30px;" onclick="parent.layer.close(parent.arrLayerIndex.pop());" on3click="history.back();"><span><i class="back">返回</i></span></a>
</td>
</tr>
</table>
</form>
</asp:PlaceHolder>


 <%
}
else
{%>


<div id="listbody">
<asp:Repeater id="Rep" runat="server">
<HeaderTemplate><ul class="draglist"></HeaderTemplate>    
<ItemTemplate><li eid="<%# Eval(PK).ToString() %>"<%# Eval("cur").ToString()=="1" ? " style=\"border-color:#0066CC;\"" : "" %>><span<%# model.Power["edit"] ? " onclick=\"edit(this,'" + Eval(PK).ToString() + "');\"" : " onclick=\"view(this,'" + Eval(PK).ToString() + "');\""  %><%# Eval("Show").ToString()=="0" ? " style=\"color:#888;\"" : "" %>><%# Eval(Name).ToString() %></span><%# model.Power["del"] ? "<i onclick=\"remove(this,'" + Eval(PK).ToString() + "');\"></i>" : ""  %></li></ItemTemplate>
<FooterTemplate></ul></FooterTemplate>
</asp:Repeater>
</div>
<script>
    $(function () {
        binddrag();
    });   
</script>
<%    
}
%>
</div>