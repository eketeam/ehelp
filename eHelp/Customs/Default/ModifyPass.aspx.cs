﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using LitJson;


    public partial class Customs_Default_ModifyPass : System.Web.UI.Page
    {
        public eUser user;
        public eModel model;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser("Application");
            user.Check();
            model = new eModel();
            #region 提交表单-修改密码
            if (Request.Form["f1"] != null)
            {
                JsonData jd = new JsonData();
                string mm = eBase.UserInfoDB.getValue("select mm from a_eke_sysUsers where Userid='" + user.ID + "'");
                string encpass = Request.Form["f1"].ToString();
                string pass = eRSA.getPass(encpass);
                if (mm != eBase.getPassWord(pass))
                {
                    jd["success"] = "0"; //第一种赋值方式
                    jd.Add("message", "旧密码不正确,修改失败!"); //第二种赋值方式
                    Response.Write(jd.ToJson());
                    Response.End();
                }

                encpass = Request.Form["f2"].ToString();
                pass = eRSA.getPass(encpass);
                string sql = "update a_eke_sysUsers set ";
                sql += "mm='" + eBase.getPassWord(pass) + "'";
                sql += " where Userid='" + user.ID + "'";
                eBase.UserInfoDB.Execute(sql);


                jd["success"] = "1"; //第一种赋值方式
                jd.Add("message", "新密码修改成功,请牢记!"); //第二种赋值方式
                Response.Write(jd.ToJson());
                Response.End();
            }
            #endregion
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (Master == null) return;
            Literal lit = (Literal)Master.FindControl("LitTitle");
            if (lit != null) lit.Text = eConfig.ApplicationTitle(user["SiteID"].ToString()); // model.ModelInfo["mc"].ToString() + " - " +

        }
    }