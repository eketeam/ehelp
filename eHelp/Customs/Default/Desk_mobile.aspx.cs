﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using EKETEAM.UserControl;
using System.Text;


    public partial class Customs_Default_Desk_mobile : System.Web.UI.Page
    {
        public string UserArea = "Application";
        private eUser user;
        public string AppItem = eParameters.Request("AppItem");
        ApplicationMenu appmenu;

        public eModel model;
        public string appTitle = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser(UserArea);
            user.Check();
            appmenu = new ApplicationMenu(user, "2");
            StringBuilder sb = new StringBuilder();
            DataRow[] rows, rs;
            #region 已分组
            rows = appmenu.ApplicationItems.Select("ModelID is Null and Show=1", "PX,addTime");
            for (int i = 0; i < rows.Length; i++)
            {
                if (!appmenu.MenuIDS.Contains(rows[i]["ApplicationItemID"].ToString())) continue;
                sb.Append("<dl class=\"icongroup\">\r\n");
                sb.Append("<dt>" + rows[i]["MC"].ToString() + "</dt>\r\n");
                sb.Append("<dd>\r\n");
                rs = appmenu.ApplicationItems.Select("ParentID='" + rows[i]["ApplicationItemID"].ToString() + "' and ModelID is not Null and Show=1", "PX,addTime");
                for (int j = 0; j < rs.Length; j++)
                {
                    if (!appmenu.MenuIDS.Contains(rs[j]["ApplicationItemID"].ToString())) continue;

                    sb.Append("<a href=\"" + rs[j]["href"].ToString() + "\">");
                    if (rs[j]["IconHTML"].ToString().Length > 0)
                    {
                        sb.Append(rs[j]["IconHTML"].ToString());
                    }
                    else
                    {
                        string icon = rs[j]["icon"].ToString();
                        if (icon.Length == 0) icon = "images/noicon.png";

                        sb.Append("<img" + (rs[j]["Finsh"].ToString() == "False" ? " class=\"gray\"" : "") + " src=\"../" + icon + "\"  />");
                    }
                    sb.Append("<p" + (rs[j]["Finsh"].ToString() == "False" ? " style=\"color:#ccc;\"" : "") + ">" + rs[j]["MC"].ToString() + "</p></a>\r\n");
                }
                sb.Append("</dd>\r\n");
                sb.Append("<div class=\"clear\"></div>\r\n");
                sb.Append("</dl>\r\n");

            }
            #endregion
            #region 未分组
            rs = appmenu.ApplicationItems.Select("ModelID is not Null and ParentID is null and Show=1", "PX,addTime");
            if (rs.Length > 0)
            {
                sb.Append("<dl class=\"icongroup\">\r\n");
                sb.Append("<dt>未分组</dt>\r\n");
                sb.Append("<dd>\r\n");
                for (int j = 0; j < rs.Length; j++)
                {
                    if (!appmenu.MenuIDS.Contains(rs[j]["ApplicationItemID"].ToString())) continue;
                    string icon = rs[j]["icon"].ToString();
                    if (icon.Length == 0) icon = "images/noicon.png";

                    sb.Append("<a href=\"" + rs[j]["href"].ToString() + "\"><img" + (rs[j]["Finsh"].ToString() == "False" ? " class=\"gray\"" : "") + " src=\"../" + icon + "\"  /><p" + (rs[j]["Finsh"].ToString() == "False" ? " style=\"color:#ccc;\"" : "") + ">" + rs[j]["MC"].ToString() + "</p></a>\r\n");
                }
                sb.Append("</dd>\r\n");
                sb.Append("<div class=\"clear\"></div>\r\n");
                sb.Append("</dl>\r\n");
            }
            #endregion
            LitMenu.Text = sb.ToString();
        }
    }
