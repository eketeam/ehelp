﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using EKETEAM.UserControl;
using System.Text;


    public partial class Customs_Default_UserCenter_mobile : System.Web.UI.Page
    {
        public string UserArea = "Application";
        public eUser user;
        public string AppItem = eParameters.Request("AppItem");
        public eModel model;
        public DataRow row_user;
        public DataRow row_pass;
        public DataRow row_info;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser(UserArea);
            user.Check();

            ApplicationMenu appmenu = new ApplicationMenu(user, "2");
            DataRow[] rows = appmenu.UserApps.Select("ModelID='13d1ceba-eef8-4471-8abe-c617fc8838ea' and ApplicationID='" + appmenu.ApplicationID + "'");//修改密码
            if (rows.Length > 0) row_pass = rows[0];

            rows = appmenu.UserApps.Select("ModelID='c76750c3-1bc5-4bdb-9805-640a62faab64' and ApplicationID='" + appmenu.ApplicationID + "'"); //个人信息
            if (rows.Length > 0) row_info = rows[0];
            


            DataTable tb = eBase.UserInfoDB.getDataTable("select * from a_eke_sysUsers where UserID='" + user.ID + "'");
            row_user = tb.Rows[0];
            eModelInfo customModel = new eModelInfo(user);
            model = customModel.Model;
        }
    }
