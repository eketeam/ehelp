﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;

public partial class Customs_eSTest : System.Web.UI.Page
{


    protected void Page_Load(object sender, EventArgs e)
    {

        s1.BindCondition += " and HelpProjectID in (1,5,6)";
        if (s1.allOptions.Rows.Count > 0)
        {
            s1.DefaultValue = s1.allOptions.Rows[0]["value"].ToString();
        }
        s2.BindCondition = "HelpProjectID='" + (s1.Value.ToString().Length > 0 ? s1.Value.ToString() : s1.DefaultValue) + "' and " + s2.BindCondition;
       
    }
}