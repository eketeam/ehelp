﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HelpHotKeys.aspx.cs" Inherits="Customs_HelpHotKeys" %>
<script>
    function binddrag()
    {
        var drag = "<%=model.Power["move"].ToString().ToLower()%>";
        if (drag != "true") {return;}
        $(".draglist li").arrangeable({
            dragEnd: function () {
                sort();
            }
        });
    };
    function loadlist()
    {
        $.ajax({
            type: 'post',
            url: document.location.href,
            data: { act: "load" },
            dataType: "html",
            success: function (data) {
                $("#listbody").html(data);
                binddrag();
            }
        });
    };
    function sort()
    {
        var ids = "";
        $(".draglist li").each(function (i, elem) {
            ids += (i>0 ? "," : "") + $(elem).attr("eid");
        });
        $.ajax({
            type: 'post',
            url: document.location.href,
            data: { act: "sort", ids: ids },
            dataType: "json",
            success: function (data) {
                layer.msg(data.message);
            }
        });
    };
    function add()
    {
        var html = '<div>';
        html += '<textarea id="keys" class="layui-layer-input" style="resize:none;" placeholder="输入热词信息，多个热词间用逗号或换行分隔!"></textarea>';
        //html += '<input id="transfer-msg" type="text" class="layui-layer-input" value="" placeholder="转账信息">';
        //html += '<input style="margin-top:10px;" id="transfer-money" type="text" class="layui-layer-input" value="" placeholder="转账金额">';
        html += '</div>';
        layer.open({
            type: 1 //Page层类型
            ,btn: ["确定", "取消"]
            , title: '添加热词'
            ,shadeClose: true //点击遮罩关闭层
            ,skin: 'layui-layer-prompt'
            ,content: html
            ,yes: function (index, layero) {
                //console.log($(layero).find("#transfer-msg").val());
                var value = $(layero).find("#keys").val();
                if (value.length == 0)
                {
                    layer.msg("请输入内容!");
                }
                $.ajax({
                    type: 'post',
                    url: document.location.href,
                    data: {act:"save",value:value},
                    dataType: "json",
                    success: function (data) {
                        layer.close(index);
                        loadlist();
                        layer.msg(data.message);
                    }
                });
                //alert();
            }
        });
    };
    function edit(obj, id) {
        var html = '<div>';
        html += '<input id="keys" type="text" class="layui-layer-input" value="' + $(obj).html() + '">';
        html += '</div>';
        layer.open({
            type: 1 //Page层类型
          , btn: ["确定", "取消"]
          , title: '修改热词'
          , shadeClose: true //点击遮罩关闭层
          ,skin: 'layui-layer-prompt'
          ,content: html
          , yes: function (index, layero) {
              var value = $(layero).find("#keys").val();
              if (value.length == 0) {
                  layer.msg("请输入内容!");
              }
              $(obj).html(value);
              $.ajax({
                  type: 'post',
                  url: document.location.href,
                  data: { act: "save",id:id, value: value },
                  dataType: "json",
                  success: function (data) {
                      layer.close(index);
                      layer.msg(data.message);
                  }
              });
          }
        });
        //layer.msg(id);
    };
    function remove(obj,id) {
        layer.confirm('确认要删除吗?', {shadeClose: true, btn: ['确定', '取消'], title: "删除提示" }, function (index) {
            //$(obj).parent().remove();
            //layer.close(index);
            $.ajax({
                type: 'post',
                url: document.location.href,
                data: { act: "del", id: id},
                dataType: "json",
                success: function (data) {
                    $(obj).parent().remove();
                    layer.close(index);
                    layer.msg(data.message);
                }
            });
        });
    };
</script>

<div class="nav">您当前位置：<a href="<%=eBase.getApplicationHomeURL() %>" target="_top">首页</a>&nbsp;->&nbsp;<%=model.ModelInfo["MC"].ToString()%>
<%
if(model.Action=="")
{
    if (model.Power["Add"].ToString().ToLower() == "true")
{
%>
<a id="btn_add" class="button btnprimary" style="<%=( (model.Action == "" || model.Action == "view" ) && model.Power["Add"].ToString().ToLower()=="true" ? "" : "display:none;" )%>" href="javascript:;" dataid="<%=""%>" onclick="add();"><span><i class="add">添加</i></span></a>
<%
}
}
%>

</div>

<style>
.draglist li {float:left; 
    border:1px solid #ccc;margin:10px; position:relative;wid3th:120px;height:40px;line-height:40px;text-align:center; vertical-align:middle;
    background-color:#fff;
    box-shadow: 0 0 6px #ccc;
     cursor: move;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        border-radius: 5px;
    }
    .draglist li span {
    cursor:pointer;margin-left:25px;margin-right:25px; 
    }
    .draglist li i {
        display:none;width:20px;height:20px;position:absolute;top:0px;right:1px; border:0px solid #ff0000;cursor:pointer;
        background:url(../Plugins/eControls/default/images/eFrameTab_close_cur.gif) no-repeat center center;
    }
    .draglist li:hover i {
    display:inline-block;
    }
</style>
<%=ProjectList %>
<asp:Literal id="LitBody" runat="server" />


<div style="margin:10px;">

<div id="listbody">
<asp:Repeater id="Rep" runat="server">
<HeaderTemplate><ul class="draglist"></HeaderTemplate>    
<ItemTemplate><li eid="<%# Eval(PK).ToString() %>"><span<%# model.Power["edit"].ToString().ToLower() == "true" ? " onclick=\"edit(this,'" + Eval(PK).ToString() + "');\"" : ""  %>><%# Eval("Name").ToString() %></span><%# model.Power["del"].ToString().ToLower() == "true" ? "<i onclick=\"remove(this,'" + Eval(PK).ToString() + "');\"></i>" : ""  %></li></ItemTemplate>
<FooterTemplate></ul></FooterTemplate>
</asp:Repeater>
</div>
<script>
    $(function () {
        binddrag();        
    });   
</script>
</div>