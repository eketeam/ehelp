﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Excel_Templates.aspx.cs" Inherits="eFrameWork.Customs.Excel_Templates" %>
<div class="nav">您当前位置：<a href="<%=eBase.getApplicationHomeURL() %>">首页</a>&nbsp;->&nbsp;<asp:Literal id="litNav" runat="server" /></div>
<script>
    var FileSelectFormIndex = null;
    var path = "<%=path%>";
</script>
<script src="../Scripts/clipboard.min.js"></script>
<script>
    var menu_a=null;
    var menu_type=0;
    var menu_name="";
    function contextmenu(evt,obj,type,name)
    {
        //type 1文件夹，2文件
        if (evt.button != 2) { return; }
        var ext = $(obj).attr("ext");

        if (".bak".indexOf(ext) == -1 || ext.length == 0) {
            $(".etreeMenu a:contains('备份')").show();
        }
        else {
            $(".etreeMenu a:contains('备份')").hide();
        }
        if (".html.htm.css.js.txt".indexOf(ext) > -1 && ext.length>2)
        {
            $(".etreeMenu a:contains('编辑')").show();
        }
        else
        {
            $(".etreeMenu a:contains('编辑')").hide();
        }
        if (type == 2)
        {
            $(".etreeMenu a:contains('下载')").attr("href","?" + (typeof (AppItem) == "string" && AppItem.length > 0 ? "AppItem=" + AppItem : "ModelID=" + ModelID) + "&act=download" + (path.length>0 ? "&path=" + path : "") + "&name=" + name);
            $(".etreeMenu a:contains('下载')").show();
        }
        else {
            $(".etreeMenu a:contains('下载')").hide();
        }
        if (".zip".indexOf(ext) == -1 || ext.length==0) {
            $(".etreeMenu a:contains('压缩')").show();
        }
        else {
            $(".etreeMenu a:contains('压缩')").hide();
        }
        if (".zip".indexOf(ext) > -1 && ext.length > 2) {
            $(".etreeMenu a:contains('解压')").show();
        }
        else {
            $(".etreeMenu a:contains('解压')").hide();
        }
        menu_type=type;
        menu_a=obj;
        menu_name=name;
        var oRect = obj.getBoundingClientRect();	
        var top = eScroll().top + oRect.top + obj.offsetHeight;
        var left = eScroll().left + oRect.right - obj.offsetWidth;	
        top= eScroll().top + evt.clientY;
        left= eScroll().left + evt.clientX;
        var menu=getobj("etreeMenu");
        menu.style.top=top + "px";
        menu.style.left=left + "px";
        show_etreeMenu();	
    };

    function bodyck(e)
    {
        hide_etreeMenu();
    };
    function bodykd(e)
    {
        e=window.event||e; 
        if(e.keyCode==27){hide_etreeMenu();}
    };
    function show_etreeMenu()
    {
        var menu=getobj("etreeMenu");
        menu.style.display="";
        if(document.body.addEventListener){document.body.addEventListener("keydown",bodykd, false);}else{document.body.attachEvent("onkeydown",bodykd);}
        if(document.body.addEventListener){document.body.addEventListener("click",bodyck, false);}else{document.body.attachEvent("onclick",bodyck);}
    };
    function hide_etreeMenu()
    {
        var menu=getobj("etreeMenu");
        menu.style.display="none";
        if (document.body.addEventListener){document.body.removeEventListener('keydown', bodykd, false);}else{document.body.detachEvent("onkeydown",bodykd);}
        if (document.body.addEventListener){document.body.removeEventListener('click', bodyck, false);}else{document.body.detachEvent("onclick",bodyck);}
    };
    $(document).ready(function()
    {
        //document.body.oncontextmenu=function(){return false;};	
        document.body.oncontextmenu = function () {
            var src = getEventObject();
            //document.title = src.tagName;
            if (src.tagName == "DT" || src.tagName == "DD" || src.tagName == "DIV") {
                return false;
            }
            return true;
        };
    });

    //重命名文件、文件夹
    function file_rename()
    {
        //ReName(menu_name,menu_type);
        layer.prompt({ title: '输入文件' + (menu_type == 1 ? '夹' : '') + '名称', formType: 3, value: menu_name }, function (value, index) {
            var url = "?" + (typeof (AppItem) == "string" && AppItem.length > 0 ? "AppItem=" + AppItem : "ModelID=" + ModelID) + "&act=rename" + (path.length > 0 ? "&path=" + path : "") + "&type=" + menu_type + "&oldname=" + menu_name + "&newname=" + value;
            url+="&ajax=true&t="+now();
            layer.close(index);
            //document.location.href = url;
            $.ajax({
                type:"get",
                async:false,
                url:url,
                dataType:"json",
                success:function(data)
                {
                    if(data.success=="1")
                    {
                        if(data.errcode=="0"){file_relad();}
                        layer.msg(data.message);      
                    }
                }
            });

        });
    };
    function file_back()
    {
        var url="?" + (typeof (AppItem) == "string" && AppItem.length > 0 ? "AppItem=" + AppItem : "ModelID=" + ModelID)+ "&act=bak&type=" + menu_type + (path.length>0 ? "&path=" + path : "") + "&name=" + encodeURIComponent(menu_name);
        url+="&ajax=true&t="+now();
        //document.location.assign(url);
        $.ajax({
            type:"get",
            async:false,
            url:url,
            dataType:"json",
            success:function(data)
            {
                if(data.success=="1")
                {
                    if(data.errcode=="0"){file_relad();}
                    layer.msg(data.message);      
                }
            }
        });
    };
    //删除文件、文件夹
    function file_del()
    {
        //var _back=confirm('您确定要删除该文件' + (menu_type==1 ? '夹' : '') + '吗？'); if(!_back){return;};
        var url="?" + (typeof (AppItem) == "string" && AppItem.length > 0 ? "AppItem=" + AppItem : "ModelID=" + ModelID)+ "&act=del&type=" + menu_type + (path.length>0 ? "&path=" + path : "") + "&name=" + encodeURIComponent(menu_name);
        url+="&ajax=true&t="+now();
        //document.location.assign(url);
        layer.confirm('您确定要删除该文件' + (menu_type == 1 ? '夹' : '') + '吗？', {
            title:"文件" +  (menu_type == 1 ? "夹" : "")  + "删除确认?"
            ,btn: ['删除', '取消'] //按钮
            ,shadeClose: true //点击遮罩关闭层
        }, function (index) {
            //document.location.assign(url);

            //
            $.ajax({
                type:"get",
                async:false,
                url:url,
                dataType:"json",
                success:function(data)
                {
                    layer.close(index);
                    if(data.success=="1")
                    {                       
                        if(data.errcode=="0"){file_relad();}         
                        layer.msg(data.message);                    
                    }
                }
            });

        });
    };
    var eaceditor;
    //编辑文件
    function file_edit()
    {
        var name=menu_name;
        //editFile(name);  
        //return;
        var url = "?";
        if (typeof (AppItem) == "string" && AppItem.length > 0) {
            url += "AppItem=" + AppItem + "&";
        }
        else {
            if (typeof (ModelID) == "string" && ModelID.length > 0) {
                url += "ModelID=" + ModelID + "&";
            }
        }
        url += "act=edit" + (path.length>0 ? "&path=" + path : "") + "&name=" + name;
        $.ajax({
            type:"get",
            async:false,
            url:url,
            dataType:"json",
            success:function(json)
            {
                if(json.success=="1")
                {
                    //alert(json.value);
                    var html='';
                    if(name.toLowerCase().indexOf(".html")>-1)
                    {
                        html+='<div style="padding:8px;background-color:#f2f2f2;"><a href="javascript:;" onclick="showHtmlTag();" style="color:#0026ff;">插入数据</a>&nbsp;&nbsp;<a href="javascript:;" onclick="showPathSelect();" style="color:#0026ff;">插入库文件</a></div>';
                    }
                    //html+='<textarea id="content" name="content" class="autoNumber" style="width:98%;height:540px;">' + json.value + '</textarea>';

                    html+='<pre id="eaceditor" style="">';
                    html+=json.value;
                    html+='</pre>';

                  
                    ///
                    layer.open({
                    type: 1 //此处以iframe举例
					,title: name
                        //,skin: 'layui-layer-molv' //加上边框 layui-layer-rim  layui-layer-lan layui-layer-molv layer-ext-moon
					,shadeClose: false //点击遮罩关闭层
					, area: ['90%', '90%']
					,shade: 0.2 //透明度
					,maxmin: false
					,resize: false
					,btnAlign: 'l' //lcr
					,moveType: 0 //拖拽模式，0或者1
					,anim: 0 //0-6的动画形式，-1不开启
					,content: html
					,btn: ['保存修改','关闭'] //只是为了演示
					,yes: function(index,layero)
					{ 

					    //var value=$("#content").val().replace(/\r/ig,"").replace(/\n/ig,"0x\\r\\n");
					    //var value=$("#content").val().encode();
					    var value=eaceditor.getValue();
					    url = "?";
					    if (typeof (AppItem) == "string" && AppItem.length > 0) {
					        url += "AppItem=" + AppItem + "&";
					    }
					    else {
					        if (typeof (ModelID) == "string" && ModelID.length > 0) {
					            url += "ModelID=" + ModelID + "&";
					        }
					    }
					    url += "act=save" + (path.length>0 ? "&path=" + path : "") + "&name=" + name;
					    $.ajax({
					        type: "post", 
					        async: true,
					        data:{value:value},
					        url: url,
					        dataType: "json",
					        success: function (data) {
					            layer.msg(data.message);
					            //layer.close(index); 不关闭、继续修改
					        },
					        error: function (XMLHttpRequest, textStatus, errorThrown) {				
					        }
					    });

					}	
					,cancel: function(index, layero)
					{
					    //layer.msg("X关闭!");
					}
					,success: function(layero, index)
					{
					    var h=$('#layui-layer'+ index).height()-140;
					    var w=$('#layui-layer'+ index).width()-6;

					    //alert($('#layui-layer'+ index).height());
                        /*
					 
					    $("#content").height(h).width(w);
					    $("#content").setTextareaCount({width: "40px",bgColor: "#f2f2f2",color: "#000",display: "inline-block"});
                        */
					    eaceditor = ace.edit("eaceditor");
					    eaceditor.setTheme("ace/theme/twilight");//tomorrow
					    eaceditor.session.setMode("ace/mode/html");
					    $("#eaceditor").height(h).width(w);
					}
                    });

                    ///
                }
            }
        });
    };
    //压缩文件
    function file_zip()
    {
        var url = "?" + (typeof (AppItem) == "string" && AppItem.length > 0 ? "AppItem=" + AppItem : "ModelID=" + ModelID)+ "&act=zip&type=" + menu_type + (path.length > 0 ? "&path=" + path : "") + "&name=" + encodeURIComponent(menu_name);
        //document.location.assign(url);
        url+="&ajax=true&t="+now();
        $.ajax({
            type:"get",
            async:false,
            url:url,
            dataType:"json",
            success:function(data)
            {
                if(data.success=="1")
                {
                    if(data.errcode=="0"){file_relad();}
                    layer.msg(data.message);      
                }
            }
        });
    };
    //解压文件
    function file_unzip()
    {
        //var _back = confirm('解压会覆盖现有文件,确定要执行吗?');
        //if(!_back){return;};
        var url = "?" + (typeof (AppItem) == "string" && AppItem.length > 0 ? "AppItem=" + AppItem : "ModelID=" + ModelID) + "&act=unzip&type=" + menu_type + (path.length > 0 ? "&path=" + path : "") + "&name=" + encodeURIComponent(menu_name);
        url+="&ajax=true&t="+now();
        //document.location.assign(url);
        layer.confirm('解压会覆盖现有文件,确定要执行吗?', {
            title:"文件解压确认?"
            ,btn: ['解压', '取消'] //按钮
            ,shadeClose: true //点击遮罩关闭层
        }, function (index) {
            //document.location.assign(url);
            $.ajax({
                type:"get",
                async:false,
                url:url,
                dataType:"json",
                success:function(data)
                {
                    layer.close(index);
                    if(data.success=="1")
                    {
                        if(data.errcode=="0"){file_relad();}
                        layer.msg(data.message);      
                    }
                }
            });
        });
    };
    //重新加载
    function file_relad()
    {
        var url = "?";
        if (typeof (AppItem) == "string" && AppItem.length > 0) {
            url += "AppItem=" + AppItem + "&";
        }
        else {
            if (typeof (ModelID) == "string" && ModelID.length > 0) {
                url += "ModelID=" + ModelID + "&";
            }
        }
        url += "act=reload&ajax=true" + (path.length>0 ? "&path=" + path : "");
        url+="&t="+now();
        $.ajax({
            type:"get",
            async:false,
            url:url,
            dataType:"json",
            success:function(data)
            {
                if(data.success=="1")
                {
					if(data.errcode=="0"){$("#filemanage_files").html(data.body);}
                }
            }
        });
    };
    //文件上传
    function file_upload(obj)
    {
        if(obj.files)
        {
            var badexts=$(obj).attr("badexts");
            if(!badexts){badexts="";}
            if(badexts.length>0)
            {
                badexts="." + badexts.toLowerCase()+ ".";
                for(var i=0;i<obj.files.length;i++)
                {
                    var arr=obj.files[i].name.toLowerCase().split(".");
                    var ext="." + arr[arr.length-1] + ".";
                    if( badexts.indexOf(ext)>-1)
                    {
                        obj.value = "";
                        layer.msg("文件类型." +  arr[arr.length-1] + "被禁止上传!");
                        return;
                    }
                }
            }
        }        
        var url = "?";
        if (typeof (AppItem) == "string" && AppItem.length > 0) {
            url += "AppItem=" + AppItem + "&";
        }
        else {
            if (typeof (ModelID) == "string" && ModelID.length > 0) {
                url += "ModelID=" + ModelID + "&";
            }
        }
        url += "act=upload" + (path.length > 0 ? "&path=" + path : "");
        url+="&ajax=true&t="+now();
        if (obj.files) {
            var formData = new FormData();
            for (var i = 0; i < obj.files.length; i++) {
                formData.append('files', obj.files[i], obj.files[i].name);
            }
            $(obj).hide();
            $.ajax({
                type: "POST",
                async: true,
                data: formData,
                url: url,
                contentType: false,
                //dataType: "formData",
                cache: false,//上传文件无需缓存
                processData: false,//用于对data参数进行序列化处理 这里必须false
                dataType: "json",
                success: function (data) {
                     //alert(JSON.stringify(data));
                    obj.value = "";
                    if(data.errcode=="0")
                    {
                        file_relad();
                    }
                    layer.msg(data.message);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
            $(obj).show();



        }
        else {       
            $.ajaxFileUpload({
                type: "post",
                url: url, //用于文件上传的服务器端请求地址 
                secureuri: false, //一般设置为false 
                fileElementId: $(obj).attr("id"), //文件上传空间的id属性  <input type="file" id="file" name="file" /> 
                dataType: "json", //返回值类型 一般设置为json 		
                complete:function(){},
                success: function (data, status)  //服务器成功响应处理函数
                {
                    //alert(JSON.stringify(data));
                    obj.value = "";
                    if(data.errcode=="0")
                    {
                        file_relad();
                    }
                    layer.msg(data.message);
                },
                error: function (data, status, e)//服务器响应失败处理函数
                {
                }
            });
        }
    };
    //新建文件
    function file_newFile()
    {
        layer.prompt({ title: '输入文件名称', formType: 3, value: '' }, function (value, index) {
            //var url ="?" + (typeof(AppItem)=="string" && AppItem.length > 0 ? "AppItem=" + AppItem  : "ModelID=" + ModelID ) + "&act=addfile" + (path.length>0 ? "&path=" + path : "") + "&name=" + value;
            var url ="?";
            if(typeof(AppItem)=="string" && AppItem.length > 0)
            {
                url+="AppItem=" + AppItem + "&";
            }
            else
            {
                if(typeof(ModelID)=="string" && ModelID.length > 0)
                {
                    url+="ModelID=" + ModelID + "&";
                }
            }
            url+= "act=newfile" + (path.length>0 ? "&path=" + path : "") + "&name=" + value;
            url+="&ajax=true&t="+now();
            layer.close(index);
            //document.location.href = url;
            $.ajax({
                type:"get",
                async:false,
                url:url,
                dataType:"json",
                success:function(data)
                {
                    if(data.success=="1")
                    {
                        if(data.errcode=="0"){file_relad();}
                        layer.msg(data.message);      
                    }
                }
            });
        });
    };
    //新建文件夹
    function file_newFolder()
    {
        layer.prompt({ title: '输入文件夹名称', formType: 3, value: name }, function (value, index) {
            //var url = "?" +  (typeof(AppItem)=="string" && AppItem.length > 0 ? "AppItem=" + AppItem  : "ModelID=" + ModelID ) + "&act=addfolder" + (path.length>0 ? "&path=" + path : "") + "&name=" + value;
            var url ="?";
            if(typeof(AppItem)=="string" && AppItem.length > 0)
            {
                url+="AppItem=" + AppItem + "&";
            }
            else
            {
                if(typeof(ModelID)=="string" && ModelID.length > 0)
                {
                    url+="ModelID=" + ModelID + "&";
                }
            }
            url+= "act=newfolder" + (path.length>0 ? "&path=" + path : "") + "&name=" + value;
            url+="&ajax=true&t="+now();
            layer.close(index);
            //document.location.href = url;
            $.ajax({
                type:"get",
                async:false,
                url:url,
                dataType:"json",
                success:function(data)
                {
                    if(data.success=="1")
                    {
                        if(data.errcode=="0"){file_relad();}
                        layer.msg(data.message);      
                    }
                }
            });
        });
    };

</script>

<div id="etreeMenu" class="etreeMenu" style="display:none;z-index:1000;">
<%if (model.Power["rename"])
  { %><a href="javascript:;" onclick="file_rename();">重命名</a><%} %>
<%if (model.Power["edit"])
  { %><a href="javascript:;" onclick="file_edit();">编辑</a><%} %>
<%if (model.Power["backup"]) 
  { %><a href="javascript:;" onclick="file_back();">备份</a><%} %>
<%if (model.Power["zip"]) 
  { %><a href="javascript:;" onclick="file_zip();">压缩</a><%} %>
<%if ( model.Power["unzip"]) 
  { %><a href="javascript:;" onclick="file_unzip();">解压</a><%} %>
<%if ( model.Power["download"])
  { %><a href="javascript:;" target="_blank">下载</a><%} %>
<%if ( model.Power["del"])  
  { %><a href="javascript:;" onclick="file_del();">删除</a><%} %>
</div>
<div style="margin:10px;">
<asp:Literal id="litBody" runat="server" />
</div>
<script type="text/javascript">
    var clip = new Clipboard(".copypath");
    clip.on('success', function (e) {
        //layer.open({ content: "复制成功!请打开要赠送好友的聊天窗口粘贴.", skin: "msg", time: 2 });
        layer.msg("地址复制成功!");
    });

    clip.on('error', function (e) {
        //alert("复制失败")
    });
</script>