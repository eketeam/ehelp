﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PartOne.aspx.cs" Inherits="Customs_PartOne" %>
<ev:eCustomModel ID="subModel1" TableName="Demo_Customs_PartOne" runat="server">
<ev:eCustomForm  runat="server">
<table width="320" border="0" cellpadding="0" cellspacing="0" class="eDataView">
<colgroup>
<col width="120" />
<col />
</colgroup>
<tr>
<td class="title"><%=(modelInfo.Action.ToLower() == "add" || modelInfo.Action.ToLower() == "edit" ? "<ins>*</ins>" : "") %>地址：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="mx_F1" Name="mx_F1" ControlType="text" Field="DZ" FieldName="地址" NotNull="True" MaxLength="50" runat="server" /></span></td>
</tr>
<tr>
<td class="title"><%=(modelInfo.Action.ToLower() == "add" || modelInfo.Action.ToLower() == "edit" ? "<ins>*</ins>" : "") %>电话：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="mx_F2" Name="mx_F2" ControlType="text" Field="gddh" FieldName="电话" NotNull="True" MaxLength="50" runat="server" /></span></td>
</tr>
</table>
</ev:eCustomForm>
</ev:eCustomModel>