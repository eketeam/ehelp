﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using EKETEAM.Data;
using EKETEAM.FrameWork;

namespace eFrameWork.AppOld
{
    public partial class Custom : System.Web.UI.Page
    {
        public string UserArea = "Application";
        public eModel model;
        public bool Ajax = false;
        public string AppItem = eParameters.Request("AppItem");
        public eUser user;
        protected void Page_Load(object sender, EventArgs e)
        {
            
            //System.Web.UI.HtmlControls.HtmlInputHidden
            user = new eUser(UserArea);
            user.Check();
            ApplicationMenu appmenu = new ApplicationMenu(user, "1");//生成首页URL
            //eModelInfo customModel = new eModelInfo(user);
            //model = customModel.Model;
            model = new eModel();
            string path = model.ModelInfo["AspxFile"].ToString();
            //eBase.Writeln(path);
            /*
            if (Request.Form["__VIEWSTATE"] != null)
            {
                for (int i = 0; i < Request.Form.Count; i++)
                {
                    string getkeys = Request.Form.Keys[i];
                    eBase.Writeln(getkeys + "=" + Request.Form[getkeys].ToString());
                }
                eBase.End();
            }
            */
            if (path.IndexOf("?") > -1 || path.ToLower().StartsWith("http"))
            {
                Response.Redirect(path, true);
            }
            else
            {
                eBase.ExecutePage(path, LitBody);
            }
            
            /*
            if (!eBase.parseBool(model.ModelInfo["AutoLayout"]))
            {
                Response.Write(LitBody.Text);
                this.Page.Visible = false;
            }
            */
        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            if (Request.QueryString["ajax"] != null) Ajax = Convert.ToBoolean(Request.QueryString["ajax"]);
            if (Master == null) return;
            //eBase.Writeln(Ajax.ToString());
            if (!Ajax)
            {
                MasterPageFile = "Main.Master";
            }
            else
            {
                MasterPageFile = "MainNone.Master";
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (Master == null) return;
            Literal lit = (Literal)Master.FindControl("LitTitle");
            if (lit != null) lit.Text = model.ModelInfo["mc"].ToString() + "-" + eConfig.ApplicationTitle(user["SiteID"].ToString());// model.ModelInfo["mc"].ToString() + " - " + 

            lit = (Literal)Master.FindControl("LitJavascript");
            if (lit != null && model.Javasctipt.Length > 0) lit.Text = model.Javasctipt;

            lit = (Literal)Master.FindControl("LitStyle");
            if (lit != null && model.cssText.Length > 0) lit.Text = model.cssText;

        }
    }
}