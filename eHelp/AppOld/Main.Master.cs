﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Linq;
using System.Text;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using System.Diagnostics;


namespace eFrameWork.AppOld
{
    public partial class Main : System.Web.UI.MasterPage
    {
        public string AppItem = eParameters.Request("AppItem");
        public eUser user;
        public string sysNameorLogo = "";
        public ApplicationMenu appmenu;
        public string appNames = "";

        public DataRow row_pass;
        public DataRow row_info;
        public DataRow row_acc;
        protected void Page_Init(object sender, EventArgs e)
        {
            user = new eUser("Application");
            user.Check();//检测用户是否登录,未登录则跳转到登录页
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            //10ms
          
            sysNameorLogo = eConfig.ApplicationLogo(user["SiteID"].ToString());
            appmenu = new ApplicationMenu(user, "1");
            //eBase.Writeln(appmenu.UserApps.Rows.Count.ToString());
            //eBase.PrintDataTable(appmenu.UserApps);
            //eBase.PrintDataTable(appmenu.Applications);
            //eBase.PrintDataTable(appmenu.ApplicationItems);

            if (appmenu.Applications.Rows.Count == 0)
            {
                Response.Write("没有权限!");
                Response.End();
            }
            else
            {
                if (appmenu.Applications.Rows.Count > 1)//两个及以上数量应用显示
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (DataRow dr in appmenu.Applications.Rows)
                    {
                        sb.Append("<a" + (appmenu.ApplicationID == dr["ApplicationID"].ToString() ? " class=\"cur\"" : "") + " href=\"" + (appmenu.ApplicationID == dr["ApplicationID"].ToString() ? "javascript:;" : dr["href"].ToString() + (eRequest.QueryString("debug") == null ? "" : "&debug=1")) + "\">");
                        if (dr["IconHTML"].ToString().Length > 10)
                        {
                            sb.Append(dr["IconHTML"].ToString());
                        }
                        else
                        {
                            string iconpath = dr["Icon"].ToString().Length > 10 ? dr["Icon"].ToString() : "../images/none.gif";
                            string iconactivepath = dr["IconActive"].ToString().Length > 0 ? dr["IconActive"].ToString() : iconpath;
                            sb.Append("<img class=\"def\" src=\"" + iconpath + "\">");// onerror=\"this.src='../images/none.gif';\"
                            sb.Append("<img class=\"cur\" src=\"" + iconactivepath + "\">");
                        }
                        sb.Append("<p>" + dr["MC"].ToString() + "</p>");
                        sb.Append("</a>");
                    }
                    appNames = sb.ToString();
                }
            }
            //eBase.WriteTip("Master共用 " + sw.Elapsed.TotalMilliseconds.ToString() + " 毫秒");



            DataRow[] rows = appmenu.UserApps.Select("ModelID='13d1ceba-eef8-4471-8abe-c617fc8838ea'");//修改密码  and ApplicationID='" + appmenu.ApplicationID + "'
            if (rows.Length > 0) row_pass = rows[0];

            rows = appmenu.UserApps.Select("ModelID='c76750c3-1bc5-4bdb-9805-640a62faab64'"); //个人信息  and ApplicationID='" + appmenu.ApplicationID + "'
            if (rows.Length > 0) row_info = rows[0];

            rows = appmenu.UserApps.Select("ModelID='f1578584-713e-48e2-a392-2ed0d5cb83bc'"); //切换帐号 and ApplicationID='" + appmenu.ApplicationID + "'
            if (rows.Length > 0) row_acc = rows[0];

        }
        protected void Page_Load1(object sender, EventArgs e)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            //10ms
            user = new eUser("Application");
            user.Check();//检测用户是否登录,未登录则跳转到登录页
            sysNameorLogo = eConfig.ApplicationLogo(user["SiteID"].ToString());



            appmenu = new ApplicationMenu(user,"1");
            //eBase.PrintDataTable(appmenu.ApplicationItems);
            eBase.PrintDataTable(appmenu.Applications);

            //eBase.PrintDataTable(appmenu.UserApps);
            //eBase.End();

            Stopwatch sw1 = new Stopwatch();
            sw1.Start();
            var qr = appmenu.UserApps.AsEnumerable().Where(row=> row["apptype"].ToString()=="3" || row["apptype"].ToString()==appmenu.AppType).Select(row =>
                      new { ApplicationID = row["ApplicationID"].ToString(), MC = row["MC"].ToString(), Pic = row["Pic"].ToString(), Icon = row["Icon"].ToString(), IconActive = row["IconActive"].ToString(), IconHTML = row["IconHTML"].ToString(), appSort = row["appSort"], appAddTime = row["appAddTime"] }
                      ).Distinct().OrderBy(p => p.appSort).OrderBy(p => p.appAddTime).ToList();

            sw1.Stop();
           //eBase.Writeln(qr.Count().ToString() +  "qr共用 " + sw1.Elapsed.TotalMilliseconds.ToString() + " 毫秒");

           //eBase.Writeln(appmenu.UserApps.Rows.Count.ToString());
            //eBase.PrintDataTable(appmenu.UserApps);

          //  return;
            //eBase.PrintDataTable(appmenu.UserApps);



            if (qr.Count() == 0)
            {
                Response.Write("没有权限!");
                Response.End();
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                foreach (var dr in qr)
                {
                    sb.Append("<a" + (appmenu.ApplicationID == dr.ApplicationID ? " class=\"cur\"" : "") + " href=\"" + (appmenu.ApplicationID == dr.ApplicationID ? "javascript:;" : appmenu.getTopModelUrl(dr.ApplicationID)) + "\">");
                    if (dr.IconHTML.Length > 10)
                    {
                        sb.Append(dr.IconHTML);
                    }
                    else
                    {
                        string iconpath = dr.Icon;
                        string iconactivepath = dr.IconActive.Length > 0 ? dr.IconActive : iconpath;
                        //  sb.Append("<img src=\"" + (appmenu.ApplicationID != dr["ApplicationID"].ToString() ? iconpath : iconactivepath) + "\" onerror=\"this.src='../images/none.gif';\">");
                        sb.Append("<img class=\"def\" src=\"" + iconpath + "\" onerror=\"this.src='../images/none.gif';\">");
                        sb.Append("<img class=\"cur\" src=\"" + iconactivepath + "\" onerror=\"this.src='../images/none.gif';\">");
                    }
                    sb.Append("<p>" + dr.MC + "</p>");
                    sb.Append("</a>");
                   
                } 
                appNames = sb.ToString();
            }
            //eBase.WriteTip("Master共用 " + sw.Elapsed.TotalMilliseconds.ToString() + " 毫秒");
        }
    }
}