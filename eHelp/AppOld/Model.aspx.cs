﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Diagnostics;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using EKETEAM.UserControl;

namespace eFrameWork.AppOld
{

    public partial class Model : System.Web.UI.Page
    {
        public string UserArea = "Application";
        public eModel model;
        public eUser user;
        public string mode = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            //Menu Menu1 = (Menu)Master.FindControl("Menu1");if (Menu1 != null){Menu1.aaa = "BBB"; }
            user = new eUser(UserArea);
            user.Check();

            
            model = new eModel();
            model.clientMode = "pc";
            LitBody.Text = model.autoHandle();

            //List<string> list = eBase.DataBase.getParentIDS("Dictionaries", "DictionarieID", "ParentID", "7db7fe42-c415-4b19-b38e-c2c02f0ab70b", "deltag=0",2);
            //eBase.WriteDebug("取所有下级测试：" + string.Join(",", list) );


        }
        protected void Page_PreInit(object sender, EventArgs e)
        {
            
            if (Master == null) return;
            if (!eRequest.Ajax)
            {
                MasterPageFile = "Main.Master";
            }
            else
            {
                MasterPageFile = "MainNone.Master";
            }
           
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {

            if (Master == null) return;

            Literal lit = (Literal)Master.FindControl("LitTitle");
            if (lit != null) lit.Text = model.ModelInfo["mc"].ToString() + "-" + eConfig.ApplicationTitle(user["SiteID"].ToString()); // model.ModelInfo["mc"].ToString() + " - " +               

            lit = (Literal)Master.FindControl("LitJavascript");
            if (lit != null && model.Javasctipt.Length>0) lit.Text = model.Javasctipt;

            lit = (Literal)Master.FindControl("LitStyle");
            if (lit != null && model.cssText.Length > 0) lit.Text = model.cssText;

        }
    }
}