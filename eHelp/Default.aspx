﻿<%@ Page Language="C#" MasterPageFile="Main.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteBody" runat="server">
<style>
    a {
    text-decoration:none;color: #0066CC;
    }
    html, body {
    padding:0px;margin:0px;
    }
    .body {
    margin:10px;
    }
    dl {padding:0px;margin:0px;
    }
    dt, dd {padding:0px;margin:0px;
    }
    dt {
    font-family: PingFangSC-Medium;
font-size: 20px;
color: #191c3d;
line-height: 26px;
margin:10px 0px 10px 0px;
    }
        dt a {
        color:#333;
        }
    dd {
    border:1px solid #f1f1f1;padding:15px;line-height:32px;
    }
    ul, li {
    list-style:none;padding:0px;margin:0px;
    }
    li {
    display:inline-block;max-width:195px;width:195px;
    }
        li a {display:block; white-space:nowrap; overflow:hidden;-o-text-overflow:ellipsis;text-overflow:ellipsis;
        }
</style>
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout: fixed;">
<colgroup>
<col width="260" />
<col />
<col width="220" />
</colgroup>
  <tr valign="top">
    <td>&nbsp;</td>
    <td><div class="body" style="margin-top:60px;"><asp:Literal ID="LitBody" runat="server" /></div></td>
    <td>&nbsp;</td>
  </tr>
</table>
    
</asp:Content>