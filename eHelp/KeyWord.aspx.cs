﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;

namespace eFrameWork
{
    public partial class KeyWord : System.Web.UI.Page
    {
        public eHelpCenter hc;
        private eDataBase db;
        private eDataBase DataBase
        {
            get
            {
                if (db == null) db = new eDataBase();
                return db;
            }
        }
        public string Title = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            //Response.Write(Request.Url.PathAndQuery);
            hc = new eHelpCenter();
            string id = eParameters.QueryString("id");
            DataTable tb = DataBase.getDataTable("select * from Help_KeyWords where HelpKeyID='" + id + "'");
            if (tb.Rows.Count > 0)
            {
                Title = tb.Rows[0]["Name"].ToString();
                LitBody.Text = tb.Rows[0]["Body"].ToString();
            }
            else
            {
                LitBody.Text = "暂无相关信息!";
            }
        }
    }
}