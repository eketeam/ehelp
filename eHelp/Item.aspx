﻿<%@ Page Language="C#" MasterPageFile="Main.Master" AutoEventWireup="true" CodeFile="Item.aspx.cs" Inherits="Item" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteBody" runat="server">
    
<div class="navlist" style="">
<h1 class="doctag" style="">标签</h1>
<div class="mytags"><%=hc.Tags %></div>
<h1 class="docstructure" style="display:none;">文档结构</h1>    
</div>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout: fixed;">
<colgroup>
<col width="260" />
<col />
<col width="220" />
</colgroup>
  <tr valign="top">
    <td width="260" bgcolor="#F7F9FD">
        <div style="mar3gin:6px 6px 6px 8px;">
        <h1 class="itemmore"><%=hc.ProjectName%><%=hc.ProjectMenu() %>
        </h1>
        <asp:Literal id="litMenu" runat="server" /></div>
    </td>
    <td>
        <div class="nav"><asp:Literal id="LitNav" runat="server" /></div>
	<div class="body">
        <asp:Literal id="LitBody" runat="server" />
        <%if (hc.simpleInteract)
          { %>
          <hr />
        <div class="helpresult">
        <div style="line-height:45px;">以上信息是否对您有帮助？</div>
        <div class="helpopt">
            <a href="javascript:;" onclick="Interact(1);" class="cur">是</a>
            <a href="javascript:;" onclick="Interact(0);">否</a>
        </div>
        </div>
        <%} %>
	</div>
    </td>
    <td>&nbsp;</td>
  </tr>
</table>



</asp:Content>