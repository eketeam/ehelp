﻿var b_animate=false;
function now(){date=new Date();var t=date.getFullYear().toString();t+=(date.getMonth()+1).toString();t+=(date.getDate()).toString();t+=(date.getHours()).toString();t+=(date.getMinutes()).toString();t+=(date.getSeconds()).toString();t+=(date.getMilliseconds()).toString();return t;};
String.prototype.getquerystring = function(name){var reg = new RegExp("(^|&|\\?)"+ name +"=([^&]*)(&|$)","i"),r; if (r=this.match(reg)){return r[2]; }return ""; };
String.prototype.removequerystring=function(str)
{
	
	if(this.indexOf("?") == -1){return this;}

	var reg = new RegExp("(^|&|\\?)"+ str +"=","i");
	var r=this.match(reg);
	if(!r){return this;}
	var v=this.getquerystring(str);	
	v=v.replace(/\\/g,"\\\\");
	var _back=this.replace(new RegExp(str + "=" + v + "&", "gi"),"");//中间
	_back=_back.replace(new RegExp("&" + str + "=" + v, "gi"),"");//最后
	_back=_back.replace(new RegExp("\\?" + str + "=" + v, "gi"),"");//最前
	return _back;
};
String.prototype.addquerystring=function(name,value)
{
	if(this.indexOf("?") == -1){return this + "?" + name + "=" + value;} //没有参数
	var reg = new RegExp("(^|&|\\?)"+ name +"=","i");
	var r=this.match(reg);
	if(!r) //不存在往后面加
	{
		return this + "&" + name + "=" + value;
	}
	//替换
	var v=this.getquerystring(name);	 
	return this.replace(new RegExp(name + "=" + v, "gi"),name + "=" + value);
};
function Interact(value)
{
	var url=document.location.href;
	url=url.addquerystring("act","addinteract");
	//url+="&act=addinteract";
	$.ajax({
			type: 'post',
			async: false,
			url:  url,
			data:{value:value},
			dataType: "html",
			success: function(data)
			{
				$(".helpresult").html("<p>感谢您的反馈！</p>");
			}
		});	
};
function hotkey(url,id)
{
	$.ajax({
			type: 'post',
			async: false,
			url:  url,	
			data:{hotkeyid:id},
			dataType: "html",
			success: function(data)
			{
			}
		});	
	document.location.assign(url);
};
function tags(url,id)
{
	$.ajax({
			type: 'post',
			async: false,
			url:  url,	
			data:{tagid:id},
			dataType: "html",
			success: function(data)
			{
			}
		});	
	document.location.assign(url);
};
function goSearch(frm)
{
	var url=$(frm).attr("action");
	url+=url.indexOf("?")>-1? "&" : "?";
	url+="key=" + frm.key.value;
	if(frm.key.value.length>0)
	{
		$.ajax({
			type: 'post',
			async: false,
			url:  url,	
			data:{key:frm.key.value},
			dataType: "html",
			success: function(data)
			{
			}
		});	
	}
	document.location.assign(url);
	return false;
};
function showKeyWords(title,url)
{
	//var url="KeyWord-" + id + ".html?t="+now();
	layer.open({
      type: 2,
      title: title,
      maxmin: false,
      shadeClose: true, //点击遮罩关闭层
      area : ["60%" , "80%"],	
	  // content: [url,'no'], 
	  content: url,
	  success: function(layero, index){},
	  cancel: function(index, layero){layer.close(index);},
	  end : function(index){layer.close(index);}
    });	
	//alert(id);	
};
function show_eTreeView(obj)
{
	
	var evt;
	var src;
	
	if(window.event)
	{
		evt=window.event;
		src = evt.srcElement;
	}
	else
	{
		var o = arguments.callee;
		var e;
		while(o != null)
		{
			e = o.arguments[0];
			if(e && (e.constructor == Event || e.constructor == MouseEvent)){evt=e;src=evt.target; break;}
			o = o.caller;
		}
	}
	var li=$(obj).parent();
	if(src.tagName=="SPAN")li=li.parent();
	//document.title= src.tagName;
	if(src.tagName!="LI" && src.tagName!="A" && src.tagName!="SPAN"){return false;}
	if (evt.stopPropagation){evt.stopPropagation();}else{evt.cancelBubble = true;}
	if (evt.preventDefault){evt.preventDefault();}else{evt.cancelBubble = true;}
	var ul=li.children("ul");	
	if(ul.length == 0){return false;}
	
	if(li.attr("class")=="close")
	{
		li.attr("class","open");
		//ul.show();
		ul.slideDown();
	}
	else
	{
		li.attr("class","close");
		//ul.hide();
		ul.slideUp();
				
	}	
	//if(li.attr("class")!="close"&&li.attr("class")!="open"){return false;}
	return false;
};
function gotop()
{
	// document.body.scrollTop = document.documentElement.scrollTop = 0;
 	scrollTo(0,0);
	$(".gotop").hide();
};
function showcode()
{
	if($('.qrimg img').length==0)
	{
		//$('.qrimg').html('<img src="plugins/urlcode.aspx?url=' + encodeURIComponent($('.qrimg').attr("codeurl"))  + '&scale=8" width="180" height="180" style="margin:0px;" />');
	}
	//$('.qrimg').show();
	//alert($('.qrimg').attr("codeurl") + "::" + $('.qrimg img').length);
};
function getVirtualPath()
{
	var elements = document.getElementsByTagName('script');
	var element=elements[elements.length-1];
	var src=element.getAttribute("src");
	if(src.toLowerCase().indexOf("http")>-1)
	{
		var i= src.toLowerCase().indexOf("/scripts/");
		if(i>0)
		{
			return src.substring(0,i+1);
		}
	}
	//if(src.startsWith("/")){return "/"};
	if(src.indexOf("/")==0){return "/"};
	var virtualPath="";
	if(src.indexOf("../")>-1)
	{
		for(var i=0;i<src.match(/\.\.\//ig).length;i++)
		{
			virtualPath+="../";
		}
	}
	return virtualPath;
};
$(document).ready(function(){
//alert(getVirtualPath());
//jQuery('#qrcode').qrcode("https://www.jq22.com");
	//$('.qrimg').qrcode({ render: "canvas",     width: 180,    height: 180,    text: $('.qrimg').attr("codeurl")});
	if($('.qrimg img').length==0)
	{
		var _path= typeof(path) == "string" ? path : "";
		$('.qrimg').html('<img src="' + _path + 'plugins/urlcode.aspx?url=' + encodeURIComponent($('.qrimg').attr("codeurl"))  + '&scale=8" width="180" height="180" style="margin:0px;" />');
	}
    $(window).scroll(function(){
		
        var topp = $(document).scrollTop();
        if(topp > 90)
		{ 
			$(".gotop").show();
		}
		else
		{
			$(".gotop").hide();
		}
		if(b_animate){return;}
		var elements=$(".navlist").find("h1,h2,h3,h4");
		if(elements.length==0){return;}
		if(topp<=parseInt($(elements[0]).prop("_top")))
		{
			if($(elements[0]).hasClass("cur")){return;}
			$(".navlist .cur").removeClass("cur");
			$(elements[0]).addClass("cur");
		}
		if(topp>=parseInt($(elements[elements.length-1]).prop("_top")))
		{
			if($(elements[elements.length-1]).hasClass("cur")){return;}
			$(".navlist .cur").removeClass("cur");
			$(elements[elements.length-1]).addClass("cur");
		}
		for(var i=elements.length-1;i>=0;i--)
		{
			if(topp>= parseInt($(elements[i]).prop("_top")) - 50 )
			{
				if($(elements[i]).hasClass("cur")){return;}
				$(".navlist .cur").removeClass("cur");
				$(elements[i]).addClass("cur");
				return;
			}
		}		
    });
	

	

});  

$(window).load(function(){
	
	
	$(".body").find("h1,h2,h3,h4").each(function(i,el){		
	
		var item=$(this).clone();
		if(item.text().length<2) {return false;}
		//alert(item.text());
		item.html(item.text())
		item.innerHTML=item.innerText;
		if(item.text()=="目录"){return false;}
		

		
		if(i==0){item.addClass("cur");}
		item.prop("_top",parseInt($(this).offset().top));
		
		item.click(function(){
			var _top=parseInt($(this).prop("_top"));
			b_animate=true;
			$("html,body").animate({scrollTop: _top}, 200,function(){ b_animate=false;});
			//scrollTo(0,_top);
			$(".navlist .cur").removeClass("cur");
			$(this).addClass("cur");
		});
		$(".navlist").append(item);
		//$(this).offset().top
		//alert( $(this).html() + "::" + $(this).offset().top);
	});
	
	var taglen=$(".mytags a").length;
	if(taglen>0){$(".doctag").show();}
	var doclen=$(".body").find("h1,h2,h3:not(:contains('目录')),h4").length;
	if(doclen>0){$(".docstructure").show();}
	if(taglen>0 || doclen>0){$(".navlist").show();}

}); 