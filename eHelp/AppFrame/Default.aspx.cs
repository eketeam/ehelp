﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using EKETEAM.FrameWork;
using EKETEAM.Data;

namespace eFrameWork.AppFrame
{
    public partial class Default : System.Web.UI.Page
    {
        public eUser user;
        public ApplicationMenu appmenu;
        public string appNames = "";
        public string toptab = "";
        public string topframe = "";

        public string appTitle = "";
        public string AppItem = eParameters.Request("AppItem");
        private string _top_fixed;
        public bool TopFixed
        {
            get
            {
                if (_top_fixed == null)
                {
                    string sname = "Application_TopFixed";
                    if (Session[sname] == null)
                    {

                        string tmp = eBase.UserInfoDB.getValue("select parValue from a_eke_sysUserCustoms where ParName='application_top_fixed' and UserID='" + user.ID + "'");
                        if (tmp.Length == 0) tmp = "true";
                        Session[sname] = tmp;
                        _top_fixed = tmp;
                    }
                    else
                    {
                        _top_fixed = Session[sname].ToString();
                    }
                }
                return eBase.parseBool(_top_fixed);
            }
        }
        private string _left_fixed;
        public bool LeftFixed
        {
            get
            {
                if (_left_fixed == null)
                {
                    string sname = "Application_LeftFixed";
                    if (Session[sname] == null)
                    {
                        string tmp = eBase.UserInfoDB.getValue("select parValue from a_eke_sysUserCustoms where ParName='application_left_fixed' and UserID='" + user.ID + "'");
                        if (tmp.Length == 0) tmp = "true";
                        Session[sname] = tmp;
                        _left_fixed = tmp;
                    }
                    else
                    {
                        _left_fixed = Session[sname].ToString();
                    }
                }
                return eBase.parseBool(_left_fixed);
            }
        }

        public string sysNameorLogo = "";

        public DataRow row_pass;
        public DataRow row_info;
        public DataRow row_acc;
        protected void Page_Load(object sender, EventArgs e)
        {

            user = new eUser("Application");
            user.Check();
          
            appTitle = eConfig.ApplicationTitle(user["SiteID"].ToString());
            sysNameorLogo = eConfig.ApplicationLogo(user["SiteID"].ToString());


     
            #region 设置布局
            string act = eParameters.QueryString("act");
            if (act == "getappmenu")
            {
                Page page = new Page();
                UserControl control = (UserControl)page.LoadControl(new eFileInfo(Request.Url.AbsolutePath).Path + "Menu.ascx");
                page.Controls.Add(control);
                System.IO.StringWriter sw = new System.IO.StringWriter();
                Server.Execute(page, sw, true);
                sw.Close();
                Response.Write(sw.ToString());
                eBase.End();
            }
            if (act == "setfixed")
            {
                string name = eParameters.QueryString("name");
                string value = eParameters.QueryString("value");
                string type = name == "application_left_fixed" ? "左侧固定" : "顶部固定";
                if (name == "application_left_fixed")
                {
                    Session.Remove("Application_LeftFixed");
                }
                else
                {
                    Session.Remove("Application_TopFixed");
                }
                /*
                string sql = "if exists (select * from a_eke_sysUserCustoms Where parName='" + name + "' and UserID='" + user.ID + "')";
                sql += "update a_eke_sysUserCustoms set parValue='" + value + "' where parName='" + name + "' and UserID='" + user.ID + "'";
                sql += " else ";
                sql += "insert into a_eke_sysUserCustoms (UserCustomID,UserID,parName,MC,parValue) ";
                sql += " values ('" + Guid.NewGuid().ToString() + "','" + user.ID + "','" + name + "','" + type + "','" + value + "')";
                */
                 string sql = "";
                 string ct = eBase.UserInfoDB.getValue("select count(1) from a_eke_sysUserCustoms Where parName='" + name + "' and UserID='" + user.ID + "'");
                 if (ct == "0")
                 {
                     sql = "insert into a_eke_sysUserCustoms (UserCustomID,UserID,parName,MC,parValue) ";
                     sql += " values ('" + Guid.NewGuid().ToString() + "','" + user.ID + "','" + name + "','" + type + "','" + value + "')";               
                 }
                 else
                 {
                     sql += "update a_eke_sysUserCustoms set parValue='" + value + "' where parName='" + name + "' and UserID='" + user.ID + "'";
                 }
                 eBase.UserInfoDB.Execute(sql);
                Response.End();
            }
            #endregion
            appmenu = new ApplicationMenu(user,"1");
            if (AppItem.Length == 0) AppItem = appmenu.ApplicationItemID;

            if (appmenu.ApplicationID.Length > 0)
            {               
                if (appmenu.Applications.Rows.Count > 0)
                {
                    DataRow row = appmenu.Applications.Rows[0];
                    toptab = "<a href=\"javascript:;\" _href=\"" + row["ApplicationItemID"].ToString() + "\" class=\"cur\"><span title=\"" + row["ModelName"].ToString() + "\">" + row["ModelName"].ToString() + "</span></a>\r\n";
                    topframe = "<iframe scrolling=\"no\" frameborder=\"0\" src=\"" + row["href"].ToString() + "\"></iframe>\r\n";
                }
            }

            #region 多应用
            if (appmenu.Applications.Rows.Count == 0)
            {
                Response.Write("没有权限!");
                Response.End();
            }
            else
            {
                if (appmenu.Applications.Rows.Count > 1)//两个及以上数量应用显示
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (DataRow dr in appmenu.Applications.Rows)
                    {
                        sb.Append("<a" + (appmenu.ApplicationID == dr["ApplicationID"].ToString() ? " class=\"cur\"" : "") + " href=\"javascript:;\" eAppID=\"" + dr["ApplicationID"].ToString() + "\" onclick=\"getAppMenu(this,'" + dr["ApplicationID"].ToString() + "');\">");
                        if (dr["IconHTML"].ToString().Length > 10)
                        {
                            sb.Append(dr["IconHTML"].ToString());
                        }
                        else
                        {
                            string iconpath = dr["Icon"].ToString().Length > 10 ? dr["Icon"].ToString() : "../images/none.gif";
                            string iconactivepath = dr["IconActive"].ToString().Length > 0 ? dr["IconActive"].ToString() : iconpath;
                            sb.Append("<img class=\"def\" src=\"" + iconpath + "\">");// onerror=\"this.src='../images/none.gif';\"
                            sb.Append("<img class=\"cur\" src=\"" + iconactivepath + "\">");
                        }
                        sb.Append("<p>" + dr["mc"].ToString() + "</p>");
                        sb.Append("</a>");
                    }
                    appNames = sb.ToString();
                }
            }
            #endregion

            DataRow[] rows = appmenu.UserApps.Select("ModelID='13d1ceba-eef8-4471-8abe-c617fc8838ea'");//修改密码
            if (rows.Length > 0) row_pass = rows[0];

            rows = appmenu.UserApps.Select("ModelID='c76750c3-1bc5-4bdb-9805-640a62faab64'"); //个人信息
            if (rows.Length > 0) row_info = rows[0];

            rows = appmenu.UserApps.Select("ModelID='f1578584-713e-48e2-a392-2ed0d5cb83bc'"); //切换帐号
            if (rows.Length > 0) row_acc = rows[0];
        }
    }
}