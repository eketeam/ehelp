﻿<%@ Page Language="C#" MasterPageFile="Main.Master" AutoEventWireup="true" CodeFile="Update.aspx.cs" Inherits="eFrameWork.Manage.Update" %>
<%@ Register Src="GroupMenu.ascx" TagPrefix="uc1" TagName="GroupMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<uc1:GroupMenu runat="server" ID="GroupMenu" />
<div class="nav">您当前位置：<a href="Default.aspx">首页</a>&nbsp;->&nbsp;在线更新<asp:Literal id="litNav" runat="server" /></div>
<style>
html, body {margin:0px;padding:0px;font-size:13px;}
.upload,.upload li {margin:0px;padding:0px;list-style:none;}
.upload li {te3xt-indent:10px;border:1px dashed #fff;line-hei4ght:28px;font-size:13px;color:#333;padding-left:28px;}
.upload li.folder {background:transparent url('../Plugins/fileManage/images/folder.gif') no-repeat 2px 3px;background-size:24px 24px;}
.upload li.file {background:transparent url('../Plugins/fileManage/images/file.gif') no-repeat 2px 3px;background-size:24px 24px; }

.upload li * {vertical-align:middle;line-height:30px;display:inline-block;}
.upload li a {text-decoration:none;border:1px dashed transparent; overflow:hidden;color:#0000CC;}
.upload li a:focus { outline:none;}
.upload li:hover {border:1px dashed #ccc;}
.path {background-color:#f2f2f2;margin:6px;padding-left:8px;line-height:28px; font-family:宋体;}
.path a {text-decoration:none;color:#0026ff;}
</style>
 <link href="../Plugins/eTree/default/style.css" rel="stylesheet" type="text/css" />
<body>
<div class="path">
<asp:Literal id="LitNav1" runat="server" />
</div>
    

<div style="margin:10px;">
<%if (eRegisterInfo.Base == 0 && eRegisterInfo.Loaded)
  { %>
<div class="tips" style="margin-bottom:6px;"><b>更新提示</b><br>更新服务针对授权用户使用，<a href="http://frame.eketeam.com/getSerialNumber.aspx" style="color:#ff0000;" target="_blank">申请临时授权</a>后可使用。</div>
    <%} %>



<asp:Literal id="litBody" runat="server" />
</div>
</asp:Content>