﻿using System;
using System.Data;
using System.Data.Common;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using EKETEAM.Data;
using EKETEAM.FrameWork;

namespace eFrameWork.Manage
{
    public partial class DataSources : System.Web.UI.Page
    {
        public string act = eParameters.Request("act");
        public eForm edt;
        public eUser user;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser("Manage");
            edt = new eForm("a_eke_sysDataSources", user);
            if (act.Length == 0)
            {
                List();
                return;
            }
            #region 连接测试
            if (act == "tryconn")
            {
                string type = eParameters.Form("type");
                string connstr = eParameters.Form("value");
                DbProviderFactory provider = DbProviderFactories.GetFactory(type);
                DbConnection conn = provider.CreateConnection();

                string message = "";
                string errorcode="0";
                try
                {
                    conn.ConnectionString = connstr;
                    conn.Open();
                    conn.Close();
                    message = "连接成功!";
                    errorcode = "0";
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                    errorcode = "-1";
                }
                if (errorcode == "0")
                {
                    eResult.Success(message);
                }
                else
                {
                    eResult.Error(message);
                }
            }
            #endregion
            if (act == "active") //是否为用户库
            {
                string sql = eParameters.Replace("update a_eke_sysDataSources set RunUserPower='{querystring:value}' where DataSourceID='{querystring:id}'", null, null);
                eBase.DataBase.Execute(sql);
                eBase.clearDataCache("a_eke_sysDataSources");
                Response.Redirect(Request.ServerVariables["HTTP_REFERER"] == null ? "DataSources.aspx" : Request.ServerVariables["HTTP_REFERER"].ToString(), true);
                eBase.End();
            }
            #region 信息添加、编辑
            edt.AddControl(eFormControlGroup);
            edt.onChange += new eFormTableEventHandler(edt_onChange);
            edt.Handle();
            #endregion

        }
        public void edt_onChange(object sender, eFormTableEventArgs e)
        {
            if (e.eventType == eFormTableEventType.Inserting)
            {
                if (user["ServiceID"].Length > 0) edt.Fields.Add("ServiceID", user["ServiceID"]);
            }
            if (e.eventType == eFormTableEventType.Inserted || e.eventType == eFormTableEventType.Updated)
            {
                //eBase.UserInfoDB = null;
            }
        }
        private void List()
        {
            eList datalist = new eList("a_eke_sysDataSources");
            datalist.Where.Add("delTag=0");
            datalist.Where.Add("ServiceID" + (user["ServiceID"].Length == 0 ? " is null" : "='" + user["ServiceID"] + "'"));
            datalist.OrderBy.Add("addTime desc");
            datalist.Bind(Rep, ePageControl1);
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (Master == null) return;
            Literal lit = (Literal)Master.FindControl("LitTitle");
            if (lit != null)
            {
                lit.Text = "数据源 - " + eConfig.manageName();
            }
        }
    }
}