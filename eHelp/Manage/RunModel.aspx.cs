﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using EKETEAM.FrameWork;
using EKETEAM.Data;

public partial class Manage_RunModel : System.Web.UI.Page
{
    public string UserArea = "Manage";
    public eModel model;
    public string ModelID = eParameters.Request("modelid");
    eUser user;
    protected void Page_Load(object sender, EventArgs e)
    {
        user = new eUser(eBase.getUserArea(UserArea));
        user.Check();
        model = new eModel(ModelID, user);       
        #region 赋权限
        foreach (string key in model.Power.Keys)
        {
            model.Power[key] = true;
            //eBase.Writeln(key);
        }
        //eBase.Print(model.Power);
        //eBase.End();
        
        #endregion
        LitBody.Text= model.autoHandle();       
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Master == null) return;
        Literal lit = (Literal)Master.FindControl("LitTitle");
        if (lit != null)
        {
            lit.Text = "运行测试 - " + eConfig.manageName();
        }

        lit = (Literal)Master.FindControl("LitJavascript");
        if (lit != null && model.Javasctipt.Length > 0) lit.Text = model.Javasctipt;


        lit = (Literal)Master.FindControl("LitStyle");
        if (lit != null && model.cssText.Length > 0) lit.Text = model.cssText;
    }
}