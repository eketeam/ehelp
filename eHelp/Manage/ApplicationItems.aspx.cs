﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using EKETEAM.UserControl;
using LitJson;

namespace eFrameWork.Manage
{
    public partial class ApplicationItems : System.Web.UI.Page
    {
        public string appName = "";
        public string AppId = eParameters.QueryString("AppId");
        public void updatePX(string ParentID)
        {
            string sql = "update a_eke_sysApplicationItems set PX=(";
            sql += "select b.rownum from ";
            sql += "(";
            sql += "select ROW_NUMBER() over(order by px,addtime) as rownum,ApplicationItemID,addTime from a_eke_sysApplicationItems where delTag=0 and ApplicationID='" + AppId + "'";
            sql += (ParentID.Length == 0 ? " and ParentID IS NULL" : " and ParentID='" + ParentID + "'");
            sql += ") as b where b.ApplicationItemID=a_eke_sysApplicationItems.ApplicationItemID";
            sql += ")  where delTag=0 and ApplicationID='" + AppId + "'";
            sql += (ParentID.Length == 0 ? " and ParentID IS NULL" : " and ParentID='" + ParentID + "'");
            eBase.DataBase.Execute(sql);
        }
        private string getModelAddItems(string modelid)
        {
            DataRow[] rs = eBase.a_eke_sysModelItems.Select("ModelID='" + modelid + "' and showAdd=1 and len(code)>0 and len(isnull(addControlType,''))>0", "AddOrder, PX, addTime");
            string json = "[";
            for (int i = 0; i < rs.Length; i++)
            {
                if (i > 0) json += ",";
                //json += "{\"text\":\"" + rs[i]["MC"].ToString() + "\",\"value\":\"" + rs[i]["frmName"].ToString() + "\"}";
                json += "{\"text\":\"" + rs[i]["MC"].ToString() + "\",\"value\":\"" + rs[i]["ModelItemID"].ToString() + "\"}";
            }
            json += "]";
            return json;
        }
        public string getJsonText(string options,string jsonstr, string name)
        {
            // eJson opts = new eJson(options);
            JsonData opts = options.ToJsonData();

           
            DataTable dt = opts.toRows();
            //eBase.Writeln(options);
           // eBase.Writeln(jsonstr);
           // eBase.PrintDataTable(dt);
            //eBase.End();
            //eBase.Writeln(opts.Collection.Count.ToString());
            StringBuilder sb = new StringBuilder();
            if (jsonstr.Length > 0)
            {
               // eJson json = new eJson(jsonstr);
                //foreach (eJson m in json.GetCollection())
                JsonData json = jsonstr.ToJsonData();
                foreach (JsonData m in json)
                {
                   // eBase.Writeln(name);
                    if (dt.Columns.Contains("value"))
                    {
                        DataRow[] rs = dt.Select("value='" + m.getValue(name) + "'");
                        string text = rs.Length > 0 ? rs[0]["text"].ToString().HtmlDecode() : "";
                        sb.Append("<span style=\"display:inline-block;margin-right:6px;border:1px solid #ccc;padding:3px 12px 3px 12px;\">" + text + "</span>");
                    }
                    else
                    {
                        //eBase.Writeln("no");
                        //eBase.Writeln(options);
                        //eBase.Writeln(jsonstr);
                        //eBase.PrintDataTable(dt);
                        //eBase.End();
                    }
                }
                //eBase.End();
            }
            return sb.ToString();
        }
        /// <summary>
        /// 模块列表
        /// </summary>
        private void getAppItems(StringBuilder sb, string ParentID, int level)
        {
            updatePX(ParentID);
            string sql = "SELECT a.ApplicationID,a.ApplicationItemID,a.PackID,a.ModelID,a.ParentID,a.MC,a.condDisable,a.Condition,a.URL,a.Show,a.Finsh,a.DefaultValue,a.Icon,a.IconActive,a.IconHTML,a.Extend,a.PX,b.Auto,b.AutoLayout,b.AspxFile FROM a_eke_sysApplicationItems a";
            sql += " left join a_eke_sysModels b on a.ModelID=b.ModelID";
            sql += " where a.ApplicationID='" + AppId + "' " + (ParentID.Length == 0 ? " and a.ParentID is null" : " and a.ParentID='" + ParentID + "'") + " and a.delTag=0";
            sql += " order by a.PX,a.addTime";
            DataTable tb=eBase.DataBase.getDataTable(sql);
            for (int i = 0; i < tb.Rows.Count; i++)
            {
                DataRow dr=tb.Rows[i];
                int ct = 0;
                if (dr["modelid"].ToString().Length == 0) ct = Convert.ToInt32(eBase.DataBase.getValue("select count(1) from a_eke_sysApplicationItems where ApplicationID='" + AppId + "'  and ParentID='" + dr["ApplicationItemID"].ToString() + "' and delTag=0"));
                int textIndent = level * 25;
                //if (ct > 0 && level > 0) textIndent -= 18;

                sb.Append("<tr "+ (ParentID.Length>0?" style=\"display:none;\"":"" )+" o_nclick=\"showitems(this);\" erowid=\"" + dr["ApplicationItemID"].ToString() + "\" parentid=\"" + dr["ParentID"].ToString() + "\">\r\n");
                sb.Append("<td height=\"26\" align=\"center\"><a title=\"删除模块\" href=\"javascript:;\" onclick=\"delApp(this,'" + dr["ApplicationItemID"].ToString() + "');\"><img width=\"16\" height=\"16\" src=\"images/del.png\" border=\"0\"></a></td>\r\n");

                sb.Append("<td><input reload=\"true\" type=\"checkbox\" onclick=\"setApp(this,'" + dr["ApplicationItemID"].ToString() + "','show');\"" + (eBase.parseBool(dr["show"]) ? " checked" : "") + " /></td>\r\n");
                sb.Append("<td style=\"text-indent:" + textIndent.ToString() + "px;\">");
                if (ct > 0) { sb.Append("<a href=\"javascript:;\" class=\"close\" onclick=\"showitems(this.parentNode.parentNode);\" style=\"margin:0px;\"></a>"); }
                //sb.Append("<span>" + dr["mc"].ToString() + "</span>");

                sb.Append("<input reload=\"true\" class=\"text\" style=\"width:" + (250 - textIndent - 20 - (ct > 0 ? 18 : 0)) + "px;\" type=\"text\" value=\"" + dr["MC"].ToString().HtmlEncode() + "\" oldvalue=\"" + dr["MC"].ToString().HtmlEncode() + "\" onBlur=\"setApp(this,'" + dr["ApplicationItemID"].ToString() + "','mc');\">");
                sb.Append("</td>\r\n");
                sb.Append("<td>");
                //sb.Append("<select reload=\"true\" onChange=\"setApp(this,'" + dr["ApplicationItemID"].ToString() + "','ModelID');\" style=\"width:140px;\">\r\n");
                //sb.Append("<option value=\"NULL\">无</option>\r\n");
                //sb.Append(eBase.DataBase.getOptions("select MC,ModelID from a_eke_sysModels where delTag=0 and (Type=1 or Type=4 or Type=5) and subModel=0 order by addTime desc", "MC", "ModelID",dr["ModelID"].ToString()));
                //sb.Append("<select>");


                sb.Append("<input reload=\"true\" class=\"text\" type=\"text\" value=\"" + dr["ModelID"].ToString() + "\" oldvalue=\"" + dr["ModelID"].ToString() + "\" onBlur=\"setApp(this,'" + dr["ApplicationItemID"].ToString() + "','modelid');\" style=\"display:none;\">");
                sb.Append("<a href=\"javascript:;\" onclick=\"selectModel(this);\" title=\"选择模块\"><img src=\"images/link.png\"></a>");             
                if (dr["ModelID"].ToString().Length > 0)
                {
                    sb.Append("<a href=\"javascript:;\" onclick=\"clearModel(this);\" title=\"清除选择\"><img src=\"images/unlink.png\"></a>\r\n");
                    //sb.Append("<a href=\"ModelItems.aspx?ModelID=" + dr["ModelID"].ToString() + "\" class=\"editmodel\" target=\"_blank\">&nbsp;</a>");

                    sb.Append("<a href=\"ModelItems.aspx?ModelID=" + dr["ModelID"].ToString() + "\" target=\"_blank\" style=\"display:inline;\">" + eBase.DataBase.getValue("select mc from a_eke_sysModels where ModelID='" + dr["ModelID"].ToString() + "'") + "</a>");
                  
                }
                sb.Append("</td>\r\n");
                sb.Append("<td>");
                sb.Append("<select reload=\"true\" onChange=\"setApp(this,'" + dr["ApplicationItemID"].ToString() + "','ParentID');\" style=\"width:90px;\">\r\n");
                sb.Append("<option value=\"NULL\">无</option>\r\n");
                sb.Append(eBase.DataBase.getOptions("select ApplicationItemID,MC from a_eke_sysApplicationItems where delTag=0 and ModelID is null and len(mc)>0 and ApplicationID='" + dr["ApplicationID"].ToString() + "' and ApplicationItemID!='" + dr["ApplicationItemID"].ToString() + "' order by PX,addTime", "MC", "ApplicationItemID", dr["ParentID"].ToString()));
                sb.Append("<select>");
                sb.Append("</td>\r\n");
                sb.Append("<td>");
                if (dr["ModelID"].ToString().Length > 0 && !eBase.parseBool( dr["show"]))
                {

                    sb.Append("<select reload=\"false\" onChange=\"setApp(this,'" + dr["ApplicationItemID"].ToString() + "','PackID');\" style=\"width:90px;\">\r\n");
                    sb.Append("<option value=\"NULL\">无</option>\r\n");
                    //sb.Append(eBase.DataBase.getOptions("select ApplicationItemID,MC from a_eke_sysApplicationItems where delTag=0 and (len(ModelID)>0" + (dr["PackID"].ToString().Length > 0 ? " or ApplicationItemID='" + dr["PackID"].ToString() + "'" : "") + ") and len(mc)>0 and ApplicationItemID!='" + dr["ApplicationItemID"].ToString() + "' order by PX,addTime", "MC", "ApplicationItemID", dr["PackID"].ToString()));
                    sb.Append(eBase.DataBase.getOptions("select ApplicationItemID,MC from a_eke_sysApplicationItems where ApplicationID='" + AppId + "' and show=1 and delTag=0 " + (dr["parentID"].ToString().Length > 0 ? " and ParentID='" + dr["parentID"].ToString() + "'" : " and ParentID is null") + " and (len(ModelID)>0" + (dr["PackID"].ToString().Length > 0 ? " or ApplicationItemID='" + dr["PackID"].ToString() + "'" : "") + ") and len(mc)>0 and ApplicationItemID!='" + dr["ApplicationItemID"].ToString() + "' order by PX,addTime", "MC", "ApplicationItemID", dr["PackID"].ToString()));
                    sb.Append("<select>");

                }
                else
                {
                    sb.Append("&nbsp;");
                }
                sb.Append("</td>");
                sb.Append("<td>");
                sb.Append("<input class=\"text\" type=\"text\" value=\"" + dr["condDisable"].ToString() + "\" oldvalue=\"" + dr["condDisable"].ToString() + "\" ondblclick=\"dblClick(this,'" + "禁用条件');\" onBlur=\"setApp(this,'" + dr["ApplicationItemID"].ToString() + "','condDisable');\">");
                sb.Append("</td>");
                sb.Append("<td>");
                if (dr["ModelID"].ToString().Length > 0)
                {
                    //sb.Append("<input class=\"text\" type=\"text\" value=\"" + dr["Condition"].ToString().HtmlEncode() + "\" oldvalue=\"" + dr["Condition"].ToString().HtmlEncode() + "\" onBlur=\"setApp(this,'" + dr["ApplicationItemID"].ToString() + "','condition');\">");
                    sb.Append("<textarea class=\"text\" oldvalue=\"" + dr["Condition"].ToString().HtmlEncode() + "\" style=\"width:145px;max-width:145px;\" ondblclick=\"dblClick(this,'" + "模块条件');\" onBlur=\"setApp(this,'" + dr["ApplicationItemID"].ToString() + "','condition');\">" + dr["Condition"].ToString().HtmlEncode() + "</textarea>");
                }
                else
                {
                    sb.Append("&nbsp;");
                }
                sb.Append("</td>\r\n");
                sb.Append("<td>");
                if (dr["ModelID"].ToString().Length > 0 && eBase.parseBool( dr["Auto"]))
                {
                    string options = getModelAddItems(dr["ModelID"].ToString());
                    sb.Append("<input id=\"dv_" + dr["ApplicationItemID"].ToString().Replace("-", "") + "\" class=\"text\" type=\"text\" options=\"" + options.HtmlEncode() + "\" value=\"" + dr["DefaultValue"].ToString().HtmlEncode() + "\" oldvalue=\"" + dr["DefaultValue"].ToString().HtmlEncode() + "\" onBlur=\"setApp(this,'" + dr["ApplicationItemID"].ToString() + "','defaultvalue');\" style=\"width:80px;display:none;\">");
                    sb.Append("<a href=\"javascript:;\" onclick=\"defaultValue_Edit('dv_" + dr["ApplicationItemID"].ToString().Replace("-", "") + "','默认值');\" class=\"editmodel\">&nbsp;</a>");

                    if(dr["DefaultValue"].ToString().Length>0) sb.Append(getJsonText(options, dr["DefaultValue"].ToString(), "name"));

                   // eBase.Writeln(options);
                    //Response.Write();
                    //getJsonText
                }
                else
                {
                    sb.Append("&nbsp;");
                }
                sb.Append("</td>\r\n");
                sb.Append("<td>");
                sb.Append("<textarea class=\"text\" style=\"width:5px;max-width:5px;display:none;\" onBlur=\"postApp(this,'" + dr["ApplicationItemID"].ToString() + "','extend');\" >");
                sb.Append(dr["Extend"].ToString().HtmlEncode());
                sb.Append("</textarea>");
                sb.Append("<a href=\"javascript:;\" onclick=\"dbledit(this);\" class=\"editmodel\">&nbsp;</a>");

                sb.Append("</td>\r\n");
                sb.Append("<td>");
                string _url = dr["url"].ToString();
                if (_url.Length == 0 && dr["ModelID"].ToString().Length > 0)
                {
                    /*
                    if (eBase.parseBool(dr["AutoLayout"]) || dr["AspxFile"].ToString().IndexOf("/") > -1)
                    {
                        _url = (eBase.parseBool(dr["Auto"]) ? "Model.aspx" : "Custom.aspx") + "?AppItem=" + dr["ApplicationItemID"].ToString();
                    }
                    else
                    {
                        _url = dr["AspxFile"].ToString() + "?AppItem=" + dr["ApplicationItemID"].ToString();
                    }
                    */
                    _url = (eBase.parseBool(dr["Auto"]) ? "Model.aspx" : "Custom.aspx") + "?AppItem=" + dr["ApplicationItemID"].ToString();
                }
                // sb.Append("<input class=\"text\" type=\"text\" value=\"" + _url + "\" oldvalue=\"" + _url + "\"  style=\"width:120px;\" onBlur=\"setApp(this,'" + dr["ApplicationItemID"].ToString() + "','url');\" />");
                sb.Append("<textarea class=\"text\" oldvalue=\"" + _url + "\"  style=\"width:135px;max-width:135px;\" ondblclick=\"dblClick(this,'" + "URL');\" onBlur=\"setApp(this,'" + dr["ApplicationItemID"].ToString() + "','url');\" >");
                sb.Append(_url);
                sb.Append("</textarea>");
                sb.Append("</td>\r\n");
                /*
                sb.Append("<td>");             
                _url = dr["murl"].ToString();
                if (_url.Length == 0 && dr["ModelID"].ToString().Length > 0)
                {
                    _url = (eBase.parseBool(dr["Auto"]) ? "Model.aspx" : "Custom.aspx") + "?AppItem=" + dr["ApplicationItemID"].ToString();
                }
                sb.Append("<textarea class=\"text\" oldvalue=\"" + _url + "\"  style=\"width:115px;max-width:115px;\" onBlur=\"setApp(this,'" + dr["ApplicationItemID"].ToString() + "','murl');\" >");
                sb.Append(_url);
                sb.Append("</textarea>");
                sb.Append("</td>\r\n");
                */
                sb.Append("<td>");
                sb.Append("<input class=\"text\" type=\"text\" value=\"" + dr["icon"].ToString() + "\" oldvalue=\"" + dr["icon"].ToString() + "\"  style=\"display:none;width:140px;\" onBlur=\"setApp(this,'" + dr["ApplicationItemID"].ToString() + "','icon');\" />");
                sb.Append("<img src=\"../" + dr["icon"].ToString() + "\" style=\"max-width:21px;max-height:20px;vertical-align:middle;margin-right:6px;\" onerror=\"this.src='../images/none.gif';\">");
                sb.Append("<a href=\"javascript:;\" onclick=\"openIconForm(this);\">选择</a>");
                sb.Append("</td>\r\n");
                sb.Append("<td>");
                sb.Append("<input class=\"text\" type=\"text\" value=\"" + dr["iconactive"].ToString() + "\" oldvalue=\"" + dr["iconactive"].ToString() + "\"  style=\"display:none;width:140px;\" onBlur=\"setApp(this,'" + dr["ApplicationItemID"].ToString() + "','iconactive');\" />");
                sb.Append("<img src=\"../" + dr["iconactive"].ToString() + "\" style=\"max-width:20px;max-height:20px;vertical-align:middle;margin-right:6px;\" onerror=\"this.src='../images/none.gif';\">");
                sb.Append("<a href=\"javascript:;\" onclick=\"openIconForm(this);\">选择</a>");
                sb.Append("</td>\r\n");
                //字体图标
                sb.Append("<td>");
                sb.Append("<input class=\"text\" type=\"text\" value=\"" + dr["IconHTML"].ToString().HtmlEncode() + "\" oldvalue=\"" + dr["IconHTML"].ToString().HtmlEncode() + "\"  style=\"display:none;\" onBlur=\"setApp(this,'" + dr["ApplicationItemID"].ToString() + "','iconhtml');\" />");
                sb.Append("<span id=\"spanico\" style=\"font-size:18px;margin-right:6px;\">" + dr["IconHTML"].ToString() + "</span>");
                sb.Append("<a href=\"javascript:;\" onclick=\"SelectICO(this);\">选择</a>");
                sb.Append("</td>\r\n");
                sb.Append("<td><select reload=\"false\" onChange=\"setApp(this,'" + dr["ApplicationItemID"] + "','finsh');\" style=\"width:70px;color:#" + (eBase.parseBool(dr["finsh"]) ? "33cc00" : "ff0000") + ";\">\r\n");
                //sb.Append("<option value=\"\">无</option>\r\n");
                sb.Append("<option style=\"color:#33cc00;\" value=\"1\"" + (eBase.parseBool( dr["finsh"]) ? " selected=\"true\"" : "") + ">完成</option>\r\n");
                sb.Append("<option style=\"color:#ff0000;\" value=\"0\"" + (!eBase.parseBool(dr["finsh"]) ? " selected=\"true\"" : "") + ">未完成</option>\r\n");
                sb.Append("<select></td>\r\n");
                sb.Append("<td style=\"cursor:move;\">&nbsp;</td>\r\n"); // dr["px"].ToString() 
                sb.Append("</tr>\r\n");

                if (dr["modelid"].ToString().Length > 0) continue;
                getAppItems(sb, dr["ApplicationItemID"].ToString(), level + 1);
            }
        }
        /// <summary>
        /// 模块列表
        /// </summary>
        private void getAppItems(StringBuilder sb)
        {
            sb.Append("<table id=\"eDataTable_Items\" class=\"eDataTable\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" widt5h=\"100%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr bgcolor=\"#f2f2f2\">\r\n");
            sb.Append("<td height=\"25\" width=\"30\" align=\"center\"><a title=\"添加模块\" href=\"javascript:;\" onclick=\"addApp(this);\"><img width=\"16\" height=\"16\" src=\"images/add.png\" border=\"0\"></a></td>\r\n");
            sb.Append("<td width=\"40\">显示</td>\r\n");
            sb.Append("<td width=\"250\">名称</td>\r\n");
            sb.Append("<td width=\"180\">模块</td>\r\n");
            sb.Append("<td width=\"100\">上级</td>\r\n");
            sb.Append("<td width=\"100\">合并到</td>\r\n");
            sb.Append("<td width=\"70\">禁用条件</td>\r\n");
            sb.Append("<td width=\"160\">模块条件</td>\r\n");
            sb.Append("<td width=\"150\">默认值</td>\r\n");
            sb.Append("<td width=\"40\">扩展</td>\r\n");
            sb.Append("<td width=\"150\">URL</td>\r\n");
            /*
            sb.Append("<td width=\"120\">mURL</td>\r\n");
            */
            sb.Append("<td width=\"80\">图标</td>\r\n");
            sb.Append("<td width=\"80\">活动图标</td>\r\n");
            sb.Append("<td width=\"80\">字体图标</td>\r\n");
            sb.Append("<td width=\"80\">状态</td>\r\n");
            sb.Append("<td width=\"60\">顺序</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody eSize=\"false\" eMove=\"true\">\r\n");
            getAppItems(sb, "", 0);
            sb.Append("</tbody>\r\n");
            sb.Append("</table>\r\n");
        }

        private DataTable _menuitems;
        public DataTable MenuItems
        {
            get
            {
                if (_menuitems == null)
                {
                    _menuitems = eBase.DataBase.getDataTable("select * from a_eke_sysApplicationMenus where ApplicationID='" + AppId + "' and delTag=0");
                }
                return _menuitems;
            }
        }
        public void updateMenuPX(string ParentID)
        {
            string sql = "update a_eke_sysApplicationMenus set PX=(";
            sql += "select b.rownum from ";
            sql += "(";
            sql += "select ROW_NUMBER() over(order by px,addtime) as rownum,ApplicationMenuID,addTime from a_eke_sysApplicationMenus where delTag=0 and ApplicationID='" + AppId + "'";
            sql += (ParentID.Length == 0 ? " and ParentID IS NULL" : " and ParentID='" + ParentID + "'");
            sql += ") as b where b.ApplicationMenuID=a_eke_sysApplicationMenus.ApplicationMenuID";
            sql += ")  where delTag=0 and ApplicationID='" + AppId + "'";
            sql += (ParentID.Length == 0 ? " and ParentID IS NULL" : " and ParentID='" + ParentID + "'");
            eBase.DataBase.Execute(sql);
        }
        //移动端菜单项
        private void getMenuItems(StringBuilder sb, string ParentID, int level)
        {
            updateMenuPX(ParentID);
            DataRow[] rows = MenuItems.Select(ParentID.Length == 0 ? "ParentID is null" : "ParentID='" + ParentID + "'", "px,addtime");
            foreach (DataRow dr in rows)
            {
                int ct = 0;
                if (dr["ParentID"].ToString().Length == 0) ct = MenuItems.Select("ParentID='" + dr["ApplicationMenuID"].ToString() + "'").Length;
                int textIndent = level * 25;

                sb.Append("<tr erowid=\"" + dr["ApplicationMenuID"].ToString() + "\" parentid=\"" + dr["ParentID"].ToString() + "\">\r\n");
                sb.Append("<td height=\"26\" align=\"center\"><a title=\"删除菜单\" href=\"javascript:;\" onclick=\"delMenu(this,'" + dr["ApplicationMenuID"].ToString() + "');\"><img width=\"16\" height=\"16\" src=\"images/del.png\" border=\"0\"></a></td>\r\n");
                sb.Append("<td><input reload=\"true\" type=\"checkbox\" onclick=\"setMenu(this,'" + dr["ApplicationMenuID"].ToString() + "','show');\"" + (eBase.parseBool( dr["show"]) ? " checked" : "") + " /></td>\r\n");

                sb.Append("<td style=\"text-indent:" + textIndent.ToString() + "px;\">");
                if (ct > 0) { sb.Append("<a href=\"javascript:;\" class=\"open\" _onclick=\"showsub(this);\" style=\"margin:0px;\"></a>"); }
                sb.Append("<input reload=\"true\" class=\"text\" style=\"width:" + (250 - textIndent - 20 - (ct > 0 ? 18 : 0)) + "px;\" type=\"text\" value=\"" + dr["MC"].ToString().HtmlEncode() + "\" oldvalue=\"" + dr["MC"].ToString().HtmlEncode() + "\" onBlur=\"setMenu(this,'" + dr["ApplicationMenuID"].ToString() + "','mc');\">");
                sb.Append("</td>\r\n");

                sb.Append("<td>");
                sb.Append("<select reload=\"true\" onChange=\"setMenu(this,'" + dr["ApplicationMenuID"].ToString() + "','ParentID');\" style=\"width:140px;\">\r\n");
                sb.Append("<option value=\"NULL\">无</option>\r\n");
                sb.Append(MenuItems.Select("ParentID is null").toDataTable().Select("len(mc)>0 and ApplicationID='" + dr["ApplicationID"].ToString() + "' and ApplicationMenuID<>'" + dr["ApplicationMenuID"].ToString() + "'", "px,addtime").toDataTable().toOptions("ApplicationMenuID", "MC", dr["ParentID"].ToString()));
                sb.Append("<select>");
                sb.Append("</td>\r\n");

                sb.Append("<td>");
                sb.Append("<input class=\"text\" type=\"text\" value=\"" + dr["condDisable"].ToString() + "\" oldvalue=\"" + dr["condDisable"].ToString() + "\" ondblclick=\"dblClick(this,'" + "禁用条件');\" onBlur=\"setMenu(this,'" + dr["ApplicationMenuID"].ToString() + "','condDisable');\">");
                sb.Append("</td>");
               // sb.Append("<td><input class=\"text\" type=\"text\" value=\"" + dr["url"].ToString() + "\" oldvalue=\"" + dr["url"].ToString() + "\"  style=\"width:330px;\" onBlur=\"setMenu(this,'" + dr["ApplicationMenuID"].ToString() + "','url');\" /></td>\r\n");

                sb.Append("<td><textarea class=\"text\" oldvalue=\"" + dr["url"].ToString() + "\"  style=\"width:335px;max-width:335px;\" ondblclick=\"dblClick(this,'" + "URL');\" onBlur=\"setMenu(this,'" + dr["ApplicationMenuID"].ToString() + "','url');\" >" + dr["url"].ToString() + "</textarea></td>\r\n");//sb.Append("<td>图标</td>\r\n");
                sb.Append("<td>");
                sb.Append("<input class=\"text\" type=\"text\" value=\"" + dr["icon"].ToString() + "\" oldvalue=\"" + dr["icon"].ToString() + "\"  style=\"display:none;width:140px;\" onBlur=\"setMenu(this,'" + dr["ApplicationMenuID"].ToString() + "','icon');\" />");
                sb.Append("<img src=\"../" + dr["icon"].ToString() + "\" style=\"min-width:20px;min-height:20px;max-width:24px;max-height:24px;vertical-align:middle;margin-right:6px;\" onerror=\"this.src='../images/none.gif';\">");
                sb.Append("<a href=\"javascript:;\" onclick=\"openIconForm(this,'','upload');\">选择</a>");
                sb.Append("</td>\r\n");
                sb.Append("<td>");
                sb.Append("<input class=\"text\" type=\"text\" value=\"" + dr["IconActive"].ToString() + "\" oldvalue=\"" + dr["IconActive"].ToString() + "\"  style=\"display:none;width:140px;\" onBlur=\"setMenu(this,'" + dr["ApplicationMenuID"].ToString() + "','IconActive');\" />");
                sb.Append("<img src=\"../" + dr["IconActive"].ToString() + "\" style=\"min-width:20px;min-height:20px;max-width:24px;max-height:24px;vertical-align:middle;margin-right:6px;\" onerror=\"this.src='../images/none.gif';\">");
                sb.Append("<a href=\"javascript:;\" onclick=\"openIconForm(this,'','upload');\">选择</a>");
                sb.Append("</td>\r\n");
                //字体图标
                sb.Append("<td>");
                sb.Append("<input class=\"text\" type=\"text\" value=\"" + dr["IconHTML"].ToString().HtmlEncode() + "\" oldvalue=\"" + dr["IconHTML"].ToString().HtmlEncode() + "\"  style=\"display:none;\" onBlur=\"setMenu(this,'" + dr["ApplicationMenuID"].ToString() + "','iconhtml');\" />");
                sb.Append("<span id=\"spanico\" style=\"font-size:18px;margin-right:6px;\">" + dr["IconHTML"].ToString() + "</span>");
                sb.Append("<a href=\"javascript:;\" onclick=\"SelectICO(this);\">选择</a>");
                sb.Append("</td>\r\n");
                sb.Append("<td style=\"cursor:move;\">&nbsp;</td>\r\n"); // dr["px"].ToString() 
                sb.Append("</tr>\r\n");

                if (dr["ParentID"].ToString().Length > 0) continue;
                getMenuItems(sb, dr["ApplicationMenuID"].ToString(), level + 1);
            }
        }
        /// <summary>
        /// 移动端菜单
        /// </summary>
        /// <param name="sb"></param>
        private void getMenuItems(StringBuilder sb)
        {

            sb.Append("<div style=\"margin:6px;line-height:23px;\"><b>移动端底部菜单</b></div>\r\n");
            sb.Append("<table id=\"eDataTable_Menus\" class=\"eDataTable\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" widt5h=\"100%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr bgcolor=\"#f2f2f2\">\r\n");
            sb.Append("<td height=\"25\" width=\"30\" align=\"center\"><a title=\"添加菜单\" href=\"javascript:;\" onclick=\"addMenu(this);\"><img width=\"16\" height=\"16\" src=\"images/add.png\" border=\"0\"></a></td>\r\n");
            sb.Append("<td width=\"40\">显示</td>\r\n");
            sb.Append("<td width=\"250\">名称</td>\r\n");
            sb.Append("<td width=\"150\">上级</td>\r\n");
            sb.Append("<td width=\"80\">禁用条件</td>\r\n");
            sb.Append("<td width=\"250\">URL</td>\r\n");
            sb.Append("<td width=\"80\">图标</td>\r\n");
            sb.Append("<td width=\"80\">活动图标</td>\r\n");
            sb.Append("<td width=\"80\">字体图标</td>\r\n");
            sb.Append("<td width=\"60\">顺序</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody eSize=\"false\" eMove=\"true\">\r\n");
            getMenuItems(sb, "", 0);

            sb.Append("</tbody>\r\n");
            sb.Append("</table>\r\n");
        }

        /// <summary>
        /// 移动端首页图标
        /// </summary>
        private void getIconItems(StringBuilder sb)
        {
            string sql = "update a_eke_sysApplicationIcons set PX=(";
            sql += "select b.rownum from ";
            sql += "(";
            sql += "select ROW_NUMBER() over(order by px,addtime) as rownum,ApplicationIconID,addTime from a_eke_sysApplicationIcons where delTag=0 and ApplicationID='" + AppId + "'";
            sql += ") as b where b.ApplicationIconID=a_eke_sysApplicationIcons.ApplicationIconID";
            sql += ")  where delTag=0 and ApplicationID='" + AppId + "'";
            eBase.DataBase.Execute(sql);

            sb.Append("<div style=\"margin:6px;line-height:23px;\"><b>移动端首页图标</b></div>\r\n");
            sb.Append("<table id=\"eDataTable_Icons\" class=\"eDataTable\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" widt5h=\"100%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr bgcolor=\"#f2f2f2\">\r\n");
            sb.Append("<td height=\"25\" width=\"30\" align=\"center\"><a title=\"添加图标\" href=\"javascript:;\" onclick=\"add_Icon(this);\"><img width=\"16\" height=\"16\" src=\"images/add.png\" border=\"0\"></a></td>\r\n");
            sb.Append("<td width=\"40\">显示</td>\r\n");
            sb.Append("<td width=\"250\">名称</td>\r\n");
            sb.Append("<td width=\"80\">禁用条件</td>\r\n");
            sb.Append("<td width=\"350\">URL</td>\r\n");
            sb.Append("<td width=\"80\">图标</td>\r\n");

            sb.Append("<td width=\"60\">顺序</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody eSize=\"false\" eMove=\"true\">\r\n");

            #region body
            DataTable dt = eBase.DataBase.getDataTable("select * from a_eke_sysApplicationIcons where ApplicationID='" + AppId + "' and delTag=0 order by px,addtime");
            foreach (DataRow dr in dt.Rows)
            {



                sb.Append("<tr erowid=\"" + dr["ApplicationIconID"].ToString() + "\" parentid=\"\">\r\n");
                sb.Append("<td height=\"26\" align=\"center\"><a title=\"删除图标\" href=\"javascript:;\" onclick=\"del_Icon(this,'" + dr["ApplicationIconID"].ToString() + "');\"><img width=\"16\" height=\"16\" src=\"images/del.png\" border=\"0\"></a></td>\r\n");
                sb.Append("<td><input reload=\"true\" type=\"checkbox\" onclick=\"set_Icon(this,'" + dr["ApplicationIconID"].ToString() + "','show');\"" + (eBase.parseBool(dr["show"]) ? " checked" : "") + " /></td>\r\n");

                sb.Append("<td>");
                sb.Append("<input reload=\"true\" class=\"text\" style=\"width:220px;\" type=\"text\" value=\"" + dr["MC"].ToString().HtmlEncode() + "\" oldvalue=\"" + dr["MC"].ToString().HtmlEncode() + "\" onBlur=\"set_Icon(this,'" + dr["ApplicationIconID"].ToString() + "','mc');\">");
                sb.Append("</td>\r\n");

                sb.Append("<td>");
                sb.Append("<input class=\"text\" type=\"text\" value=\"" + dr["condDisable"].ToString() + "\" oldvalue=\"" + dr["condDisable"].ToString() + "\" ondblclick=\"dblClick(this,'" + "禁用条件');\" onBlur=\"set_Icon(this,'" + dr["ApplicationIconID"].ToString() + "','condDisable');\">");
                sb.Append("</td>");

                sb.Append("<td><textarea class=\"text\" oldvalue=\"" + dr["url"].ToString() + "\"  style=\"width:335px;max-width:335px;\" ondblclick=\"dblClick(this,'" + "URL');\" onBlur=\"set_Icon(this,'" + dr["ApplicationIconID"].ToString() + "','url');\" >" + dr["url"].ToString() + "</textarea></td>\r\n");//sb.Append("<td>图标</td>\r\n");
                sb.Append("<td>");
                sb.Append("<input class=\"text\" type=\"text\" value=\"" + dr["icon"].ToString() + "\" oldvalue=\"" + dr["icon"].ToString() + "\" style=\"display:none;width:140px;\" onBlur=\"set_Icon(this,'" + dr["ApplicationIconID"].ToString() + "','icon');\" />");
                sb.Append("<img src=\"../" + dr["icon"].ToString() + "\" style=\"min-width:20px;min-height:20px;max-width:24px;max-height:24px;vertical-align:middle;margin-right:6px;\" onerror=\"this.src='../images/none.gif';\">");
                sb.Append("<a href=\"javascript:;\" onclick=\"openIconForm(this,'','upload');\">选择</a>");
                sb.Append("</td>\r\n");

              
                sb.Append("<td style=\"cursor:move;\">&nbsp;</td>\r\n"); // dr["px"].ToString() 
                sb.Append("</tr>\r\n");

            }
            #endregion

            sb.Append("</tbody>\r\n");
            sb.Append("</table>\r\n");
        }
        private DataRow _applicationinfo;
        public DataRow ApplicationInfo
        {
            get
            {
                if (_applicationinfo == null)
                {
                    DataTable dt = eBase.DataBase.getDataTable("select * from a_eke_sysApplications where ApplicationID='" + AppId + "'");
                    if (dt.Rows.Count > 0) _applicationinfo = dt.Rows[0];
                }
                return _applicationinfo;
            }
        }
        public eUser user;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser("Manage");

            //appName = eBase.DataBase.getValue("select mc from a_eke_sysApplications where ApplicationID='" + AppId + "'");
            appName = ApplicationInfo["mc"].ToString() ;
            string sql = "";
            string act = eParameters.QueryString("act").ToLower();
            StringBuilder sb = new StringBuilder();
            if (act.Length == 0)
            {
                #region 功能清单
                getAppItems(sb);

                if (ApplicationInfo["Type"].ToString() == "2" || ApplicationInfo["Type"].ToString() == "3")
                {
                    getMenuItems(sb);
                    getIconItems(sb);
                }
                litBody.Text = sb.ToString();
                #endregion
            }
            else
            {
                string ApplicationMenuID = eParameters.QueryString("ApplicationMenuID");
                string ApplicationItemID = eParameters.QueryString("ApplicationItemID");
                string ApplicationIconID = eParameters.QueryString("ApplicationIconID");
                
                string value = eParameters.Request("value").Replace("'", "''");
                string item = eParameters.QueryString("item").ToLower();
                #region 获取数据
                if (act == "getdata")
                {
                    Response.Clear();
                    getAppItems(sb);
                    if (ApplicationInfo["Type"].ToString() == "2" || ApplicationInfo["Type"].ToString() == "3")
                    {
                        getMenuItems(sb);
                        getIconItems(sb);
                    }
                    Response.Write(sb.ToString());
                    Response.End();
                }
                #endregion

                #region 图标
                #region 添加图标
                if (act == "addicon")
                {
                    eBase.DataBase.Execute("insert into a_eke_sysApplicationIcons (ApplicationIconID,ApplicationID) values ('" + Guid.NewGuid().ToString() + "','" + AppId + "')");
                    runtimeCache.Remove();
                    eResult.Success("添加成功!");
                }
                #endregion
                #region 修改图标
                if (act == "seticon")
                {
                    sql = "update a_eke_sysApplicationIcons set " + item + "='" + value + "' where ApplicationIconID='" + ApplicationIconID + "'";
                    if (value == "NULL") sql = "update a_eke_sysApplicationIcons set " + item + "=" + value + " where ApplicationIconID='" + ApplicationIconID + "'";
                    eBase.DataBase.Execute(sql);
                    eBase.clearDataCache("a_eke_sysApplicationIcons");
                    runtimeCache.Remove();
                    eResult.Success("修改成功!");
                }
                #endregion
                #region  删除图标
                if (act == "delicon")
                {
                    eBase.DataBase.Execute("delete from a_eke_sysApplicationIcons where ApplicationID='" + AppId + "' and ApplicationIconID='" + ApplicationIconID + "'");
                    eBase.clearDataCache("a_eke_sysApplicationIcons");
                    runtimeCache.Remove();
                    eResult.Success("删除成功!");
                }
                #endregion
                #region 图标拖动排序
                if (act == "seticonorders")
                {

                    string ids = eParameters.Form("ids");
                    string[] arr = ids.Split(",".ToCharArray());
                    for (int i = 0; i < arr.Length; i++)
                    {
                        value = (i + 1).ToString();
                        eBase.DataBase.Execute("update a_eke_sysApplicationIcons set PX='" + value + "' where ApplicationID='" + AppId + "' and ApplicationIconID='" + arr[i] + "'");
                    }

                    eBase.clearDataCache("a_eke_sysApplicationIcons");
                    runtimeCache.Remove();
                    eResult.Success("排序成功!");
                }
                #endregion
                #endregion

                #region 菜单
                #region 添加菜单
                if (act == "addmenu")
                {
                    eBase.DataBase.Execute("insert into a_eke_sysApplicationMenus (ApplicationMenuID,ApplicationID) values ('" + Guid.NewGuid().ToString() + "','" + AppId + "')");
                    runtimeCache.Remove();
                    eResult.Success("添加成功!");
                }
                #endregion
                #region 修改菜单
                if (act == "setmenu")
                {
                    sql = "update a_eke_sysApplicationMenus set " + item + "='" + value + "' where ApplicationMenuID='" + ApplicationMenuID + "'";
                    if (value == "NULL") sql = "update a_eke_sysApplicationMenus set " + item + "=" + value + " where ApplicationMenuID='" + ApplicationMenuID + "'";
                    eBase.DataBase.Execute(sql);
                    eBase.clearDataCache("a_eke_sysApplicationMenus");
                    runtimeCache.Remove();
                    eResult.Success("修改成功!");
                }
                #endregion
                #region  删除菜单
                if (act == "delmenu")
                {
                    eBase.DataBase.Execute("delete from a_eke_sysApplicationMenus where ApplicationID='" + AppId + "' and ApplicationMenuID='" + ApplicationMenuID + "'");
                    eBase.clearDataCache("a_eke_sysApplicationMenus");
                    runtimeCache.Remove();
                    eResult.Success("删除成功!");
                }
                #endregion
                #region 菜单拖动排序
                if (act == "setmenuorders")
                {

                    string ids = eParameters.Form("ids");
                    string[] arr = ids.Split(",".ToCharArray());
                    for (int i = 0; i < arr.Length; i++)
                    {
                        value = (i + 1).ToString();
                        eBase.DataBase.Execute("update a_eke_sysApplicationMenus set PX='" + value + "' where ApplicationID='" + AppId + "' and ApplicationMenuID='" + arr[i] + "'");
                    }

                    if (Request.QueryString["parentid"] != null)
                    {
                        string parentid = eParameters.QueryString("parentid");
                        string oldpid = eBase.DataBase.getValue("select parentid from a_eke_sysApplicationMenus where ApplicationMenuID='" + eParameters.QueryString("AppMenu") + "'");
                        eBase.DataBase.Execute("update a_eke_sysApplicationMenus set parentid=" + (parentid.Length == 0 ? "NULL" : "'" + parentid + "'") + " where ApplicationMenuID='" + eParameters.QueryString("AppMenu") + "'");
                        updatePX(oldpid);
                    }
                    eBase.clearDataCache("a_eke_sysApplicationMenus");
                    runtimeCache.Remove();
                    eResult.Success("排序成功!");
                }
                 #endregion
                #endregion

                #region 模块
                #region 添加模块
                if (act == "addapp")
                {
                    eBase.DataBase.Execute("insert into a_eke_sysApplicationItems (ApplicationItemID,ApplicationID) values ('" + Guid.NewGuid().ToString() + "','" + AppId + "')");
                    eResult.Success("添加成功!");
                }
                #endregion
                #region 修改模块
                if (act == "setapp")
                {
                    if (item == "px" && value == "0") value = "999999";                   
                    #region 清除原来模块绑定的权限
                    if (item.ToLower() == "modelid")
                    {
                        string oldmodel = eBase.DataBase.getValue("select ModelID from a_eke_sysApplicationItems where ApplicationItemID='" + ApplicationItemID + "'");
                        if (oldmodel.Length > 0)
                        {
                            sql = "delete from a_eke_sysPowers where ApplicationItemID='" + ApplicationItemID + "'";
                            eBase.DataBase.Execute(sql);
                        }
                    }
                    #endregion

                    sql = "update a_eke_sysApplicationItems set " + item + "='" + value + "' where ApplicationItemID='" + ApplicationItemID + "'";
                    if (value == "NULL") sql = "update a_eke_sysApplicationItems set " + item + "=" + value + " where ApplicationItemID='" + ApplicationItemID + "'";
                    eBase.DataBase.Execute(sql);
                    if (item.ToLower() == "modelid") //模块名称设置
                    {                       
                        sql = "update a_eke_sysApplicationItems set MC=(select mc from a_eke_sysModels where modelid='" + value + "') where ApplicationItemID='" + ApplicationItemID + "' and (len(mc)=0 or mc is null)";//len(isnull(mc,''))
                        eBase.DataBase.Execute(sql);
                    }
                    runtimeCache.Remove();
                    eResult.Success("修改成功!");
                }
                #endregion
                #region  删除模块
                if (act == "delapp")
                {
                    eBase.DataBase.Execute("delete from a_eke_sysApplicationItems where ApplicationID='" + AppId + "' and ApplicationItemID='" + ApplicationItemID + "'");
                    runtimeCache.Remove();
                    eBase.clearDataCache("a_eke_sysApplicationItems");
                    eResult.Success("删除成功!");
                }
                #endregion
                #region  模块拖动排序
                if (act == "setorders")
                {
                  
                    string ids = eParameters.Form("ids");
                    string[] arr = ids.Split(",".ToCharArray());
                    for (int i = 0; i < arr.Length; i++)
                    {
                        value = (i + 1).ToString();
                        eBase.DataBase.Execute("update a_eke_sysApplicationItems set PX='" + value + "' where ApplicationID='" + AppId + "' and ApplicationItemID='" + arr[i] + "'");
                    }

                    if (Request.QueryString["parentid"]!=null)
                    {
                        string parentid = eParameters.QueryString("parentid");
                        string oldpid = eBase.DataBase.getValue("select parentid from a_eke_sysApplicationItems where ApplicationItemID='" + eParameters.QueryString("appitem") + "'");
                        eBase.DataBase.Execute("update a_eke_sysApplicationItems set parentid=" + (parentid.Length == 0 ? "NULL" : "'" + parentid + "'") + " where ApplicationItemID='" + eParameters.QueryString("appitem") + "' and '" + parentid + "'!='" + eParameters.QueryString("appitem") + "'");
                        updatePX(oldpid);
                    }
                    runtimeCache.Remove();
                    eBase.clearDataCache("a_eke_sysApplicationItems");
                    eResult.Success("排序成功!");
                }
                #endregion
                #endregion
                
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (Master == null) return;
            Literal lit = (Literal)Master.FindControl("LitTitle");
            if (lit != null)
            {
                lit.Text = "应用管理 - " + eConfig.manageName() ;
            }
        }
    }
}