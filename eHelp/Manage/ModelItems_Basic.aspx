﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="true" CodeFile="ModelItems_Basic.aspx.cs" Inherits="eFrameWork.Manage.ModelItems_Basic" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
   <title><%=eConfig.manageName() %></title>
</head>
<body>
<asp:Repeater id="Rep" runat="server" >
<headertemplate>
<%#
"<table id=\"eDataTable_Basic\" class=\"eDataTable\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" width=\"99%\" style=\"min-width:1650px;\">" +
"<thead>" +
"<tr>" +
"<td width=\"110\">模块</td>" +
"<td width=\"90\">"+
""+
"显示" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(66);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"150\">名称"  + "</td>" +
//"<td width=\"60\">主键</td>" +

"<td width=\"260\">输出控件" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(68);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"80\">禁用条件</td>" +
"<td width=\"80\">合并</td>" +
"<td width=\"80\">默认值" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(74);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"80\">格式化" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(75);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +

"<td width=\"85\">表单Name" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(69);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"70\">表单ID" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(70);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"70\" title=\"电脑端宽度\">宽(PC)" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(71);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"60\" title=\"手机端宽度\">宽(M)" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(72);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"60\">高" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(73);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"40\">颜色" + "</td>" +
"<td width=\"60\">占位符</td>" +

"<td width=\"60\">单位" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(76);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"80\">提示" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(77);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"60\">跨行" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(79);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"60\">跨列" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(80);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"70\">分格符" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(82);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"150\">自定义</td>" +
"<td width=\"60\">顺序" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(83);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"</tr>" +
"</thead>"+
"<tbody eSize=\"false\" eMove=\"true\">"
%>
</headertemplate>
<itemtemplate>
<%#
"<tr" + ((Container.ItemIndex + 1) % 2 == 0 ? " class=\"alternating\" eclass=\"alternating\"" : " eclass=\"\"") + " erowid=\"" + Eval("ModelItemID") + "\" onmouseover=\"this.className='cur';\" onmouseout=\"this.className=this.getAttribute('eclass');\" >" +
"<td height=\"32\" style=\"overflow:visible;white-space:normal;\" title=\"" + Eval("ModelName") + "\">" + Eval("ModelName") + "</td>"+
"<td class=\"tdshowedit\">"+
"<label><input class=\"showedit\" reloadbak=\"true\" type=\"checkbox\" onclick=\"setModelItem(this,'" + Eval("ModelItemID") + "','showadd');\"" + (eBase.parseBool(Eval("showadd")) ? " checked" : "") + ">添加</label>"+
"<br><label><input class=\"showedit\" type=\"checkbox\" onclick=\"setModelItem(this,'" + Eval("ModelItemID") + "','showedit');\"" + (eBase.parseBool(Eval("showedit")) ? " checked" : "") + ">编辑</label>"+
"<br><label><input class=\"showedit\" type=\"checkbox\" onclick=\"setModelItem(this,'" + Eval("ModelItemID") + "','showview');\"" + (eBase.parseBool(Eval("showview")) ? " checked" : "") + ">查看</label>"+

"</td>" +
"<td>编码：" + Eval("Code") + "<br>" +
"列名：<input class=\"text\" style=\"width:80px;\" type=\"text\" oldvalue=\""+ Eval("MC") + "\" value=\""+ Eval("MC") + "\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','mc');\" />" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(67);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "<br>" + 
"</td>" +

"<td><select reload=\"true\" onChange=\"setModelItem(this,'" + Eval("ModelItemID") + "','controltype');\" sty3le=\"width:100px;\">" + eBase.getControlType(Container.DataItem) + "<select>" + //Eval("controltype").ToString()
"<span style=\"width:100px;"+(Eval("controltype").ToString()=="password" ? "" : "display:none;")+"\">&nbsp;强度：<select onChange=\"setModelItem(this,'" + Eval("ModelItemID") + "','minRank');\">" +
"<option value=\"0\">无限制</option>" +
"<option value=\"5\"" + (Eval("minRank").ToString()=="5" ? " selected=\"true\"" : "") + ">弱</option>" +
"<option value=\"10\"" + (Eval("minRank").ToString()=="10" ? " selected=\"true\"" : "") + ">中</option>" +
"<option value=\"20\"" + (Eval("minRank").ToString()=="20" ? " selected=\"true\"" : "") + ">强</option>" +
"<select></span>"+
"<label style=\""+(Eval("controltype").ToString()=="textarea" || Eval("controltype").ToString()=="text" || Eval("controltype").ToString()=="select" || Eval("controltype").ToString()=="radio" || Eval("controltype").ToString()=="checkbox" || 1==1 ? "display:block;" : "display:none;")+"\"><input reload=\"false\" type=\"checkbox\" oldvalue=\""+ Eval("EditReadOnly").ToString().Replace("True","1").Replace("False","0") + "\" value=\""+ Eval("EditReadOnly").ToString().Replace("True","1").Replace("False","0") + "\" onclick=\"setModelItem(this,'" + Eval("ModelItemID") + "','editreadonly');\"" + (eBase.parseBool(Eval("EditReadOnly")) ? " checked" : "") + ">编辑只读</label>" +
"<label style=\""+(Eval("controltype").ToString()=="textarea" ? "display:block;" : "display:none;")+"\"><input reload=\"false\" type=\"checkbox\" oldvalue=\""+ Eval("ReplaceLine").ToString().Replace("True","1").Replace("False","0") + "\" value=\""+ Eval("ReplaceLine").ToString().Replace("True","1").Replace("False","0") + "\" onclick=\"setModelItem(this,'" + Eval("ModelItemID") + "','replaceline');\"" + (eBase.parseBool( Eval("ReplaceLine")) ? " checked" : "") + " />替换换行</label>" +
"<label style=\""+(Eval("controltype").ToString()=="text" ? "display:block;" : "display:none;")+"\"><input reload=\"false\" type=\"checkbox\" oldvalue=\""+ Eval("MakePinyin").ToString().Replace("True","1").Replace("False","0") + "\" value=\""+ Eval("MakePinyin").ToString().Replace("True","1").Replace("False","0") + "\" onclick=\"setModelItem(this,'" + Eval("ModelItemID") + "','MakePinyin');\"" + ( eBase.parseBool( Eval("MakePinyin")) ? " checked" : "") + " />生成拼音</label>" +
"<label style=\"display:block;\"><input reload=\"false\" type=\"checkbox\" oldvalue=\""+ Eval("ReplaceData").ToString().Replace("True","1").Replace("False","0") + "\" value=\""+ Eval("ReplaceData").ToString().Replace("True","1").Replace("False","0") + "\" onclick=\"setModelItem(this,'" + Eval("ModelItemID") + "','replacedata');\"" + (eBase.parseBool( Eval("ReplaceData"))   ? " checked" : "") + ">替换变量</label>" +
"<span style=\""+(",autoselect,loopselect,treecheckbox,treeradio,departmentselect,".IndexOf("," + Eval("controltype").ToString() + ",")>-1 ? "display:block;" : "display:none;")+"\">&nbsp;显示层级：<input type=\"text\" oldvalue=\""+ Eval("showLevel") + "\" value=\""+ Eval("showLevel") + "\" class=\"edit\" style=\"width:40px;\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','showLevel');\"></span>" +
"<label style=\""+(Eval("Type").ToString().ToLower().IndexOf("char")>-1 && (Eval("controltype").ToString()=="text" || Eval("controltype").ToString()=="textarea") ? "display:block;" : "display:none;")+"\"><input reload=\"false\" type=\"checkbox\" oldvalue=\""+ Eval("FullWidth").ToString().Replace("True","1").Replace("False","0") + "\" value=\""+ Eval("FullWidth").ToString().Replace("True","1").Replace("False","0") + "\" onclick=\"setModelItem(this,'" + Eval("ModelItemID") + "','FullWidth');\"" + (eBase.parseBool( Eval("FullWidth"))   ? " checked" : "") + ">允许全角</label>" +
"<label style=\""+(Eval("Type").ToString().ToLower().IndexOf("char")>-1 && (Eval("controltype").ToString()=="singleuser" || Eval("controltype").ToString()=="multipleuser") ? "display:block;" : "display:none;")+"\"><input reload=\"false\" type=\"checkbox\" oldvalue=\""+ Eval("ViewSign").ToString().Replace("True","1").Replace("False","0") + "\" value=\""+ Eval("ViewSign").ToString().Replace("True","1").Replace("False","0") + "\" onclick=\"setModelItem(this,'" + Eval("ModelItemID") + "','ViewSign');\"" + (eBase.parseBool( Eval("ViewSign"))   ? " checked" : "") + ">会签</label>" +

"<span style=\"width:100px;"+(Eval("controltype").ToString()=="radio" ? "display:block;" : "display:none;")+"\">" + 
"左侧可选：<input type=\"text\" oldvalue=\""+ Eval("RadioRuleLeft") + "\" value=\""+ Eval("RadioRuleLeft") + "\" class=\"edit\" style=\"width:40px;\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','RadioRuleLeft');\">&nbsp;" +
"右侧可选：<input type=\"text\" oldvalue=\""+ Eval("RadioRuleRight") + "\" value=\""+ Eval("RadioRuleRight") + "\" class=\"edit\" style=\"width:40px;\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','RadioRuleRight');\">" + 
"</span>"+
"<span style=\"width:200px;"+(Eval("controltype").ToString()=="radio" || Eval("controltype").ToString()=="checkbox" || Eval("controltype").ToString()=="select" ? "display:block;" : "display:none;")+"\">" + 
"自定义可输入值的项：<input style=\"width:100px;\" class=\"text\" type=\"text\" oldvalue=\""+ Eval("InputValue") + "\" value=\""+ Eval("InputValue") + "\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','inputvalue');\" />" + 
"</span>"+
"<span style=\"width:100px;"+(Eval("controltype").ToString()=="date" ? "display:block;" : "display:none;")+"\"><select onChange=\"setModelItem(this,'" + Eval("ModelItemID") + "','dateformat');\">" +
"<option value=\"\">无</option>" +
"<option value=\"yyyy-MM\"" + (Eval("dateformat").ToString()=="yyyy-MM" ? " selected=\"true\"" : "") + ">yyyy-MM</option>" +
"<option value=\"yyyy-MM-dd\"" + (Eval("dateformat").ToString()=="yyyy-MM-dd" ? " selected=\"true\"" : "") + ">yyyy-MM-dd</option>" +
"<option value=\"yyyy-MM-dd HH\"" + (Eval("dateformat").ToString()=="yyyy-MM-dd HH" ? " selected=\"true\"" : "") + ">yyyy-MM-dd HH</option>" +
"<option value=\"yyyy-MM-dd HH:mm\"" + (Eval("dateformat").ToString()=="yyyy-MM-dd HH:mm" ? " selected=\"true\"" : "") + ">yyyy-MM-dd HH:mm</option>" +
"<option value=\"yyyy-MM-dd HH:mm:ss\"" + (Eval("dateformat").ToString()=="yyyy-MM-dd HH:mm:ss" ? " selected=\"true\"" : "") + ">yyyy-MM-dd HH:mm:ss</option>" +
"<select></span>"+
((Eval("controltype").ToString()=="file" || Eval("controltype").ToString()=="html" || Eval("controltype").ToString()=="image" || Eval("controltype").ToString()=="images" || Eval("controltype").ToString()=="filelist" || Eval("controltype").ToString()=="eediter") ? "<br>图片宽度:" +
"<input type=\"text\" oldvalue=\""+ Eval("PictureMaxWidth") + "\" value=\""+ Eval("PictureMaxWidth") + "\" class=\"edit\" style=\"width:50px;\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','picturemaxwidth');\">" : "") +
( Eval("controltype").ToString()=="images" ? "<br>缩略图:" + 
"<input id=\"thumbs_" +   Eval("ModelItemID").ToString().Replace("-","") + "\"  jsonformat=\"[{&quot;text&quot;:&quot;宽度&quot;,&quot;value&quot;:&quot;Width&quot;},{&quot;text&quot;:&quot;品质&quot;,&quot;value&quot;:&quot;Quality&quot;},{&quot;text&quot;:&quot;文件名后缀&quot;,&quot;value&quot;:&quot;Ext&quot;}]\" type=\"text\" oldvalue=\""+ HttpUtility.HtmlEncode(Eval("Thumbs").ToString()) + "\" value=\""+ HttpUtility.HtmlEncode(Eval("Thumbs").ToString()) + "\" class=\"edit\" style=\"width:150px;display:none;\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','thumbs');\">" +
getJsonText(Eval("Thumbs").ToString(),"Width") +
"<img src=\"images/jsonedit.png\" align=\"absmiddle\" style=\"cursor:pointer;\" onclick=\"Json_Edit('thumbs_" +  Eval("ModelItemID").ToString().Replace("-","") + "');\">" + 
"" : "") + 
"<br><select reload=\"true\" onChange=\"setModelItem(this,'" + Eval("ModelItemID") + "','fillmodelid');\" style=\"width:110px;"+(Eval("controltype").ToString()=="datatext" ? "" : "display:none;")+"\">"+
"<option value=\"NULL\">无</option>" +
allModels.Select("*","Type=3 and (Convert(ParentID, 'System.String')='" + modelid + "' or ParentID is null)","").toOptions("ModelID","MC",Eval("FillModelID").ToString()) +
// eOleDB.getOptions("select MC,ModelID from a_eke_sysModels where delTag=0 and Type=3 and ParentID='" + modelid + "'", "MC", "ModelID", Eval("FillModelID").ToString()) + 
"<select>"+
(Eval("FillModelID").ToString().Length > 10  ? "&nbsp;<a href=\"javascript:;\" onclick=\"openFillwin('" + modelid + "','" + Eval("FillModelID").ToString() + "');\">对应关系</a>": "") +
"</td>" +
"<td><input class=\"text\" type=\"text\" title=\"禁用条件\" oldvalue=\""+ Eval("conddisable") + "\" value=\""+ Eval("conddisable") + "\" ondblclick=\"dblClick(this,'" + Eval("MC") + "-禁用条件');\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','conddisable');\"></td>"+
"<td><select reload=\"true\" onChange=\"setModelItem(this,'" + Eval("ModelItemID") + "','PackID');\" style=\"width:70px;\">"+
"<option value=\"NULL\">无</option>" +
CustomColumns.Select("*","","").toOptions("ModelItemID","MC",Eval("PackID").ToString()) +
"<select>"+
"</td>"+

"<td><input class=\"text\" type=\"text\" title=\"默认值&#10&nbsp;1.固定值&#10&nbsp;2.{user:key}&#10&nbsp;3.{querystring:key}\" oldvalue=\""+ Eval("defaultvalue") + "\" value=\""+ Eval("defaultvalue") + "\" ondblclick=\"dblClick(this,'" + Eval("MC") + "-默认值');\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','defaultvalue');\"></td>" +
"<td><input class=\"text\" type=\"text\" title=\"Asp.Net格式化字符串\" oldvalue=\""+ Eval("formatstring") + "\" value=\""+ Eval("formatstring") + "\" ondblclick=\"dblClick(this,'" + Eval("MC") + "-格式化');\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','formatstring');\"></td>" +

"<td><input class=\"text\" type=\"text\" oldvalue=\""+ Eval("frmname") + "\" value=\""+ Eval("frmname") + "\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','frmname');\"></td>" +
"<td><input class=\"text\" type=\"text\" oldvalue=\""+ Eval("frmid") + "\" value=\""+ Eval("frmid") + "\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','frmid');\"></td>" +
"<td><input class=\"text\" type=\"text\" title=\"PC端的宽度\" oldvalue=\""+ Eval("width") + "\" value=\""+ Eval("width") + "\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','width');\"></td>" +
"<td><input class=\"text\" type=\"text\" title=\"移动端的宽度\" oldvalue=\""+ Eval("mwidth") + "\" value=\""+ Eval("mwidth") + "\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','mwidth');\"></td>" +
"<td><input class=\"text\" type=\"text\" title=\"高度\" oldvalue=\""+ Eval("height") + "\" value=\""+ Eval("height") + "\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','height');\"></td>" +
"<td><input class=\"text\" id=\"color_" +  Eval("ModelItemID").ToString().Replace("-","_") + "\" type=\"hidden\" title=\"颜色\" oldvalue=\""+ (Eval("color").ToString().Length==7 ? Eval("color").ToString() : "#333333") + "\" value=\""+ (Eval("color").ToString().Length==7 ? Eval("color").ToString() : "#333333") + "\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','color');\">"+
"<span id=\"span_color_" +  Eval("ModelItemID").ToString().Replace("-","_") + "\" class=\"ecolor\" style=\"cursor:pointer;background-color:" +  (Eval("color").ToString().Length==7 ? Eval("color").ToString() : "#333333") + ";\" onclick=\"showColor('color_" +  Eval("ModelItemID").ToString().Replace("-","_") + "');\">&nbsp;</span>" +
"</td>" +
"<td><input class=\"text\" type=\"text\" title=\"占位符\" oldvalue=\""+ Eval("PlaceHolder") + "\" value=\""+ Eval("PlaceHolder") + "\" ondblclick=\"dblClick(this,'" + Eval("MC") + "-占位符');\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','PlaceHolder');\"></td>" +

"<td><input class=\"text\" type=\"text\" title=\"控件后显示\" oldvalue=\""+ Eval("dw") + "\" value=\""+ Eval("dw") + "\" ondblclick=\"dblClick(this,'" + Eval("MC") + "-单位');\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','dw');\"></td>" +
"<td>"+
"<input class=\"text\" type=\"text\" title=\"鼠标进入时的提示\" oldvalue=\""+ Eval("tip") + "\" value=\""+ Eval("tip") + "\" ondblclick=\"dblClick(this,'" + Eval("MC") + "-提示');\"  onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','tip');\">" + 
"<br><input class=\"text\" type=\"text\" title=\"固定显示\" oldvalue=\""+ Eval("Caption") + "\" value=\""+ Eval("Caption") + "\" ondblclick=\"dblClick(this,'" + Eval("MC") + "-固定提示');\"  onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','Caption');\">" + 
"</td>"+
"<td><input class=\"text\" type=\"text\" title=\"跨行\" oldvalue=\""+ Eval("addrowspan") + "\" value=\""+ Eval("addrowspan") + "\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','addrowspan');\"></td>" +
"<td><input class=\"text\" type=\"text\" title=\"跨列\" oldvalue=\""+ Eval("addcolspan") + "\" value=\""+ Eval("addcolspan") + "\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','addcolspan');\"></td>" +

"<td><input class=\"text\" type=\"text\" oldvalue=\""+ Eval("splitchar") + "\" value=\""+ Eval("splitchar") + "\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','splitchar');\"></td>" +

"<td>查看" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(78);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "<br>" +
"<input class=\"text\" type=\"text\" title=\"自定义查看状态的HTML\" oldvalue=\""+ HttpUtility.HtmlEncode(Eval("viewhtml").ToString()) + "\" value=\""+ HttpUtility.HtmlEncode(Eval("viewhtml").ToString()) + "\" ondblclick=\"dblClick(this,'" + Eval("MC") + "-自定义查看');\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','viewhtml');\"><br>" +
"HTML扩展属性" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(81);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") +  "<br>" +
"<input class=\"text\" type=\"text\" title=\"增加属性或绑定事件\" oldvalue=\""+  HttpUtility.HtmlEncode(Eval("attributes").ToString()) + "\" value=\""+ HttpUtility.HtmlEncode(Eval("attributes").ToString()) + "\" ondblclick=\"dblClick(this,'" + Eval("MC") + "-HTML扩展属性');\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','attributes');\">" + 
"</td>" +


//"<td><input class=\"text\" type=\"text\" reload=\"true\" oldvalue=\""+ (Eval("addorder").ToString()=="999999" ? "" : Eval("addorder").ToString()) + "\" value=\""+ (Eval("addorder").ToString()=="999999" ? "" : Eval("addorder").ToString()) + "\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','addorder');\"></td>"+
"<td style=\"cursor:move;\">" + (Container.ItemIndex + 1) + "</td>"+
"</tr>"
%>
</itemtemplate>
<footertemplate><%#"</tbody></table>"%></footertemplate>
</asp:Repeater>
自定义列：<br />
<asp:Repeater id="RepCustom" runat="server" >
<headertemplate>
<%#
"<table id=\"eDataTable\" class=\"eDataTable\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" widt5h=\"100%\">" +
"<thead>" +
"<tr>" +
"<td height=\"25\" width=\"30\" align=\"center\"><a title=\"添加\" href=\"javascript:;\" onclick=\"addModelItem(this);\"><img width=\"16\" height=\"16\" src=\"images/add.png\" border=\"0\"></a></td>" +
"<td width=\"150\">名称" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(150);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;\">" : "") + "</td>" +
"<td width=\"150\">自定义程序" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(151);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;\">" : "") + "</td>" +
"<td width=\"250\">加载模块" + "</td>" +
"<td width=\"60\">界面</td>" +
"<td width=\"60\">验证</td>" +
//"<td width=\"150\">自定义编码</td>" +
"</tr>" +
"</thead>"
%>
</headertemplate>
<itemtemplate>
<%#
"<tr>" +
"<td height=\"26\" align=\"center\"><a title=\"删除\" href=\"javascript:;\" onclick=\"delModelItem(this,'" + Eval("ModelItemID") + "');\"><img width=\"16\" height=\"16\" src=\"images/del.png\" border=\"0\"></a></td>" +
"<td><input class=\"text\" type=\"text\" value=\""+ HttpUtility.HtmlEncode(Eval("mc").ToString()) + "\" oldvalue=\""+  HttpUtility.HtmlEncode(Eval("mc").ToString()) + "\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','mc');\"></td>" +
"<td><input reload=\"true\" class=\"text\" type=\"text\" value=\""+ Eval("ProgrameFile").ToString() + "\" oldvalue=\""+ Eval("ProgrameFile").ToString() + "\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','programefile');\"></td>" +
"<td><select reload=\"true\" onChange=\"setModelItem(this,'" + Eval("ModelItemID") + "','bindmodelid');\" style=\"width:90%;\">"+
"<option value=\"NULL\">无</option>" +
// eOleDB.getOptions("select MC,ModelID from a_eke_sysModels where delTag=0 and Type=1 and subModel= 1 and JoinMore=1 and ParentID='" + modelid + "'", "MC", "ModelID", Eval("BindModelID").ToString()) + 
allModels.Select("*","Type=1 and subModel= 1 and JoinMore=1 and Convert(ParentID, 'System.String')='" + modelid + "'","").toOptions("ModelID","MC",Eval("BindModelID").ToString()) +
"<select></td>" +
"<td>"+
//"<input reload=\"false\" name=\"hasui_" + Eval("ModelItemID").ToString().Replace("-","") + "\" id=\"hasui_" + Eval("ModelItemID").ToString().Replace("-","") + "_1\" type=\"radio\" value=\"1\" onclick=\"setModelItem(this,'" + Eval("ModelItemID") + "','hasui');\"" + (Eval("hasui").ToString()=="True" ? " checked" : "") + " /><label for=\"hasui_" + Eval("ModelItemID").ToString().Replace("-","") + "_1\">显示</label>&nbsp;"+
//"<input reload=\"false\" name=\"hasui_" + Eval("ModelItemID").ToString().Replace("-","") + "\" id=\"hasui_" + Eval("ModelItemID").ToString().Replace("-","") + "_2\" type=\"radio\" value=\"0\" onclick=\"setModelItem(this,'" + Eval("ModelItemID") + "','hasui');\"" + (Eval("hasui").ToString()=="False" ? " checked" : "") + " /><label for=\"hasui_" + Eval("ModelItemID").ToString().Replace("-","") + "_2\">不显示</label>"+

"<input reload=\"true\" id=\"hasui_" + Eval("ModelItemID") + "\" type=\"checkbox\" onclick=\"setModelItem(this,'" + Eval("ModelItemID") + "','hasui');\"" + (eBase.parseBool( Eval("hasui")) ? " checked" : "") + " />" +

"</td>" +
//"<td><input type=\"text\" value=\""+ Eval("CustomCode").ToString() + "\" oldvalue=\""+ Eval("CustomCode").ToString() + "\" class=\"edit\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','customcode');\"></td>" +
"<td>"+
"<input reload=\"true\" id=\"Equal_" + Eval("ModelItemID") + "\" type=\"checkbox\" onclick=\"setModelItem(this,'" + Eval("ModelItemID") + "','Equal');\"" + (eBase.parseBool( Eval("Equal")) ? " checked" : "") + " />" +
"</td>" +
"</tr>"
%>
</itemtemplate>
<footertemplate><%#"</table>"%></footertemplate>
</asp:Repeater>
</body>
</html>