﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using EKETEAM.Data;
using EKETEAM.FrameWork;

namespace eFrameWork.Manage
{
    public partial class Compare : System.Web.UI.Page
    {
        string olddb = "";
        string newdb="";
        public string pattern = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            string sql = "";
            olddb = eParameters.Form("olddb");
            newdb = eParameters.Form("newdb");
            pattern = eParameters.Request("pattern");
            if (pattern.Length == 0) pattern = "1";

            if (olddb.Length == 0) olddb = eBase.DataBase.DataBase;
            StringBuilder sb = new StringBuilder();
            DataTable tb = eBase.DataBase.getDataBases();

            //eBase.PrintDataTable(tb);
            foreach (DataRow dr in tb.Rows)
            {
                sb.Append("<option value=\"" + dr["name"].ToString() + "\"" + (dr["name"].ToString().ToLower() == olddb.ToLower() ? " selected=\"true\"" : "") + ">" + dr["name"].ToString() + "</option>");
            }
            Litold.Text = sb.ToString();

            sb = new StringBuilder();

            foreach (DataRow dr in tb.Rows)
            {
                sb.Append("<option value=\"" + dr["name"].ToString() + "\"" + (dr["name"].ToString().ToLower() == newdb.ToLower() ? " selected=\"true\"" : "") + ">" + dr["name"].ToString() + "</option>");
            }
            Litnew.Text = sb.ToString();



            if (olddb.Length > 0 && newdb.Length > 0)
            {
                sb = new StringBuilder();

                sb.Append("<table width=\"600\" border=\"0\" cellspacing=\"1\" cellpadding=\"0\" bgcolor=\"#CCCCCC\" clas1s=\"eDataTable\" style=\"margin-top:10px;\">\r\n");
                sb.Append("<thead>\r\n");
                sb.Append("<tr bgcolor=\"#f2f2f2\">\r\n");
                sb.Append("<td>&nbsp;" + newdb + "</td>\r\n");
                sb.Append("<td>&nbsp;" + olddb + "</td>\r\n");
                sb.Append("</tr>\r\n");
                sb.Append("</thead>\r\n");
                sb.Append("<tbody>\r\n");
                sb.Append("<tr valign=\"top\" bgcolor=\"#ffffff\">\r\n");
                sb.Append("<td heigh1t=\"25\">\r\n");

                //DataTable tb1 = eOleDB.getTables(newdb);
                //DataTable tb2 = eOleDB.getTables(olddb);

                sql = "SELECT * FROM [" + newdb + "]..sysobjects";
                sql += " WHERE xtype = 'U' and name not in ('dtproperties','sysdiagrams')";
                sql += " and name like 'a_eke_%' ";
                sql += " order by name";
                DataTable tb1 = eBase.DataBase.getDataTable(sql);

                sql = "SELECT * FROM [" + olddb + "]..sysobjects";
                sql += " WHERE xtype = 'U' and name not in ('dtproperties','sysdiagrams')";
                sql += " and name like 'a_eke_%' ";
                sql += " order by name";
                DataTable tb2 = eBase.DataBase.getDataTable(sql);

                //eBase.Writeln(tb1.Rows.Count.ToString() + "::" + tb2.Rows.Count.ToString());
                StringBuilder sbr = new StringBuilder();
                foreach (DataRow dr in tb1.Rows)
                {
                    if (dr["name"].ToString().ToLower().IndexOf("a_eke_sys") == -1) continue;
                    DataRow[] rows = tb2.Select("name='" + dr["name"].ToString() + "'");
                    //DataTable dt1 = eOleDB.getColumns(newdb, dr["name"].ToString());
                    //DataTable dt2 = eOleDB.getColumns(olddb, dr["name"].ToString());


                    sql = " SELECT a.name as code,b.name as type,a.length,a.colid as PX,d.text as def";
                    sql += " from [" + newdb + "]..syscolumns a";
                    sql += " inner join [" + newdb + "]..systypes b on a.xtype=b.xusertype ";
                    sql += " left join [" + newdb + "]..sysobjects c on a.cdefault=c.id and a.cdefault>0";
                    sql += " left join [" + newdb + "]..syscomments d on c.id=d.id";
                    sql += " where a.id=(SELECT id from [" + newdb + "]..sysobjects where name='" + dr["name"].ToString() + "' and  xtype = 'U')";
                    sql += " order by a.colid";
                    DataTable dt1 = eBase.DataBase.getDataTable(sql);

                    sql = " SELECT a.name as code,b.name as type,a.length,a.colid as PX,d.text as def";
                    sql += " from [" + olddb + "]..syscolumns a";
                    sql += " inner join [" + olddb + "]..systypes b on a.xtype=b.xusertype ";
                    sql += " left join [" + olddb + "]..sysobjects c on a.cdefault=c.id and a.cdefault>0";
                    sql += " left join [" + olddb + "]..syscomments d on c.id=d.id";

                    sql += " where a.id=(SELECT id from [" + olddb + "]..sysobjects where name='" + dr["name"].ToString() + "' and  xtype = 'U')";
                    sql += " order by a.colid";
                    DataTable dt2 = eBase.DataBase.getDataTable(sql);

         
                    bool changed = false;
                    if (dt1.Rows.Count == dt2.Rows.Count)
                    {
                        foreach (DataRow _dr in dt1.Rows)
                        {
                            DataRow[] _rs = new DataTable().Select();
                            if (pattern == "2")//详细
                            {
                                _rs = dt2.Select("code='" + _dr["code"].ToString() + "' and type='" + _dr["type"].ToString() + "' and length='" + _dr["length"].ToString() + "'");
                            }
                            else
                            {
                                _rs = dt2.Select("code='" + _dr["code"].ToString() + "' and type='" + _dr["type"].ToString() + "'");
                            }
                            if (_rs.Length == 0)
                            {
                                changed = true;
                                break;
                            }
                            else
                            {
                                if (_dr["def"].ToString() != _rs[0]["def"].ToString() && pattern=="2")
                                {
                                    changed = true;
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        changed = true;
                    }
                    sb.Append("<div class=\"close\" onclick=\"show(this);\">" + dr["name"].ToString() + "</div>");
                    sbr.Append("<div class=\"close\"" + (rows.Length == 0 || changed ? " style=\"color:#ff0000;\"" : "") + " onclick=\"show(this);\">" + dr["name"].ToString() + "</div>");

                    sb.Append("<ul style=\"display:none;margin-left:40px;\">\r\n");
                    sbr.Append("<ul style=\"display:none;margin-left:40px;\">\r\n");
                    foreach (DataRow _dr in dt1.Rows)
                    {
                        /*
                        DataRow row = null;
                        if (dt2.Rows.Count > 0)
                        {
                            DataRow[] rs = dt2.Select("code='" + _dr["code"].ToString() + "' and type='" + _dr["type"].ToString() + "' and length='" + _dr["length"].ToString() + "'");                           
                            if (rs.Length > 0)
                            {
                                if (_dr["def"].ToString() == rs[0]["def"].ToString()) row = rs[0];
                            }
                        }
                        sb.Append("<li>" + _dr["code"].ToString() + "</li>");
                        sbr.Append("<li" + (row == null ? " style=\"color:#ff0000;\"" : "") + ">" + _dr["code"].ToString() + "</li>");
                        */

                        changed = false;
                        if (dt2.Rows.Count > 0)
                        {
                            DataRow[] rs = new DataTable().Select();
                            if (pattern == "2")//详细
                            {
                                rs = dt2.Select("code='" + _dr["code"].ToString() + "' and type='" + _dr["type"].ToString() + "' and length='" + _dr["length"].ToString() + "'");
                            }
                            else
                            {
                                rs = dt2.Select("code='" + _dr["code"].ToString() + "' and type='" + _dr["type"].ToString() + "'");
                            }
                            if (rs.Length > 0)
                            {
                                if (_dr["def"].ToString() != rs[0]["def"].ToString() && pattern == "2")
                                {
                                    changed = true;
                                }
                            }
                            else
                            {
                                changed = true;
                            }
                        }

                        sb.Append("<li>" + _dr["code"].ToString() + "</li>");
                        sbr.Append("<li" + (changed ? " style=\"color:#ff0000;\"" : "") + ">" + _dr["code"].ToString() + "</li>");


                    }
                    sb.Append("</ul>\r\n");
                    sbr.Append("</ul>\r\n");                  

                }

                sb.Append("</td>\r\n");
                sb.Append("<td>\r\n");
                sb.Append(sbr.ToString());
                sb.Append("</td>\r\n");
                sb.Append("</tr>\r\n");
                sb.Append("</tbody>\r\n");
                sb.Append("</table>\r\n");



                LitBody.Text = sb.ToString();
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (Master == null) return;
            Literal lit = (Literal)Master.FindControl("LitTitle");
            if (lit != null)
            {
                lit.Text = "库对比 - " + eConfig.manageName();
            }
        }
    }
}