﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModelItems_Data.aspx.cs" Inherits="eFrameWork.Manage.ModelItems_Data" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
 <title><%=eConfig.manageName() %></title>
</head>
<body>
<asp:Repeater id="Rep" runat="server" >
<headertemplate>
<%#
"<table id=\"eDataTable\" class=\"eDataTable\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" width=\"99%\" style=\"min-width:1550px;\">" +
"<thead>" +
"<tr>" +
"<td width=\"120\">模块</td>" +
"<td width=\"90\">编码</td>" +
"<td width=\"150\">列名</td>" +
"<td width=\"60\">表单Name</td>" +
"<td>绑定" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(121);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"150\">选项" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(122);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"150\">替换" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(123);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"</tr>" +
"</thead>"
%>
</headertemplate>
<itemtemplate>
<%#
"<tr" + ((Container.ItemIndex + 1) % 2 == 0 ? " class=\"alternating\" eclass=\"alternating\"" : " eclass=\"\"") + " onmouseover=\"this.className='cur';\" onmouseout=\"this.className=this.getAttribute('eclass');\" >" +
"<td height=\"32\">" + Eval("ModelName") + "</td>" +
"<td>" + Eval("Code") + "</td>" +
"<td>"+ Eval("MC") + "</td>" +
"<td>" + Eval("frmName") + "</td>" +
"<td>"
%>
<span style="<%# ",date,textarea,password,file,image,label,percentagetext,fileselect,pathselect,sort,html,images,ecolor,bdmap,raty,filelist,eediter,tagsinput,aceediter,fromlink,".Contains("," + Eval("ControlType").ToString().ToLower() + ",") ? "display:none;":""%>">
源<select reload="true" onChange="setModelItem(this,'<%# Eval("ModelItemID")%>','datasourceid');" style="width:60px;">
    <option value="">无</option>
    <option value="maindb"<%# (Eval("DataSourceID").ToString()=="maindb" ? " selected=\"true\"" : "")%>>主库</option>
     <%# eBase.DataBase.getOptions("SELECT DataSourceID as value,MC as text FROM a_eke_sysDataSources where delTag=0 order by addTime", "text", "value", Eval("DataSourceID").ToString())%>
   </select>
表 <select reload="true" onChange="setModelItem(this,'<%# Eval("ModelItemID")%>','bindobject');" style="width:100px;">
	<option value="NULL">无</option>
	<asp:Literal id="LitObjects" runat="server" />
	</select>
	行
	<input class="text" type="text" value="<%# (Eval("bindrows").ToString()=="0" ? "" : Eval("bindrows").ToString())%>"  style="width:60px;" onBlur="setModelItem(this,'<%# Eval("ModelItemID")%>','bindrows');" />
    值
	<select onChange="setModelItem(this,'<%# Eval("ModelItemID")%>','bindvalue');" style="width:80px;">
	<option value="NULL">无</option>
	<asp:Literal id="LitValue" runat="server" />
	</select>
	文本
	<select onChange="setModelItem(this,'<%# Eval("ModelItemID")%>','bindtext');" style="width:80px;">
	<option value="NULL">无</option>
	<asp:Literal id="LitText" runat="server" />
	</select>
	条件
	<input class="text" type="text" value="<%# Eval("bindcondition")%>" oldvalue="<%# Eval("bindcondition")%>" style="width:80px;" ondblclick="dblClick(this,'<%# Eval("MC")%>-条件');" onBlur="setModelItem(this,'<%# Eval("ModelItemID")%>','bindcondition');" />
	分组
	<input class="text" type="text" value="<%# Eval("bindgroupby")%>" oldvalue="<%# Eval("bindgroupby")%>" style="width:80px;" ondblclick="dblClick(this,'<%# Eval("MC")%>-分组');" onBlur="setModelItem(this,'<%# Eval("ModelItemID")%>','bindgroupby');" />
		<br />排序
	<input class="text" type="text" value="<%# Eval("bindorderby")%>" oldvalue="<%# Eval("bindorderby")%>" style="width:80px;" ondblclick="dblClick(this,'<%# Eval("MC")%>-排序');" onBlur="setModelItem(this,'<%# Eval("ModelItemID")%>','bindorderby');" />
自动加载
	 <select reload="true" onChange="setModelItem(this,'<%# Eval("ModelItemID")%>','bindauto');" style="width:40px;">
	<option value="1"<%# (Eval("bindauto").ToString()=="True" ? " selected=\"true\"" : "")%>>是</option>
	<option value="0"<%# (Eval("bindauto").ToString()=="False" ? " selected=\"true\"" : "")%>>否</option>
	</select>
    外键：
    <select onChange="setModelItem(this,'<%# Eval("ModelItemID")%>','bindforeignkey');" style="width:80px;">
	<option value="NULL">无</option>
	<asp:Literal id="LitBindForeignKey" runat="server" />
	</select>
    继承数据源：
    <select onChange="setModelItem(this,'<%# Eval("ModelItemID")%>','setdatabase');" style="width:40px;">
        <option value="1"<%# (Eval("setdatabase").ToString()=="True" ? " selected=\"true\"" : "")%>>是</option>
	<option value="0"<%# (Eval("setdatabase").ToString()=="False" ? " selected=\"true\"" : "")%>>否</option>
    </select>
    快速绑定字典表：
    <select onChange="quickBind(this,'<%# Eval("ModelItemID")%>');" style="width:80px;">
     <option value="">无</option>
        <%# DataBase.getOptions("SELECT a.DictionarieID as value,case when c.MC is null then '' else c.MC + ' -> ' end + + case when b.MC is null then '' else b.MC + ' -> ' end + a.MC as text FROM Dictionaries a left join Dictionaries b on a.ParentID=b.DictionarieID left join Dictionaries c on b.ParentID=c.DictionarieID where a.BindItem=1 and a.delTag=0 order by a.PX,a.addTime", "text", "value", "")%>
    <%
        //DataBase.getOptions("select MC,DictionarieID from Dictionaries where delTag=0 and BindItem=1 order by px,addtime", "MC", "DictionarieID", "")
            %>
    </select>
    <br />
    <input class="text" type="text" value="<%# Eval("BindForeignkey")%>" oldvalue="<%# Eval("BindForeignkey")%>" style="width:120px;display:none;" onBlur="setModelItem(this,'<%# Eval("ModelItemID")%>','bindforeignkey');" />
	SQL选项取值：
	<input class="text" type="text" value="<%# Eval("BindSQL")%>" oldvalue="<%# Eval("BindSQL")%>" style="width:500px;" ondblclick="dblClick(this,'<%# Eval("MC")%>-SQL选项取值');" onBlur="setModelItem(this,'<%# Eval("ModelItemID")%>','bindsql');" /><br />

    <span style="<%# "searchselect,select,radio,checkbox,tree_checkbox,treeradio,autoselect,loopselect".IndexOf( Eval("ControlType").ToString().ToLower()) > -1 ? "":"display:none;"%>">
    联动传值：<select onChange="setModelItem(this,'<%# Eval("ModelItemID")%>','bindcode');" style="width:80px;">
	<option value="NULL">无</option>
	<asp:Literal id="LitCode" runat="server" />
	</select>
      联动加载：<asp:Literal id="Litcolumns" runat="server" />
    <select onChange="setModelItem(this,'<%# Eval("ModelItemID")%>','fillitem');" style="display:none;width:120px;">
        <option value="NULL">无</option>
        <asp:Literal id="Litcolumns1" runat="server" />
    </span>


    <%if(33==44){%>
	加载
	<input class="text" type="text" value="<%# Eval("BindDataPrar")%>" oldvalue="<%# Eval("BindDataPrar")%>"  style="width:80px;" onBlur="setModelItem(this,'<%# Eval("ModelItemID")%>','binddataprar');" />
	<%}%>
</span>
<%#
"</td>" +
"<td>"+

"<input class=\"text\" reload=\"true\" id=\"data_options_" +  Eval("ModelItemID").ToString().Replace("-","") + "\" jsonformat=\"[{&quot;text&quot;:&quot;文本&quot;,&quot;value&quot;:&quot;text&quot;},{&quot;text&quot;:&quot;值&quot;,&quot;value&quot;:&quot;value&quot;}]\" style=\"display:none;\" type=\"text\" value=\""+ HttpUtility.HtmlEncode(Eval("options").ToString()) + "\" oldvalue=\""+ HttpUtility.HtmlEncode(Eval("options").ToString())+ "\" titlea=\"格式：文本，值；文本，值\" ondblclick=\"dblClick(this,'" + Eval("MC") + "-选项');\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','options');\" />"+
"<img src=\"images/jsonedit.png\" align=\"absmiddle\" style=\"cursor:pointer;margin-right:5px;\" onclick=\"Json_Edit('data_options_" +  Eval("ModelItemID").ToString().Replace("-","") + "','选项');\">" + 
getJsonText(Eval("options").ToString(),"text") +

"</td>" +
"<td>"+
"<input class=\"text\" reload=\"true\" id=\"data_replace_" +  Eval("ModelItemID").ToString().Replace("-","") + "\" jsonformat=\"[{&quot;text&quot;:&quot;文本&quot;,&quot;value&quot;:&quot;text&quot;},{&quot;text&quot;:&quot;值&quot;,&quot;value&quot;:&quot;value&quot;}]\" style=\"display:none;\" type=\"text\" value=\""+ HttpUtility.HtmlEncode(Eval("replacestring").ToString()) + "\" oldvalue=\""+ HttpUtility.HtmlEncode(Eval("replacestring").ToString()) + "\"  titlea=\"格式：文本，值；文本，值\" ondblclick=\"dblClick(this,'"+ Eval("MC") + "-替换');\" onBlur=\"setModelItem(this,'" + Eval("ModelItemID") + "','replacestring');\" />"+
"<img src=\"images/jsonedit.png\" align=\"absmiddle\" style=\"cursor:pointer;margin-right:5px;\" onclick=\"Json_Edit('data_replace_" +  Eval("ModelItemID").ToString().Replace("-","") + "','替换');\">" + 
getJsonText(Eval("replacestring").ToString(),"text") +
"</td>" +
"</tr>"
%>
</itemtemplate>
<footertemplate><%#"</table>"%></footertemplate>
</asp:Repeater>
</body>
</html>
