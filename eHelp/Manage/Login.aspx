﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="eFrameWork.Manage.Login" %><!DOCTYPE html>
<html>
<head>
<title><%= eConfig.manageName() %></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<META HTTP-EQUIV="imagetoolbar" CONTENT="NO">    
</head>
<style>
html,body{height:100%;}
body{margin:0px;}
</style>
<link href="../Plugins/eLogin/default/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />
<script src="../Scripts/jquery.js"></script>
<script src="../Scripts/eketeam.js?ver=<%=Common.Version %>"></script>
<script src="../Plugins/layui226/layui.all.js"></script>
<script>
    function getrnd(type)
{
    if (type == 0 && document.getElementById("rndpic").src.indexOf("none.gif") == -1) {
        return;
    }
	document.getElementById("rndpic").src="../Plugins/RndPic.aspx?bgcolor=f7f7f7&color=990000&t=" + now();
};
function WxWorkLogin() {
    var fromURL = "<%=HttpUtility.UrlEncode(wxworkFromURL)%>";
    var url = "<%= (eBase.WXWorkAccount.getValue("Proxy").Length== 0 ? eBase.getAbsolutePath() : eBase.WXWorkAccount.getValue("Proxy")) + "Plugins/Tencent/WxWork/WxWorkScanLogin.aspx?mode=getimage&area=manage"%>";
    if (fromURL.length > 0) { url += "&fromURL=" + fromURL; }
    layer.open({
        type: 2,
        title: "企业微信扫一扫登录",
        maxmin: false,
        shadeClose: true, //点击遮罩关闭层 
        area: ["330px", "450px"],
        //  content: ['WxWorkCode.aspx','no'], 
        //content: "WxWorkCode.aspx",
        content: [url, 'no'],
        success: function (layero, index) { },
        cancel: function (index, layero) { },
        end: function (index) { }
    });
};
function WeChatLogin() {
    var fromURL = "<%=HttpUtility.UrlEncode(wechatFromURL)%>";
    var url = "<%=(eBase.WeChatAccount.getValue("Proxy").Length== 0 ? eBase.getAbsolutePath() : eBase.WeChatAccount.getValue("Proxy")) + "Plugins/Tencent/WeChat/WeChatScanLogin.aspx?mode=getimage&area=manage"%>";
    if (fromURL.length > 0) { url += "&fromURL=" + fromURL; }
    layer.open({
        type: 2,
        title: "微信扫一扫登录",
        maxmin: false,
        shadeClose: true, //点击遮罩关闭层 
        area: ["330px", "450px"],
        //content: ['WeChatCode.aspx', 'no'],
        //content: "WeChatCode.aspx",
        content: [url, 'no'],
        success: function (layero, index) { },
        cancel: function (index, layero) { },
        end: function (index) { }
    });
};
function DingTalkLogin() {
    var fromURL = "<%=HttpUtility.UrlEncode(dingtalkFromURL)%>";
    var url = "<%=(eBase.DingTalkAccount.getValue("Proxy").Length== 0 ? eBase.getAbsolutePath() : eBase.DingTalkAccount.getValue("Proxy") ) + "Plugins/aliyun/DingTalk/DingTalkScanLogin.aspx?mode=getimage&area=manage"%>";
    if (fromURL.length > 0) { url += "&fromURL=" + fromURL; }
    layer.open({
        type: 2,
        title: "钉钉扫一扫登录",
        maxmin: false,
        shadeClose: true, //点击遮罩关闭层 
        area: ["330px", "450px"],
        content: [url, 'no'],
        success: function (layero, index) { },
        cancel: function (index, layero) { },
        end: function (index) { }
    });
};
</script>
<body>
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
	
	
<table border="0" align="center" cellpadding="0" cellspacing="0" class="elogin">
  <tr>
    <td class="t_l"></td>
    <td class="t_c"></td>
    <td class="t_r"></td>
  </tr>
  <tr>
    <td class="c_l"></td>
    <td class="c_c">
		<span class="title"></span>
        <table width="500" border="0" cellspacing="0" cellpadding="0">
          <form name="form1" method="post" action="" onSubmit="return checkfrm(this);">
            <tr>
              <td width="142" height="36" align="right"><span style="font-size:12px;color:#333;">登陆帐号：</span></td>
              <td width="358"><input type="text"  name="yhm"  id="yhm" class="text" style="width:160px;" notnull="true" fieldname="登陆帐号" value=""  autocomplete="off" ></td>
            </tr>
            <tr>
              <td height="36" align="right"><span style="font-size:12px;color:#333;">登录密码：</span></td>
              <td><input type="password"  name="mm"  id="mm" class="text" style="width:160px;" notnull="true" fieldname="登录密码" value="" autocomplete="off" ></td>
            </tr>
               <%if (eConfig.openRndCode()){ %>
            <tr>
              <td height="36" align="right"><span style="font-size:12px;color:#333;">验证码：</span></td>
              <td><input type="text"  name="yzm" id="yzm" class="text" style="width:93px;vertical-align:middle;" notnull="true" fieldname="验证码" maxlength="4" value="" onfocus="getrnd(0);" autocomplete="off" >
                  <img id="rndpic" style="cursor:pointer;" onClick="getrnd();" height="16" src="../images/none.gif" align="absMiddle" border="0"></td>
            </tr>
                <%} %>
            <tr>
              <td height="36">&nbsp;</td>
              <td><input type="submit" name="Submit" value=""  class="btn">
                  <a href="javascript:;" class="wechat<%=wechat ? "":" gray"%>" onClick="<%=wechat ? "WeChatLogin();":"layer.msg('未正确绑定公众号!');"%>"></a> 
                  <a href="javascript:;" class="wxwork<%=wxwork ? "":" gray"%>" onClick="<%=wxwork ? "WxWorkLogin();":"layer.msg('未正确绑定企业微信!');"%>"></a> 
                  <a href="javascript:;" class="dingtalk<%=dingtalk ? "":" gray"%>" onClick="<%=dingtalk?"DingTalkLogin();":"layer.msg('未正确绑定钉钉应用!');"%>"></a>
              </td>
            </tr>
          </form>
        </table></td>
    <td class="c_r"></td>
  </tr>
  <tr>
    <td class="b_l"></td>
    <td class="b_c"></td>
    <td class="b_r"></td>
  </tr>
</table>

	
	</td>
  </tr>
</table>
</body>
</html>
