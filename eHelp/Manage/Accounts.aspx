﻿<%@ Page Language="C#" MasterPageFile="Main.Master" AutoEventWireup="true" CodeFile="Accounts.aspx.cs" Inherits="eFrameWork.Manage.Accounts" %>
<%@ Register Src="GroupMenu.ascx" TagPrefix="uc1" TagName="GroupMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<uc1:GroupMenu runat="server" ID="GroupMenu" />
<div class="nav">您当前位置：<a href="Default.aspx">首页</a> -> 第三方帐号<a id="btn_add" style="<%=(act == "" ? "" : "display:none;" )%>" class="button" href="<%=edt.getAddURL()%>"><span><i class="add">添加</i></span></a></div>
<%if (eRegisterInfo.Base == 0 && eRegisterInfo.Loaded)
  { %>
    <div style="margin:6px;line-height:25px;font-size:13px;">
<div class="tips" style="margin-bottom:6px;"><b>未授权提示</b><br><a href="http://frame.eketeam.com/getSerialNumber.aspx" style="color:#ff0000;" target="_blank">申请临时授权</a>,享更多功能。</div>
   </div>
 <%} %>
  <div class="tips" style="margin:6px;"><b>提示</b><br>绑定第三方帐号，进行登录验证、发送短信等。</div>
<%
if(act=="edit" || act=="add")
{
%>
<div style="margin:6px;line-height:25px;font-size:13px;">
<script>
var act=document.location.href.getquerystring("act");
var JsonTemplates=<%=JsonTemplates%>;
function changeType(obj)
{
	var td=$(obj).parents("td:first");
	var tb=td.find("table:eq(0)");
	var tbody=tb.find("tbody:eq(0)");
	
	if(obj.value.length==0)
	{
		//alert(tbody.length);
		tbody.html('');
		tb.hide();
		$("#body").val("{}");
		return;
	}
	var item;
	for(var i=0;i<JsonTemplates.length;i++)
	{
		if(JsonTemplates[i].type==obj.value)
		{
			item=JsonTemplates[i];
			break;
		}
	}
	tbody.html('');
	if(item==null)
	{
		tb.hide();
		return;
	}
	addControls(tb,item);
};

function addControls(tb,item)
{
	//var json=$("#body").val();
	//var data= json.toJson();
	
	var data = <%=data%>;


	var html='';
	for(var i=0;i < item.controls.length;i++)
	{
		var value=data[item.controls[i].field] ? data[item.controls[i].field] : (act=="add" ? item.controls[i].defaultvalue : "");

		html+='<tr>';
		html+='<td class="title">' + (item.controls[i].notnull=="true" ? "<ins>*</ins>" : "") + item.controls[i].name + '：</td>';
		html+='<td class="content"><span class="eform">';
		switch(item.controls[i].controltype)
		{
			case "text":
				html+='<input type="text" class="text" name="' + item.controls[i].field + '" fieldname="' + item.controls[i].name + '" notnull="' + item.controls[i].notnull + '" value="' + value + '" />';
				break;
		}
		if(item.controls[i].tip.length>0) html+='<i>' + item.controls[i].tip + '</i>';
		html+='</span></td>';
		html+='</tr>';
	}
	var tbody=tb.find("tbody:eq(0)");
	tbody.append(html);
	tb.show();
};

$( document ).ready(function() {
	var type="<%=type.Value.ToString()%>";
	
	var sel=$("#type");
	if(sel)
	{
		sel=sel[0];
		//sel.length=1;
		for(var i=0;i<JsonTemplates.length;i++)
		{			
			sel.options.add(new Option(JsonTemplates[i].name,JsonTemplates[i].type));
		}
		if(type.length>0)
		{
			sel.value=type;	
			changeType(sel);
		}
		if(act=="add"){ $("#tbbody").hide();}
	}
	
	
});


function getjson(obj)
{
        var json = "{";
        //if (typeof (frm) == "string") { frm = document.forms[frm]; }
        var elements = obj.find("input,select,textarea");//frm.elements;
        var idx = 0;
        for (var i = 0; i < elements.length; i++) {
            if (elements[i].getAttribute("Exclude") != null) { continue; }
            if (elements[i].name.length == 0) { continue; }
            
            var type = "";          
            if (elements[i].getAttribute("type")) { type = elements[i].getAttribute("type").toString().toLowerCase(); }
            if (type == "hidden") { continue; }
            if (type.length == 0) {
                if (elements[i].tagName.toLowerCase() != "select" && elements[i].tagName.toLowerCase() != "textarea") {
                    continue;
                }
            }
            if (type == "submit" || type == "button" || type == "reset") { continue; }
            if ((type == "radio" || type == "checkbox") && elements[i].checked == false) { continue; }

          
            
          

            if (idx > 0) json += ',';
            json += '"' + elements[i].name + '":"' + elements[i].value.replace(/\"/gi, "&quot;").encode() + '"'; //elements[i].value.encode()
			//json += '"' + elements[i].name + '":"' + elements[i].value.encode() + '"'; //elements[i].value.encode()
            idx++;
        }
        json += "}";
        return json;
};

function checkfrm(frm)
{
	var tb=$("#tbbody");
	var json=getjson(tb);
	$("#body").val(json);
	return true;
};

</script>



<textarea id="result11" name="textarea" cols="100" rows="8" style="width:99%;display:none;"><%="valuejson" %></textarea>

<input type="hidden" id="result" name="result" value="<%="valuejson" %>">

<form id="frmaddoredit" name="frmaddoredit" method="post" action="" onsubmit="return checkfrm(this);">
<input type="hidden" id="act" name="act" value="save">
<input name="fromurl" type="hidden" id="fromurl" value="<%=edt.FromURL%>">  
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="eDataView">

      <tr>
        <td width="126" class="title"><ins>*</ins>名称：</td>
        <td class="content"><span class="eform">
		 <ev:eFormControl ID="f1" Field="MC" width="600" notnull="true" fieldname="名称" runat="server" />
		</span></td>
      </tr>
	 
        <tr>
          <td class="title"><ins>*</ins>所属企业：</td>
          <td class="content"><span class="eform">
		 <ev:eFormControl ControlType="select" ID="f2" Field="SiteID" notnull="true" fieldname="所属企业" BindObject="a_eke_sysSites" BindText="MC" BindValue="SiteID" BindCondition="deltag=0" BindOrderBy="addtime" runat="server" />
		  </span></td>
        </tr>		
		 <tr>
          <td class="title"><ins>*</ins>帐号类型：</td>
          <td class="content"><span class="eform">
		   <ev:eFormControl ControlType="select" ID="typebak" Field="type" Visible="false" notnull="false" fieldname="帐号类型" Attributes="onchange=&quot;changeType(this);&quot;" runat="server" />
		  
			
			<select id="type" name="type" fieldname="帐号类型" notnull="true" onchange="changeType(this);autoFill(this,this.value,'f4');">
	  <option value="">请选择</option>
	  <!--<optgroup label="腾讯">-->
	   <!--
	  <option value="1">腾讯短信</option>
	  <option value="2">微信公众号</option>
	  <option value="3">企业微信</option>
	  <option value="4">微信支付</option>
	  
	  <option value="5">微信小程序</option>
	   </optgroup>-->
	  <!--
	   <optgroup label="阿里">
	  <option value="6">短信</option>
	  <option value="7">钉钉</option>
	  <option value="8">小程序</option>
	  </optgroup>
	  -->
	  
	</select>
		   </span>
		   <textarea id="bodyxx" name="bodyxx" style="display:none;"><%=body%></textarea>
		   <input name="body" type="hidden" id="body" value="<%=body%>" />
		   <div>
		   <table width="700" border="0" cellspacing="0" cellpadding="0" class="eDataView" id="tbbody" style="margin-top:8px;">
		   <colgroup>
		   <col width="150" />
		   <col />
		   </colgroup>
		   <tbody>
			</tbody>
		   </table>		   
		   </div>
		   </td>
        </tr>
        <tr>
          <td class="title"><ins>*</ins>绑定域名：</td>
          <td class="content"><span class="eform">
		   <ev:eFormControl ID="f5" ControlType="text" Field="DoMain" fieldname="帐号类型" notnull="true" width="600" runat="server" />
		   </span></td>
        </tr>
		<tr style="display:none;">
		 <td class="title">绑定应用：</td>
		  <td class="content"><span id="f4_box" etype="checkbox" notnull="false" fieldname="绑定应用" class="eform">
		  <ev:eFormControl ControlType="checkbox" ID="f4" Field="ApplicationIDS" notnull="false" fieldname="绑定应用" BindObject="a_eke_sysSiteItems" BindText="AppName" BindValue="ApplicationID" BindCondition="deltag=0 and siteid='{querystring:pid}'" BindOrderBy="addtime" BindAuto="false" runat="server" />
		  </span></td>
		</tr>
        <tr>
          <td class="title">代理地址：</td>
          <td class="content"><span class="eform">
		   <ev:eFormControl ID="f6" ControlType="text" Field="Proxy" fieldname="代理地址" notnull="false" width="600" runat="server" />
		   </span></td>
        </tr>
    <tr>
          <td class="title">反向代理域名：</td>
          <td class="content"><span class="eform">
		   <ev:eFormControl ID="f7" ControlType="text" Field="ReverseProxy" fieldname="反向代理域名" notnull="false" width="600" runat="server" />
		   </span></td>
        </tr>
  		<tr>
          <td class="title">说明：</td>
          <td class="content"><span class="eform">
		   <ev:eFormControl ID="f3" ControlType="textarea" Field="SM" width="600" height="60" runat="server" />
		   </span></td>
        </tr>
        <tr>
       <td colspan="2" class="title"  style="text-align:left;padding-left:100px;padding-top:10px;padding-bottom:10px;">		
		<a class="button" href="javascript:;" onclick="if(frmaddoredit.onsubmit()!=false){frmaddoredit.submit();}"><span><i class="save">保存</i></span></a>
		<a class="button" href="javascript:;" style="margin-left:30px;" onclick="history.back();"><span><i class="back">返回</i></span></a>
		</td>
	   </tr>
	 
    </table>
<asp:Literal ID="LitBody" runat="server" />




</form>

</div>
	<%}%>

<div style="margin:6px;overflow-x:auto;overflow-y:hidden;">
<asp:Repeater id="Rep" runat="server">
<headertemplate>
<%#
"<table id=\"eDataTable\" class=\"eDataTable\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" width=\"100%\">\r\n" +
"<thead>\r\n" +
"<tr bgcolor=\"#f2f2f2\">\r\n" +
"<td width=\"260\">编号</td>\r\n" +
"<td>名称</td>\r\n" +
"<td>帐号类型</td>\r\n" +
"<td>说明</td>\r\n" +
"<td width=\"100\">添加时间</td>\r\n" +
"<td width=\"120\">操作</td>\r\n" +
"</tr>\r\n" +
"</thead>\r\n"
%>
</headertemplate>
<itemtemplate>
<%#
"<tr" + ((Container.ItemIndex+1) % 2 == 0 ? " class=\"alternating\" eclass=\"alternating\"" : " eclass=\"\"") + ">\r\n" +
"<td height=\"32\"><a class=\"copy\" href=\"javascript:;\" data-clipboard-action=\"copy\" data-clipboard-text=\"" + Eval("AccountID") + "\"></a>"+ Eval("AccountID")+"</td>\r\n" +
"<td>"+ Eval("MC").ToString()+"</td>\r\n" +
"<td>"+ eBase.getAccountTypeText( Eval("Type").ToString()) +"</td>\r\n" +
"<td>"+ Eval("SM").ToString()+"</td>\r\n" +
"<td>"+ Eval("addTime","{0:yyyy-MM-dd}")+"</td>\r\n" +
"<td>"+
"<a href=\"" + edt.getActionURL("edit",Eval("AccountID").ToString())  + "\">修改</a>"+
"<a href=\""+ edt.getActionURL("del",Eval("AccountID").ToString()) +"\" onclick=\"javascript:return confirm('确认要删除吗？');\">删除</a>"+
"</td>\r\n" +
"</tr>\r\n" 
%>
</itemtemplate>
<footertemplate><%#"</table>\r\n"%></footertemplate>
</asp:Repeater>
</div>
<div style="margin:6px;"><ev:ePageControl ID="ePageControl1" PageSize="20" PageNum="9" runat="server" /></div>
</asp:Content>