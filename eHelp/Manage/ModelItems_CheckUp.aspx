﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModelItems_CheckUp.aspx.cs" Inherits="eFrameWork.Manage.ModelItems_CheckUp" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><%=eConfig.manageName() %></title>
</head>
<body>
<asp:Repeater id="Rep" runat="server" >
<headertemplate>
<%#
"<table id=\"eDataTable_CheckUp\" class=\"eDataTable\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" width=\"99%\" style=\"margin-bottom:8px;\">" +
"<thead>" +
"<tr>" +
"<td height=\"25\" width=\"35\" align=\"center\"><a title=\"添加流程\" href=\"javascript:;\" onclick=\"addCheckUp(this);\"><img width=\"16\" height=\"16\" src=\"images/add.png\" border=\"0\"></a></td>" +
"<td width=\"80\">选择状态" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(166);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"80\">填写意见" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(167);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
//"<td width=\"80\">填写意见" + "</td>" +
"<td width=\"120\">状态名称" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(168);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"120\">状态编码" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(169);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"120\">流程名称" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(170);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"120\">审批编码" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(171);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"80\">按钮名称" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(172);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td>返回流程" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(173);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"80\">流程顺序" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(174);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"</tr>" +
"</thead>"+
"<tbody eSize=\"false\" eMove=\"true\">"
%>
</headertemplate>
<itemtemplate>
<%#
"<tr" + ((Container.ItemIndex + 1) % 2 == 0 ? " class=\"alternating\" eclass=\"alternating\"" : " eclass=\"\"") + " erowid=\"" + Eval("CheckupID") + "\" onmouseover=\"this.className='cur';\" onmouseout=\"this.className=this.getAttribute('eclass');\" >" +
"<td height=\"26\" align=\"center\"><a title=\"删除流程\" href=\"javascript:;\" onclick=\"delCheckUp(this,'" + Eval("CheckupID") + "');\"><img width=\"16\" height=\"16\" src=\"images/del.png\" border=\"0\"></a></td>" +

"<td><input type=\"checkbox\" onclick=\"setCheckUp(this,'" + Eval("CheckupID") + "','showstate');\"" + (Eval("showState").ToString().Replace("1","True")=="True" ? " checked" : "") + " /></td>" +
//"<td><input type=\"checkbox\" onclick=\"setCheckUp(this,'" + Eval("CheckupID") + "','showidea');\"" + (Eval("showIdea").ToString().Replace("1","True")=="True" ? " checked" : "") + " /></td>" +

"<td><select onchange=\"setCheckUp(this,'" + Eval("CheckupID") + "','showIdea');\">" +
"<option value=\"0\"" + ( Eval("showIdea").ToString() == "0" ? " selected=\"true\"" : "") + ">无</option>" +
"<option value=\"1\"" + ( Eval("showIdea").ToString() == "1" ? " selected=\"true\"" : "") + ">打字</option>" +
"<option value=\"2\"" + ( Eval("showIdea").ToString() == "2" ? " selected=\"true\"" : "") + ">手写</option>" +
"</select>" +
"</td>" +

"<td><input class=\"text\" type=\"text\" value=\""+ Eval("MC").ToString() + "\" oldvalue=\""+ Eval("MC").ToString() + "\" onBlur=\"setCheckUp(this,'" + Eval("CheckupID") + "','mc');\"></td>" +
"<td><input class=\"text\" type=\"text\" value=\""+ Eval("Code").ToString() + "\" oldvalue=\""+ Eval("Code").ToString() + "\" onBlur=\"setCheckUp(this,'" + Eval("CheckupID") + "','code');\"></td>" +
"<td><input class=\"text\" type=\"text\" value=\""+ Eval("CheckMC").ToString() + "\" oldvalue=\""+ Eval("CheckMC").ToString() + "\" onBlur=\"setCheckUp(this,'" + Eval("CheckupID") + "','checkmc');\"></td>" +
"<td><input class=\"text\" type=\"text\" value=\""+ Eval("CheckCode").ToString() + "\" oldvalue=\""+ Eval("CheckCode").ToString() + "\" onBlur=\"setCheckUp(this,'" + Eval("CheckupID") + "','checkcode');\"></td>" +
"<td><input class=\"text\" type=\"text\" value=\""+ Eval("Text").ToString() + "\" oldvalue=\""+ Eval("Text").ToString() + "\" onBlur=\"setCheckUp(this,'" + Eval("CheckupID") + "','text');\"></td>" +
"<td><input class=\"text\" reload=\"true\" id=\"backprocess_" + Eval("CheckupID").ToString().Replace("-","") + "\" jsonformat=\"[{&quot;text&quot;:&quot;流程名称&quot;,&quot;value&quot;:&quot;text&quot;},{&quot;text&quot;:&quot;状态编码&quot;,&quot;value&quot;:&quot;value&quot;}]\" style=\"display:none;\" type=\"text\" value=\""+ HttpUtility.HtmlEncode(Eval("BackProcess").ToString()) + "\" oldvalue=\""+ HttpUtility.HtmlEncode(Eval("BackProcess").ToString())  + "\" ondblclick=\"dblClick(this,'" + Eval("MC") + "-返回流程');\" onBlur=\"setCheckUp(this,'" + Eval("CheckupID") + "','backprocess');\">"+
"<img src=\"images/jsonedit.png\" align=\"absmiddle\" style=\"cursor:pointer;margin-right:5px;\" onclick=\"Json_Edit('backprocess_" +  Eval("CheckupID").ToString().Replace("-","") + "');\">" + 
getJsonText(Eval("BackProcess").ToString(),"text") +
"</td>" +
//"<td><input class=\"text\" reload=\"true\" type=\"text\" value=\""+ (Eval("PX").ToString()=="999999" ? "" : Eval("PX").ToString()) + "\" oldvalue=\""+ (Eval("PX").ToString()=="999999" ? "" : Eval("PX").ToString()) + "\" onBlur=\"setCheckUp(this,'" + Eval("CheckupID") + "','px');\"></td>"+
"<td style=\"cursor:move;\">" + (Container.ItemIndex + 1) + "</td>"+
"</tr>"
%>
</itemtemplate>
<footertemplate><%#"</tbody></table>"%></footertemplate>
</asp:Repeater>

</body>
</html>
