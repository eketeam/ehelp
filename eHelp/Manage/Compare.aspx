﻿<%@ Page Language="C#" MasterPageFile="Main.Master" AutoEventWireup="true" CodeFile="Compare.aspx.cs" Inherits="eFrameWork.Manage.Compare" %>
<%@ Register Src="GroupMenu.ascx" TagPrefix="uc1" TagName="GroupMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
ul,li{margin:0px;padding:0px;list-style-type:none;}
.close{background:url(../images/close.gif) no-repeat scroll 8px 8px transparent;padding-left:23px; cursor:pointer;}
.open{background:url(../images/open.gif) no-repeat scroll 8px 8px transparent;padding-left:23px; cursor:pointer;}
</style>
<script>
    function show(obj)
    {
        if(obj.className.toLowerCase()=="open")
        {
            obj.className="close";
            //$(obj).next("ul").hide();
            $(obj).next("ul").slideUp();
        }
        else
        {
            obj.className="open";
            //$(obj).next("ul").show();
            $(obj).next("ul").slideDown();
        }
    };
</script>
<uc1:GroupMenu runat="server" ID="GroupMenu" />    
<div class="nav">您当前位置：<a href="Default.aspx">首页</a> -> 库对比</div>
<%if (eRegisterInfo.Base == 0 && eRegisterInfo.Loaded)
  { %>
    <div style="margin:6px;line-height:25px;font-size:13px;">
<div class="tips" style="margin-bottom:6px;"><b>未授权提示</b><br><a href="http://frame.eketeam.com/getSerialNumber.aspx" style="color:#ff0000;" target="_blank">申请临时授权</a>,享更多功能。</div>
   </div>
 <%} %>
<div style="margin:6px;line-height:25px;font-size:13px;">
     <div class="tips" style="margin-bottom:6px;"><b>提示</b><br>对比两个数据库结构之间的差异，需要同时具有两个库的权限才能正常对比。</div>
    <form id="form1" name="form1" method="post" action="">
新库：
  <select name="newdb" id="newdb" fieldname="新库" notnull="true" style="height:26px;">
      <option value="">请选择</option>
      <asp:Literal ID="Litnew" runat="server" />
    </select>&nbsp;&nbsp;
旧库：
  <select name="olddb" id="olddb" fieldname="旧库" notnull="true" style="height:26px;">
      <option value="">请选择</option>
       <asp:Literal ID="Litold" runat="server" />
  </select>&nbsp;&nbsp;
对比方式：
      <select name="pattern" id="pattern" style="height:26px;">
    <option value="1"<%=(pattern=="1" ? " selected=\"true\"" : "") %>>简单</option>
    <option value="2"<%=(pattern=="2" ? " selected=\"true\"" : "") %>>详细</option>
  </select>

  <input type="submit" name="Submit" value="开始比较" style="padding:3px 10px 3px 10px;margin-left:10px;" />
</form>



 <asp:Literal ID="LitBody" runat="server" />
</div>
</asp:Content>
