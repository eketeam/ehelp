﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModelImport.aspx.cs" Inherits="eFrameWork.Manage.ModelImport" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>导入模块</title>
      <script src="../Scripts/init.js?ver=<%=Common.Version %>"></script>
</head>
<script>
    function checkfrm(frm) {
        if (frm.imgFile.value.length == 0) {
            top.layer.msg("请选择要上传的文件!");
            frm.imgFile.focus();
            return false;
        }
        return true;
    }
</script>
<body>
<div style="margin:10px;">
<form method="post" enctype="multipart/form-data" id="Form1" onSubmit="return checkfrm(this);">
    <INPUT type="file" id="imgFile" name="imgFile" style="width:160px;overflow:hidden;vertical-align:middle;" onchange="autoupload_check(this);" allowexts="<%=allowExts.ToString()%>" preventexts="<%=eConfig.PreventExtensions.ToString()%>" accept="<%=allowExts.ToString(",")%>" />&nbsp;<INPUT style="vertical-align:middle;" name="button" type="submit" id="button" value=" 导 入 ">
	<input name="act" type="hidden" id="act" value="save">
</form>
</div>
</body>
</html>