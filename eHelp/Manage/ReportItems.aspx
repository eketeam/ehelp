﻿<%@ Page Language="C#" MasterPageFile="Main.Master" AutoEventWireup="true" CodeFile="ReportItems.aspx.cs" Inherits="eFrameWork.Manage.ReportItems" %>
<%@ Register Src="GroupMenu.ascx" TagPrefix="uc1" TagName="GroupMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<uc1:GroupMenu runat="server" ID="GroupMenu" />
<div class="nav">您当前位置：<a href="Default.aspx">首页</a> -> 报表</div>
<%if (eRegisterInfo.Base == 0 && eRegisterInfo.Loaded)
  { %>
    <div style="margin:6px;line-height:25px;font-size:13px;">
<div class="tips" style="margin-bottom:6px;"><b>未授权提示</b><br><a href="http://frame.eketeam.com/getSerialNumber.aspx" style="color:#ff0000;" target="_blank">申请临时授权</a>,享更多功能。</div>
   </div>
 <%} %>
<script>
    /* ModelItems_ReportItems.aspx 功能相同，应同步修改 */
var ReportID = "<%=ReportID%>";
function showloading()
{
	$("#divloading").show();
};
function hideloading()
{
	$("#divloading").hide();
};
function getValue(obj)
{
	if (obj.type.toLowerCase() == "radio" || obj.type.toLowerCase() == "checkbox") 
	{
		return (obj.checked ? "1" : "0");
    }
	else
	{
		return obj.value.encode();
    }
};
function addReportItem(obj,type)
{
	var _reload=parseBool(Attribute(obj,"reload"));
	var url = "?act=additem&ReportID=" + ReportID;
	url += "&type=" + type; 
	url += "&t=" + now();
	showloading();
	$.ajax({
		type: "GET", async: true,
        url: url,
        dataType: "json",
        success: function (data)
        {
			hideloading();
			//if(!_reload){return;}
            loadData();
        },
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			hideloading();
		}	
    });
};
function setMC(obj) {
    $(obj).parent().parent().find("h1 span").html(obj.value);
};
function setReport(obj, Item) {
    var _reload = parseBool(Attribute(obj, "reload"));
    //if (obj.getAttribute("oldvalue") == obj.value) { return; }
    var value = getValue(obj);
    if (value == "error") { return; }
    showloading();
    var url = "?act=setreport&ReportID=" + ReportID + "&item=" + Item + "&value=" + value + "&t=" + now();
    $.ajax({
        type: "GET", async: true,
        url: url,
        dataType: "html",
        success: function (data) {
            hideloading();
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            hideloading();
        }
    });
};
function setReportItem(obj, ReportItemID, Item)
{
	var _reload=parseBool(Attribute(obj,"reload"));
	//if (obj.getAttribute("oldvalue") == obj.value) { return; }
	var value = getValue(obj);
	if (value == "error") { return; }
	showloading();
	var url = "?act=setitem&ReportID=" + ReportID + "&ReportItemID=" + ReportItemID + "&item=" + Item + "&value=" + value + "&t=" + now();
    $.ajax({
		type: "GET", async: true,
		url: url,
		dataType: "html",
		success: function (data)
		{
		    if (Item == "finsh")
		    {
		        obj.style.color = obj.value == 1 ? "#33cc00" : "#FF0000";
		    }
			hideloading();
			if(!_reload){return;}
            loadData();
        },
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			hideloading();
		}		
	});
};
function delReportItem(obj, ReportItemID)
{
    
	var _reload=parseBool(Attribute(obj,"reload"));
	if (!confirm('确认要删除吗？')) { return; }
	showloading();
	var url = "?act=delitem&ReportID=" + ReportID + "&ReportItemID=" + ReportItemID + "&t=" + now();
    $.ajax({
		type: "GET", async: true,
		url: url,
        dataType: "html",
        success: function (data) {
            hideloading();
            
            if (obj.parentNode.tagName == "TD") {
                $(getParent(obj, "tr")).remove();
            }
            else {
                $(getParent(obj, "dl")).remove();
            }

			//if(!_reload){return;}
			//loadData();
        },
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			hideloading();
		}
    });
};
function loadData()
{
	showloading();
	var url = "?ReportID=" + ReportID + "&act=getdata&t=" + now();
	$.ajax({
		type: "GET", async: true,
        url: url,
        dataType: "html",
        success: function (data) {
			hideloading();
            $("#content").html(data);
			bindEvent();
        },
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			hideloading();
		}
    });
};
function bindEvent()
{
    $(".eDataTable").each(function (index, note) {
        var id=$(note).attr("id");
        var tb = new eDataTable(id, 1);
        tb.moveRow = function (index, nindex) {
            $("#" + id + " tbody tr td:last-child").each(function (index1, obj) {
                $(obj).html(1 + index1);
            });
            var ids = "";
            $("#" + id + " tbody tr td:nth-child(1)").each(function (index1, obj) {
                if (index1 > 0) { ids += ","; }
                ids += $(obj).parent().attr("erowid");
            });
            if (ids.length == 0) { return; }
            showloading();
            var url = "?act=setorders&ReportID=" + ReportID + "&t=" + now();
            $.ajax({
                type: "POST", async: true,
                data: { ids: ids },
                url: url,
                dataType: "html",
                success: function (data) {
                    hideloading();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    hideloading();
                }
            });
        };

    });

};
$(document).ready(function () {
	bindEvent();
});
</script>
<style>
.text{text-indent:5px;display:inline-block;width:100%;border:1px solid #ccc;font-size:12px;height:23px;line-height:23px;color:#333;}
.divloading{filter:alpha(opacity=50);-moz-opacity:0.5;-khtml-opacity: 0.5; opacity: 0.5; position:fixed;width:100%;height:100%;top:0px;left:0px;background:#cccccc url(images/loading.gif) no-repeat center center;}
.btnaddb {display:inline-block;width:30px;height:30px;font-size:1px;background:url(images/btn_add.png) center center no-repeat;background-size:100% 100%;}
textarea {text-indent:5px;border:1px solid #ccc;font-size:12px;line-height:23px;color:#666;}
</style>
<div id="divloading" class="divloading" style="display:none;">&nbsp;</div>
<div style="margin:6px;line-height:23px; font-weight:bold;font-size:15px; "><%=row["mc"].ToString()%></div>

<div style="margin:6px;">
<dl class="ePanel">
<dt><h1><a href="javascript:;" class="cur" onfocus="this.blur();"></a><span>基本设置</span></h1></dt>
<dd>

<div class="tips" style="margin-bottom:8px;">
<b>变量说明</b><br>
{year}&nbsp;引用当前数据的年,&nbsp;&nbsp;
{month}&nbsp;引用当前数据的月,&nbsp;&nbsp;
{data:value}&nbsp;引用Y轴的值,&nbsp;&nbsp;
{data:sum}&nbsp;汇总列。
</div>
报表标题：<input reload="false" class="text" type="text" value="<%= row["title"].ToString() %>" style="width:450px;" oldvalue="<%= row["title"].ToString() %>" onBlur="setReport(this,'title');"><br />
报表类型：<select style="width:110px;" onchange="setReport(this,'ControlType');">
    <%= eBase.getReportControlType(row["ControlType"].ToString())%>
    </select><br />
报表简介：<br /><textarea reload="false" type="text" style="width:450px;" oldvalue="<%= row["Introduce"].ToString() %>" onBlur="setReport(this,'introduce');"><%= row["Introduce"].ToString() %></textarea><br />
报表宽度：<input reload="false" class="text" type="text" style="width:70px;" value="<%= row["width"].ToString() %>" oldvalue="<%= row["width"].ToString() %>" onBlur="setReport(this,'width');">
&nbsp;&nbsp;报表高度：<input reload="false" class="text" type="text" style="width:70px;" value="<%= row["height"].ToString() %>" oldvalue="<%= row["height"].ToString() %>" onBlur="setReport(this,'height');">
&nbsp;&nbsp;报表高度(M)：<input reload="false" class="text" type="text" style="width:70px;" value="<%= row["mheight"].ToString() %>" oldvalue="<%= row["mheight"].ToString() %>" onBlur="setReport(this,'mheight');"><br />
<input id="chkswap" style="vertical-align:middle;" type="checkbox" onclick="setReport(this,'swap');"<%= eBase.parseBool( row["swap"]) ? " checked=\"true\"" : "" %> /><label for="chkswap" style="vertical-align:middle;display:inline-block;margin-left:5px;margin-right:5px;">数据反转</label><span style="vertical-align:middle;color:#777;">将Y轴和X轴对换</span><br />

定时刷新：<input reload="false" class="text" type="text" style="width:60px;" value="<%= row["Interval"].ToString() %>" oldvalue="<%= row["Interval"].ToString() %>" onBlur="setReport(this,'interval');"> <span style="vertical-align:middle;color:#777;">单位：秒</span><br />
</dd>
 </dl>
  
</div>



<div id="content" style="position:relative;margin:6px;border:0px solid #ff0000; -webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;">
<asp:PlaceHolder ID="ControlGroup" runat="server">

<dl class="ePanel">
<dt><h1><a href="javascript:;" class="cur" onfocus="this.blur();"></a><span>一维</span></h1></dt>
<dd>
<b>表格属性</b>&nbsp;&nbsp;名称：<input reload="false" class="text" type="text" value="<%= row["YName"].ToString() %>" style="width:150px;" oldvalue="<%= row["YName"].ToString() %>" onBlur="setReport(this,'yname');">
&nbsp;&nbsp;宽度：<input reload="false" class="text" type="text" value="<%= row["ywidth"].ToString() %>" style="width:150px;" oldvalue="<%= row["ywidth"].ToString() %>" onBlur="setReport(this,'ywidth');">
&nbsp;&nbsp;标题对齐：<select style="width:60px;" onchange="setReport(this,'ytitlealign');">
        <option value="left"<%= row["ytitlealign"].ToString() == "left" ? " selected=\"true\"" : ""%>>左</option>
        <option value="center"<%= row["ytitlealign"].ToString() == "center" ? " selected=\"true\"" : ""%>>中</option>
        <option value="right"<%= row["ytitlealign"].ToString() == "right" ? " selected=\"true\"" : ""%>>右</option>
    </select>
&nbsp;&nbsp;内容对齐：<select style="width:60px;" onchange="setReport(this,'ybodyalign');">
        <option value="left"<%= row["ybodyalign"].ToString() == "left" ? " selected=\"true\"" : ""%>>左</option>
        <option value="center"<%= row["ybodyalign"].ToString() == "center" ? " selected=\"true\"" : ""%>>中</option>
        <option value="right"<%= row["ybodyalign"].ToString() == "right" ? " selected=\"true\"" : ""%>>右</option>
    </select>


<asp:Repeater id="RepY" runat="server" >
<headertemplate>
<table id="eDataTable_Itemsy" class="eDataTable" border="0" cellpadding="0" cellspacing="1" wi4dth="100%" style="margin-top:6px;">
<thead>
<tr bgcolor="#f2f2f2">
<td height="25" width="30" align="center"><a title="添加列" href="javascript:;" onclick="addReportItem(this,'Y');"><img width="16" height="16" src="images/add.png" border="0"></a></td>
<td width="40">显示</td>
<td width="160">名称</td>
<td width="120">宽度</td>
<td width="90">标题对齐</td>
<td width="90">内容对齐</td>
<td width="150">固定值</td>
<td width="300">取值</td>
<td width="300">取名称</td>
<td width="180">时间轴</td>
<td width="60">顺序</td>
</tr>
</thead>
<tbody eSize="false" eMove="true">
</headertemplate>
<itemtemplate>
<tr erowid="<%#Eval("ReportItemID")%>">
<td height="26" align="center">
<a title="删除列" href="javascript:;" onclick="delReportItem(this,'<%# Eval("ReportItemID") %>');"><img width="16" height="16" src="images/del.png" border="0"></a></td>
<td><input reload="false" type="checkbox" onclick="setReportItem(this,'<%# Eval("ReportItemID") %>','show');"<%# (eBase.parseBool(Eval("show"))? " checked" : "") %> /></td>
<td><input reload="false" class="text" type="text" value="<%# Eval("MC").ToString() %>" oldvalue="<%# Eval("MC").ToString() %>" onBlur="setReportItem(this,'<%#Eval("ReportItemID") %>','mc');"></td>
<td><input class="text" type="text" value="<%# Eval("width").ToString()%>" oldvalue="<%# Eval("width").ToString()%>" onBlur="setReportItem(this,'<%# Eval("ReportItemID") %>','width');"></td>
<td>
    <select style="width:60px;" onchange="setReportItem(this,'<%# Eval("ReportItemID") %>','titlealign');">
        <option value="left"<%# Eval("titlealign").ToString() == "left" ? " selected=\"true\"" : ""%>>左</option>
        <option value="center"<%# Eval("titlealign").ToString() == "center" ? " selected=\"true\"" : ""%>>中</option>
        <option value="right"<%# Eval("titlealign").ToString() == "right" ? " selected=\"true\"" : ""%>>右</option>
    </select>
</td>
<td>
    <select style="width:60px;" onchange="setReportItem(this,'<%# Eval("ReportItemID") %>','bodyalign');">
        <option value="left"<%# Eval("bodyalign").ToString() == "left" ? " selected=\"true\"" : ""%>>左</option>
        <option value="center"<%# Eval("bodyalign").ToString() == "center" ? " selected=\"true\"" : ""%>>中</option>
        <option value="right"<%# Eval("bodyalign").ToString() == "right" ? " selected=\"true\"" : ""%>>右</option>
    </select>
</td>
<td><input class="text" type="text" value="<%# Eval("value").ToString()%>" oldvalue="<%# Eval("value").ToString()%>" onBlur="setReportItem(this,'<%# Eval("ReportItemID") %>','value');"></td>
<td><input class="text" type="text" value="<%# Eval("valuesql").ToString()%>" oldvalue="<%# Eval("valuesql").ToString()%>" onBlur="setReportItem(this,'<%# Eval("ReportItemID") %>','valuesql');"></td>
<td><input class="text" type="text" value="<%# Eval("bindsql").ToString()%>" oldvalue="<%# Eval("bindsql").ToString()%>" onBlur="setReportItem(this,'<%# Eval("ReportItemID") %>','bindsql');"></td>
<td>
    <select style="width:60px;" onchange="setReportItem(this,'<%# Eval("ReportItemID") %>','timetype');">
        <option value="">无</option>
        <option value="year"<%# Eval("timetype").ToString() == "year" ? " selected=\"true\"" : ""%>>年</option>
        <option value="quarter"<%# Eval("timetype").ToString() == "quarter" ? " selected=\"true\"" : ""%>>季度</option>
        <option value="month"<%# Eval("timetype").ToString() == "month" ? " selected=\"true\"" : ""%>>月</option>
        <option value="day"<%# Eval("timetype").ToString() == "day" ? " selected=\"true\"" : ""%>>日</option>
    </select><br />
    <span style="display:<%# Eval("timetype").ToString().Length == 0 ? "none" : "" %>;">
    起始年：<input class="text" type="text" style="width:40px;" value="<%# Eval("minyear").ToString()%>" oldvalue="<%# Eval("minyear").ToString()%>" onBlur="setReportItem(this,'<%# Eval("ReportItemID") %>','minyear');"><br />
    结束年：<input class="text" type="text" style="width:40px;" value="<%# Eval("maxyear").ToString()%>" oldvalue="<%# Eval("maxyear").ToString()%>" onBlur="setReportItem(this,'<%# Eval("ReportItemID") %>','maxyear');"><br />
    默认年：<input class="text" type="text" style="width:40px;" value="<%# Eval("defaultyear").ToString()%>" oldvalue="<%# Eval("defaultyear").ToString()%>" onBlur="setReportItem(this,'<%# Eval("ReportItemID") %>','defaultyear');"><br />
    默认月：<input class="text" type="text" style="width:40px;" value="<%# Eval("defaultmonth").ToString()%>" oldvalue="<%# Eval("defaultmonth").ToString()%>" onBlur="setReportItem(this,'<%# Eval("ReportItemID") %>','defaultmonth');"><br />
    </span>
</td>

<td style="cursor:move;"><%# (Container.ItemIndex + 1)%></td>
</tr>    
</itemtemplate>
<footertemplate></tbody></table></footertemplate>
</asp:Repeater>
</dd>
</dl>



<dl class="ePanel">
<dt><h1><a href="javascript:;" class="cur" onfocus="this.blur();"></a><span>二维</span></h1></dt>
<dd>

<asp:Repeater id="RepX" runat="server" >
<headertemplate>
<table id="eDataTable_Itemsx" class="eDataTable" border="0" cellpadding="0" cellspacing="1" wi4dth="100%" style="margin-top:6px;">
<thead>
<tr bgcolor="#f2f2f2">
<td height="25" width="30" align="center"><a title="添加列" href="javascript:;" onclick="addReportItem(this,'X');"><img width="16" height="16" src="images/add.png" border="0"></a></td>
<td width="40">显示</td>
<td width="160" style="min-width:100px; width:auto;">名称</td>
<td width="120">宽度</td>
<td width="90">标题对齐</td>
<td width="90">内容对齐</td>
<td width="80">固定值</td>
<td width="450">取值</td>
<td width="60">顺序</td>
</tr>
</thead>
<tbody eSize="false" eMove="true">
</headertemplate>
<itemtemplate>
<tr erowid="<%#Eval("ReportItemID")%>">
<td height="26" align="center">
<a title="删除列" href="javascript:;" onclick="delReportItem(this,'<%# Eval("ReportItemID") %>');"><img width="16" height="16" src="images/del.png" border="0"></a></td>
<td><input reload="false" type="checkbox" onclick="setReportItem(this,'<%# Eval("ReportItemID") %>','show');"<%# (eBase.parseBool( Eval("show")) ? " checked" : "") %> /></td>
<td><input reload="false" class="text" type="text" value="<%# Eval("MC").ToString() %>" oldvalue="<%# Eval("MC").ToString() %>" onBlur="setReportItem(this,'<%#Eval("ReportItemID") %>','mc');"></td>
<td><input class="text" type="text" value="<%# Eval("width").ToString()%>" oldvalue="<%# Eval("width").ToString()%>" onBlur="setReportItem(this,'<%# Eval("ReportItemID") %>','width');"></td>
<td>
    <select style="width:60px;" onchange="setReportItem(this,'<%# Eval("ReportItemID") %>','titlealign');">
        <option value="left"<%# Eval("titlealign").ToString() == "left" ? " selected=\"true\"" : ""%>>左</option>
        <option value="center"<%# Eval("titlealign").ToString() == "center" ? " selected=\"true\"" : ""%>>中</option>
        <option value="right"<%# Eval("titlealign").ToString() == "right" ? " selected=\"true\"" : ""%>>右</option>
    </select>
</td>
<td>
    <select style="width:60px;" onchange="setReportItem(this,'<%# Eval("ReportItemID") %>','bodyalign');">
        <option value="left"<%# Eval("bodyalign").ToString() == "left" ? " selected=\"true\"" : ""%>>左</option>
        <option value="center"<%# Eval("bodyalign").ToString() == "center" ? " selected=\"true\"" : ""%>>中</option>
        <option value="right"<%# Eval("bodyalign").ToString() == "right" ? " selected=\"true\"" : ""%>>右</option>
    </select>
</td>
<td><input class="text" type="text" value="<%# Eval("value").ToString()%>" oldvalue="<%# Eval("value").ToString()%>" onBlur="setReportItem(this,'<%# Eval("ReportItemID") %>','value');"></td>
<td><input class="text" type="text" value="<%# Eval("valuesql").ToString()%>" oldvalue="<%# Eval("valuesql").ToString()%>" onBlur="setReportItem(this,'<%# Eval("ReportItemID") %>','valuesql');"></td>
<td style="cursor:move;"><%# (Container.ItemIndex + 1)%></td>
</tr>    
</itemtemplate>
<footertemplate></tbody></table></footertemplate>
</asp:Repeater>
</dd>
</dl>
<script>
    function viewReport()
    {
        var url=document.location.href.addquerystring("act","viewreport");
        $.ajax({
            type: 'get',
            url: url,
            dataType: "html",
            success: function(data)
            {
                $("#reportbody").html(data);
            }
        });
    };
</script>
<a class="button" href="javascript:;" onclick="viewReport();"><span><i class="search">预览</i></span></a>
<div id="reportbody" style="box-shadow: 0 0 6px 0 #ccc;border-radius: 5px;padding:10px;margin-top:15px;max-width:600px;min-height:450px;"></div>
</asp:PlaceHolder>
</div>
</asp:Content>
