﻿<%@ Page Language="C#" MasterPageFile="Main.Master" AutoEventWireup="true" CodeFile="Config.aspx.cs" Inherits="eFrameWork.Manage.Config" %>
<%@ Register Src="GroupMenu.ascx" TagPrefix="uc1" TagName="GroupMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<uc1:GroupMenu runat="server" ID="GroupMenu" />
 <script src="javascript/columns.js?ver=<%=Common.Version %>"></script>
<div class="nav">您当前位置：<a href="Default.aspx">首页</a> -> 配置</div>
<div style="margin:6px;line-height:25px;font-size:13px;">
<asp:PlaceHolder ID="eFormControlGroup" runat="server">
<form id="frmaddoredit" name="frmaddoredit" method="post" action="">
<input type="hidden" id="act" name="act" value="save">
<input type="hidden" id="fromurl" name="fromurl" value="">
<input type="hidden" id="id" name="id" value="<%=eform.ID%>">

<!-- 系统设置 -->
<dl class="ePanel">
<dt><h1 onclick="showPanel(this);"><a href="javascript:;" onfocus="this.blur();" class=""></a>系统设置</h1></dt>
<dd style="display: block;">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="eDataView">
<colgroup>
<col width="120" />
<col />
</colgroup>
<tr>
<td class="title"><ins>*</ins>开发平台名称：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F1" Name="F1" ControlType="text" Field="manageName" Width="500px" FieldName="开发平台名称" NotNull="True" runat="server" /></span></td>
</tr>

<tr>
<td class="title"><ins>*</ins>运行平台名称：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F2" Name="F2" ControlType="text" Field="systemName" Width="500px" FieldName="运行平台名称" NotNull="True" runat="server" /></span></td>
</tr>

<tr>
<td class="title">运行平台简介：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F32" Name="F32" ControlType="textarea" Field="systemProfiles" Width="750px" Height="90px;" FieldName="运行平台简介" NotNull="False" htmlTag="true" runat="server" /></span></td>
</tr>

<tr>
<td class="title">授权码：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F18" Name="F18" ControlType="textarea" Field="RegData" Width="750px" Height="90px;" FieldName="注册数据" NotNull="False" runat="server" /></span></td>
</tr>
<tr>
    <td class="title">全局变灰：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F23" Name="F23" ControlType="radio" Field="systemGrey" FieldType="bit" FieldName="全局变灰" Options="[{text:开启,value:1},{text:关闭,value:0}]" NotNull="False" runat="server" /></span></td>

</tr>

</table>
</dd>
</dl>



<!-- 开发设置 -->
<dl class="ePanel">
<dt><h1 onclick="showPanel(this);"><a href="javascript:;" onfocus="this.blur();" class="cur"></a>开发设置</h1></dt>
<dd style="display: none;">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="eDataView">
<colgroup>
<col width="120" />
<col />
<col width="120" />
<col />
</colgroup>
<tr>
<td class="title">调试模式：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F5" Name="F5" ControlType="radio" Field="Debug" FieldType="bit" FieldName="调试模式" Options="[{text:打开,value:1},{text:关闭,value:0}]" NotNull="False" runat="server" /></span></td>
<td class="title">显示帮助：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F6" Name="F6" ControlType="radio" Field="showHelp" FieldType="bit" FieldName="显示帮助" Options="[{text:打开,value:1},{text:关闭,value:0}]" NotNull="False" runat="server" /></span></td>
</tr>

<tr>
<td class="title"><ins>*</ins>上传路径：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F3" Name="F3" ControlType="text" Field="UploadPath" Width="500px" FieldName="上传路径" NotNull="True" runat="server" /></span></td>
<td class="title">保存源文件：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F10" Name="F10" ControlType="radio" Field="saveSource" FieldType="bit" FieldName="保存源文件" Options="[{text:打开,value:1},{text:关闭,value:0}]" NotNull="False" runat="server" /></span></td>
</tr>


    
<tr>
<%
//<td class="title">版本：</td>
//<td class="content"><span class="eform"><ev:eFormControl ID="F24" Name="F24" ControlType="radio" Field="newversion" FieldType="bit" FieldName="版本" Options="[{text:新版本,value:1},{text:稳定版,value:0}]" NotNull="False" runat="server" /></span></td>
%>
<td class="title">资源版本号：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F22" Name="F22" ControlType="text" Field="Version" Width="500px" FieldName="资源版本号" NotNull="False" runat="server" /></span></td>
<td class="title"><ins>*</ins>错误日志：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F13" Name="F13" ControlType="select" Field="ErrorLog" FieldType="int" FieldName="错误日志" Options="[{text:关闭,value:0},{text:文件,value:1},{text:数据库,value:2}]" NotNull="True" runat="server" /></span></td>
</tr>
<tr>
<td class="title">数据日志：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F9" Name="F9" ControlType="radio" Field="DataLog" FieldType="bit" FieldName="数据日志" Options="[{text:打开,value:1},{text:关闭,value:0}]" NotNull="False" runat="server" /></span></td>
<td class="title">保留数量：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F42" Name="F42" ControlType="text" Field="LogCount" Width="500px" FieldName="保留数量" NotNull="False" runat="server" /></span></td>
</tr>

<tr>
<td class="title">登录验证码：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F19" Name="F19" ControlType="radio" Field="openRndCode" FieldType="bit" FieldName="登录验证码" Options="[{text:打开,value:1},{text:关闭,value:0}]" NotNull="False" runat="server" /></span></td>
<td class="title">附件服务器地址：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F15" Name="F15" ControlType="text" Field="AccessorysURL" Width="500px" FieldName="附件服务器地址" NotNull="False" runat="server" /></span></td>
</tr>

<tr>
<td class="title">API toKen密钥：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F16" Name="F16" ControlType="text" Field="toKenSecret" Width="500px" FieldName="toKen密钥" NotNull="False" runat="server" /></span></td>
<td class="title"><ins>*</ins>加密方式：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F17" Name="F17" ControlType="select" Field="passType" FieldName="加密方式" Options="[{text:md5(32位),value:md532},{text:md5(16位),value:md516},{text:sha256,value:sha256}]" NotNull="True" runat="server" /></span></td>
</tr>



<tr>
<td class="title">自动下载文件：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F38" Name="F38" ControlType="radio" Field="autoDownFile" FieldType="bit" FieldName="自动下载文件" Options="[{text:开启,value:1},{text:关闭,value:0}]" NotNull="False" runat="server" /></span></td>
<td class="title">自动删除文件：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F39" Name="F39" ControlType="radio" Field="autoDeleteFile" FieldType="bit" FieldName="自动删除文件" Options="[{text:开启,value:1},{text:关闭,value:0}]" NotNull="False" runat="server" /></span></td>

</tr>

<tr>
<td class="title"><ins>*</ins>角色选择：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F37" Name="F37" ControlType="select" Field="roleControlType" FieldName="角色选择" Options="[{text:多选,value:checkbox},{text:单选,value:radio}]" NotNull="True" runat="server" /></span></td>
<td class="title">登录用户信息：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F44" Name="F44" ControlType="hidden" Field="saveUserInfo" FieldType="string" NotNull="False" Attributes="jsonformat=&quot;保存名称,name;字段编码,value&quot;" runat="server" />
    <img src="images/jsonedit.png" style="cursor:pointer;margin-right:5px;" onclick="Json2_Edit('F44','登录用户信息');" align="absmiddle"></span></td>
</tr>



</table>
</dd>
</dl>

<!-- 安全设置 -->
<dl class="ePanel">
<dt><h1 onclick="showPanel(this);"><a href="javascript:;" onfocus="this.blur();" class="cur"></a>安全设置</h1></dt>
<dd style="display: none;">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="eDataView">
<colgroup>
<col width="120" />
<col />
<col width="120" />
<col />
</colgroup>
<tr>
<td class="title">安全友好错误信息：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F41" Name="F41" ControlType="radio" Field="safeErrors" FieldType="bit" FieldName="安全友好错误信息" Options="[{text:打开,value:1},{text:关闭,value:0}]" NotNull="False" runat="server" /></span></td>
<td class="title">允许跨端口映射：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F34" Name="F34" ControlType="radio" Field="portMapping" FieldType="bit" FieldName="允许跨端口映射" Options="[{text:允许,value:1},{text:不允许,value:0}]" NotNull="False" runat="server" /></span></td>
</tr>

<tr>
<td class="title">登录错误锁定次数：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F20" Name="F20" ControlType="text" Field="LoginErrors" Width="500px" FieldName="登录错误锁定次数" NotNull="False" runat="server" /></span></td>
<td class="title">安全排除目录：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F14" Name="F14" ControlType="text" Field="ExcludeFolders" Width="500px" FieldName="安全排除目录" NotNull="False" runat="server" /></span></td>
</tr>
<tr>
<td class="title">禁止执行目录：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F40" Name="F40" ControlType="text" Field="preventFolders" Width="500px" FieldName="禁止执行目录" NotNull="False" runat="server" /><i>多个目录之间逗号分隔</i></span></td>
<td class="title">登录超时时间：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F33" Name="F33" ControlType="text" Field="loginTimeOut" Width="80px" FieldName="登录超时时间" MinValue="1" NotNull="False" runat="server" /><i>分钟</i></span></td>
</tr>
<tr>
<td class="title">允许文件类型：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F4" Name="F4" ControlType="text" Field="fileExt" Width="500px" FieldName="允许文件类型" NotNull="False" runat="server" /></span></td>
<td class="title">禁止文件类型：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F36" Name="F36" ControlType="text" Field="blackExt" Width="500px" FieldName="禁止文件类型" NotNull="False" runat="server" /></span></td>
</tr>
<tr>
    <td class="title">单点登录：</td>
    <td class="content"><span class="eform"><ev:eFormControl ID="F43" Name="F43" ControlType="radio" Field="singleSign" FieldType="int" FieldName="单点登录" Options="[{text:打开,value:1},{text:关闭,value:0}]" NotNull="False" runat="server" /></span></td>
    <td class="title">默认密码强度：</td>
    <td class="content"><span class="eform"><ev:eFormControl ID="F45" Name="F45" ControlType="text" Field="defaultRank" FieldType="int" FieldName="默认密码强度" NotNull="False" runat="server" /><i>弱:0-9 中:10-19 强:20+</i></span></td>
</tr>
<tr>
    <td class="title">单次令牌有效时长：</td>
    <td class="content" colspan="3"><span class="eform"><ev:eFormControl ID="F46" Name="F46" ControlType="text" Field="singleToKenTime" FieldType="int" FieldName="单次令牌有效时长" NotNull="False" runat="server" /><i>秒</i></span></td> 
</tr>
</table>
</dd>
</dl>



<dl class="ePanel">
<dt><h1 onclick="showPanel(this);"><a href="javascript:;" onfocus="this.blur();" class="cur"></a>网站设置</h1></dt>
<dd style="display: none;">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="eDataView">
<colgroup>
<col width="120" />
<col />
</colgroup>
<tr>
<td class="title"><ins>*</ins>HTML编辑器：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F21" Name="F21" ControlType="select" Field="htmlEditor" FieldName="HTML编辑器" Options="[{text:KindEditor,value:kindeditor},{text:uEditor,value:ueditor}]" NotNull="True" runat="server" /></span></td>
</tr>
<tr>
<td class="title">页面缓存：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F8" Name="F8" ControlType="radio" Field="PageCache" FieldType="bit" FieldName="页面缓存" Options="[{text:打开,value:1},{text:关闭,value:0}]" NotNull="False" runat="server" /><i>CMS功能</i></span></td>
</tr>
<tr>
<td class="title">静态化URL：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F11" Name="F11" ControlType="radio" Field="staticURL" FieldType="bit" FieldName="静态化URL" Options="[{text:打开,value:1},{text:关闭,value:0}]" NotNull="False" runat="server" /><i>CMS功能</i></span></td>
</tr>
<tr>
<td class="title">自动跳转HTTPS：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F35" Name="F35" ControlType="radio" Field="autoHttpsSkip" FieldType="bit" FieldName="自动跳转HTTPS" Options="[{text:跳转,value:1},{text:不跳转,value:0}]" NotNull="False" runat="server" /></span></td>
</tr>
<tr>
    <td class="title">前台缓存地址：</td>
    <td class="content"><span class="eform"><ev:eFormControl ID="F47" Name="F47" Width="360" ControlType="text" Field="sebCacheAddr" FieldType="string" FieldName="前台缓存地址" NotNull="False" runat="server" /></span></td> 
</tr>


</table>
</dd>
</dl>



<dl class="ePanel">
<dt><h1 onclick="showPanel(this);"><a href="javascript:;" onfocus="this.blur();" class="cur"></a>系统字段</h1></dt>
<dd style="display: none;">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="eDataView">
<colgroup>
<col width="120" />
<col />
<col width="120" />
<col />
</colgroup>
<tr>
<td class="title">添加时间字段：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F25" Name="F25" ControlType="text" Field="addTimeTag" Width="200px" FieldName="添加时间字段" NotNull="False" runat="server" /></span></td>
<td class="title">添加用户字段：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F26" Name="F26" ControlType="text" Field="addUserTag" Width="200px" FieldName="添加用户字段" NotNull="False" runat="server" /></span></td>
</tr>

<tr>
<td class="title">编辑时间字段：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F27" Name="F27" ControlType="text" Field="editTimeTag" Width="200px" FieldName="编辑时间字段" NotNull="False" runat="server" /></span></td>
<td class="title">编辑用户字段：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F28" Name="F28" ControlType="text" Field="editUserTag" Width="200px" FieldName="编辑用户字段" NotNull="False" runat="server" /></span></td>
</tr>

<tr>
<td class="title">删除时间字段：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F29" Name="F29" ControlType="text" Field="deleteTimeTag" Width="200px" FieldName="删除时间字段" NotNull="False" runat="server" /></span></td>
<td class="title">删除用户字段：</td>
<td class="content"><span class="eform"><ev:eFormControl ID="F30" Name="F30" ControlType="text" Field="deleteUserTag" Width="200px" FieldName="删除用户字段" NotNull="False" runat="server" /></span></td>
</tr>

<tr>
<td class="title">删除标记字段：</td>
<td class="content" colspan="3"><span class="eform"><ev:eFormControl ID="F31" Name="F31" ControlType="text" Field="deleteTag" Width="200px" FieldName="删除标记字段" NotNull="False" runat="server" /></span></td>
</tr>

</table>
</dd>
</dl>


<table width="100%" border="0" cellpadding="0" cellspacing="0" class="eDataView">
<tr>
<td class="title"  style="text-align:left;padding-left:100px;padding-top:10px;padding-bottom:10px;">
    <a class="button" href="javascript:;" onclick="ajaxSubmit(frmaddoredit);"><span><i class="save">保存</i></span></a>
</td>
</tr>
</table>

</form>
</asp:PlaceHolder>
</div>
</asp:Content>