﻿<%@ Page Language="C#" MasterPageFile="Main.Master" AutoEventWireup="true" CodeFile="Services.aspx.cs" Inherits="eFrameWork.Manage.Services" %>
<%@ Register Src="GroupMenu.ascx" TagPrefix="uc1" TagName="GroupMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<uc1:GroupMenu runat="server" ID="GroupMenu" />
<div class="nav">您当前位置：<a href="Default.aspx">首页</a> -> 服务商管理<a id="btn_add" style="float:right;margin-top:4px;<%=(act == "" ? "" : "display:none;" )%>" class="button" href="<%=edt.getAddURL()%>"><span><i class="add">添加</i></span></a></div>
<%if (eRegisterInfo.Base == 0 && eRegisterInfo.Loaded)
  { %>
    <div style="margin:6px;line-height:25px;font-size:13px;">
<div class="tips" style="margin-bottom:6px;"><b>未授权提示</b><br><a href="http://frame.eketeam.com/getSerialNumber.aspx" style="color:#ff0000;" target="_blank">申请临时授权</a>,享更多功能。</div>
   </div>
 <%} %>
<%
if(act=="edit" || act=="add")
{
%>
<div style="margin:6px;">
 <asp:PlaceHolder ID="eFormControlGroup" runat="server">
<form name="frmaddoredit" id="frmaddoredit" method="post" action="<%=edt.getSaveURL()%>">
	<input name="id" type="hidden" id="id" value="<%=id%>">
    <input name="act" type="hidden" id="act" value="save">  
	<input name="fromurl" type="hidden" id="fromurl" value="<%=edt.FromURL%>">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="eDataView">
      <tr>
        <td width="126" class="title"><font color="#FF0000">*</font> 服务商名称：</td>
        <td class="content"><span class="eform">
		<ev:eFormControl ID="f1" Field="MC" width="300" FieldName="服务商名称"  notnull="true" runat="server" />
		</span></td>
      </tr>
     <tr>
        <td class="title">地址：</td>
        <td class="content"><span class="eform">
		<ev:eFormControl ID="F4" Field="DZ" width="300" FieldName="地址"  notnull="false" runat="server" />
		</span></td>
      </tr>  

     <tr>
        <td class="title">联系人：</td>
        <td class="content"><span class="eform">
		<ev:eFormControl ID="F5" Field="LXR" width="300" FieldName="联系人"  notnull="false" runat="server" />
		</span></td>
      </tr>  
     <tr>
        <td class="title">联系电话：</td>
        <td class="content"><span class="eform">
		<ev:eFormControl ID="F6" Field="LXDH" width="300" FieldName="联系电话"  notnull="false" runat="server" />
		</span></td>
      </tr>  

      <tr>
        <td class="title">说明：</td>
        <td class="content"><span class="eform">
		<ev:eFormControl ID="f7" ControlType="textarea" Field="SM" width="300" FieldName="说明"  notnull="false" runat="server" />
		</span></td>
      </tr>  
  <tr>
        <td class="title">状态：</td>
        <td class="content"><span class="eform">
		<ev:eFormControl ID="f8" ControlType="radio" Field="Active" FieldName="状态" Options="[{text:启用,value:True},{text:停用,value:False}]" DefaultValue="True" notnull="false" runat="server" />
		</span></td>
      </tr>  
 
	 <tr>
       <td colspan="2" class="title"  style="text-align:left;padding-left:100px;padding-top:10px;padding-bottom:10px;">		
		<a class="button" href="javascript:;" onclick="if(frmaddoredit.onsubmit()!=false){frmaddoredit.submit();}"><span><i class="save">保存</i></span></a>
		<a class="button" href="javascript:;" style="margin-left:30px;" onclick="history.back();"><span><i class="back">返回</i></span></a>
		</td>
	   </tr>	
    </table>
	</form>
	</asp:PlaceHolder>
</div>
	<%}else{%>	
<div style="margin:6px;overflow-x:auto;overflow-y:hidden;">
<ev:eListControl ID="eDataTable" Class="eDataTable" CellSpacing="1" LineHeight="32" runat="server" >
    <ev:eListColumn ControlType="text" FieldName="编号" Width="300" runat="server">{data:ID}</ev:eListColumn>
    <ev:eListColumn ControlType="text" Field="MC" FieldName="服务商名称" runat="server" />
    <ev:eListColumn ControlType="text" Field="Active" FieldName="状态" Options="[{text:启用,value:True},{text:停用,value:False}]" runat="server" />
    <ev:eListColumn ControlType="text" Field="SM" FieldName="说明" runat="server" />
    <ev:eListColumn ControlType="text" Field="addTime" FieldName="添加时间" FormatString="{0:yyyy-MM-dd HH:mm:ss}" runat="server" />
    <ev:eListColumn ControlType="text" FieldName="操作" Width="130" runat="server">
    <a href="{base:url}act=edit&id={data:ID}">修改</a>
    <a href="{base:url}act=del&id={data:ID}" onclick="javascript:return confirm('确认要删除吗？');">删除</a>
</ev:eListColumn>
</ev:eListControl>


</div>
<div style="margin:6px;"><ev:ePageControl ID="ePageControl1" PageSize="20" PageNum="9" runat="server" /></div>
<%}%>	
</asp:Content>
