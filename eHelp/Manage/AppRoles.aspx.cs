﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using EKETEAM.Data;
using EKETEAM.FrameWork;


namespace eFrameWork.Manage
{
    public partial class AppRoles : System.Web.UI.Page
    {
        public string act = eParameters.Request("act");
        public eForm eform;
        public eAction Action;
        public string id = eParameters.Request("id");
        public eUser user;
        public eList elist;
        protected void Page_Load(object sender, EventArgs e)
        {           
            user = new eUser("Manage");
            eform = new eForm("a_eke_sysRoles", user);
            eform.DataBase = eBase.UserInfoDB;
            eform.AddControl(eSubForm1);
            //eform.AutoRedirect = false;
            eform.AddControl(eFormControlGroup);
            eform.onChange += new eFormTableEventHandler(eform_onChange);

            Action = new eAction();
            Action.Actioning += new eActionHandler(Action_Actioning);
            Action.Listen();
            if (act == "add" || act == "edit" || act == "copy")
            {
                eBase.clearDataCache("a_eke_sysPowers");
            }
        }
        public void eform_onChange(object sender, eFormTableEventArgs e)
        {
            if (e.eventType == eFormTableEventType.Inserting)
            {
                if (user["ServiceID"].Length > 0) eform.Fields.Add("ServiceID", user["ServiceID"]);
            }
        }
        protected void Action_Actioning(string Actioning)
        {
            switch (Actioning)
            {
                case "":
                    elist = new eList("a_eke_sysRoles");
                    elist.DataBase = eBase.UserInfoDB;
                    elist.Fields.Add("RoleID,MC,SM,addTime");
                    elist.Where.Add("delTag=0");
                    elist.Where.Add("ServiceID" + (user["ServiceID"].Length == 0 ? " is null" : "='" + user["ServiceID"] + "'"));
                    elist.Where.Add(eSearchControlGroup);
                    elist.OrderBy.Default = "addTime";//默认排序
                    elist.Bind(eDataTable, ePageControl1);
                    break;
                default:
                    eform.Handle();
                    break;
            }
            //Response.Write(Actioning + "A");
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (Master == null) return;
            Literal lit = (Literal)Master.FindControl("LitTitle");
            if (lit != null)
            {
                lit.Text = "角色管理 - " + eConfig.manageName(); 
            }
        }
    }
}