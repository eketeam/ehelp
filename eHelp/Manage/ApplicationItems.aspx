﻿<%@ Page Language="C#" MasterPageFile="Main.Master" AutoEventWireup="true" CodeFile="ApplicationItems.aspx.cs" Inherits="eFrameWork.Manage.ApplicationItems" %>
<%@ Register Src="GroupMenu.ascx" TagPrefix="uc1" TagName="GroupMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<uc1:GroupMenu runat="server" ID="GroupMenu" />
<div class="nav">您当前位置：<a href="Default.aspx">首页</a> -> 应用管理 -> <%=appName %></div>
<%if (eRegisterInfo.Base == 0 && eRegisterInfo.Loaded)
  { %>
    <div style="margin:6px;line-height:25px;font-size:13px;">
<div class="tips" style="margin-bottom:6px;"><b>未授权提示</b><br><a href="http://frame.eketeam.com/getSerialNumber.aspx" style="color:#ff0000;" target="_blank">申请临时授权</a>,享更多功能。</div>
   </div>
 <%} %>
<script>
var AppId = "<%=AppId%>";
function showloading()
{
	$("#divloading").show();
};
function hideloading()
{
	$("#divloading").hide();
};
function getValue(obj)
{
	if (obj.type.toLowerCase() == "radio" || obj.type.toLowerCase() == "checkbox") 
	{
		return (obj.checked ? "1" : "0");
    }
	else
	{
		return obj.value.encode();
    }
};
//添加菜单
function addMenu(obj)
{
	var _reload=parseBool(Attribute(obj,"reload"));
	var _url = "?act=addmenu&AppId=" + AppId + "&t=" + now();
	showloading();
	$.ajax({
		type: "GET", async: true,
        url: _url,
        dataType: "json",
        success: function (data)
        {
			hideloading();
			//if(!_reload){return;}
            loadData();
        },
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			hideloading();
		}	
    });
};
//修改菜单
function setMenu(obj, ApplicationMenuID, Item)
{
	var _reload=parseBool(Attribute(obj,"reload"));
	if (obj.getAttribute("oldvalue") == obj.value) { return; }
	var value = getValue(obj);
	if (value == "error") { return; }
	showloading();
    var url = "?act=setmenu&AppId=" + AppId + "&ApplicationMenuID=" + ApplicationMenuID + "&item=" + Item + "&value=" + value + "&t=" + now();
    $.ajax({
		type: "GET", async: true,
		url: url,
		dataType: "html",
		success: function (data)
		{
		    if (Item == "finsh")
		    {
		        obj.style.color = obj.value == 1 ? "#33cc00" : "#FF0000";
		    }
			hideloading();
			if(!_reload){return;}
            loadData();
        },
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			hideloading();
		}		
	});
};
//删除菜单
function delMenu(obj, ApplicationMenuID)
{
	var _reload=parseBool(Attribute(obj,"reload"));
	if (!confirm('确认要删除吗？')) { return; }
	showloading();
	var _url = "?act=delmenu&AppId=" + AppId + "&ApplicationMenuID=" + ApplicationMenuID + "&t=" + now();
    $.ajax({
		type: "GET", async: true,
		url: _url,
		dataType: "html",
        success: function (data) {
			hideloading();
			$(getParent(obj,"tr")).remove();
			//if(!_reload){return;}
			//loadData();
        },
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			hideloading();
		}
    });
};
//添加图标
function add_Icon(obj)
{
	var _reload=parseBool(Attribute(obj,"reload"));
	var _url = "?act=addicon&AppId=" + AppId + "&t=" + now();
	showloading();
	$.ajax({
		type: "GET", async: true,
        url: _url,
        dataType: "json",
        success: function (data)
        {
			hideloading();
			//if(!_reload){return;}
            loadData();
        },
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			hideloading();
		}	
    });
};
//修改图标
function set_Icon(obj, ApplicationIconID, Item)
{
	var _reload=parseBool(Attribute(obj,"reload"));
	if (obj.getAttribute("oldvalue") == obj.value) { return; }
	var value = getValue(obj);
	if (value == "error") { return; }
	showloading();
    var url = "?act=seticon&AppId=" + AppId + "&ApplicationIconID=" + ApplicationIconID + "&item=" + Item + "&value=" + value + "&t=" + now();
    $.ajax({
		type: "GET", async: true,
		url: url,
		dataType: "html",
		success: function (data)
		{
			hideloading();
			if(!_reload){return;}
            loadData();
        },
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			hideloading();
		}		
	});
};
//删除图标
function del_Icon(obj, ApplicationIconID)
{
	var _reload=parseBool(Attribute(obj,"reload"));
	if (!confirm('确认要删除吗？')) { return; }
	showloading();
	var _url = "?act=delicon&AppId=" + AppId + "&ApplicationIconID=" + ApplicationIconID + "&t=" + now();
    $.ajax({
		type: "GET", async: true,
		url: _url,
		dataType: "html",
        success: function (data) {
			hideloading();
			$(getParent(obj,"tr")).remove();
			//if(!_reload){return;}
			//loadData();
        },
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			hideloading();
		}
    });
};



function addApp(obj)
{
	var _reload=parseBool(Attribute(obj,"reload"));
	var _url = "?act=addapp&AppId=" + AppId + "&t=" + now();
	showloading();
	$.ajax({
		type: "GET", async: true,
        url: _url,
        dataType: "json",
        success: function (data)
        {
			hideloading();
			//if(!_reload){return;}
            loadData();
        },
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			hideloading();
		}	
    });
};
function setApp(obj, ApplicationItemID, Item)
{
	var _reload=parseBool(Attribute(obj,"reload"));
	if (obj.getAttribute("oldvalue") == obj.value) { return; }
	var value = getValue(obj);
	if (value == "error") { return; }
	showloading();
    //var url = "?act=setapp&AppId=" + AppId + "&ApplicationItemID=" + ApplicationItemID + "&item=" + Item + "&value=" + value + "&t=" + now();
	var url = "?act=setapp&AppId=" + AppId + "&ApplicationItemID=" + ApplicationItemID + "&item=" + Item + "&t=" + now();
    $.ajax({
		type: "POST", async: true,
		url: url,
		data:{value:value},
		dataType: "html",
		success: function (data)
		{
		    if (Item == "finsh")
		    {
		        obj.style.color = obj.value == 1 ? "#33cc00" : "#FF0000";
		    }
			hideloading();
			if(!_reload){return;}
            loadData();
        },
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			hideloading();
		}		
	});
};
function postApp(obj, ApplicationItemID, Item)
{
	var _reload=parseBool(Attribute(obj,"reload"));
	if (obj.getAttribute("oldvalue") == obj.value) { return; }
	var value = getValue(obj);
	if (value == "error") { return; }
	showloading();
    var url = "?act=setapp&AppId=" + AppId + "&ApplicationItemID=" + ApplicationItemID + "&item=" + Item + "&t=" + now();
    $.ajax({
		type: "Post", async: true,
		data:{value:value},
		url: url,
		dataType: "html",
		success: function (data)
		{
		    if (Item == "finsh")
		    {
		        obj.style.color = obj.value == 1 ? "#33cc00" : "#FF0000";
		    }
			hideloading();
			if(!_reload){return;}
            loadData();
        },
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			hideloading();
		}		
	});
};
//双击修改
function dbledit(obj)
{
	var value=$(obj).prev().val();
	
	layer.open({
      type: 1,
	  title: '扩展属性',
	  scrollbar: false,
      area: ['600px', '360px'],
      shadeClose: true, //点击遮罩关闭
      //content: '<div style="width:100%;height:100%;overflow:hidden;padding:10px;"><textarea name="textareabody" id="textareabody" style="padding:5px;font-size:12px;line-height:20px;width:95%;height:85%;border:1px solid #ccc;">' + value + '</textarea></div>',
	  content: '<textarea name="textareabody" id="textareabody" style="margin:10px;padding:5px;font-size:12px;line-height:20px;min-width:90%;min-height:75%;border:1px solid #ccc;">' + value + '</textarea>',
	  btn: ['确定', '取消'],
	  yes: function(index,layero)
	  {
	  	var nvalue=$("#textareabody").val();
		if($(obj).prev().get(0).tagName=="TEXTAREA"){nvalue=nvalue.replace(/\n/g, "");}
		else{nvalue=nvalue.replace(/\n/g, " ");}
		$(obj).prev().val(nvalue);
		$(obj).prev().get(0).onblur();
		arrLayerIndex.pop();
		layer.close(index);
	  },
      cancel: function (index, layero) 
	  {
	  	arrLayerIndex.pop();
      },
	  success: function (layero, index)
	  {
	  	arrLayerIndex.push(index);
      }
    });
};
//双击修改
function dblClick(obj, title) {
    var type = obj.type;
    var value = obj.value;
    layer.open({
        type: 1,
        title: title,
        scrollbar: false,
        area: ['600px', '360px'],
        shadeClose: true, //点击遮罩关闭
        //content: '<div style="width:100%;height:100%;overflow:hidden;padding:10px;"><textarea name="textareabody" id="textareabody" style="padding:5px;font-size:12px;line-height:20px;width:95%;height:85%;border:1px solid #ccc;">' + value + '</textarea></div>',
        content: '<textarea name="textareabody" id="textareabody" style="margin:10px;padding:5px;font-size:12px;line-height:20px;min-width:90%;min-height:75%;border:1px solid #ccc;">' + value + '</textarea>',
        btn: ['确定', '取消'],
        yes: function (index, layero) {
            var nvalue = $("#textareabody").val();
            if (obj.tagName == "TEXTAREA") { nvalue = nvalue.replace(/\n/g, ""); }
            else { nvalue = nvalue.replace(/\n/g, " "); }
            obj.value = nvalue;
            obj.onblur();
            arrLayerIndex.pop();
            layer.close(index);
        },
        cancel: function (index, layero) {
            arrLayerIndex.pop();
        },
        success: function (layero, index) {
            arrLayerIndex.push(index);
        }
    });
};
function delApp(obj, ApplicationItemID)
{
	var _reload=parseBool(Attribute(obj,"reload"));
	if (!confirm('确认要删除吗？')) { return; }
	showloading();
	var _url = "?act=delapp&AppId=" + AppId + "&ApplicationItemID=" + ApplicationItemID + "&t=" + now();
    $.ajax({
		type: "GET", async: true,
		url: _url,
		dataType: "html",
        success: function (data) {
			hideloading();
			$(getParent(obj,"tr")).remove();
			//if(!_reload){return;}
			//loadData();
        },
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			hideloading();
		}
    });
};
function loadData()
{
	showloading();
	var url = "ApplicationItems.aspx?AppId=" + AppId + "&act=getdata&t=" + now();
	$.ajax({
		type: "GET", async: true,
        url: url,
        dataType: "html",
        success: function (data) {
			hideloading();
            $("#content").html(data);
			bindEvent();
        },
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			hideloading();
		}
    });
};
function bindEvent()
{
	var tb2=getobj("eDataTable_Menus");
	if(tb2)
	{
		tb2=new eDataTable("eDataTable_Icons",1);
		tb2.moveRow=function(index,nindex)
		{
			var count=$("#eDataTable_Icons tr").length;
			var cur=$("#eDataTable_Icons tr:eq("+ nindex +")");
			var idx=0;
			if(nindex<count-1)
			{
				idx=nindex+1;
			}
			else
			{
				idx=nindex-1;
			}	
			var obj=$("#eDataTable_Icons tr:eq("+ idx +")");
			
			var url = "?act=seticonorders&AppId=" + AppId ;		
			url+="&t=" + now();	

			var ids="";
			$("#eDataTable_Icons tbody tr").each(function(index1,node){	
				if(index1>0){ids+=",";}
				ids+=$(node).attr("erowid");
			}); 
			if(ids.length==0){return;}

			$.ajax({
				type: "POST", async: true,
				data:{ids:ids},
				url: url,
				dataType: "json",
				success: function (data) 
				{
					hideloading();
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					hideloading();
				}
   			});
			
		};
	}
	var tb1=getobj("eDataTable_Menus");
	if(tb1)
	{
		tb1=new eDataTable("eDataTable_Menus",1);
		tb1.moveRow=function(index,nindex)
		{
			var count=$("#eDataTable_Menus tr").length;
			var cur=$("#eDataTable_Menus tr:eq("+ nindex +")");
			var idx=0;
			if(nindex<count-1)
			{
				idx=nindex+1;
			}
			else
			{
				idx=nindex-1;
			}	
			var obj=$("#eDataTable_Menus tr:eq("+ idx +")");
			cur.find("td:eq(2)").css("text-indent", obj.find("td:eq(2)").css("text-indent"));
			
			var url = "?act=setmenuorders&AppId=" + AppId + "&AppMenu=" + cur.attr("erowid");
			var oldpid= cur.attr("parentid");
			var newpid= obj.attr("parentid");
			if(oldpid!=newpid)
			{
				url+="&parentid=" + newpid;
				cur.attr("parentid",newpid);
			}			
			url+="&t=" + now();	
			var ids="";
			$("#eDataTable_Menus tr[parentid='" + newpid + "']").each(function(index1,node){	
				if(index1>0){ids+=",";}
				ids+=$(node).attr("erowid");
			}); 
			if(ids.length==0){return;}

			$.ajax({
				type: "POST", async: true,
				data:{ids:ids},
				url: url,
				dataType: "json",
				success: function (data) 
				{
					hideloading();
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					hideloading();
				}
   			});
			
		};
	}
	var tb=getobj("eDataTable_Items");
	if(tb)
	{
	    tb = new eDataTable("eDataTable_Items", 1);
	    tb.onclick = function (obj)
	    {
	        showitems(obj);
	    };
		tb.moveRow=function(index,nindex)
		{
			var count=$("#eDataTable_Items tr").length;
			var cur=$("#eDataTable_Items tr:eq("+ nindex +")");
			
			//outText(cur.find("td:eq(1) span").html() + "::" + cur.attr("parentid") + "::"+ cur.find("td:eq(1)").css("text-indent"));		

			
			//if(nindex<index){document.title="上移";}else{document.title="下移";}
			var idx=0;
			if(nindex<count-1)
			{
				idx=nindex+1;
			}
			else
			{
				idx=nindex-1;
			}

		    //var obj=$("#eDataTable_Items tr:eq("+ idx +")");
			var obj;
			if (nindex < count - 1)
			{
			    obj = cur.nextAll(":visible:eq(0)");
			}
			else
			{
			    obj = cur.prevAll(":visible:eq(0)");
			}
			
			//alert(obj.length + "::" + obj.find("td:eq(2) input").val());
			//return;
			
			
			var url = "?act=setorders&AppId=" + AppId + "&AppItem=" + cur.attr("erowid");
			var oldpid= cur.attr("parentid");
			var newpid = obj.attr("parentid");
			//alert(nindex + "::" + oldpid + "::" + idx + "::" + newpid );
			
			if(oldpid!=newpid)
			{
				url+="&parentid=" + newpid;
				cur.attr("parentid", newpid);

				cur.find("td:eq(2)").css("text-indent", obj.find("td:eq(2)").css("text-indent"));
				cur.find("td:eq(2) input").css("width", obj.find("td:eq(2) input").css("width"));
			}
			url+="&t=" + now();	
			var ids="";
			$("#eDataTable_Items tr[parentid='" + newpid + "']").each(function(index1,node){	
				if(index1>0){ids+=",";}
				ids+=$(node).attr("erowid");
			}); 
			if (ids.length == 0) { return; }
			var erowid = cur.attr("erowid");
			var rens = $("#eDataTable_Items").find("tr[parentid='" + erowid + "']");
			rens.each(function (i, elem) {			  
			    $(elem).insertAfter(cur);
			    cur = $(elem);
			});
			//return;
			//alert(ids);
			//return;
			//alert($("#eDataTable_Items tr[parentid='" + newpid + "']").length);
			//alert(url);
			//return;
			//outText(obj.find("td:eq(1) span").html() + "::" + obj.attr("parentid") + "::"+ obj.find("td:eq(1)").css("text-indent"));
			
			$.ajax({
				type: "POST", async: true,
				data:{ids:ids},
				url: url,
				dataType: "json",
				success: function (data) 
				{
					hideloading();
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					hideloading();
				}
   			});
			
			
			
		};
		tb.moveRow2=function(index,nindex)
		{
			$("#eDataTable_Items tbody tr td:last-child").each(function(index1,obj){
				$(obj).html(1+index1);
			}); 
			var ids="";
			$("#eDataTable_Items tbody tr td:nth-child(2) input:checked").each(function(index1,obj){	
				if(index1>0){ids+=",";}
				ids+=$(obj).parent().parent().attr("erowid");
			}); 
			if(ids.length==0){return;}
			alert(ids);
			return;
			showloading();
			var url = "?act=setorders&AppId=" + AppId + "&t=" + now();	
			$.ajax({
				type: "POST", async: true,
				data:{ids:ids},
				url: url,
				dataType: "json",
				success: function (data) 
				{
					hideloading();
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
					hideloading();
				}
   			});
		};
	}
};
var icon_a = null;
function openIconForm(obj,path)
{
    icon_a = obj;
    var url = "../Plugins/IconSelect.aspx";
    if (path && path.length > 0) { url += "?path=" + path; }
    layer.open({
        type: 2
      , title: "选择图标"
      , shadeClose: true //点击遮罩关闭层
      , area: ["80%", "80%"]
      , content: url
      , success: function (layero, index) {
          arrLayerIndex.push(index);
      }
      , cancel: function (index, layero) {
          arrLayerIndex.pop();
      }

    });
};
function setIcon(path) {

    var img = $(icon_a).parent().find("img:eq(0)");
    img.attr("src", "../" + path);
    var input = $(icon_a).parent().find("input:eq(0)");
    input.val(path);
    //input.get(0).onBlur();
    input.trigger('blur');
    layer.close(arrLayerIndex.pop());
};
$(document).ready(function () {
	bindEvent();
});

var obj_a=null;
function selectModel(obj)
{
    obj_a = obj;
    var url = "setModel.aspx";
    layer.open({
        type: 2
      , title: "选择模块"
      , shadeClose: true //点击遮罩关闭层
      , area: ["80%", "90%"]

      , content: url

      , success: function (layero, index) {
          arrLayerIndex.push(index);
      }
      , cancel: function (index, layero) {
          arrLayerIndex.pop();
      }

    });
};
function setModel(modelid)
{
    var input = $(obj_a).parent().find("input:eq(0)");
    input.val(modelid);
    input.trigger('blur');
    layer.close(arrLayerIndex.pop());
};
function clearModel(obj)
{
    var input = $(obj).parent().find("input:eq(0)");
    input.val('NULL');
    input.trigger('blur');
};



function defaultValue_Add(objid) {
    var obj = $("#" + objid);
    var jsonstr = obj.attr("options");
    var json = jsonstr.toJson();
    var html = '<tr>';
    html += '<td height="30" align="center"><img src="images/del.png" style="cursor:pointer;" onclick="defaultValue_Delete(this);" /></td>';
    html += '<td>';
    var operator = "=";
    html += '<select name="name">';
    html += '<option value="">请选择</option>';
    json.foreach(function (e)
    {        
        html += '<option value="' + json[e].value + '">' + json[e].text + '</option>';
    });
    html += '</select>';
    html += '</td>';
    html += '<td><input type="text" name="value" value="" style="border:0px;background-color:transparent;width:100%;" /></td>';

    var len = $("#JsonTable tbody > tr").length + 1;
    html += '<td style="cursor:move;">' + len + '</td>';
    html += '</tr>';
    $("#JsonTable tbody").append(html);
    var tb = new eDataTable("JsonTable", 1);
};
//Json编辑-删除
function defaultValue_Delete(obj) {
    if (!confirm('确认要删除吗？')) { return; }
    $(obj).parents("tr:first").remove();
    $("#JsonTable tbody > tr > td:last-child").each(function (index1, obj) {
        $(obj).html(1 + index1);
    });
};
//Json编辑
function defaultValue_Edit(objid, title) {
  
    var obj = $("#" + objid);
    var jsonstr = obj.attr("options");
    var json = jsonstr.toJson();
    var valuestr = obj.val().length < 3 ? "[]" : obj.val();
    var values = valuestr.toJson();



    var html = '<table id="JsonTable" class="eDataTable" border="0" cellpadding="0" cellspacing="1" width="500" style="margin:10px;">';
    html += '<colgroup>';
    html += '<col width="35" />';
    html += '<col width="80" />';
    html += '<col width="90" />';
    html += '<col width="35" />';
    html += '</colgroup>';
    html += '<thead>';
    html += '<tr>';
    html += '<td height="30" align="center"><img src="images/add.png" style="cursor:pointer;" onclick="defaultValue_Add(\'' + objid + '\');" /></td>';
    html += '<td>列名</td>';
    html += '<td>默认值</td>';
    html += '<td>顺序</td>';
    html += '</tr>';
    html += '</thead>';
    html += '<tbody eMove="true">';
    for (var j = 0; j < values.length; j++) {
        html += '<tr>';
        html += '<td height="30" align="center"><img src="images/del.png" style="cursor:pointer;" onclick="defaultValue_Delete(this);" /></td>';
        html += '<td>';
        var operator = "=";
        html += '<select name="name">';
        html += '<option value="">请选择</option>';
        json.foreach(function (e) {
            html += '<option value="' + json[e].value + '"' + (json[e].value.toLowerCase() == values[j]["name"].toLowerCase() ? ' selected="selected"' : '') + '>' + json[e].text + '</option>';
        });
        html += '</select>';
        html += '</td>';

        html += '<td><input type="text" name="value" value="' + values[j]["value"] + '" style="border:0px;background-color:transparent;width:100%;" /></td>';
        html += '<td style="cursor:move;">' + (j + 1) + '</td>';
        html += '</tr>';
    }
    html += '</tbody>';
    html += '</table>';
    layer.open({
        type: 1,
        title: title ? title : "选项",
        scrollbar: false,
        area: ['550px', '60%'],
        shadeClose: true, //点击遮罩关闭
        content: html,
        btn: ['确定', '取消'],
        yes: function (index, layero) {

            var hasnull = false;
            var _json = '[';
            var _html = '';
            $("#JsonTable tbody tr").each(function (index1, objtr) {
                if (index1 > 0) { _json += ','; }
                _json += '{';
                $(objtr).find("input,select").each(function (index2, input) {

                    if (input.value.length == 0) { hasnull = true; }
                    if (index2 > 0) { _json += ','; }
                    if (input.name == "name") { _html += '<span style="display:inline-block;margin-right:6px;border:1px solid #ccc;padding:3px 12px 3px 12px;">' + $(input).find("option:selected").text() + '</span>'; }

                    _json += '"' + input.name + '":"' + input.value.replace(/\"/g, "&quot;") + '"';
                });
                _json += '}';
            });
            if (hasnull) { alert("数据不能为空!"); return; }
            _json += ']';
            if (_json.length == 0) { _json = ''; }
            //alert(obj.next().get(0).tagName + "::" + _html + "::" + _json);
            obj.parent().find("span").remove();
            obj.next().after(_html);
            obj.val(_json);
            obj.get(0).onblur();

            layer.close(index);
        },
        cancel: function (index, layero) {
            //arrLayerIndex.pop();
        },
        success: function (layero, index) {

            //arrLayerIndex.push(index);
            var tb = getobj("JsonTable");
            if (tb) {
                tb = new eDataTable("JsonTable", 1);
                tb.moveRow = function (index, nindex) {
                    $("#JsonTable tbody > tr > td:last-child").each(function (index1, obj) {
                        $(obj).html(1 + index1);
                    });
                };
            }

        }
    });

};
var fspan = null;
function SelectICO(obj) {
    fspan = obj.previousSibling;
    var url = "../Plugins/FontIco.aspx";
    layer.open({
        type: 2
      , title: "选择图标"
      , shadeClose: true //点击遮罩关闭层
      , area: ["80%", "80%"]

      , content: url

      , success: function (layero, index) {
          arrLayerIndex.push(index);
      }
      , cancel: function (index, layero) {
          arrLayerIndex.pop();
      }

    });

};
function setIco(obj) {
    
    fspan.innerHTML = obj.innerHTML.replace("<i></i>", "");
    var input = fspan.previousSibling;
    input.value = obj.innerHTML;
    $(input).trigger('blur');
    layer.close(arrLayerIndex.pop());
};
function showitems(tr)
{
    var src = getEventObject();
    if (src.tagName == "A")
    {
        if (src.className != "open" && src.className != "close") {
            return;
        }
    }
    if (src.tagName == "TD" || src.tagName == "A") {

        var erowid = $(tr).attr("erowid");
        var rens = $("#eDataTable_Items").find("tr[parentid='" + erowid + "']");
        if (rens.length > 0)
        {
            var ico = $(tr).find("td:eq(2) a");
            var cls = ico.attr("class");
            if (cls == "open") {
                rens.hide();
                ico.attr("class", "close");
            }
            else {
                rens.show();
                ico.attr("class", "open");
            }
        }
    }
};
</script>
<style>
.text{display:inline-block;width:100%;border:1px solid #ccc;font-size:12px;height:23px;line-height:23px;padding-left:4px;}
.divloading{filter:alpha(opacity=50);-moz-opacity:0.5;-khtml-opacity: 0.5; opacity: 0.5; position:fixed;width:100%;height:100%;top:0px;left:0px;background:#cccccc url(images/loading.gif) no-repeat center center;}

.eDataTable td * {vertical-align:middle;}
a.close, a.open {border:0px solid #000;display:inline-block;width:18px;height:18px;}
a.close {background:url("../images/close.gif") no-repeat left center;}
a.open {background:url("../images/open.gif") no-repeat left center;}
.editmodel{display:inline-block;width:23px;height:23px;ma4rgin-left:3px;
 background:url("images/jsonedit.png") no-repeat left center;}
</style>
<div id="divloading" class="divloading" style="display:none;">&nbsp;</div>
<div style="margin:6px;line-height:23px;"><b>功能模块</b></div>
<div id="content" style="margin:6px;border:0px solid #ff0000;">
<asp:Literal id="litBody" runat="server" />
</div>



</asp:Content>
