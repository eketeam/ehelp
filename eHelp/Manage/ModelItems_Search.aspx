﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModelItems_Search.aspx.cs" Inherits="eFrameWork.Manage.ModelItems_Search" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
 <title><%=eConfig.manageName() %></title>
</head>
<body>

<asp:Repeater id="Rep" runat="server">
<headertemplate>
<%#
"<table id=\"eDataTable_Search\" class=\"eDataTable\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" width=\"99%\" style=\"min-width:1690px;\">" +
"<thead>" +
"<tr>" +
"<td width=\"30\" bgc6olor=\"#ffffff\" align=\"center\"><a title=\"添加条件\" href=\"javascript:;\" onclick=\"addModelCondition(this);\"><img width=\"16\" height=\"16\" src=\"images/add.png\" border=\"0\"></a></td>" +
"<td width=\"50\">显示" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(108);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"80\">表单Name" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(109);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"90\">条件名称" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(110);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"180\">输出控件" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(111);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"80\">条件列" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(114);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td width=\"80\">操作符" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(115);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
"<td style=\"min-width:400px;\">选项" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(116);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +
//"<td width=\"60\">合并列</td>" +
"<td width=\"70\">禁用条件</td>" +
"<td width=\"70\">默认值</td>" +

"<td width=\"200\">其他属性</td>" +
"<td width=\"100\">说明</td>" +
"<td width=\"60\">顺序" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(120);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "</td>" +

"</tr>" +
"</thead>"+
"<tbody eSize=\"false\" eMove=\"true\">"
%>
</headertemplate>
<itemtemplate>
<%#
"<tr" + ((Container.ItemIndex + 1) % 2 == 0 ? " class=\"alternating\" eclass=\"alternating\"" : " eclass=\"\"") + " erowid=\"" + Eval("ModelConditionID") + "\" onmouseover=\"this.className='cur';\" onmouseout=\"this.className=this.getAttribute('eclass');\" >" +
"<td align=\"center\"><a title=\"删除条件\" href=\"javascript:;\" onclick=\"delModelCondition(this,'" + Eval("ModelConditionID") + "');\"><img width=\"16\" height=\"16\" src=\"images/del.png\" border=\"0\"></a></td>" +
"<td><input reloadbak=\"true\" type=\"checkbox\" onclick=\"setModelCondition(this,'" + Eval("ModelConditionID") + "','show');\"" + (eBase.parseBool(Eval("show")) ? " checked" : "") + " /></td>" +
"<td>s" + Eval("num") + "</td>" +
"<td><input class=\"text\" type=\"text\" value=\""+ Eval("MC") + "\" oldvalue=\""+ Eval("MC") + "\" onBlur=\"setModelCondition(this,'" + Eval("ModelConditionID") + "','mc');\" /></td>" +
"<td>" +"<select reload=\"true\" onChange=\"setModelCondition(this,'" + Eval("ModelConditionID") + "','controltype');\" style=\"width:65px;\">" + eBase.getSearchControlType(Eval("ControlType").ToString())+"<select>"+
"<select onChange=\"setModelCondition(this,'" + Eval("ModelConditionID") + "','multiselect');\" style=\"width:65px;" + (Eval("ControlType").ToString()=="link" ? "" : "display:none;") + "\">" +
"<option value=\"0\"" + (Eval("multiselect").ToString()=="False" ? " selected=\"true\"" : "") + ">单选</option>"+
"<option value=\"1\"" + (Eval("multiselect").ToString()=="True" ? " selected=\"true\"" : "") + ">多选</option>"+
"<select>"+

"<span style=\"width:60px;"+(Eval("ControlType").ToString()=="listtree" ? "" : "display:none;")+"\">" + 
"<input class=\"text\" style=\"width:45px;\" type=\"text\" title=\"显示层级\" value=\""+ Eval("showLevel") + "\" oldvalue=\""+ Eval("showLevel") + "\" onBlur=\"setModelCondition(this,'" + Eval("ModelConditionID") + "','showLevel');\" />" + 
"</span>"+
"<select onChange=\"setModelCondition(this,'" + Eval("ModelConditionID") + "','dateformat');\" style=\"width:100px;"+(Eval("ControlType").ToString()=="date" || Eval("ControlType").ToString()=="dateregion" ? "" : "display:none;")+"\">" +
"<option>无</option>" +
"<option value=\"yyyy-MM-dd\"" + (Eval("dateformat").ToString()=="yyyy-MM-dd" ? " selected=\"true\"" : "") + ">yyyy-MM-dd</option>" +
"<option value=\"yyyy-MM-dd HH\"" + (Eval("dateformat").ToString()=="yyyy-MM-dd HH" ? " selected=\"true\"" : "") + ">yyyy-MM-dd HH</option>" +
"<option value=\"yyyy-MM-dd HH:mm\"" + (Eval("dateformat").ToString()=="yyyy-MM-dd HH:mm" ? " selected=\"true\"" : "") + ">yyyy-MM-dd HH:mm</option>" +
"<option value=\"yyyy-MM-dd HH:mm:ss\"" + (Eval("dateformat").ToString()=="yyyy-MM-dd HH:mm:ss" ? " selected=\"true\"" : "") + ">yyyy-MM-dd HH:mm:ss</option>" +
"<select>"+
"</td>" +

"<td>"
%>
<%if(ModelInfo["Type"].ToString()!="10"){%>
<select onChange="setModelConditionColumn(this,'<%# Eval("ModelConditionID")%>','code');" style="width:75px;<%# getDisplay(Eval("ModelConditionID").ToString())%>">
	<option value="" modelid="<%# Eval("ModelID")%>">无</option>
	<asp:Literal id="LitColnums" runat="server" />
</select>
<%}else {%>
<input class="text" type="text" value="<%# Eval("code")%>" oldvalue="<%# Eval("code")%>" onBlur="setModelCondition(this,'<%# Eval("ModelConditionID")%>','code');" />
<%} %>
<%#
"</td>" +
"<td>"+
"<select reload=\"true\" onChange=\"setModelCondition(this,'" + Eval("ModelConditionID") + "','operator');\" style=\"width:75px;" + getDisplay(Eval("ModelConditionID").ToString()) + "\">" +
eBase.getOperator(Eval("operator").ToString())+"<select>" +
"</td>" +
"<td>" +


"<a style=\"" + ( ",text,yearmonthtree,quick,date,dateregion,numberregion,".Contains("," + Eval("ControlType").ToString().ToLower() + ",") ? "display:none;" : "") + "\" href=\"javascript:;\" onclick=\"loadColumnOptions(this,'" + Eval("ModelConditionID") + "');\">同步</a>"+


"<span style=\""+(Eval("operator").ToString()=="custom" ? "" : "display:none;")+"\">自定义查询："+
"<input class=\"text\" type=\"text\" oldvalue=\""+ Eval("Custom") + "\" value=\""+ Eval("Custom") + "\" ondblclick=\"dblClick(this,'" + Eval("MC") + "-自定义');\" onBlur=\"setModelCondition(this,'" + Eval("ModelConditionID") + "','custom');\" style=\"width:300px;\"><br></span>"
%>
<span<%# ( Eval("ControlType").ToString()=="yearmonthtree" && Eval("Code").ToString().Length==0 ? "" : " style=\"display:none;\"")%>>
 选项：<input id="search_option_<%# Eval("ModelConditionID").ToString().Replace("-","") %>" jsonformat="[{&quot;text&quot;:&quot;文本&quot;,&quot;value&quot;:&quot;text&quot;},{&quot;text&quot;:&quot;值&quot;,&quot;value&quot;:&quot;value&quot;}]" type="text" value="<%# HttpUtility.HtmlEncode(Eval("options").ToString())%>" class="edit" style="width:300px;display:none;" onDblClick="dblClick(this,'<%# Eval("MC")%>-选项');" onBlur="setModelCondition(this,'<%# Eval("ModelConditionID")%>','options');" />
    <img src="images/jsonedit.png" align="absmiddle" style="cursor:pointer;" onClick="Json_Edit('search_option_<%# Eval("ModelConditionID").ToString().Replace("-","") %>','选项');">
     <%# getJsonText(Eval("options").ToString(),"text")%>
</span>
<span<%# ((Eval("ControlType").ToString()=="text" || Eval("ControlType").ToString()=="yearmonthtree" || Eval("ControlType").ToString()=="date" || Eval("ControlType").ToString()=="quick" || Eval("ControlType").ToString()=="dateregion" || Eval("ControlType").ToString()=="numberregion" || Eval("ControlType").ToString()=="") ? " style=\"display:none;\"" : "")%>>
<span id="spanbind" runat="server">
 
    源<select reload="true" onChange="setModelCondition(this,'<%# Eval("ModelConditionID")%>','datasourceid');" style="width:60px;">
    <option value="">无</option>
    <option value="maindb"<%# (Eval("DataSourceID").ToString()=="maindb" ? " selected=\"true\"" : "")%>>主库</option>
     <%# eBase.DataBase.getOptions("SELECT DataSourceID as value,MC as text FROM a_eke_sysDataSources where delTag=0 order by addTime", "text", "value", Eval("DataSourceID").ToString())%>
   </select>
    表<select reload="true" onChange="setModelCondition(this,'<%# Eval("ModelConditionID")%>','bindobject');" style="width:80px;">
	<option value="NULL">无</option>
	<asp:Literal id="LitObjects" runat="server" />
	</select>
	行
	<input class="text" type="text" value="<%# (Eval("bindrows").ToString()=="0" ? "" : Eval("bindrows").ToString())%>" style="width:40px;" onBlur="setModelCondition(this,'<%# Eval("ModelConditionID")%>','bindrows');" />
	值
	<select onChange="setModelCondition(this,'<%# Eval("ModelConditionID")%>','bindvalue');" style="width:80px;">
	<option value="NULL">无</option>
	<asp:Literal id="LitValue" runat="server" />
	</select>
	文本
	<select onChange="setModelCondition(this,'<%# Eval("ModelConditionID")%>','bindtext');" style="width:80px;">
	<option value="NULL">无</option>
	<asp:Literal id="LitText" runat="server" />
	</select><br />
	条件
	<input class="text" type="text" value="<%# Eval("bindcondition")%>" style="width:80px;" onDblClick="dblClick(this,'<%# Eval("MC")%>-条件');" onBlur="setModelCondition(this,'<%# Eval("ModelConditionID")%>','bindcondition');" />
	分组
	<input class="text" type="text" value="<%# Eval("bindgroupby")%>" style="width:80px;" onDblClick="dblClick(this,'<%# Eval("MC")%>-分组');" onBlur="setModelCondition(this,'<%# Eval("ModelConditionID")%>','bindgroupby');" />
	排序
	<input class="text" type="text" value="<%# Eval("bindorderby")%>" style="width:80px;" onDblClick="dblClick(this,'<%# Eval("MC")%>-排序');" onBlur="setModelCondition(this,'<%# Eval("ModelConditionID")%>','bindorderby');" />
	自动加载
	 <select onChange="setModelCondition(this,'<%# Eval("ModelConditionID")%>','bindauto');" style="width:40px;">
	<option value="1"<%# (Eval("bindauto").ToString()=="True" ? " selected=\"true\"" : "")%>>是</option>
	<option value="0"<%# (Eval("bindauto").ToString()=="False" ? " selected=\"true\"" : "")%>>否</option>
	</select><br />
    外键
    <select onChange="setModelCondition(this,'<%# Eval("ModelConditionID")%>','bindforeignkey');" style="width:80px;">
	<option value="NULL">无</option>
	<asp:Literal id="LitBindForeignKey" runat="server" />
	</select>
    <input class="text" type="text" value="<%# Eval("BindForeignkey")%>" style="width:80px;display:none;" onDblClick="dblClick(this,'<%# Eval("MC")%>-外键');" onBlur="setModelCondition(this,'<%# Eval("ModelConditionID")%>','bindforeignkey');" />

     联动传值 <select onChange="setModelCondition(this,'<%# Eval("ModelConditionID")%>','bindcode');" style="width:80px;">
	<option value="">无</option>
	<asp:Literal id="LitCode" runat="server" />
	</select> 联动加载 <asp:Literal id="Litcolumns" runat="server" />
    <select onChange="setModelCondition(this,'<%# Eval("ModelConditionID")%>','fillitem');" style="display:none;width:120px;">
        <option value="NULL">无</option>
        <asp:Literal id="Litcolumns1" runat="server" />
     </select>
    </span> <br />
    SQL选项取值：
    <input class="text" type="text" value="<%# Eval("bindsql")%>" style="width:380px;" onDblClick="dblClick(this,'<%# Eval("MC")%>-SQL选项取值');" onBlur="setModelCondition(this,'<%# Eval("ModelConditionID")%>','bindsql');" />
    <br />
	选项：<input id="search_options_<%# Eval("ModelConditionID").ToString().Replace("-","") %>"  jsonformat="[{&quot;text&quot;:&quot;文本&quot;,&quot;value&quot;:&quot;text&quot;},{&quot;text&quot;:&quot;值&quot;,&quot;value&quot;:&quot;value&quot;}]" type="text" value="<%# HttpUtility.HtmlEncode(Eval("options").ToString())%>" class="edit" style="width:300px;display:none;" onDblClick="dblClick(this,'<%# Eval("MC")%>-选项');" onBlur="setModelCondition(this,'<%# Eval("ModelConditionID")%>','options');" />
    <img src="images/jsonedit.png" align="absmiddle" style="cursor:pointer;" onClick="Json_Edit('search_options_<%# Eval("ModelConditionID").ToString().Replace("-","") %>','选项');">
     <%# getJsonText(Eval("options").ToString(),"text")%>
</span>
<span id="spanoptions" style="<%# (",text,yearmonthtree,quick,date,dateregion,numberregion,".IndexOf("," + Eval("ControlType").ToString().ToLower() + ",")==-1 ? "" : "display:none;")%>"><br />自定义条件选项：<asp:Literal id="LitOptions" runat="server" /></span>
</span>
<%#
"</td>" +
//"<td><input type=\"text\" value=\""+ (Eval("colspan").ToString()=="0" ? "" : Eval("colspan").ToString()) + "\" oldvalue=\""+ (Eval("colspan").ToString()=="0" ? "" : Eval("colspan").ToString()) + "\" style=\"border:1px solid #ccc;width:55px;\" onBlur=\"setModelCondition(this,"+ Eval("ModelConditionID")+",'colspan');\"></td>" +
"<td><input class=\"text\" type=\"text\" title=\"禁用条件\" oldvalue=\""+ Eval("conddisable") + "\" value=\""+ Eval("conddisable") + "\" ondblclick=\"dblClick(this,'" + Eval("MC") + "-禁用条件');\" onBlur=\"setModelCondition(this,'" + Eval("ModelConditionID") + "','conddisable');\"></td>" +

"<td><input class=\"text\" type=\"text\" title=\"默认值\" oldvalue=\""+ Eval("defaultvalue") + "\" value=\""+ Eval("defaultvalue") + "\" ondblclick=\"dblClick(this,'" + Eval("MC") + "-默认值');\" onBlur=\"setModelCondition(this,'" + Eval("ModelConditionID") + "','defaultvalue');\"></td>" +

"<td>"+
(eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(112);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "宽：<input class=\"text\" style=\"width:40px;\" type=\"text\" oldvalue=\""+ Eval("width") + "\" value=\""+ Eval("width") + "\" onBlur=\"setModelCondition(this,'" + Eval("ModelConditionID") + "','width');\">" + 
(eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(113);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "高：<input class=\"text\" style=\"width:40px;\" type=\"text\" oldvalue=\""+ Eval("height") + "\" value=\""+ Eval("height") + "\" onBlur=\"setModelCondition(this,'" + Eval("ModelConditionID") + "','height');\"><br>" + 
"跨行" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(117);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "：<input class=\"text\" style=\"width:40px;\" type=\"text\" oldvalue=\""+ Eval("rowspan") + "\" value=\""+ Eval("rowspan") + "\" onBlur=\"setModelCondition(this,'" + Eval("ModelConditionID") + "','rowspan');\">" +
"跨列" + (eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(118);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "：<input class=\"text\" style=\"width:40px;\" type=\"text\" oldvalue=\""+ Eval("colspan") + "\" value=\""+ Eval("colspan") + "\" onBlur=\"setModelCondition(this,'" + Eval("ModelConditionID") + "','colspan');\"><br>" +
"占位符：<input class=\"text\" style=\"width:120px;\" type=\"text\" oldvalue=\""+ Eval("PlaceHolder") + "\" value=\""+ Eval("PlaceHolder") + "\" onBlur=\"setModelCondition(this,'" + Eval("ModelConditionID") + "','placeholder');\"><br>" + 
(eConfig.showHelp() ? "<img src=\"images/help.gif\" align=\"absmiddle\" border=\"0\" onclick=\"showHelp(119);\" title=\"查看帮助\" alt=\"查看帮助\" style=\"cursor:pointer;margin-bottom:4px;\">" : "") + "HTML扩展属性：<br>" +

"<input class=\"text\" type=\"text\" oldvalue=\""+  HttpUtility.HtmlEncode(Eval("attributes").ToString()) + "\" value=\""+ HttpUtility.HtmlEncode(Eval("attributes").ToString()) + "\" ondblclick=\"dblClick(this,'" + Eval("MC") + "-HTML扩展属性');\" onBlur=\"setModelCondition(this,'" + Eval("ModelConditionID") + "','attributes');\">"+
"</td>" +

"<td><input class=\"text\" type=\"text\" value=\""+ Eval("SM").ToString() + "\" oldvalue=\""+ Eval("SM").ToString() + "\"  ondblclick=\"dblClick(this,'" + Eval("MC") + "-说明');\" onBlur=\"setModelCondition(this,'" + Eval("ModelConditionID") + "','sm');\" /></td>" +

//"<td><input class=\"text\" reload=\"true\" type=\"text\" value=\""+ (Eval("px").ToString()=="999999" ? "" : Eval("px").ToString()) + "\" oldvalue=\""+ (Eval("px").ToString()=="999999" ? "" : Eval("px").ToString()) + "\" onBlur=\"setModelCondition(this,'" + Eval("ModelConditionID") + "','px');\" /></td>" +
"<td style=\"cursor:move;\">" + (Container.ItemIndex + 1) + "</td>"+
"</tr>"
%>
</itemtemplate>
<footertemplate><%#"</tbody></table>"%></footertemplate>
</asp:Repeater>
</body>
</html>
