﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using EKETEAM.FrameWork;
using EKETEAM.Data;
using EKETEAM.UserControl;
using LitJson;

namespace eFrameWork.Manage
{
    public partial class Accounts : System.Web.UI.Page
    {
        public string act = eParameters.Request("act");
        public eForm edt;
        public eUser user;
        public string JsonTemplates = "";
        public string body = "{}";
        public string data = "{}";
        public eFormControl type;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser("Manage");
            edt = new eForm("a_eke_sysAccounts", user);
            JsonTemplates = eBase.getAccountTemplate();

            if (act.Length == 0)
            {
                List();
                return;
            }

            #region 信息添加、编辑
            edt.AddControl(f1);
            edt.AddControl(f2);
            eFormControl _body = new eFormControl("body");
            _body.Field = "body";
            edt.AddControl(_body);

            type = new eFormControl("type");
            type.Field = "type";
            edt.AddControl(type);

            edt.AddControl(f3);
            edt.AddControl(f4);
            edt.AddControl(f5);
            edt.AddControl(f6);
            edt.AddControl(f7);
            edt.onChange += new eFormTableEventHandler(edt_onChange);
            edt.Handle();
            if (_body.Value.ToString().StartsWith("{"))
            {
                data = _body.Value.ToString(); 
                body = _body.Value.ToString().HtmlEncode(); 
            }
            if (type.Value.ToString().Length > 0)
            {
                f4.BindCondition = "deltag=0 and siteid='" + type.Value.ToString() + "'";
                f4.BindAuto = true;
            }

            #endregion

        }
        public void edt_onChange(object sender, eFormTableEventArgs e)
        {
            if (e.eventType == eFormTableEventType.Inserting)
            {
                //if (user["ServiceID"].Length > 0) edt.Fields.Add("ServiceID", user["ServiceID"]);
            }
        }
        private void List()
        {
            eList datalist = new eList("a_eke_sysAccounts");
            datalist.Where.Add("delTag=0");
            //datalist.Where.Add("ServiceID" + (user["ServiceID"].Length == 0 ? " is null" : "='" + user["ServiceID"] + "'"));
            datalist.OrderBy.Add("addTime desc");
            datalist.Bind(Rep, ePageControl1);
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            Literal lit = (Literal)Master.FindControl("LitTitle");
            if (lit != null)
            {
                lit.Text = "第三方帐号 - " + eConfig.manageName();
            }
        }
    }
}