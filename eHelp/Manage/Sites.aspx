﻿<%@ Page Language="C#" MasterPageFile="Main.Master" AutoEventWireup="true" CodeFile="Sites.aspx.cs" Inherits="eFrameWork.Manage.Sites" %>
<%@ Register Src="GroupMenu.ascx" TagPrefix="uc1" TagName="GroupMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<uc1:GroupMenu runat="server" ID="GroupMenu" />
<div class="nav">您当前位置：<a href="Default.aspx">首页</a> -> 企业管理<a id="btn_add" style="<%=(act == "" ? "" : "display:none;" )%>" class="button" href="<%=edt.getAddURL()%>"><span><i class="add">添加</i></span></a></div>
<%if (eRegisterInfo.Base == 0 && eRegisterInfo.Loaded)
  { %>
    <div style="margin:6px;line-height:25px;font-size:13px;">
<div class="tips" style="margin-bottom:6px;"><b>未授权提示</b><br><a href="http://frame.eketeam.com/getSerialNumber.aspx" style="color:#ff0000;" target="_blank">申请临时授权</a>,享更多功能。</div>
   </div>
 <%} %>
  <div class="tips" style="margin:6px;"><b>提示</b><br>管理使用系统的企业,同一套系统可多家企业共用。</div>
<%
if(act=="edit" || act=="add")
{
%>
<div style="margin:6px;">
 <asp:PlaceHolder ID="eFormControlGroup" runat="server">
<form name="frmaddoredit" id="frmaddoredit" method="post" action="<%=edt.getSaveURL()%>">
	<input name="id" type="hidden" id="id" value="<%=id%>">
    <input name="act" type="hidden" id="act" value="save">  
	<input name="fromurl" type="hidden" id="fromurl" value="<%=edt.FromURL%>">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="eDataView">
      <tr>
        <td width="126" class="title"><font color="#FF0000">*</font> 企业名称：</td>
        <td class="content"><span class="eform">
		<ev:eFormControl ID="f1" Field="MC" width="300" FieldName="企业名称"  notnull="true" runat="server" />
		</span></td>
      </tr>
     <tr>
        <td class="title">地址：</td>
        <td class="content"><span class="eform">
		<ev:eFormControl ID="F2" Field="DZ" width="300" FieldName="地址"  notnull="false" runat="server" />
		</span></td>
      </tr>  

     <tr>
        <td class="title">联系人：</td>
        <td class="content"><span class="eform">
		<ev:eFormControl ID="F3" Field="LXR" width="300" FieldName="联系人"  notnull="false" runat="server" />
		</span></td>
      </tr>  
     <tr>
        <td class="title">联系电话：</td>
        <td class="content"><span class="eform">
		<ev:eFormControl ID="F4" Field="LXDH" width="300" FieldName="联系电话"  notnull="false" runat="server" />
		</span></td>
      </tr>
	   <tr>
        <td class="title">公共域名：</td>
        <td class="content"><span class="eform">
		<ev:eFormControl ID="F5" Field="DoMain" width="300" FieldName="公共域名"  notnull="false" runat="server" />
		<i>后台和前台共用的域名</i></span></td>
      </tr>
	   <tr>
        <td class="title">系统名称：</td>
        <td class="content"><span class="eform">
		<ev:eFormControl ID="F6" Field="appsName" width="300" FieldName="系统名称"  notnull="false" runat="server" />
		<i>后台系统名称</i></span></td>
      </tr>
     <tr>
        <td class="title">系统简介：</td>
        <td class="content"><span class="eform">
		<ev:eFormControl ID="F7" ControlType="textarea" Field="Profiles" width="300" FieldName="系统简介"  notnull="false" runat="server" />
		</span></td>
      </tr>  
      <tr>
        <td class="title">附件目录：</td>
        <td class="content"><span class="eform">
		<ev:eFormControl ID="F8" Field="AttachmentBasePath" width="300" FieldName="附件目录" DefaultValue="{siteid}" notnull="false" runat="server" />
		<i>{siteid}代表以站点(企业)ID作为根目录文件夹</i></span></td>
      </tr>
     <tr>
        <td class="title">系统LOGO：</td>
        <td class="content"><span class="eform">
		<ev:eFormControl ID="F9" ControlType="fileselect" Field="appsLogo" width="300" FieldName="系统LOGO" Options="{&quot;basepath&quot;:&quot;upload&quot;}" notnull="false" runat="server" />
		</span></td>
      </tr>
	  <tr valign="top">
            <td class="title">后台应用：</td>
              <td class="content">
                   <ev:eSubForm ID="eSubForm1" FileName="SiteItems.aspx" runat="server" />
                  </td>
        </tr>
    <tr valign="top">
        <td class="title">前台站点：</td>
          <td class="content">
              <ev:eSubForm ID="eSubForm2" FileName="WebSites.aspx" runat="server" />
          </td>
    </tr>
      <tr>
        <td class="title">说明：</td>
        <td class="content"><span class="eform">
		<ev:eFormControl ID="f10" ControlType="textarea" Field="SM" width="300" FieldName="说明"  notnull="false" runat="server" />
		</span></td>
      </tr>  
 
	 <tr>
       <td colspan="2" class="title"  style="text-align:left;padding-left:100px;padding-top:10px;padding-bottom:10px;">		
		<a class="button" href="javascript:;" onclick="if(frmaddoredit.onsubmit()!=false){frmaddoredit.submit();}"><span><i class="save">保存</i></span></a>
		<a class="button" href="javascript:;" style="margin-left:30px;" onclick="history.back();"><span><i class="back">返回</i></span></a>
		</td>
	   </tr>	
    </table>
	</form>
	</asp:PlaceHolder>
</div>
	<%}else{%>	
<div style="margin:6px;overflow-x:auto;overflow-y:hidden;">
<ev:eListControl ID="eDataTable" Class="eDataTable" CellSpacing="1" LineHeight="32" runat="server" >
    <ev:eListColumn ControlType="text" FieldName="编号" Width="60" runat="server">{data:ID}</ev:eListColumn>
    <ev:eListColumn ControlType="text" Field="MC" FieldName="企业名称" runat="server" />
	<ev:eListColumn ControlType="text" Field="appsName" FieldName="系统名称" runat="server" />
    <ev:eListColumn ControlType="text" Field="appsLogo" FieldName="系统Logo" runat="server" >
<img src="../{data:appsLogo}" width="30" style="max-width:30px;max-height:30px;" onerror="this.src='../images/none.gif';">
</ev:eListColumn>
    <ev:eListColumn ControlType="text" Field="SM" FieldName="说明" runat="server" />
    <ev:eListColumn ControlType="text" Field="addTime" FieldName="添加时间" Width="100" FormatString="{0:yyyy-MM-dd}" runat="server" />
    <ev:eListColumn ControlType="text" FieldName="操作" Width="130" runat="server">
    <a href="{base:url}act=edit&id={data:ID}">修改</a>
    <a href="{base:url}act=del&id={data:ID}" onclick="javascript:return confirm('确认要删除吗？');">删除</a>
</ev:eListColumn>
</ev:eListControl>


</div>
<div style="margin:6px;"><ev:ePageControl ID="ePageControl1" PageSize="20" PageNum="9" runat="server" /></div>
<%}%>	
</asp:Content>
