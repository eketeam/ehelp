﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.FrameWork;
using EKETEAM.Data;
using System.Text;
using System.Text.RegularExpressions;
using LitJson;

namespace eFrameWork.Manage
{
    public partial class ModelItems_WebAPI : System.Web.UI.Page
    {
        public string modelid = eParameters.QueryString("modelid");
        private DataRow _modelinfo;
        public DataRow ModelInfo
        {
            get
            {
                if (_modelinfo == null)
                {
                    DataTable dt = eBase.DataBase.getDataTable("select b.Type as bType, a.* from a_eke_sysModels a left join a_eke_sysModels b on a.ParentID=b.ModelID where a.ModelID='" + modelid + "'");
                    if (dt.Rows.Count > 0) _modelinfo = dt.Rows[0];
                }
                return _modelinfo;
            }
        }
        private eUser user;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser("Manage");
            user.Check();
            Response.Write("<a href=\"http://frame.eketeam.com\" style=\"float:right;\" target=\"_blank\" title=\"eFrameWork开发框架\"><img src=\"images/help.gif\"></a>");

            if (eConfig.showHelp())
            {
                Response.Write("<div class=\"tips\" style=\"margin-bottom:6px;\">");
                Response.Write("<b>WebAPI</b><br>");
                Response.Write("该模块提供的WebAPI接口说明。<br>");
                Response.Write("</div> ");
            }

            StringBuilder sb;
            DataTable dt;
            int row = 0;
            //DataRow dr = eBase.DataBase.getDataTable("select * from a_eke_sysModels where ModelID='" + modelid + "'").Select()[0];




            if (!eBase.parseBool( ModelInfo["subModel"]) ) eBase.Write("<label><input type=\"checkbox\" name=\"apisplitpage\" id=\"apisplitpage\" onclick=\"setModel(this,'apisplitpage');\"" + (  eBase.parseBool(ModelInfo["apisplitpage"]) ? " checked" : "") + " />启用分页</label>");


            eBase.Write("&nbsp;JSON别名：<input type=\"text\" value=\"" + ModelInfo["JsonAlias"].ToString() + "\" oldvalue=\"" + ModelInfo["JsonAlias"].ToString() + "\"  class=\"edit\" style=\"width:80px;\" onBlur=\"setModel(this,'jsonalias');\" />");
            eBase.Write("&nbsp;&nbsp;<label><input type=\"checkbox\" name=\"apifieldname\" id=\"apifieldname\" onclick=\"setModel(this,'apifieldname');\"" + ( eBase.parseBool(ModelInfo["apiFieldName"]) ? " checked" : "") + " />字段名输出</label>");
            eBase.Write("&nbsp;&nbsp;<a href=\"http://frame.eketeam.com/Resource.aspx?id=8\" target=\"_blank\">下载使用帮助类</a>");

            if (ModelInfo["auto"].ToString().ToLower() == "false")
            {
                Response.End();
            }

            JsonData jd = JsonMapper.ToObject("{}");
            if (ModelInfo["Propertys"].ToString().Length > 2 && ModelInfo["Propertys"].ToString().StartsWith("{"))
            {
                jd = JsonMapper.ToObject(ModelInfo["Propertys"].ToString());
            }


            #region 令牌接口
            sb = new StringBuilder();
            sb.Append("<div class=\"powerico\" style=\"margin-top:6px;\">\r\n");
            sb.Append("<a href=\"javascript:;\" class=\"close\" onclick=\"showPower(this);\"><b>令牌接口</b></a>");
            sb.Append("</div>\r\n");
            sb.Append("<div style=\"margin-left:20px;display:none;\">\r\n");
            #region 接口地址
            sb.Append("1. 接口地址<br>\r\n");
            sb.Append("<table border=\"0\" cellpadding=\"8\" cellspacing=\"1\" width=\"100%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>描述</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\"><b>值</b></td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">接口地址</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">" + eBase.getBaseURL() + "AppWebAPI/getToKen.aspx</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">请求方式</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">Post</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</tbody>\r\n");
            sb.Append("</table>");
            #endregion
            #region 请求参数
            sb.Append("2. 请求参数<br>\r\n");
            sb.Append("<table border=\"0\" cellpadding=\"8\" cellspacing=\"1\" width=\"100%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>名称</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>类型</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\"><b>说明</b></td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">contentType</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">header</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">application/x-www-form-urlencoded</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">username</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">data</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">用户帐号</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">password</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">data</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">帐号密码</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</tbody>\r\n");
            sb.Append("</table>");
            #endregion
            #region 返回结果
            sb.Append("3. 返回结果(JSON格式)<br>\r\n");
            sb.Append("<table border=\"0\" cellpadding=\"8\" cellspacing=\"1\" width=\"100%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>名称</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\"><b>说明</b></td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">成功</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">");
            sb.Append("{<br>");
            sb.Append("\"errcode\":\"0\",<br>");
            sb.Append("\"message\":\"请求成功!\",<br>");
            sb.Append("\"token\":\"06841095ADDB705...\"<br>");
            sb.Append("}<br>");
            sb.Append("</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">失败</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">");
            sb.Append("{<br>");
            sb.Append("\"errcode\":\"1005\",<br>");
            sb.Append("\"message\":\"登录信息有误!\"<br>");
            sb.Append("}<br>");
            sb.Append("</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</tbody>\r\n");
            sb.Append("</table>");
            #endregion
            sb.Append("</div>");
            eBase.Write(sb.ToString());
            #endregion
            #region 添加接口
            sb = new StringBuilder();

            sb.Append("<div class=\"powerico\">\r\n");
            sb.Append("<a href=\"javascript:;\" class=\"close\" onclick=\"showPower(this);\"><b>添加接口</b></a>");
            sb.Append("</div>\r\n");
            sb.Append("<div style=\"margin-left:20px;display:none;\">\r\n");
            #region 接口地址


            //sb.Append("<input type=\"checkbox\" name=\"anonymousadd\" id=\"anonymousadd\" onclick=\"setModelPropertys(this,'anonymousadd');\"" + (jd.Contains("anonymousadd") && Convert.ToBoolean(jd.getValue("anonymousadd")) ? " checked" : "") + " /><label for=\"anonymousadd\">匿名访问</label><br>");
            sb.Append("<label><input type=\"checkbox\" name=\"anonymousadd\" id=\"anonymousadd\" onclick=\"setModel(this,'anonymousadd');\"" + (ModelInfo.Table.Columns.Contains("anonymousadd") && eBase.parseBool(ModelInfo["anonymousadd"]) ? " checked" : "") + " />匿名访问</label><br>");
            sb.Append("1. 接口地址<br>\r\n");
            sb.Append("<table border=\"0\" cellpadding=\"8\" cellspacing=\"1\" width=\"100%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>描述</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\"><b>值</b></td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">接口地址</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">" + eBase.getBaseURL() + "AppWebAPI/Model.aspx?AppItem=应用模块编号</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">请求方式</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">Post</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</tbody>\r\n");
            sb.Append("</table>");
            #endregion
            #region 请求参数
            sb.Append("2. 请求参数<br>\r\n");
            sb.Append("<table border=\"0\" cellpadding=\"8\" cellspacing=\"1\" width=\"99%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>名称</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>类型</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\"><b>说明</b></td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">auth</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">header</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">token (匿名时值为：anonymous)</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">contentType</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">header</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">application/json</td>\r\n");
            sb.Append("</tr>\r\n");


            /*
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">act</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">data</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">save</td>\r\n");
            sb.Append("</tr>\r\n");
            dt = eBase.DataBase.getDataTable("SELECT MC,frmName FROM a_eke_sysModelItems where ModelID='" + modelid + "' and delTag=0 and showAdd=1 order by addorder, PX, addTime");
            row = 0;
            foreach (DataRow _dr in dt.Rows)
            {
                row++;
                sb.Append("<tr>\r\n");
                sb.Append("<td bgcolor=\"" + (row % 2 == 0 ? "#FFFFFF" : "#FAFAFA") + "\">" + _dr["frmName"].ToString() + "</td>\r\n");
                sb.Append("<td bgcolor=\"" + (row % 2 == 0 ? "#FFFFFF" : "#FAFAFA") + "\">data</td>\r\n");
                sb.Append("<td bgcolor=\"" + (row % 2 == 0 ? "#FFFFFF" : "#FAFAFA") + "\">" + _dr["MC"].ToString() + "</td>\r\n");
                sb.Append("</tr>\r\n");
            }
            */



            JsonData json = new JsonData();
            json.Add("id", "");
            json.Add("act", "save");

            //dt = eBase.DataBase.getDataTable("SELECT [MC],[frmName] FROM [a_eke_sysModelItems] where ModelID='" + modelid + "' and delTag=0 and showAdd=1 order by addorder, PX, addTime");
            dt = eBase.DataBase.getDataTable("SELECT MC,frmName FROM a_eke_sysModelItems where ModelID='" + modelid + "' and delTag=0 and showAdd=1 order by addorder, PX, addTime");//order by addTime
            //eBase.PrintDataTable(dt);
            foreach (DataRow _dr in dt.Rows)
            {
                json.Add(_dr["frmName"].ToString(), _dr["MC"].ToString());
            }
            DataTable dtt = eBase.DataBase.getDataTable("select ModelID,MC,JsonAlias from a_eke_sysModels where ParentID='" + modelid + "' and delTag=0");
            foreach (DataRow _dr in dtt.Rows)
            {
                json.Add((_dr["JsonAlias"].ToString().Length > 0 ? _dr["JsonAlias"].ToString() : "eformdata_" + _dr["ModelID"].ToString()), "[{...},{...}]");
            }
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">data</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">json</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">" + json.Unescape() + "</td>\r\n");
            sb.Append("</tr>\r\n");



            sb.Append("</tbody>\r\n");
            sb.Append("</table>");
            #endregion
            #region 返回结果
            sb.Append("3. 返回结果(JSON格式)<br>\r\n");
            sb.Append("<table border=\"0\" cellpadding=\"8\" cellspacing=\"1\" width=\"99%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>名称</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\"><b>说明</b></td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">成功</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">");
            sb.Append("{<br>");
            sb.Append("\"errcode\":\"0\",<br>");
            sb.Append("\"message\":\"请求成功!\",<br>");
            sb.Append("\"id\":\"07677fb8-d2...\"<br>");
            sb.Append("}<br>");
            sb.Append("</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">失败</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">");
            sb.Append("{<br>");
            sb.Append("\"errcode\":\"1004\",<br>");
            sb.Append("\"message\":\"toKen已过期!\"<br>");
            sb.Append("}<br>");
            sb.Append("</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</tbody>\r\n");
            sb.Append("</table>");
            #endregion

            sb.Append("</div>");
            eBase.Write(sb.ToString());
            #endregion
            #region 更新接口
            sb = new StringBuilder();
            sb.Append("<div class=\"powerico\">\r\n");
            sb.Append("<a href=\"javascript:;\" class=\"close\" onclick=\"showPower(this);\"><b>更新接口</b></a>");
            sb.Append("</div>\r\n");
            sb.Append("<div style=\"margin-left:20px;display:none;\">\r\n");
            #region 接口地址
            //sb.Append("<input type=\"checkbox\" name=\"anonymousupdate\" id=\"anonymousupdate\" onclick=\"setModelPropertys(this,'anonymousupdate');\"" + (jd.Contains("anonymousupdate") && Convert.ToBoolean(jd.getValue("anonymousupdate")) ? " checked" : "") + " /><label for=\"anonymousupdate\">匿名访问</label><br>");
            sb.Append("<label><input type=\"checkbox\" name=\"anonymousupdate\" id=\"anonymousupdate\" onclick=\"setModel(this,'anonymousupdate');\"" + (ModelInfo.Table.Columns.Contains("anonymousupdate") && eBase.parseBool(ModelInfo["anonymousupdate"]) ? " checked" : "") + " />匿名访问</label><br>");
            sb.Append("1. 接口地址<br>\r\n");
            sb.Append("<table border=\"0\" cellpadding=\"8\" cellspacing=\"1\" width=\"99%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>描述</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\"><b>值</b></td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">接口地址</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">" + eBase.getBaseURL() + "AppWebAPI/Model.aspx?AppItem=应用模块编号</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">请求方式</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">Post</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</tbody>\r\n");
            sb.Append("</table>");
            #endregion
            #region 请求参数


            sb.Append("2. 请求参数<br>\r\n");
            sb.Append("<table border=\"0\" cellpadding=\"8\" cellspacing=\"1\" width=\"99%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>名称</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>类型</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\"><b>说明</b></td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">auth</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">header</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">token (匿名时值为：anonymous)</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">contentType</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">header</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">application/json</td>\r\n");
            sb.Append("</tr>\r\n");
            /*
            sb.Append("<tr>\r\n");          
            sb.Append("<td bgcolor=\"#FFFFFF\">act</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">data</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">save</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">id</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">data</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">07677fb8-d2...</td>\r\n");
            sb.Append("</tr>\r\n");
            dt = eBase.DataBase.getDataTable("SELECT MC,frmName FROM a_eke_sysModelItems where ModelID='" + modelid + "' and delTag=0 and showAdd=1 order by addorder, PX, addTime");
            row = 0;
            foreach (DataRow _dr in dt.Rows)
            {
                row++;
                sb.Append("<tr>\r\n");
                sb.Append("<td bgcolor=\"" + (row % 2 == 0 ? "#FAFAFA" : "#FFFFFF") + "\">" + _dr["frmName"].ToString() + "</td>\r\n");
                sb.Append("<td bgcolor=\"" + (row % 2 == 0 ? "#FAFAFA" : "#FFFFFF") + "\">data</td>\r\n");
                sb.Append("<td bgcolor=\"" + (row % 2 == 0 ? "#FAFAFA" : "#FFFFFF") + "\">" + _dr["MC"].ToString() + "</td>\r\n");
                sb.Append("</tr>\r\n");
            }
            */



            json = new JsonData();
            json.Add("id", "1");
            json.Add("act", "save");
            dt = eBase.DataBase.getDataTable("SELECT MC,frmName FROM a_eke_sysModelItems where ModelID='" + modelid + "' and delTag=0 and showAdd=1 order by addorder, PX, addTime");
            foreach (DataRow _dr in dt.Rows)
            {
                json.Add(_dr["frmName"].ToString(), _dr["MC"].ToString());
            }
            dtt = eBase.DataBase.getDataTable("select ModelID,MC,JsonAlias from a_eke_sysModels where ParentID='" + modelid + "' and delTag=0");
            foreach (DataRow _dr in dtt.Rows)
            {
                json.Add((_dr["JsonAlias"].ToString().Length > 0 ? _dr["JsonAlias"].ToString() : "eformdata_" + _dr["ModelID"].ToString()), "[{...},{...}]");
            }
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">data</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">json</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">" + json.Unescape() + "</td>\r\n");
            sb.Append("</tr>\r\n");


            sb.Append("</tbody>\r\n");
            sb.Append("</table>");
            #endregion
            #region 返回结果
            sb.Append("3. 返回结果(JSON格式)<br>\r\n");
            sb.Append("<table border=\"0\" cellpadding=\"8\" cellspacing=\"1\" width=\"99%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>名称</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\"><b>说明</b></td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">成功</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">");
            sb.Append("{<br>");
            sb.Append("\"errcode\":\"0\",<br>");
            sb.Append("\"message\":\"请求成功!\",<br>");
            sb.Append("\"id\":\"07677fb8-d2...\"<br>");
            sb.Append("}<br>");
            sb.Append("</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">失败</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">");
            sb.Append("{<br>");
            sb.Append("\"errcode\":\"1004\",<br>");
            sb.Append("\"message\":\"toKen已过期!\"<br>");
            sb.Append("}<br>");
            sb.Append("</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</tbody>\r\n");
            sb.Append("</table>");
            #endregion

            sb.Append("</div>");
            eBase.Write(sb.ToString());
            #endregion

            #region 编辑状态数据接口
            sb = new StringBuilder();
            sb.Append("<div class=\"powerico\">\r\n");
            sb.Append("<a href=\"javascript:;\" class=\"close\" onclick=\"showPower(this);\"><b>编辑状态数据接口</b></a>");
            sb.Append("</div>\r\n");
            sb.Append("<div style=\"margin-left:20px;display:none;\">\r\n");
            #region 接口地址
            //sb.Append("<input type=\"checkbox\" name=\"anonymousedit\" id=\"anonymousedit\" onclick=\"setModelPropertys(this,'anonymousedit');\"" + (jd.Contains("anonymousedit") && Convert.ToBoolean(jd.getValue("anonymousedit")) ? " checked" : "") + " /><label for=\"anonymousedit\">匿名访问</label><br>");
            sb.Append("<label><input type=\"checkbox\" name=\"anonymousedit\" id=\"anonymousedit\" onclick=\"setModel(this,'anonymousedit');\"" + (ModelInfo.Table.Columns.Contains("anonymousedit") && eBase.parseBool(ModelInfo["anonymousedit"]) ? " checked" : "") + " />匿名访问</label><br>");

            sb.Append("1. 接口地址<br>\r\n");
            sb.Append("<table border=\"0\" cellpadding=\"8\" cellspacing=\"1\" width=\"100%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>描述</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\"><b>值</b></td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">接口地址</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">" + eBase.getBaseURL() + "AppWebAPI/Model.aspx?AppItem=应用模块编号</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">请求方式</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">Post</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</tbody>\r\n");
            sb.Append("</table>");
            #endregion
            #region 请求参数
            sb.Append("2. 请求参数<br>\r\n");
            sb.Append("<table border=\"0\" cellpadding=\"8\" cellspacing=\"1\" width=\"99%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>名称</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>类型</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\"><b>说明</b></td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>");

            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">auth</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">header</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">token (匿名时值为：anonymous)</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">contentType</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">header</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">application/x-www-form-urlencoded</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">act</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">data</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">edit</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">id</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">data</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">07677fb8-d2...</td>\r\n");
            sb.Append("</tr>\r\n");


            sb.Append("</tbody>\r\n");
            sb.Append("</table>");
            #endregion
            #region 返回结果
            sb.Append("3. 返回结果(JSON格式)<br>\r\n");
            sb.Append("<table border=\"0\" cellpadding=\"8\" cellspacing=\"1\" width=\"99%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>名称</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\"><b>说明</b></td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">成功</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">");
            sb.Append("{<br>");
            sb.Append("\"errcode\":\"0\",<br>");
            sb.Append("\"message\":\"请求成功!\",<br>");
            sb.Append("\"data\":{");
            row = 0;
            foreach (DataRow _dr in dt.Rows)
            {
                if (row > 0) sb.Append(",");
                sb.Append("\"" + _dr["frmName"].ToString() + "\":\"...\"");
                row++;
            }
            sb.Append("}<br>");
            sb.Append("}<br>");
            sb.Append("</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">失败</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">");
            sb.Append("{<br>");
            sb.Append("\"errcode\":\"1004\",<br>");
            sb.Append("\"message\":\"toKen已过期!\"<br>");
            sb.Append("}<br>");
            sb.Append("</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</tbody>\r\n");
            sb.Append("</table>");
            #endregion

            sb.Append("</div>");
            eBase.Write(sb.ToString());
            #endregion

            #region 查看状态数据接口
            sb = new StringBuilder();
            sb.Append("<div class=\"powerico\">\r\n");
            sb.Append("<a href=\"javascript:;\" class=\"close\" onclick=\"showPower(this);\"><b>查看状态数据接口</b></a>");
            sb.Append("</div>\r\n");
            sb.Append("<div style=\"margin-left:20px;display:none;\">\r\n");
            #region 接口地址
            //sb.Append("<input type=\"checkbox\" name=\"anonymousview\" id=\"anonymousview\" onclick=\"setModelPropertys(this,'anonymousview');\"" + (jd.Contains("anonymousview") && Convert.ToBoolean(jd.getValue("anonymousview")) ? " checked" : "") + " /><label for=\"anonymousview\">匿名访问</label><br>");
            sb.Append("<label><input type=\"checkbox\" name=\"anonymousview\" id=\"anonymousview\" onclick=\"setModel(this,'anonymousview');\"" + (ModelInfo.Table.Columns.Contains("anonymousview") && eBase.parseBool(ModelInfo["anonymousview"]) ? " checked" : "") + " />匿名访问</label><br>");

            sb.Append("1. 接口地址<br>\r\n");
            sb.Append("<table border=\"0\" cellpadding=\"8\" cellspacing=\"1\" width=\"99%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>描述</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\"><b>值</b></td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">接口地址</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">" + eBase.getBaseURL() + "AppWebAPI/Model.aspx?AppItem=应用模块编号</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">请求方式</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">Post</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</tbody>\r\n");
            sb.Append("</table>");
            #endregion
            #region 请求参数
            sb.Append("2. 请求参数<br>\r\n");
            sb.Append("<table border=\"0\" cellpadding=\"8\" cellspacing=\"1\" width=\"99%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>名称</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>类型</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\"><b>说明</b></td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>");

            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">auth</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">header</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">token (匿名时值为：anonymous)</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">contentType</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">header</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">application/x-www-form-urlencoded</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">act</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">data</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">view</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">id</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">data</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">07677fb8-d2...</td>\r\n");
            sb.Append("</tr>\r\n");


            sb.Append("</tbody>\r\n");
            sb.Append("</table>");
            #endregion
            #region 返回结果
            sb.Append("3. 返回结果(JSON格式)<br>\r\n");
            sb.Append("<table border=\"0\" cellpadding=\"8\" cellspacing=\"1\" width=\"99%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>名称</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\"><b>说明</b></td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">成功</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">");
            sb.Append("{<br>");
            sb.Append("\"errcode\":\"0\",<br>");
            sb.Append("\"message\":\"请求成功!\",<br>");
            sb.Append("\"data\":{");
            row = 0;
            foreach (DataRow _dr in dt.Rows)
            {
                if (row > 0) sb.Append(",");
                sb.Append("\"" + _dr["frmName"].ToString() + "\":\"...\"");
                row++;
            }
            sb.Append("}<br>");
            sb.Append("}<br>");
            sb.Append("</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">失败</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">");
            sb.Append("{<br>");
            sb.Append("\"errcode\":\"1004\",<br>");
            sb.Append("\"message\":\"toKen已过期!\"<br>");
            sb.Append("}<br>");
            sb.Append("</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</tbody>\r\n");
            sb.Append("</table>");
            #endregion

            sb.Append("</div>");
            eBase.Write(sb.ToString());
            #endregion



            #region 数据集合接口
            sb = new StringBuilder();
            sb.Append("<div class=\"powerico\">\r\n");
            sb.Append("<a href=\"javascript:;\" class=\"close\" onclick=\"showPower(this);\"><b>数据集合接口(列表)</b></a>");
            sb.Append("</div>\r\n");
            sb.Append("<div style=\"margin-left:20px;display:none;\">\r\n");
            #region 接口地址
            //sb.Append("<input type=\"checkbox\" name=\"anonymouslist\" id=\"anonymouslist\" onclick=\"setModelPropertys(this,'anonymouslist');\"" + (jd.Contains("anonymouslist") && Convert.ToBoolean(jd.getValue("anonymouslist")) ? " checked" : "") + " /><label for=\"anonymouslist\">匿名访问</label><br>");
            sb.Append("<label><input type=\"checkbox\" name=\"anonymouslist\" id=\"anonymouslist\" onclick=\"setModel(this,'anonymouslist');\"" + (ModelInfo.Table.Columns.Contains("anonymouslist") && eBase.parseBool(ModelInfo["anonymouslist"]) ? " checked" : "") + " />匿名访问</label><br>");
            sb.Append("1. 接口地址<br>\r\n");
            sb.Append("<table border=\"0\" cellpadding=\"8\" cellspacing=\"1\" width=\"99%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>描述</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\"><b>值</b></td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">接口地址</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">" + eBase.getBaseURL() + "AppWebAPI/Model.aspx?AppItem=应用模块编号</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">请求方式</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">Get</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</tbody>\r\n");
            sb.Append("</table>");
            #endregion
            #region 请求参数
            sb.Append("2. 请求参数<br>\r\n");
            sb.Append("<table border=\"0\" cellpadding=\"8\" cellspacing=\"1\" width=\"99%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>名称</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>类型</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\"><b>说明</b></td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">auth</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">header</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">token (匿名时值为：anonymous)</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</tbody>\r\n");
            sb.Append("</table>");
            #endregion
            #region 返回结果
            sb.Append("3. 返回结果(JSON格式)<br>\r\n");
            sb.Append("<table border=\"0\" cellpadding=\"8\" cellspacing=\"1\" width=\"99%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>名称</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\"><b>说明</b></td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">成功</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">");
            sb.Append("{<br>");
            sb.Append("\"errcode\":\"0\",<br>");
            sb.Append("\"message\":\"请求成功!\",<br>");

            sb.Append("\"page\":\"1\",<br>");
            sb.Append("\"pagesize\":\"10\",<br>");
            sb.Append("\"pagecount\":\"3\",<br>");
            sb.Append("\"recordscount\":\"30\",<br>");


            sb.Append("\"data\":[");
            sb.Append("{");
            row = 0;
            foreach (DataRow _dr in dt.Rows)
            {
                if (row > 0) sb.Append(",");
                sb.Append("\"" + _dr["frmName"].ToString() + "\":\"...\"");
                row++;
            }
            sb.Append("},");
            sb.Append("{");
            row = 0;
            foreach (DataRow _dr in dt.Rows)
            {
                if (row > 0) sb.Append(",");
                sb.Append("\"" + _dr["frmName"].ToString() + "\":\"...\"");
                row++;
            }
            sb.Append("}");
            sb.Append("]<br>");
            sb.Append("}<br>");
            sb.Append("</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">失败</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">");
            sb.Append("{<br>");
            sb.Append("\"errcode\":\"1004\",<br>");
            sb.Append("\"message\":\"toKen已过期!\"<br>");
            sb.Append("}<br>");
            sb.Append("</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</tbody>\r\n");
            sb.Append("</table>");
            #endregion

            sb.Append("</div>");
            eBase.Write(sb.ToString());
            #endregion


            #region 删除接口
            sb = new StringBuilder();
            sb.Append("<div class=\"powerico\">\r\n");
            sb.Append("<a href=\"javascript:;\" class=\"close\" onclick=\"showPower(this);\"><b>删除接口</b></a>");
            sb.Append("</div>\r\n");
            sb.Append("<div style=\"margin-left:20px;display:none;\">\r\n");
            #region 接口地址
            //sb.Append("<input type=\"checkbox\" name=\"anonymousdel\" id=\"anonymousdel\" onclick=\"setModelPropertys(this,'anonymousdel');\"" + (jd.Contains("anonymousdel") && Convert.ToBoolean(jd.getValue("anonymousdel")) ? " checked" : "") + " /><label for=\"anonymousdel\">匿名访问</label><br>");
            sb.Append("<label><input type=\"checkbox\" name=\"anonymousdel\" id=\"anonymousdel\" onclick=\"setModel(this,'anonymousdel');\"" + (ModelInfo.Table.Columns.Contains("anonymousdel") && eBase.parseBool(ModelInfo["anonymousdel"]) ? " checked" : "") + " />匿名访问</label><br>");
            sb.Append("1. 接口地址<br>\r\n");
            sb.Append("<table border=\"0\" cellpadding=\"8\" cellspacing=\"1\" width=\"99%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>描述</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\"><b>值</b></td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">接口地址</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">" + eBase.getBaseURL() + "AppWebAPI/Model.aspx?AppItem=应用模块编号</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">请求方式</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">Post</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</tbody>\r\n");
            sb.Append("</table>");
            #endregion
            #region 请求参数
            sb.Append("2. 请求参数<br>\r\n");
            sb.Append("<table border=\"0\" cellpadding=\"8\" cellspacing=\"1\" width=\"99%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>名称</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>类型</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\"><b>说明</b></td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>");

            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">auth</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">header</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">token (匿名时值为：anonymous)</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">contentType</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">header</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">application/x-www-form-urlencoded</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">act</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">data</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">del</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">id</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">data</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">07677fb8-d2...</td>\r\n");
            sb.Append("</tr>\r\n");


            sb.Append("</tbody>\r\n");
            sb.Append("</table>");
            #endregion
            #region 返回结果
            sb.Append("3. 返回结果(JSON格式)<br>\r\n");
            sb.Append("<table border=\"0\" cellpadding=\"8\" cellspacing=\"1\" width=\"99%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>名称</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\"><b>说明</b></td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">成功</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">");
            sb.Append("{<br>");
            sb.Append("\"errcode\":\"0\",<br>");
            sb.Append("\"message\":\"请求成功!\",<br>");
            sb.Append("\"id\":\"07677fb8-d2...\"<br>");
            sb.Append("}<br>");
            sb.Append("</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">失败</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">");
            sb.Append("{<br>");
            sb.Append("\"errcode\":\"1004\",<br>");
            sb.Append("\"message\":\"toKen已过期!\"<br>");
            sb.Append("}<br>");
            sb.Append("</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</tbody>\r\n");
            sb.Append("</table>");
            #endregion

            sb.Append("</div>");
            eBase.Write(sb.ToString());
            #endregion

            #region 文件上传接口
            sb = new StringBuilder();
            sb.Append("<div class=\"powerico\">\r\n");
            sb.Append("<a href=\"javascript:;\" class=\"close\" onclick=\"showPower(this);\"><b>文件上传接口</b></a>");
            sb.Append("</div>\r\n");
            sb.Append("<div style=\"margin-left:20px;display:none;\">\r\n");
            #region 接口地址
            //sb.Append("<input type=\"checkbox\" name=\"anonymousupload\" id=\"anonymousupload\" onclick=\"setModelPropertys(this,'anonymousupload');\"" + (jd.Contains("anonymousupload") && Convert.ToBoolean(jd.getValue("anonymousupload")) ? " checked" : "") + " /><label for=\"anonymousupload\">匿名访问</label><br>");
            sb.Append("<label><input type=\"checkbox\" name=\"anonymousupload\" id=\"anonymousupload\" onclick=\"setModel(this,'anonymousupload');\"" + (ModelInfo.Table.Columns.Contains("anonymousupload") && eBase.parseBool(ModelInfo["anonymousupload"]) ? " checked" : "") + " />匿名访问</label><br>");

            sb.Append("1. 接口地址<br>\r\n");
            sb.Append("<table border=\"0\" cellpadding=\"8\" cellspacing=\"1\" width=\"99%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>描述</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\"><b>值</b></td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">接口地址</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">" + eBase.getBaseURL() + "Plugins/ProUpload.aspx?type=file</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">请求方式</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">Post</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</tbody>\r\n");
            sb.Append("</table>");
            #endregion
            #region 请求参数
            sb.Append("2. 请求参数<br>\r\n");
            sb.Append("<table border=\"0\" cellpadding=\"8\" cellspacing=\"1\" width=\"99%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>名称</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>类型</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\"><b>说明</b></td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>");

            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">auth</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">header</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">token (匿名时值为：anonymous)</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">contentType</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">header</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">multipart/form-data</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">imgFile</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">data</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">form-data</td>\r\n");
            sb.Append("</tr>\r\n");



            sb.Append("</tbody>\r\n");
            sb.Append("</table>");
            #endregion
            #region 返回结果
            sb.Append("3. 返回结果(JSON格式)<br>\r\n");
            sb.Append("<table border=\"0\" cellpadding=\"8\" cellspacing=\"1\" width=\"99%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>名称</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\"><b>说明</b></td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">成功</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">");
            sb.Append("{<br>");
            sb.Append("\"errcode\":\"0\",<br>");
            sb.Append("\"message\":\"请求成功!\",<br>");
            sb.Append("\"files\":[{\"url\":\"a.jpg\"}]<br>");
            sb.Append("}<br>");
            sb.Append("</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">失败</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">");
            sb.Append("{<br>");
            sb.Append("\"errcode\":\"1004\",<br>");
            sb.Append("\"message\":\"toKen已过期!\"<br>");
            sb.Append("}<br>");
            sb.Append("</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</tbody>\r\n");
            sb.Append("</table>");
            #endregion

            sb.Append("</div>");
            eBase.Write(sb.ToString());
            #endregion


            #region 错误代码
            sb = new StringBuilder();
            sb.Append("<div class=\"powerico\">\r\n");
            sb.Append("<a href=\"javascript:;\" class=\"close\" onclick=\"showPower(this);\"><b>错误代码</b></a>");
            sb.Append("</div>\r\n");
            sb.Append("<div style=\"margin-left:20px;display:none;\">\r\n");

            #region 接口地址
            sb.Append("<table border=\"0\" cellpadding=\"8\" cellspacing=\"1\" width=\"99%\">\r\n");
            sb.Append("<thead>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\" width=\"150\"><b>errcode</b></td>\r\n");
            sb.Append("<td bgcolor=\"#E7E7E7\"><b>message</b></td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("</thead>\r\n");
            sb.Append("<tbody>");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">0</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">请求成功!</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">1001</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">toKen不能为空!</td>\r\n");
            sb.Append("</tr>\r\n");

            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">1002</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">toKen已失效!</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">1003</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">toKen验签失败!</td>\r\n");
            sb.Append("</tr>\r\n");

            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">1004</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">toKen已过期!</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">1005</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">登录信息错误!</td>\r\n");
            sb.Append("</tr>\r\n");

            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">1006</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">没有添加权限!</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">1007</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">没有修改权限!</td>\r\n");
            sb.Append("</tr>\r\n");


            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">1008</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">没有删除权限!</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">1009</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">没有查看权限!</td>\r\n");
            sb.Append("</tr>\r\n");

            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">1010</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">没有列表权限!</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">1011</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">信息不存在或已删除!</td>\r\n");
            sb.Append("</tr>\r\n");

            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">1012</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">访问未被许可!</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">1013</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">该toKen不是本系统颁发!</td>\r\n");
            sb.Append("</tr>\r\n");

            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">1014</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">该用户已被禁用!</td>\r\n");
            sb.Append("</tr>\r\n");
            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">1015</td>\r\n");
            sb.Append("<td bgcolor=\"#FAFAFA\">没有绑定帐号(小程序)!</td>\r\n");
            sb.Append("</tr>\r\n");

            sb.Append("<tr>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">1016</td>\r\n");
            sb.Append("<td bgcolor=\"#FFFFFF\">表单必填参数不全!</td>\r\n");
            sb.Append("</tr>\r\n");

            sb.Append("</tbody>\r\n");
            sb.Append("</table>");
            #endregion



            sb.Append("</div>");
            eBase.Write(sb.ToString());
            #endregion

            Response.End();
        }

    }
}