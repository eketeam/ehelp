﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="setModel.aspx.cs" Inherits="eFrameWork.Manage.setModel" %>
<!DOCTYPE html>
<html>
<head>
    <title><asp:Literal ID="LitTitle" runat="server" /></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=9;IE=8;IE=7;" />
	<meta http-equiv="X-UA-Compatible" content="edge" /><!--标准模式-->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">	
	<meta name="renderer" content="webkit"><!--极速模式-->
	<meta name="renderer" content="webkit|ie-comp|ie-stand"> <!--极速模式，兼容模式，IE模式打开-->
	<meta http-equiv="imagetoolbar" content="no" />
    <link href="../Plugins/layui226/css/layui.css" rel="stylesheet" type="text/css" />     
    <link href="../Plugins/eControls/default/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />   
    <link href="../Plugins/Theme/default/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />   
	<link href="../Plugins/Theme/manage/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />
	<script src="../Scripts/Init.js?ver=<%=Common.Version %>"></script>
</head>
<body>
<div style="margin:6px;">
<dl id="eSearchBox" class="ePanel">
<dt><h1 onclick="showPanel(this);" class="search"><a href="javascript:;" class="cur" onfocus="this.blur();"></a>搜索</h1></dt>
<dd style="display:none;">
<asp:PlaceHolder ID="eSearchControlGroup" runat="server">
<form id="frmsearch" name="frmsearch" method="post" onsubmit="return goSearch(this);" action="<%=elist.getSearchURL()%>">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="eDataView">
<colgroup>
<col width="120" />
<col />
</colgroup>
<tr>
<td class="title">模块名称：</td>
<td class="content"><span class="eform"><ev:eSearchControl ID="s1" Name="s1" ControlType="text" Field="MC" Operator="like" FieldName="模块名称" DataType="string" Width="300px" runat="server" /></span></td>
</tr>
<tr>
<td class="title">应用名称：</td>
<td class="content"><span class="eform"><ev:eSearchControl ID="s2" Name="s2" ControlType="radio" Field="ModelID" Operator="custom" FieldName="应用名称" BindObject="a_eke_sysApplications" BindValue="ApplicationID" BindText="MC" BindCondition="deltag=0" BindOrderBy="addTime" Custom="ModelID in (select ModelID from a_eke_sysApplicationItems where ApplicationID='{querystring:s2}' and delTag=0)" DataType="string" runat="server" /></span></td>
</tr>
<tr>
<td class="title">标签：</td>
<td class="content"><span class="eform"><ev:eSearchControl ID="s3" Name="s3" ControlType="checkbox" Field="LabelIDS" Operator="like" FieldName="标签" BindObject="a_eke_sysLabels" BindValue="LableID" BindText="MC" BindCondition="deltag=0 and Type='1'" BindOrderBy="px,addTime" DataType="string" runat="server" /></span></td>
</tr>

<tr>
<td colspan="2" class="title" style="text-align:left;padding-left:125px;"><a class="button" href="javascript:;" onclick="if(frmsearch.onsubmit()!=false){frmsearch.submit();}"><span><i class="search">搜索</i></span></a></td>
</tr>
</table>
</form>
</asp:PlaceHolder>
</dd>
</dl>
<asp:Repeater id="Rep" runat="server">
<headertemplate>
<%#
"<table id=\"eDataTable\" class=\"eDataTable\" border=\"0\" cellpadding=\"0\" cellspacing=\"1\" width=\"100%\">\r\n"+
"<thead>\r\n"+
  "<tr bgcolor=\"#f2f2f2\">\r\n"+
	"<td width=\"250\">模块名称</td>\r\n"+
    "<td>模块类型</td>\r\n"+
	"<td>属性</td>\r\n"+    
    "<td>程序文件</td>\r\n"+
    "<td>标签</td>\r\n"+
	"<td>说明</td>\r\n"+


  "</tr>\r\n"+
"</thead>\r\n" %></headertemplate><itemtemplate><%# "<tr" + ((Container.ItemIndex+1) % 2 == 0 ? " class=\"alternating\" eclass=\"alternating\"" : " eclass=\"\"") + " onclick=\"parent.setModel('" + Eval("ModelID")  + "');\">\r\n"+
	"<td height=\"32\">" + Eval("MC")  + "</td>\r\n"+
    "<td>"+ Eval("Type").ToString().Replace("10","自定义报表").Replace("1","模块").Replace("2","菜单").Replace("3","数据模块").Replace("4","汇总报表模块").Replace("5","流水报表模块") + "</td>\r\n" +
	"<td>"+ Eval("Auto").ToString().Replace("True","配置").Replace("1","配置").Replace("False","自定义").Replace("0","自定义") + "</td>\r\n"+   
	"<td>"+ Eval("AspxFile") + "</td>\r\n"+
    "<td>"+ getLabels(Eval("LabelIDS").ToString()) + "</td>\r\n"+
    "<td>"+ Eval("SM") + "</td>\r\n"+

"</tr>\r\n"%></itemtemplate>
<footertemplate><%#"</table>\r\n"%></footertemplate>
</asp:Repeater>
</div>
<div style="margin:6px;"><ev:ePageControl ID="ePageControl1" PageSize="20" PageNum="9" runat="server" /></div>
</body>
</html>