﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModelCopy.aspx.cs" Inherits="Manage_ModelCopy" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>复制模块</title>
    <link href="../Plugins/layui226/css/layui.css?ver=1.0.4" rel="stylesheet" type="text/css" />     
    <link href="../Plugins/eControls/default/style.css?ver=1.0.4" rel="stylesheet" type="text/css" />   
    <link href="../Plugins/Theme/default/style.css?ver=1.0.4" rel="stylesheet" type="text/css" />   
	<link href="../Plugins/Theme/manage/style.css?ver=1.0.4" rel="stylesheet" type="text/css" />
	<script src="../Scripts/Init.js?ver=1.0.4"></script>
</head>
<body>
<div style="margin:10px;">
<form id="form1" name="form1" method="post" action="">
    <input name="act" type="hidden" id="act" value="save" />
    <asp:Literal id="litBody" runat="server" />
    <%if(eParameters.Request("act").Length==0){ %>
    <div style="margin-top:15px;text-align:center; ">
    <a class="button" href="javascript:;" onclick="if(form1.onsubmit()!=false){form1.submit();}"><span><i class="submit">复制</i></span></a>
     </div>
    <%} %>
</form>
</div>
</body>
</html>