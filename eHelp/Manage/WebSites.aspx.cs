﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using LitJson;


public partial class Manage_WebSites : System.Web.UI.Page
{
    private eDataBase _database;
    private eDataBase DataBase
    {
        get
        {
            if (_database == null)
            {
                _database = eConfig.DefaultDataBase;
            }
            return _database;
        }
    }
    private DataTable _webs;
    public DataTable Webs
    {
        get
        {
            if (_webs == null)
            {
                string sql = "select WebID,'' as id,SiteName as name,domain as wdomain,'false' as " + DataBase.StartNameSplitChar + "delete" + DataBase.EndNameSplitChar + " from a_eke_sysWebSites where SiteID=" + siteid + " and deltag=0 and (ParentID=0 or ParentID is null)";
                _webs = DataBase.getDataTable(sql);
                if (_webs.Rows.Count > 0)
                {
                    foreach (DataRow dr in _webs.Rows)
                    {
                        dr["id"] = dr["WebID"].ToString();
                    }
                }
            }
            return _webs;
        }
    }
    private eUser user;
    public string siteid =  eParameters.QueryString("id");
    public string act = "";
    private string parentModelID = "";
    public string json = "[]";
    public string AppItem = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        user = new eUser("Manage");
        user.Check();


        //eBase.PrintDataTable(Webs);
        siteid = eParameters.QueryString("id");
        act = eParameters.Request("act").ToLower();
        if (AppItem.Length > 0)
        {
            DataRow[] appRows = eBase.a_eke_sysApplicationItems.Select("ApplicationItemID='" + AppItem + "'");
            if (appRows.Length == 0) return;
            parentModelID = appRows[0]["ModelID"].ToString();
        }
        switch (act)
        {
            case "del":
                string sql = "update a_eke_sysWebSites set delTag=1 where SiteID=" + siteid;
                DataBase.Execute(sql);
                break;
            case "save":
                save();
                break;
        }
    }
    private void save()
    {
        if (siteid.Length == 0)
        {
            if (HttpContext.Current.Items["ID"] != null) siteid = HttpContext.Current.Items["ID"].ToString();
        }
        string jsonstr = eParameters.Form("eformdata_" + parentModelID);
        if (jsonstr.Length > 0)
        {
            //eBase.AppendLog("XXX:" + jsonstr);
        }
        else
        {
            jsonstr = eParameters.Form("f11");
            //eBase.AppendLog("YYYY:" + jsonstr);
            if (jsonstr.StartsWith("["))
            {
                //eBase.AppendLog("XXX:" + jsonstr);
                JsonData json = JsonMapper.ToObject(jsonstr);
                foreach (JsonData item in json)
                {
                    #region 循环
                    string id = item.getValue("id");// item["id"].toString();
                    string name = item.getValue("name");//item["name"].toString();
                    string domain = item.getValue("wdomain");//item["domain"].toString();
                    string delete = item.getValue("delete");//item["delete"].toString();
                    //eBase.AppendLog(id + ":" + name + ":" + ":" + domain + ":" + ":" + delete);
                    eTable etb = new eTable("a_eke_sysWebSites", user);
                    if (delete == "true")
                    {
                        #region 删除
                        etb.Where.Add("WebID='" + id + "'");
                        etb.Delete();
                        #endregion
                    }
                    else
                    {
                        if (id.Length == 0)
                        {
                            //eBase.AppendLog("添加");
                            #region 添加
                            // where SiteID=" + siteid
                            string maxid = DataBase.getValue("select ISNULL(MAX(basecode),10) from a_eke_sysWebSites");
                            maxid = (Convert.ToInt32(maxid) + 1).ToString();
                           // eBase.AppendLog(maxid);
                            if (maxid.Length > 0)
                            { 
                                etb.Fields.Add("BaseCode", maxid);
                                etb.Fields.Add("ParentCode", "");
                                etb.Fields.Add("WebCode", maxid);
                            }
                            etb.Fields.Add("siteid", siteid);
                            etb.Fields.Add("SiteName", name);
                            etb.Fields.Add("domain", domain);
                            //eBase.AppendLog(etb.addSQL);
                            etb.Add();
                            #endregion
                        }
                        else
                        {
                           // eBase.AppendLog("更新" + id + "::" + domain);
                            #region 更新
                            etb.Fields.Add("SiteName", name);
                            etb.Fields.Add("domain", domain);
                            etb.Where.Add("WebID=" + id);
                            //eBase.AppendLog(etb.updateSql);
                            etb.Update();
                            #endregion
                        }
                    }
                    #endregion
                }
            }
        }
    }
}