﻿<%@ Page Language="C#" MasterPageFile="Main.Master" AutoEventWireup="true" CodeFile="Items.aspx.cs" Inherits="Items" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteBody" runat="server">
<div class="navlist">
<h1 style="text-align:center;font-weight:bold;font-size:14px;color:#333;padding-top:6px;">标签</h1>
<div class="mytags"><%=hc.Tags %></div>  
</div>


<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" style="table-layout: fixed;">
<colgroup>
<col width="260" />
<col />
<col width="220" />
</colgroup>
  <tr valign="top">
    <td width="260" bgcolor="#F7F9FD">
        <div style="mar3gin:6px 6px 6px 8px;">
        <h1 class="itemmore"><%=hc.ProjectName%><%=hc.ProjectMenu() %>
        </h1>
        <asp:Literal id="litMenu" runat="server" /></div>
    </td>
    <td>
        <div class="nav"><asp:Literal id="LitNav" runat="server" /></div>
	<div class="body">
        <asp:Literal id="LitBody" runat="server" />
	</div>
    </td>
    <td>&nbsp;</td>
  </tr>
</table>
    
</asp:Content>