﻿using System;
using System.Web.UI;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Text;
using EKETEAM.Data;
using EKETEAM.FrameWork;

namespace eFrameWork
{
    public partial class Global : System.Web.HttpApplication
    {

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (eConfig.Debug()) eBase.Writeln("BeginRequest");
            Application["StartTime"] = System.DateTime.Now;
        }
        protected void Application_EndRequest(object sender, EventArgs e)
        {
            if (eConfig.Debug()) eBase.Writeln("EndRequest");
            if (Application["StartTime"] != null)
            {

                System.DateTime startTime = (System.DateTime)Application["StartTime"];
                System.DateTime endTime = System.DateTime.Now;
                System.TimeSpan ts = endTime - startTime;
                //Response.Write("<br>页面执行时间:" + ts.TotalMilliseconds + " 毫秒"); //测试完指定页面后请注释，否则影响有些输出结果(如：json)
            }
        }


    }
}