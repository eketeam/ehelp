﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.FrameWork;
using EKETEAM.Data;


public partial class Search : System.Web.UI.Page
{
    public string key = eParameters.Request("key");
    public eHelpCenter hc;
    protected void Page_Load(object sender, EventArgs e)
    {
       // Response.Write(Request.Url.PathAndQuery);
        ((_Main)Master).hc = hc = new eHelpCenter();
        litMenu.Text = hc.getTreeView();
       // eBase.Writeln(hc.PID);
        //DataRow[] prs = hc.HelpProjects.Select("Show=1");
         if (hc.Projects.Rows.Count > 1)
         {
             string url = eBase.getAbsolutePath();
             if (eBase.IsMobile() || eBase.IsMobileURL()) url += "phone/";
             LitNav.Text = "<a href=\"" + url + "\">首页</a>" + (hc.PID.Length>0 ? "<span></span><a href=\"" + hc.getTopLink(hc.PID) + "\">" + hc.ProjectName + "</a>" : "") + "<span></span>搜索";
         }
        LitBody.Text = hc.getSearchBody();
    }
}