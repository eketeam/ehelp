﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.FrameWork;
using EKETEAM.Data;
using LitJson;
using EKETEAM.Tencent.WxWork;

namespace eFrameWork.Plugins
{
    public partial class BindWxWork : System.Web.UI.Page
    {
        public string UserArea = "Application";
        public eUser user;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser(UserArea);
            if (Request.QueryString["data"] != null)
            {
                string data = Request.QueryString["data"].ToString();
                string base64 = Base64.Decode(data);
                if (base64.Length > 5 && base64.StartsWith("\"") && base64.EndsWith("\"")) base64 = base64.Substring(1, base64.Length - 2);
                base64 = base64.Replace("\\\\/", "\\/");
                base64 = base64.Replace("\\\"", "\"");
                if (base64.StartsWith("{"))
                {
                    JsonData json = JsonMapper.ToObject(base64);
                    //eBase.Print(json);
                    
                    eTable etb = new eTable("a_eke_sysUsers", user);
                    string qyuserid = json.GetValue("UserId");
                    etb.DataBase = eBase.UserInfoDB;
                    etb.Fields.Add("qyUserID", qyuserid);
                    etb.Fields.Add("name", json.GetValue("name"));
                    etb.Fields.Add("gender", json.GetValue("gender"));
                    string headimgurl = json.GetValue("avatar").Replace("\\", "");
                    //eBase.Writeln(user.ID + "::" + qyuserid + "::" + headimgurl);
                    //eBase.End();
                    string face = WxWorkHelper.downHeadImage(user.ID, qyuserid, headimgurl);
                    etb.Fields.Add("avatar", headimgurl);
                    etb.Fields.Add("face", face);
                    etb.Where.Add("UserID='" + user.ID + "'");
                    etb.Update();
                    Response.Write("<script>if(top.frames.length==1){top.CloseWxWork();}else{parent.CloseWxWork();}</script>");
                    //eBase.Writeln("123");
                }
                eBase.End();
            }
            if (eBase.WXWorkAccount.getValue("Proxy").Length == 0 && (eBase.WXWorkAccount.getValue("CorpID").Length == 0 || eBase.WXWorkAccount.getValue("AgentId").Length == 0 || eBase.WXWorkAccount.getValue("Secret").Length == 0))
            {
                litBody.Text = "系统没有绑定企业微信帐号!";
                return;
            }
          
            string qyUserID = eBase.UserInfoDB.getValue("select qyUserID from a_eke_sysUsers where UserID='" + user.ID + "'");
            if (qyUserID.Length > 0)
            {
                litBody.Text = "已绑定!";
            }
            else
            {
                litBody.Text = "<a class=\"button btnprimary\" href=\"javascript:;\" onclick=\"BindWxWork();\"><span style=\"letter-spacing:1px;\"><i class=\"\">授权绑定</i></span></a>";
            }
        }
    }
}