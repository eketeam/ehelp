﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.FrameWork;
using EKETEAM.Data;
using LitJson;
using EKETEAM.Tencent.WxWork;

public partial class Plugins_Tencent_WxWork_scanQRCode : System.Web.UI.Page
{
    public ShareData shareData = new ShareData();
    public string callback = "scanQRCode_callback";
    public string obj = eParameters.QueryString("obj");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["callback"] != null) callback = Request.QueryString["callback"].ToString();
        shareData.title = "标题";
        shareData.desc = "描述";
        shareData.link = Request.QueryString["fromurl"] != null ? Request.QueryString["fromurl"].ToString().UrlDecode() : Request.Url.AbsoluteUri;
        //shareData.imgurl = "ht tp://test.ynzfwh.com/WeChat/images/touch-icon.png";
        WxWorkHelper.getShareData(shareData);
    }
}