﻿<%@ Page Language="C#" %>
<%@ Import Namespace="LitJson" %>
<script runat=server>
public void Page_Load(Object sender, EventArgs e)
{
    Response.Buffer = true;
    Response.ExpiresAbsolute = DateTime.Now.AddDays(-2);
    Response.Cache.SetExpires(DateTime.Now.AddDays(-2));
    Response.Expires = 0;
    Response.CacheControl = "no-cache";
    /*
    if (Request.ServerVariables["HTTP_USER_AGENT"] != null)
    {
        if (Request.ServerVariables["HTTP_USER_AGENT"].ToLower().IndexOf("firefox") > -1)
        {
            //Response.CacheControl = "max-age=0";//系统异常
        }
        else
        {
            //Response.CacheControl = "no-cache";
        }
    }
    */
    eRSA rsa = new eRSA();
    JsonData json = new JsonData();
    json.Add("success", "1");
    json.Add("modulus", rsa.Modulus);
    json.Add("exponent", rsa.Exponent);
    json.Add("message", "请求成功!");
    Response.Write(json.ToJson());
    Response.End();
}
</script>