﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="counterSign.aspx.cs" Inherits="Plugins_counterSign" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>会签</title>
     <link href="../Plugins/eControls/default/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />   
    <script src="../Scripts/Init.js?ver=<%=Common.Version %>""></script>
</head>
<script>var AppItem = "<%=AppItem%>";</script>
<script>
$(document).ready(function () {

    if (typeof (eSignIn) === "object")//签名插件
    {
        eSignIn.init();
    }
});
</script>
<body>
  <div style="margin:15px 8px 8px 8px;">
 <canvas id="esignin" class="esignin">浏览器不支持签名!</canvas>
        <div style="margin-top:15px; text-align:center;">
            <a class="button" href="javascript:;" onclick="saveCounterSign('<%=UserArea %>','<%=itemid %>','<%=pid %>');" style="margin-right:15px;margin-left:8px;"><span style="letter-spacing:1px;"><i class="submit">确认签名</i></span></a>
          <a class="button" href="javascript:;" onclick="eSignIn.clear();"><span style="letter-spacing:1px;"><i class="edit">重签</i></span></a>
        </div>
</div>
</body>
</html>