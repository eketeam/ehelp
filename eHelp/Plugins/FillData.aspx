﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FillData.aspx.cs" Inherits="eFrameWork.Plugins.FillData" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
<%if(IsMobile){ %>
    <link href="../Plugins/Theme/default/mobile/base.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" /><!-- 移动端基础样式-->
    <link href="../Plugins/eControls/default/mobile/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" /><!-- eFrameWork控件样式-->
<%}else{ %>
    <link href="../Plugins/eControls/default/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />  
  	<link href="../Plugins/Theme/default/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />  
<%} %>
</head>
<script>var ModelID = "<%=ModelID%>";</script>
<script src="../Scripts/Init.js?ver=<%=Common.Version %>"></script>
<%if(IsMobile){ %>
<script src="Theme/default/mobile/layout.js?ver=<%=Common.Version %>"></script>
<%} %>
<style>
body {overflow:auto;margin-top:0px;}
</style>
<asp:Literal ID="LitJavascript" runat="server" />
<asp:Literal ID="LitStyle" runat="server" />
<body>
        <%= (model.ModelInfo["StartHTML"].ToString().Length < 3 ? "" : "<div class=\"starthtml\">" + model.ModelInfo["StartHTML"].ToString() + "</div>")   %>
    <asp:Literal ID="LitBody" runat="server" />
</body>
</html>