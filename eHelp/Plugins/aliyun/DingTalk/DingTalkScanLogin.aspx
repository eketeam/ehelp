﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DingTalkScanLogin.aspx.cs" Inherits="eFrameWork.Plugins.DingTalkScanLogin" %>
<%@ Import Namespace="EKETEAM.FrameWork" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>钉钉扫码登录</title>
	 <script src="http://g.alicdn.com/dingding/dinglogin/0.0.5/ddLogin.js"></script>
     <script src="<%=eBase.getAbsolutePath() %>Scripts/jquery.js"></script>
</head>
<body>
     <div id="login_container"></div>
</body>
</html>
<script>
var handleMessage = function (event) {
  var origin = event.origin;
  //console.log("origin", event.origin);
  if( origin == "https://login.dingtalk.com" ) { //判断是否来自ddLogin扫码事件。
    var loginTmpCode = event.data; 
    //获取到loginTmpCode后就可以在这里构造跳转链接进行跳转了
    //alert(loginTmpCode);
    //console.log("loginTmpCode", loginTmpCode);
	var redirect_uri = encodeURIComponent('<%=Request.Url.PathAndFile()%>');
	var url="https://oapi.dingtalk.com/connect/oauth2/sns_authorize?appid=<%=eBase.DingTalkAccount.getValue("AppKey")%>&response_type=code&scope=snsapi_login&state=" +  encodeURIComponent("<%=fromURL%>") + "&redirect_uri=" + redirect_uri + "&loginTmpCode=" + loginTmpCode;
	document.location.assign(url);
  }
};
if (typeof window.addEventListener != 'undefined') {
    window.addEventListener('message', handleMessage, false);
} else if (typeof window.attachEvent != 'undefined') {
    window.attachEvent('onmessage', handleMessage);
}
<%if(Request.QueryString["code"]==null){%>
$(function () {	
	var url = encodeURIComponent('<%=Request.Url.PathAndFile()%>');
	var goto = encodeURIComponent('https://oapi.dingtalk.com/connect/oauth2/sns_authorize?appid=<%=eBase.DingTalkAccount.getValue("AppKey")%>&response_type=code&scope=snsapi_login&state=' +  encodeURIComponent('<%=fromURL%>') + '&redirect_uri='+url);
	var obj = DDLogin({
     id:"login_container",//这里需要你在自己的页面定义一个HTML标签并设置id，例如<div id="login_container"></div>或<span id="login_container"></span>
     //goto: "ht tps://oapi.dingtalk.com/connect/oauth2/sns_authorize?appid=< %=eBase.DingTalkAccount.getValue("CorpId") %>&response_type=code&scope=snsapi_login&state=STATE&redirect_uri=< %=Request.Url.PathAndFile().UrlEncode()%>", //请参考注释里的方式
	 goto:goto,
     style: "border:none;background-color:#FFFFFF;",
     width : "310",
     height: "400"
 	});
});
<%}%>
</script>