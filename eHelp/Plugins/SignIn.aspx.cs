﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using EKETEAM.FrameWork;
using EKETEAM.Data;

public partial class Plugins_SignIn : System.Web.UI.Page
{
    public string UserArea = "Application";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Form.Count == 0) return;
        string id = eParameters.QueryString("id");


        eUser user = new eUser(eBase.getUserArea(UserArea));
        if (!user.Logined) eResult.Error("登录后操作!");
        user.Check();
        eModel model = new eModel();

        string signfile = savefile();
        string act = eParameters.QueryString("act");
        if (act.Length == 0)
        {
            eTable etb = new eTable(model.ModelInfo["code"].ToString());
            etb.Fields.Add("SignIn", "1");
            etb.Fields.Add("SignFile", signfile);
            etb.Fields.Add("SignTime", DateTime.Now);
            etb.Where.Add(etb.primaryKey, "=", id);
           // etb.Where.Add("SignIn=0");
            etb.Where.Add("UserID='" + user.ID + "'");

            etb.Update();
        }

        //eResult.Success("签收成功!");
        eResult.Message(new { success = 1, errcode = "0", signfile = signfile, message = "签收成功!" });
        //eResult.Success(model.ModelInfo["code"].ToString() + "::" + id);
        //eResult.Success(signfile);
    }
    /// <summary>
    /// 保存文件
    /// </summary>
    /// <returns>文件相对路径</returns>
    private string savefile()
    {
        string virtualDir = eConfig.UploadPath();
        string basePath = HttpContext.Current.Server.MapPath("~/") + virtualDir.Replace("/", "\\");
        if (!Directory.Exists(basePath)) Directory.CreateDirectory(basePath);
        string filename = eBase.GetFileName() + ".png";       
        while (File.Exists(basePath + filename))
        {
            filename = eBase.GetFileName() + ".png";
        }
        string imageData = eParameters.Form("imageData");
        using (FileStream fs = new FileStream(basePath + filename, FileMode.Create))
        {
            using (BinaryWriter bw = new BinaryWriter(fs))
            {
                byte[] data = Convert.FromBase64String(imageData);
                bw.Write(data);
                bw.Close();
            }
        }
        return virtualDir + filename;
    }
}