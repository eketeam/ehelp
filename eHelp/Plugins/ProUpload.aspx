﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProUpload.aspx.cs" Inherits="eFrameWork.Plugins.ProUpload" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>附件上传</title>
</head>
<script src="../Scripts/init.js?ver=<%=Common.Version %>"></script>
<script>
function _$(objName){
	try{
		if (document.getElementById){
			return eval('document.getElementById("'+objName+'")');
		}
		else{
			return eval('document.all.'+objName);
		}
	}
	catch(e){}
};
function del() {
    if (confirm("确认要删除文件吗？"))
        return true;
    else
        return false;
};
function checkfrm(frm) {
    if (frm.imgFile.value.length == 0) {
        parent.layer.msg("请选择要上传的文件!");
        frm.imgFile.focus();
        return false;
    }
    return true;
};
</script>
<style>
body{margin:0px;padding-top:0px;font-size:12px;}
#imgFile{font-size:12px;height:22px;font-family:'宋体';}
#button{border:1px solid #cccccc;}
.btn {border:1px solid #cccccc;font-size:12px;height:22px;font-family:'宋体';padding-left:6px;padding-right:6px;}
input {vertical-align:middle;}
form * {vertical-align:middle;}
</style>
<body bgcolor="#ffffff">
<%
if(Request.QueryString["act"]==null)
{
%>
<form method="post" enctype="multipart/form-data" id="Form1" style="margin:0px;padding:0px;" onsubmit="return checkfrm(this);">
<INPUT type="file" id="imgFile" name="imgFile"  onchange="autoupload_check(this);" runat="server" /><INPUT class="btn" name="button" type="submit" id="button" value="本地上传">
<%if(piceidtonline){ %>
    <INPUT class="btn" name="btnsel" type="button" id="Button1" onclick="parent.openPictureEditor(this,'<%=obj%>');" value="图片编辑">
 <%} %>
<%if(1==2){ %>
<INPUT class="btn" name="btnsel" type="button" id="btnsel" onclick="parent.fileLibrary_select('');" value=" 选 择 ">
    <%} %>
<input type="hidden" name="act" id="act" value="save">
<input type="hidden" name="formhost" id="formhost" value="<%=formhost %>">
</form>
<%
}
%>
</body>
</html>