﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using EKETEAM.Data;
using EKETEAM.FrameWork;
namespace eFrameWork.Plugins
{
    public partial class IconSelect : System.Web.UI.Page
    {
        private string basePath = "";//当前目录
        public string path = eParameters.QueryString("path");
        string aspxFile = eBase.getAspxFileName();
        protected void Page_Load(object sender, EventArgs e)
        {
            basePath = eRunTime.fileManagePath;
            if (!Directory.Exists(basePath)) Directory.CreateDirectory(basePath);
            if (path.Contains("..")) path = "";//禁止越权上级目录
            if (path.Length > 0) path = path.toUrlFormat();
            if (path.EndsWith("/")) path = path.Substring(0, path.Length - 1);
            if (!Directory.Exists(basePath + path.toLocalFormat())) path = "";
            if (path.Length > 0) basePath += path.toLocalFormat() + "\\"; 


            StringBuilder sb = new StringBuilder();
            sb.Append("<div class=\"filemanage_local\">");
            if (path.Length > 0)
            {
                sb.Append("<a href=\"" + aspxFile  + "\">根目录</a>");
                string[] arr = path.Split("/".ToCharArray());
                string tmp = "";
                for (int i = 0; i < arr.Length; i++)
                {
                    if (i > 0) tmp += "/";
                    tmp += arr[i];
                    if (i == arr.Length - 1)
                    {
                        sb.Append(" / " + arr[i]);
                    }
                    else
                    {
                        sb.Append(" / <a href=\"" + aspxFile + "?path=" + tmp + "\">" + arr[i] + "</a>");
                    }

                }
            }
            else
            {
                sb.Append("根目录");
            }
            sb.Append("</div>");

            sb.Append("<div class=\"filemanage_files\" style=\"margin:8px;\">");
            eDirectoryInfo dinfo = new eDirectoryInfo(basePath);
            foreach (DirectoryInfo folder in dinfo.GetDirectories())
            {
                sb.Append("<a href=\"" + aspxFile + "?path=" + (path.Length > 0 ? path + "/" : path) + folder.Name.ToString() + "\" title=\"" + folder.Name + "\">\r\n");
                sb.Append("<dl>\r\n");
                sb.Append("<dt class=\"folder\"></dt>\r\n");
                sb.Append("<dd>" + folder.Name + "</dd>\r\n");
                sb.Append("</dl>\r\n");
                sb.Append("</a>\r\n");
            }

            foreach (FileInfo file in dinfo.GetFiles())
            {
                string _path = (basePath + file.Name).toVirtualUrl();
                sb.Append("<a href=\"javascript:;\" onclick=\"parent.setIcon('" + _path + "');\" title=\"" + file.Name + "\">");
                sb.Append("<dl>\r\n");
                if (".jpg.jpeg.gif.bmp.png.tif".IndexOf(file.Extension.ToLower()) > -1)
                {
                    sb.Append("<dt" + (".gif.png".IndexOf(file.Extension.ToLower()) > -1 ? " style=\"background-color:#f1f1f1;\"" : "") + "><img src=\"../" + _path + "\" /></dt>\r\n");
                }
                else
                {
                    sb.Append("<dt class=\"" + eRunTime.getFileClass(file.Extension.ToLower()) + "\"></dt>\r\n");
                }
                sb.Append("<dd>" + file.Name + "</dd>\r\n");
                sb.Append("</dl>\r\n");
                sb.Append("</a>\r\n");
            }
            sb.Append("</div>");

            litBody.Text = sb.ToString();
        }
    }
}