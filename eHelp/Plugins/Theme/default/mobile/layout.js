//摘自淘宝移动端
(function(win, lib) {	
    var doc = win.document;
    var docEl = doc.documentElement;
    var metaEl = doc.querySelector('meta[name="viewport"]');
    var flexibleEl = doc.querySelector('meta[name="flexible"]');
    var dpr = 0;
    var scale = 0;
    var tid;
    var flexible = lib.flexible || (lib.flexible = {});
    var designPixel = 750;//设计稿件尺寸
    
    if (metaEl) { 
        //console.warn('将根据已有的meta标签来设置缩放比例');       
        var match = metaEl.getAttribute('content').match(/initial\-scale=([\d\.]+)/);
        if (match) {
            scale = parseFloat(match[1]);
            dpr = parseInt(1 / scale);
        }
    } else if (flexibleEl) {
        var content = flexibleEl.getAttribute('content');
        if (content) {
            var initialDpr = content.match(/initial\-dpr=([\d\.]+)/);
            var maximumDpr = content.match(/maximum\-dpr=([\d\.]+)/);
            if (initialDpr) {
                dpr = parseFloat(initialDpr[1]);
                scale = parseFloat((1 / dpr).toFixed(2));    
            }
            if (maximumDpr) {
                dpr = parseFloat(maximumDpr[1]);
                scale = parseFloat((1 / dpr).toFixed(2));    
            }
        }
    }
    if (!dpr && !scale) {
        var isAndroid = win.navigator.appVersion.match(/android/gi);
        var isIPhone = win.navigator.appVersion.match(/iphone/gi);
        var devicePixelRatio = win.devicePixelRatio;
        if (isIPhone) {            
            if (devicePixelRatio >= 3 && (!dpr || dpr >= 3)) {                
                dpr = 3;
            } else if (devicePixelRatio >= 2 && (!dpr || dpr >= 2)){
                dpr = 2;
            } else {
                dpr = 1;
            }
        } else {            
            dpr = 1;
        }
        scale = 1 / dpr;
    }

    docEl.setAttribute('data-dpr', dpr);
    if (!metaEl) {
        metaEl = doc.createElement('meta');
        metaEl.setAttribute('name', 'viewport');
        metaEl.setAttribute('content', 'initial-scale=' + scale + ', maximum-scale=' + scale + ', minimum-scale=' + scale + ', user-scalable=no');
        if (docEl.firstElementChild) {
            docEl.firstElementChild.appendChild(metaEl);
        } else {
            var wrap = doc.createElement('div');
            wrap.appendChild(metaEl);
            doc.write(wrap.innerHTML);
        }
    }

    function refreshRem(){
        var width = docEl.getBoundingClientRect().width;
        if (width / dpr > designPixel) {    //如果分辨率不是1，那么获取的物理宽度应该乘以分辨率，才是最终可用的width
            width = width * dpr;
        }
        var rem = width / (designPixel/100); //计算最终还原到设计图上的比例，从而设置到文档上
        docEl.style.fontSize = rem + 'px';
        flexible.rem = win.rem = rem;
    }

    win.addEventListener('resize', function() {
        clearTimeout(tid);
        tid = setTimeout(refreshRem, 300);
    }, false);
    win.addEventListener('pageshow', function(e) {
        if (e.persisted) {
            clearTimeout(tid);
            tid = setTimeout(refreshRem, 300);
        }
    }, false);

    if (doc.readyState === 'complete') {
        doc.body.style.fontSize = 16 * dpr + 'px';
    } else {
        doc.addEventListener('DOMContentLoaded', function(e) {
            doc.body.style.fontSize = 16 * dpr + 'px';
        }, false);
    }
    refreshRem();

    flexible.dpr = win.dpr = dpr;
    flexible.refreshRem = refreshRem;
    flexible.rem2px = function(d) {
        var val = parseFloat(d) * this.rem;
        if (typeof d === 'string' && d.match(/rem$/)) {
            val += 'px';
        }
        return val;
    }
    flexible.px2rem = function(d) {
        var val = parseFloat(d) / this.rem;
        if (typeof d === 'string' && d.match(/px$/)) {
            val += 'rem';
        }
        return val;
    }

})(window, window['lib'] || (window['lib'] = {}));

(function($) {
	$.fn.asideUi = function(options) {
		var defaults = {
			size: '100%',
			hasmask: true,
			position: 'left',
			sidertime: 300
		};
		var val = $.extend(defaults, options);
		var obj = function() {},
			_self = this,
			thisMask = $("<div class='ui-aside-mask' ontouchmove='return false;'></div>"),
			thisCss = {},
			thisCss2 = {};
		thisCss[val.position] = '-' + val.size;
		this.css({
			'top': (val.position == "bottom") ? "auto" : 0,
			'bottom': 0
		});
		thisCss2[val.position] = 0;
		_self.css(thisCss);
		thisMask.on('touchmove', function(event) {
			event.stopPropagation(); 
			event.preventDefault();
		});
		/*
		var startx, starty;
		
		 _self.bind("touchstart", function (events) {
            startY = events.originalEvent.changedTouches[0].pageY;
			//alert(startY);
        });
		 
		_self.bind("touchmove", function (e) {
            e.preventDefault();
			var endy = e.changedTouches[0].pageY;
			var scrollTop = $(this).scrollTop();
			 
			var they=8;//Math.abs(endy-startY);
			if(endy > startY)
			{
				//alert("下");
				$(this).scrollTop(scrollTop - they);
			}
			else
			{
				//alert("上");
				$(this).scrollTop(scrollTop + they);
			}
			//var ulheight = $(this).height();
			
			 //alert(scrollTop);
			// 
        });
		*/
		
		obj.toggle = function() {
			
			if(_self.hasClass('ui-aside-open')) {
				_self.removeClass('ui-aside-open');
				_self.animate(thisCss, val.sidertime);
				$('.ui-aside-mask').animate({
					'opacity': 0
				}, 100, function() {
					$(this).remove();
				});
			} else {
				if($(".ui-aside-open").length>0){return;} //不打开两个块区域
				_self.addClass('ui-aside-open');
				_self.animate(thisCss2, val.sidertime);
				if(val.hasmask) {
					$('body').append(thisMask);
					$(".ui-aside-mask").animate({
						'opacity': 1
					}, 100);
					//thisMask.click(function() {	obj.toggle();});
					thisMask.on("touchend",function() {	obj.toggle();});
				}
			}
		}
	
		//thisMask.click(function() {obj.toggle();});
		

		return obj;
	};
})(window.Zepto || window.jQuery);