var layout_moving_time=180;
var layout_border_width=5;
function handlerInTop()
{
	 $(".eLayout_top").unbind("mouseenter",handlerInTop);
	 $(".eLayout_top").animate({top:'0'},layout_moving_time,function(){$(".eLayout_top").unbind("mouseleave",handlerOutTop).bind("mouseleave",handlerOutTop);});	 
	 $(".eLayout_top").removeClass("eLayout_top_float").addClass("eLayout_top_fixed eLayout_top_alpha");
};
function handlerOutTop()
{
	var h= $(".eLayout_top").height()-layout_border_width;
	$(".eLayout_top").unbind("mouseleave",handlerOutTop);
	$(".eLayout_top").animate({top:-h},layout_moving_time,function(){$(".eLayout_top").unbind("mouseenter",handlerInTop).bind("mouseenter",handlerInTop);});
	$(".eLayout_top").removeClass("eLayout_top_fixed").addClass("eLayout_top_float");
};
function handlerInLeft()
{

	 $(".eLayout_menu").unbind("mouseenter",handlerInLeft);	
	 $(".eLayout_menu").animate({left:'0'},layout_moving_time,function(){$(".eLayout_menu").unbind("mouseleave",handlerOutLeft).bind("mouseleave",handlerOutLeft);});
	 $(".eLayout_menu").removeClass("eLayout_menu_float").addClass("eLayout_menu_fixed eLayout_left_alpha");
	 $(".eLayout_menu").getNiceScroll().show();	 
};
function handlerOutLeft()
{
	var w=$(".eLayout_menu").width()-layout_border_width;
	$(".eLayout_menu").unbind("mouseleave",handlerOutLeft);	
	$(".eLayout_menu").animate({left:-w},layout_moving_time,function(){$(".eLayout_menu").unbind("mouseenter",handlerInLeft).bind("mouseenter",handlerInLeft);});	
	$(".eLayout_menu").removeClass("eLayout_menu_fixed").addClass("eLayout_menu_float");
	$(".eLayout_menu").getNiceScroll().hide();
};
function setFixed(name,value)
{
	var url="";
	if(typeof(layout_setFixed_file)=="string")
	{
		url=layout_setFixed_file + "?act=setfixed";
	}
	else
	{
		url="model.aspx?act=setfixed";
	
	}
	if(typeof(AppItem)=="string" && AppItem.length > 0){url+="&AppItem="+AppItem;}
	url+="&name=" + name + "&value=" + value + "&t=" + now();
	$.ajax({
			type: 'get',
			url: url,			
			dataType: "html",
			success: function(data)
			{				
			}
		});
};
function switchLeftFix()
{
	if($(".leftfixbtn").hasClass("leftfixbtn_float"))
	{
		//document.title="修改为固定";
		$(".leftfixbtn").removeClass("leftfixbtn_float").addClass("leftfixbtn_fixed");
		if($(".eLayout_content").hasClass("eLayout_content_float"))
		{
			$(".eLayout_content").removeClass("eLayout_content_float").addClass("eLayout_content_fixed");
		}
		$(".eLayout_menu").unbind("mouseenter",handlerInLeft);
		$(".eLayout_menu").unbind("mouseleave",handlerOutLeft);	

		$(".eLayout_menu").removeClass("eLayout_left_alpha");
		 
		setFixed("application_left_fixed","true");
	}
	else
	{
		//document.title="修改为浮动";
		$(".leftfixbtn").removeClass("leftfixbtn_fixed").addClass("leftfixbtn_float");
		if($(".eLayout_content").hasClass("eLayout_content_fixed"))
		{
			$(".eLayout_content").removeClass("eLayout_content_fixed").addClass("eLayout_content_float");
		}
		$(".eLayout_menu").unbind("mouseenter",handlerInLeft).bind("mouseenter",handlerInLeft);
		$(".eLayout_menu").unbind("mouseleave",handlerOutLeft).bind("mouseleave",handlerOutLeft);

		$(".eLayout_menu").addClass("eLayout_left_alpha");
		setFixed("application_left_fixed","false");
	}
};
function switchTopFix()
{
	if($(".topfixbtn").hasClass("topfixbtn_float"))
	{
		//document.title="修改为固定";
		$(".topfixbtn").removeClass("topfixbtn_float").addClass("topfixbtn_fixed");
		if($(".eLayout_body").hasClass("eLayout_body_float"))
		{
			$(".eLayout_body").removeClass("eLayout_body_float").addClass("eLayout_body_fixed");
		}
		$(".eLayout_top").unbind("mouseenter",handlerInTop);
		$(".eLayout_top").unbind("mouseleave",handlerOutTop);	
		$(".eLayout_top").removeClass("eLayout_top_alpha");
		setFixed("application_top_fixed","true");
		
	}
	else
	{
		//document.title="修改为浮动";
		$(".topfixbtn").removeClass("topfixbtn_fixed").addClass("topfixbtn_float");

		if($(".eLayout_body").hasClass("eLayout_body_fixed"))
		{
			$(".eLayout_body").removeClass("eLayout_body_fixed").addClass("eLayout_body_float");
		}
		$(".eLayout_top").unbind("mouseenter",handlerInTop).bind("mouseenter",handlerInTop);
		$(".eLayout_top").unbind("mouseleave",handlerOutTop).bind("mouseleave",handlerOutTop);
		$(".eLayout_top").addClass("eLayout_top_alpha");
		setFixed("application_top_fixed","false");
	}	
	eLayout_autoSize();
};
$(window).resize(function() {
  eLayout_autoSize();
});
function eLayout_autoSize()
{
	$(".eFrameTab dd").css("height", ($(".eFrameTab").parent().height() - $(".eFrameTab dt").outerHeight()) + "px");
};
$(document).ready(function(){

	$(".eLayout_top_float").unbind("mouseenter",handlerInTop).bind("mouseenter",handlerInTop);
	$(".eLayout_menu_float").unbind("mouseenter",handlerInLeft).bind("mouseenter",handlerInLeft);
	
	eLayout_autoSize();//兼容google等浏览器，去调用一次。	 
	if(typeof($.fn.niceScroll)=="function")
	{
		$(".eLayout_menu").niceScroll({  cursorcolor:"#ccc",cursorwidth:10, cursoropacitymax:0.5, cursorborder:"none"});
		if($(".eLayout_menu").hasClass("eLayout_menu_float"))
		{
			$(".eLayout_menu").getNiceScroll().hide();
		}
		if($(".eFrameTab").length==0)
		{
			//$(".eLayout_content").niceScroll({  cursorcolor:"#333",cursorwidth:10,  cursoropacitymax:0.5, cursorborder:"none"});	
		}
		else
		{
			$(".eLayout_content").css("overflow","hidden");
			//$(".eLayout_content").css("overflow","auto");
		}
	}
	
});