﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Items.aspx.cs" Inherits="Phone_Items" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
 <title><%=hc.Title%></title>
 <meta name="Keywords" content="<%=hc.Keywords%>" />
 <meta name="Description" content="<%=hc.Description%>" />
 <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
 <link href="<%=eBase.getAbsolutePath() %>Plugins/Theme/default/mobile/base.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" /><!-- 移动端基础样式-->
 <link href="<%=eBase.getAbsolutePath() %>phone/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />
</head>
<script src="<%=eBase.getAbsolutePath() %>Scripts/jquery.js"></script>
<script src="<%=eBase.getAbsolutePath() %>Plugins/Theme/default/mobile/layout.js?ver=<%=Common.Version %>"></script>
<script src="<%=eBase.getAbsolutePath() %>phone/view.js?ver=<%=Common.Version %>"></script>
<body>
<div id="eSearchBox" style="display:none;">
<h1>搜索<a href="javascript:;" class="close" nowait="true" onclick="switchSearch();"></a></h1>
<div id="SearchItems"></div>

<form id="form1" name="form1" method="post" onsubmit="return goSearch(this);" action="<%=hc.SearchURL %>">

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="eDataView">
<tr>
<td width="60%"><input type="text" name="key" id="key" value="<%=hc.Key %>" class="text" placeholder="输入关键词" autocomplete="off" /></td>
<td width="40%"><a class="button" href="javascript:;" onclick="form1.onsubmit();">搜索</a></td>
</tr>
</table>
</form>

</div>

<div class="eHeader_space"></div> 
<%=hc.Header%>

<aside class="ui-aside" style="width:6rem;background-color:#FFF;">
<div style="height:56px;"></div>
<div id="sideparent" style="overflow:hidden;overflow-y:scroll;overflow-x:hidden;">
<div id="sidebody">
<asp:Literal id="litMenu" runat="server" />
<div class="clear"></div>
</div>
</div>
</aside>


<div class="eHeader">
<div class="left"><a href="javascript:;" nowait="true" class="menu" _onclick="_showmenu();"></a></div>
<div class="center"><%=hc.ProjectName%></div>
<div class="right">
<a href="javascript:;" class="search" nowait="true" onclick="switchSearch();"></a>
</div>
</div>    


<div class="nav"><asp:Literal id="LitNav" runat="server" /></div>
	<div class="body" style="backgrou5nd-color:#f1f1f1;">
        <asp:Literal id="LitBody" runat="server" />
	</div>

<div class="cpspace"></div>
<div class="copyright">技术支持：<a href="http://frame.eketeam.com/">eFrameWork低代码平台</a></div>
<%=hc.Footer%>
</body>
</html>