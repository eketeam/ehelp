﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.FrameWork;

public partial class Phone_Search : System.Web.UI.Page
{
    public string key = eParameters.Request("key");
    public eHelpCenter hc;
    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Write(Request.Url.PathAndQuery);

        hc = new eHelpCenter();
        litMenu.Text = hc.getTreeView();
        //DataRow[] prs = hc.HelpProjects.Select("Show=1");
        if (hc.Projects.Rows.Count > 1)
        {
            string url = eBase.getAbsolutePath();
            if (eBase.IsMobile() || eBase.IsMobileURL()) url += "phone/";
            LitNav.Text = "<a href=\"" + url + "\">首页</a><span></span><a href=\"" + hc.getTopLink(hc.PID) + "\">" + hc.ProjectName + "</a><span></span>搜索";
        }
        else
        {
            LitNav.Text = "<a href=\"" + hc.getTopLink(hc.PID) + "\">" + hc.ProjectName + "</a><span></span>搜索";
        }
        LitBody.Text = hc.getSearchBody();
    }
}