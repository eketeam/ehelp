﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using EKETEAM.Data;
using EKETEAM.FrameWork;


public partial class Phone_Items : System.Web.UI.Page
{
    public eHelpCenter hc;
    protected void Page_Load(object sender, EventArgs e)
    {
        //eBase.Writeln(Request.Url.PathAndQuery);

        hc = new eHelpCenter();
        litMenu.Text = hc.getTreeView();
        LitNav.Text = hc.getNav();
        LitBody.Text = hc.getList();
    }
}