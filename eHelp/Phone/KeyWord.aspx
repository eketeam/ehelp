﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="KeyWord.aspx.cs" Inherits="eFrameWork.Phone_KeyWord" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
     <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<title><%=Title%></title>
    <script src="./Scripts/jquery.js"></script>
    <script src="./view.js?ver=<%=Common.Version %>"></script>
</head>
<style>
*{box-sizing: border-box;}
html,body,dd,ul,li{margin:0px;padding:0px;}
html,body{
font-family:  "Helvetica Neue", Helvetica, "PingFang SC", "Hiragino Sans GB", "Microsoft YaHei", "微软雅黑", Arial, sans-serif,PingFangSC-Semibold;
}
body {    padding:10px;}
a{ text-decoration:none;color: #334356;}
a:hover{color: #1C85E7;}
    h1, h2, h3, h4 {margin:0px;padding:0px;
    }
 h1 {
    line-height:55px; font-weight:normal;font-size:22px;border-bottom:1px solid #ccc;color:#444;
    }
    h2 { line-height:45px;
    }
     h3 { line-height:35px;
    }
.body
{

 margin-top:10px;line-height:22px;font-size:14px;
}
    .body p {
    margin:0px;padding:0px;
    }


li{list-style-type:none;padding-left:20px;}
li.dics{ background-color:#ccc;}
</style>
<body>
<%=hc.Header%>
<h1><%=Title%></h1>
<div class="body">
<asp:Literal ID="LitBody" runat="server" /> 
</div>
<%=hc.Footer%>
</body>
</html>