﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Phone_Default" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<title><%=hc.Title%></title>
<meta name="Keywords" content="<%=hc.Keywords%>" />
<meta name="Description" content="<%=hc.Description%>" />
</head>
<style>
    a {
    text-decoration:none;color: #0066CC;
    }
    html, body {
    padding:0px;margin:0px;
    }
    .body {
    margin:10px;
    }
    dl {padding:0px;margin:0px;
    }
    dt, dd {padding:0px;margin:0px;
    }
    dt {
    font-family: PingFangSC-Medium;
font-size: 20px;
color: #191c3d;
line-height: 26px;
margin:10px 0px 10px 0px;
    }
        dt a {
        color:#555;font-size:18px;
        }
    dd {
    border:1px solid #f1f1f1;padding:15px;line-height:35px;
    }
    ul, li {
    list-style:none;padding:0px;margin:0px;
    }
    li {
    display:inline-block;max-wi3dth:195px;wid3th:195px;width:45%;fl4oat:left;
    }
      li a {display:block; white-space:nowrap; overflow:hidden;-o-text-overflow:ellipsis;text-overflow:ellipsis;
        }
    .logo {
    vertical-align:middle;height:50px;line-height:40px;
    }
   .logo * { vertical-align:middle;
    }
        .logo .name {height:40px;color:#444;font-size:14px;
        }
        .logo img {
        max-width:260px;max-height:40px;
        }
.cpspace {height:38px;}
.copyright{color:#444;font-size:13px; text-align:center;border-top:1px solid #E7E9EC;line-height:35px;width:100%;background-color:#fff;
position: fixed;left: 0px;bottom: 0px;}
.copyright a{color:#444;}
</style>
<body>
<%=hc.Header%>
    <div style="border-bottom:1px solid #E7E9EC;" class="logo"><%=hc.LogoAndName() %></div>
    <div class="body">
   <asp:Literal ID="LitBody" runat="server" /> 
    </div>

<div class="cpspace"></div>
<div class="copyright">技术支持：<a href="http://frame.eketeam.com/">eFrameWork低代码平台</a></div>
<%=hc.Footer%>
</body>
</html>