﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using EKETEAM.Data;
using EKETEAM.FrameWork;

public partial class Phone_Item : System.Web.UI.Page
{
    public eHelpCenter hc;
    protected void Page_Load(object sender, EventArgs e)
    {
        hc = new eHelpCenter();
        //litMenu.Text = hc.getTree("");
        litMenu.Text = hc.getTreeView();
        LitNav.Text = hc.getNav();
        LitBody.Text = hc.getBody();
    }
}