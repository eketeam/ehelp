﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="app_UserCenter.aspx.cs" Inherits="eFrameWork.AppMobile.app_UserCenter" %>
<%@ Register src="ToolMenu.ascx" tagname="ToolMenu" tagprefix="uc1" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><%=appTitle %></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0,uc-fitscreen=yes" />
    
    <link href="../Plugins/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet">
    <link href="../Plugins/Theme/default/mobile/base.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" /><!-- 移动端基础样式-->
    <link href="../Plugins/eControls/default/mobile/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" /><!-- eFrameWork控件样式-->
    <link href="../Plugins/Theme/default/mobile/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" /><!--移动端样式-->


    <link href="style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery.js"></script>
	<script src="../Plugins/layui226/layui.all.js?ver=<%=Common.Version %>"></script>
    <script src="../Scripts/eketeam.js?ver=<%=Common.Version %>"></script>
    <script src="../Plugins/Theme/default/mobile/layout.js?ver=<%=Common.Version %>"></script>
    <script src="js/fast555click.js?ver=<%=Common.Version %>"></script>
</head>
<style>
html,body{ background-color:#F5F5F5;}
</style>
<body>


<div class="wapper">
<div class="title">个人中心</div>
</div>


<div class="info">
<img class="face" src="<%=user["face"].ToString().Length==0 ? "images/touch-icon.png" : user["face"].ToString()%>">
<div class="name"><%=user["name"].ToString() %></div>
<span><img src="images/icon-2.png"><u>部门</u><i><%=row["Department"].ToString() %></i></span>
<span><img src="images/icon-1.png"><u>岗位</u><i><%=row["zw"].ToString() %></i></span>
</div>


<div class="listitem">
<a href="app_UserInfo.aspx?AppItem=<%=AppItem%>" class="user">个人信息</a>
<a href="app_ModifyPass.aspx?AppItem=<%=AppItem%>" class="pass">修改密码</a>
<a href="LoginOut.aspx" onClick="javascript:return confirm('确认要退出吗？');" class="out">退出登录</a>
</div>


<uc1:ToolMenu ID="ToolMenu1" runat="server" />
</body>
</html>