﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using EKETEAM.Data;
using EKETEAM.FrameWork;

namespace eFrameWork.AppMobile
{
    public partial class Custom : System.Web.UI.Page
    {
        public string UserArea = "Application";        
        public eModel model;      
        public string addUrl = "";
        public bool Ajax = false;
        public eUser user;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["ajax"] != null) Ajax = Convert.ToBoolean(Request.QueryString["ajax"]);
            user = new eUser(UserArea);
            user.Check();
           

            eModelInfo customModel = new eModelInfo(user);
            model = customModel.Model;
            model.clientMode = "mobile";
            //model.viewActionBotton = true; //查看对允许编辑、删除等动作
            //model.ModelInfo["viewActionBotton"] = true; //查看对允许编辑、删除等动作
            //if (model.Action.Length == 0) model.Action = "view";
            addUrl = Request.Url.PathAndQuery.addQuery("act","add");

            string path = model.ModelInfo["mAspxFile"].ToString().Length > 0 ? model.ModelInfo["mAspxFile"].ToString() : model.ModelInfo["AspxFile"].ToString();
            eBase.ExecutePage(path, LitBody);
            /*
            if (!eBase.parseBool(model.ModelInfo["mAutoLayout"]))
            {
                Response.Write(LitBody.Text);
                this.Page.Visible = false;
            }
            */
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (Master == null) return;
            Literal lit = (Literal)Master.FindControl("LitTitle");
            if (lit != null) lit.Text = eConfig.ApplicationTitle(user["SiteID"].ToString()); // model.ModelInfo["mc"].ToString() + " - " +

            lit = (Literal)Master.FindControl("LitJavascript");
            if (lit != null && model.Javasctipt.Length > 0) lit.Text = model.Javasctipt;


            lit = (Literal)Master.FindControl("LitStyle");
            if (lit != null && model.cssText.Length > 0) lit.Text = model.cssText;
        }
    }
}