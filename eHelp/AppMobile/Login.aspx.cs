﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;

namespace eFrameWork.AppMobile
{
    
    public partial class Login : System.Web.UI.Page
    {
        public string UserArea = "Application";
        public bool isIphone()
        {
            if (Request.QueryString["app"] != null) return false;
            string userAgent = Request.ServerVariables["HTTP_USER_AGENT"].ToString().Replace(" ", "").ToLower();
            if (userAgent.IndexOf("iphone;") > -1) return true;
            return false;
        }
        public bool isIphoneSafari()
        {
            string userAgent = Request.ServerVariables["HTTP_USER_AGENT"].ToString().Replace(" ", "").ToLower();
            if (userAgent.IndexOf("iphone;") > -1 && userAgent.IndexOf("safari/") > -1) return true;
            return false;
        }
        public bool isAndroid()
        {
            if (Request.QueryString["app"] != null) return false;
            string userAgent = Request.ServerVariables["HTTP_USER_AGENT"].ToString().Replace(" ", "").ToLower();
            if (userAgent.IndexOf(";android") > -1) return true;
            return false;
        }

        public bool isAndroidBrowser()
        {

            string userAgent = Request.ServerVariables["HTTP_USER_AGENT"].ToString().Replace(" ", "").ToLower();
            if (userAgent.IndexOf("vivobrowser/") > -1) return true;
            if (userAgent.IndexOf("micromessenger/") > -1) return false;
            if (userAgent.IndexOf("alipayclient/") > -1) return false;
            if (userAgent.IndexOf("qqtheme/") > -1) return false;

            return true;
        }

        public string fromURL = "";
        protected void Page_Load(object sender, EventArgs e)
        {

            string userAgent = Request.ServerVariables["HTTP_USER_AGENT"].ToString().Replace(" ", "").ToLower();
            //if (userAgent.IndexOf("wxwork/") > -1) Response.Redirect("qyLogin.aspx", true);
            eFHelper.checkErrorCount(); //检查登录错误次数是否超过设定值
            fromURL = eParameters.QueryString("fromURL");//登录来源页面URL
            if (fromURL.Length == 0) fromURL = eBase.getLocalPath();
            string UserAgent = Request.ServerVariables["HTTP_USER_AGENT"];
            if (string.IsNullOrEmpty(UserAgent))
            {
                eFHelper.setErrorSession(); //增加登录错误次数
                Response.Write("<script>alert('登录信息不正确！');document.location='" + fromURL + "';</script>");
                Response.End();
            }
            string LoginFile = "Login.aspx";
            if (fromURL.Length > 0) LoginFile += "?fromURL=" + HttpUtility.UrlEncode(fromURL);
            if (Request.Form["yhm"] != null)
            {
                if (eConfig.openRndCode()) eFHelper.checkRndCode(Request.Form["yzm"].ToString(), fromURL); //验证验证码是否正确
                string sql = "Select top 1 * From a_eke_sysUsers Where delTag=0 and Active=1 and YHM='" + eParameters.Form("yhm") + "'";
                string siteid = eParameters.Form("siteid");
                if (siteid.Length > 0) sql += " and SiteID='" + siteid + "'";
                DataTable tb = eBase.UserInfoDB.getDataTable(sql);
                if (tb.Rows.Count == 0)
                {
                    eFHelper.setErrorSession(); //增加登录错误次数
                    Response.Write("<script>alert('登录信息不正确！');document.location='" + LoginFile + "';</script>");
                    Response.End();
                }
                else
                {
                    string encpass = Request.Form["mm"].ToString(); //得到RSA加密的密码
                    string pass = eRSA.getPass(encpass);//进行RSA解密
                    if (eBase.getPassWord(pass) == tb.Rows[0]["mm"].ToString())
                    {
                        #region 单点登录
                        if (eConfig.singleSign)
                        {
                            string ip = eBase.getIP();
                            string lastip = eBase.UserInfoDB.getValue("select top 1 IP from a_eke_sysUserLog where UserID='" + tb.Rows[0]["userid"].ToString() + "' order by addtime desc");
                            if (ip.Length > 7 && lastip.Length > 7 && lastip != ip)
                            {
                                string lastUserAgent = eBase.UserInfoDB.getValue("select UserAgent from a_eke_sysUserLog where UserID='" + tb.Rows[0]["userid"].ToString() + "' and DATEDIFF(dd,getdate(),addTime)=0");
                                if (lastUserAgent.Length > 0)
                                {
                                    if (lastUserAgent.ToLower() != UserAgent.ToLower())
                                    {
                                        eFHelper.setErrorSession(); //增加登录错误次数
                                        Response.Write("<script>alert('请勿违规登录,否则封禁帐号!');document.location='" + fromURL + "';</script>");
                                        Response.End();
                                    }
                                }
                            }
                        }
                        #endregion
                        eUser user = new eUser(UserArea);
                        #region 多身份
                        user["userpid"] = "";
                        if (tb.Rows[0]["ParentID"].ToString().Length > 0)
                        {
                            user["userpid"] = tb.Rows[0]["ParentID"].ToString();
                        }
                        else
                        {
                            string tmp = eBase.UserInfoDB.getValue("select count(1) from a_eke_sysUsers Where delTag=0 and Active=1 and ParentID='" + tb.Rows[0]["UserID"].ToString() + "'");
                            if (tmp != "0")
                            {
                                user["userpid"] = tb.Rows[0]["UserID"].ToString();
                            }
                        }
                        #endregion

                        user["app"] = eParameters.Form("app");//是否APP运行
                        user["iphoneapp"] = eParameters.Form("iphoneapp"); //是否WebAPP运行
                        eFHelper.saveUserInfo(user, tb.Rows[0]); //保存用户登录信息
                        eFHelper.UserLoginLog(user); //用户登录日志
                        if (eParameters.Form("iphoneapp") == "1" || eParameters.Form("app")=="true")
                        {
                            Response.Write("<script>localStorage.cookie=document.cookie;document.location='" + HttpUtility.UrlDecode(fromURL) + "';</script>");
                            Response.End();
                        }
                        Response.Redirect(HttpUtility.UrlDecode(fromURL), true);
                    }
                    else
                    {
                        eFHelper.setErrorSession();//增加登录错误次数
                        Response.Write("<script>alert('登录信息不正确！');document.location='" + LoginFile + "';</script>");
                        Response.End();
                    }
                }
            }
        }
    }
}