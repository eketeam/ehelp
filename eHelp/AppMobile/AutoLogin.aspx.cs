﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;

namespace eFrameWork.AppMobile
{
    public partial class AutoLogin : System.Web.UI.Page
    {
        private eUser user;
        protected void Page_Load(object sender, EventArgs e)
        {
            //user = new eUser(dmConfig.getWebArea());
            user = new eUser("Web");
            if (user.Logined)
            {
                eUser nUser = new eUser("Mobile");
                nUser["id"] = user["id"].ToString();
                nUser["name"] = user["nick"].ToString();
                nUser.Save();
                Response.Redirect("Default.aspx", true);
            }
            else
            {
                Response.Write("请先登录!");
                Response.End();
            }            
        }
    }
}