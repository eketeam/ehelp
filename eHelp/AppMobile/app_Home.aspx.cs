﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using EKETEAM.UserControl;
using System.Text;

namespace eFrameWork.AppMobile
{
    public partial class app_Home : System.Web.UI.Page
    {
        public string UserArea = "Application";
        private eUser user;
        public string AppItem = eParameters.Request("AppItem");
        ApplicationMenu appmenu;

        public eModel model;
        public string appTitle = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser(UserArea);
            user.Check();
            appTitle = eConfig.ApplicationTitle(user["SiteID"].ToString());
            appmenu = new ApplicationMenu(user, "2");

            StringBuilder sb = new StringBuilder();
            DataRow[] rows, rs;
            #region 已分组

            rows = appmenu.AppItems.Select("ModelID is Null and Show=1", "PX,addTime");
            for (int i = 0; i < rows.Length; i++)
            {
                if (!appmenu.MenuIDS.Contains(rows[i]["ApplicationItemID"].ToString()) && rows[i]["URL"] == DBNull.Value) continue;
                sb.Append("<dl class=\"icongroup\">\r\n");
                sb.Append("<dt>" + rows[i]["MC"].ToString() + "</dt>\r\n");
                sb.Append("<dd>\r\n");
                rs = appmenu.AppItems.Select("ParentID='" + rows[i]["ApplicationItemID"].ToString() + "' and ModelID is not Null and Show=1", "PX,addTime");
                for (int j = 0; j < rs.Length; j++)
                {
                    if (!appmenu.MenuIDS.Contains(rs[j]["ApplicationItemID"].ToString())) continue;

                    sb.Append("<a href=\"" + rows[i]["url"].ToString() + "\">");
                    if (rs[j]["IconHTML"].ToString().Length > 0)
                    {
                        sb.Append(rs[j]["IconHTML"].ToString());
                    }
                    else
                    {
                        sb.Append("<img" + (rs[j]["Finsh"].ToString() == "False" ? " class=\"gray\"" : "") + " src=\"../" + rs[j]["icon"].ToString() + "\"  />");
                    }
                    sb.Append("<p" + (rs[j]["Finsh"].ToString() == "False" ? " style=\"color:#ccc;\"" : "") + ">" + rs[j]["MC"].ToString() + "</p></a>\r\n");
                }
                sb.Append("</dd>\r\n");
                sb.Append("<div class=\"clear\"></div>\r\n");
                sb.Append("</dl>\r\n");

            }
            #endregion
            #region 未分组
            rs = appmenu.AppItems.Select("ModelID is not Null and ParentID is null and Show=1", "PX,addTime");
            if (rs.Length > 0)
            {
                sb.Append("<dl class=\"icongroup\">\r\n");
                sb.Append("<dt>未分组</dt>\r\n");
                sb.Append("<dd>\r\n");
                for (int j = 0; j < rs.Length; j++)
                {
                    if (!appmenu.MenuIDS.Contains(rs[j]["ApplicationItemID"].ToString())) continue;
                    sb.Append("<a href=\"" + rs[j]["url"].ToString() + "\"><img" + (rs[j]["Finsh"].ToString() == "False" ? " class=\"gray\"" : "") + " src=\"../" + rs[j]["icon"].ToString() + "\"  /><p" + (rs[j]["Finsh"].ToString() == "False" ? " style=\"color:#ccc;\"" : "") + ">" + rs[j]["MC"].ToString() + "</p></a>\r\n");
                }
                sb.Append("</dd>\r\n");
                sb.Append("<div class=\"clear\"></div>\r\n");
                sb.Append("</dl>\r\n");
            }
            #endregion
            LitMenu.Text = sb.ToString();
        }
    }
}