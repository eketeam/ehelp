﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="app_Desk.aspx.cs" Inherits="eFrameWork.AppMobile.app_Desk" %>
<%@ Import Namespace="EKETEAM.FrameWork" %>
<%@ Register src="ToolMenu.ascx" tagname="ToolMenu" tagprefix="uc1" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0,uc-fitscreen=yes" />
     <title><%= appTitle %></title>     
    <link href="../Plugins/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet">
    <link href="../Plugins/Theme/default/mobile/base.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" /><!-- 移动端基础样式-->
    <link href="../Plugins/eControls/default/mobile/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" /><!-- eFrameWork控件样式-->
    <link href="../Plugins/Theme/default/mobile/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" /><!--移动端样式-->
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/eketeam.js?ver=<%=Common.Version %>"></script>
    <script src="../Plugins/Theme/default/mobile/layout.js?ver=<%=Common.Version %>"></script>
    <link rel="stylesheet" type="text/css" href="../Plugins/swiper342/swiper342.css"/>
    <link rel="stylesheet" type="text/css" href="../Plugins/swiper342/index.css"/> 
    <script type="text/javascript" src="../Plugins/swiper342/swiper342.min.js"></script>
</head>
 <style>
html,body {background-color:#EDEDED;}
</style>
<body>
<div style="padding:10px;background-color:#EDEDED;">
<asp:Literal ID="LitMenu" runat="server" />
    </div>
<uc1:ToolMenu ID="ToolMenu1" runat="server" />

</body>
</html>