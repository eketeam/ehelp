﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using EKETEAM.UserControl;
using System.Text;

namespace eFrameWork.AppMobile
{
    public partial class app_Statistics : System.Web.UI.Page
    {
        public string UserArea = "Application";
        private eUser user;
        public string AppItem = eParameters.Request("AppItem");
        public eModel model;
        public string appTitle = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser(UserArea);
            user.Check();
            appTitle = eConfig.ApplicationTitle(user["SiteID"].ToString());

            eModelInfo customModel = new eModelInfo(user);
            model = customModel.Model;

        }
    }
}