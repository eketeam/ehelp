﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="eFrameWork.AppMobile.Login" %>
<%@ Import Namespace="EKETEAM.FrameWork" %>
<!DOCTYPE html>
<html>
<head>
    <title><%= eConfig.ApplicationTitle("") %></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="eFrameWork">
    <meta name="renderer" content="webkit">
    <meta name="apple-mobile-web-app-status-bar-style" content="default" />	
    <link rel="apple-touch-icon-precomposed" href="/AppMobile/images/touch-icon.png"/>
</head>
<link href="../Plugins/Theme/default/mobile/base.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />
<script src="../Scripts/jquery.js?ver=<%=Common.Version %>"></script>
<script src="../Plugins/layui226/layui.all.js?ver=<%=Common.Version %>"></script>
<script src="../Scripts/eketeam.js?ver=<%=Common.Version %>"></script>
<script src="../Plugins/Theme/default/mobile/layout.js?ver=<%=Common.Version %>"></script>

<script>
    $(document).ready(function () {
        $('.container').css({ width: $(document).width(), height: $(document).height() });   // 设置屏幕宽高
    });
    function getrnd(type) {
        if (type == 0 && document.getElementById("rndpic").src.indexOf("none.gif") == -1) {
            return;
        }
        document.getElementById("rndpic").src = "../Plugins/RndPic.aspx?t=" + now();
    };
</script>
<style>
html,body{background-color:#f5f5f5;}
.container {
    width:100%;height:100%;
    position:relative;
}
.area{width:100%;height:3.2rem; text-align:center;line-height:2rem;font-size:.5rem;color:#fff;
background: -webkit-linear-gradient(bottom,#1fd5e6,#499BFE);
background: -o-linear-gradient(bottom,#21b6f8,#499BFE);
background: -moz-linear-gradient(bottom,#21b6f8,#499BFE);
background: -mos-linear-gradient(bottom,#1fd5e6,#499BFE);
}
.form
{
background: #fff;
border-radius: 5px;
width: 90%;
margin: -1.5rem auto 10px;
z-index: 2;
padding:.28rem;
border:0px solid #000;
padding-bottom:.5rem;

}
.form h1{ font-size:0.3rem; font-weight:normal;color:#444;margin:0px;padding:0px;margin-bottom:0.2rem;}
input{ disp3lay:inline-block; width:100%; background-color:#f1f0f0;border:0px;height:0.8rem;l4ine-height:0.8rem;font-size:0.3rem;color:#555;
padding-bottom:2px;
}
input.user{background:#f1f0f0 url(images/user.png) no-repeat 0.1rem center;background-size:.58rem;padding-left:.75rem;}
input.tel{background:#f1f0f0 url(images/tel.png) no-repeat 0.2rem center;background-size:.5rem;padding-left:.75rem;}
input.pass{background:#f1f0f0 url(images/pass.png) no-repeat 0.1rem center;background-size:.65rem;padding-left:.75rem;}
input.code{background:#f1f0f0 url(images/code.png) no-repeat 0.2rem center;background-size:.42rem;padding-left:.75rem;}
.btn{display:block;
border:0px;
backgro3und: -webkit-linear-gradient(top,#1fd5e6,#499BFE);
backgrou3nd: -o-linear-gradient(top,#21b6f8,#499BFE);
backgrou3nd: -moz-linear-gradient(top,#21b6f8,#499BFE);
backgrou3nd: -mos-linear-gradient(top,#1fd5e6,#499BFE);
background-color: #499BFE;
border-radius: 22px;color: #fff;width: 100%;height: .8rem;line-height: .8rem;box-shadow: 0 5px 9px #dadefd;text-align:center;text-decoration:none;
font-size:.3rem;}

.cel{
margin-bottom:0.26rem; 
display: -webkit-box;
display: -webkit-flex;
display: flex;
-webkit-box-align: center;
-webkit-align-items: center;
align-items: center;
}

.cel .box{-webkit-box-flex: 1;-webkit-flex: 1;flex: 1;}

.icons{text-align:center;font-size:0px;margin-top:10px;}
.icons a{border:0px solid #000;display:inline-block;margin-left:.3rem;margin-right:.3rem;}

.fixed-bottom {
    position:absolute;
    width: 100%;
    bottom: 10px;
    left: 0;
    text-align: center;
    color: #499BFE;
}
.fixed-bottom a {
    color: #499BFE;
    text-decoration: none;
    padding: 6px 10px 6px 10px;
    font-size: 14px;
}
#step1{width:100%;height:100%;position:fixed; background:rgba(0,0,0,0.7);z-index:19991014; text-align:right; vertical-align:top;}
#step1 img{max-width:100%;}
#step2{width:100%;height:100%;position:absolute; background:#F3F2F2;z-index:19991014;}
#step2 img{max-width:100%;position:absolute; bottom:10px;}
.gray{filter:grayscale(100%);filter: grayscale(1);FILTER: gray;-webkit-filter:grayscale(100%);-moz-filter:grayscale(100%);-ms-filter:grayscale(100%);
-o-filter:grayscale(100%);filter:progid:DXImageTransform.Microsoft.BasicImage(grayscale=1);-webkit-filter:grayscale(1);
}
</style>
<script>
    if (typeof localStorage === "object") {

    }
</script>
<%if (Request.QueryString["app"] != null || isIphone())
  { %>
 <script>
     if (typeof localStorage === "object")
     {
         if (localStorage.cookie && 1==2)
         {
             //document.cookie = localStorage.cookie;
             //document.location.href = "<%=HttpUtility.UrlDecode(fromURL)%>";
         }         
     }
</script>
<%} %>
<body>
<div id="step1" onClick="$(this).hide();" style="display:none;">
<img src="images/step1.png">
</div>
<div id="step2" onClick="$(this).hide();" style="display:none;">
<img src="images/step2.png">
</div>

<div class="container">
<div class="area"><%= eConfig.ApplicationTitle("") %></div>

<div class="form">
 <form name="form1" method="post" action="" onSubmit="return checkfrm(this);">
   <input type="hidden" id="iphoneapp" name="iphoneapp" value="0" />
   <input type="hidden" name="app" value="<%=(Request.QueryString["app"]!=null ? Request.QueryString["app"].ToString() : "")%>" />
<h1>帐号登录</h1>
<div class="cel"><input type="text" id="yhm" name="yhm" class="user" notnull="true" fieldname="登录帐号"  placeholder="登录帐号" /></div>
<div class="cel" style="display:none;">
<div class="box">
<input type="text" name="sjh" id="sjh" class="tel" fieldname="手机号"  placeholder="手机号" />
</div>
<div style="width:1.8rem;margin-left:10px;">
<a class="btn" href="javascript:;" style="border-radius: 5px;">获取验证码</a>
</div>

</div>
<div class="cel"><input type="password" name="mm" id="mm" class="pass" notnull="true" fieldname="登录密码"   placeholder="登录密码" autocomplete="off" /></div>
 <%if (eConfig.openRndCode()){ %>
<div class="cel" style="margin-bottom:20px;"><input type="text" name="yzm" id="yzm" maxlength="4" class="code" notnull="true" fieldname="验证码" onFocus="getrnd(0);" placeholder="验证码" autocomplete="off" />
<img id="rndpic" style="cursor:pointer;width:0.95rem;height:0.36rem;margin-left:5px;" onClick="getrnd();" src="../images/none.gif">
</div> <%} %>



<input type="submit" name="Submit" class="btn" value="登录" />
</form>
</div>

<div style="text-align:center;color:#555;">其他登录方式</div>

<div class="icons">
<a href="javascript:;" class="gray" onClick="layer.msg('可根据业务需求集成第三方登录通道一键登录!');"><img src="images/icon1.png" width="40" height="40"></a>
<a href="javascript:;" class="gray" onClick="layer.msg('可根据业务需求集成第三方登录通道一键登录!');"><img src="images/icon2.png" width="40" height="40"></a>
<a href="javascript:;" class="gray" onClick="layer.msg('可根据业务需求集成第三方登录通道一键登录!');"><img src="images/icon3.png" width="40" height="40"></a>
</div>

 <%if((isIphone() || isAndroid())){%>
        <div id="setup" class="fixed-bottom">
			 <%if(isIphone()){%>			 
			 <%if(!isIphoneSafari()){%><a href="javascript:;" onClick="$('#step1').show();">添加到主屏幕&nbsp;&gt;</a><%}%>			 
			 <%if(isIphoneSafari()){%><a href="javascript:;" onClick="$('#step2').show();">添加到主屏幕&nbsp;&gt;</a><%}%>
			 <%}%>
			 
			 <%if(isAndroid()){%>			 
			  <%if(!isAndroidBrowser()){%><a href="javascript:;" onClick="$('#step1').show();">下载客户端&nbsp;&gt;</a><%}%>
             <%if(isAndroidBrowser()){%><a href="http://demo.eketeam.com/upload/app.apk">下载客户端&nbsp;&gt;</a><%}%>
			<%}%>
        </div>
<%}%>
 
 </div>
</body>
</html>
 <%if(isIphone()){%>
 <script>
     (function (document, navigator, standalone) {
         if (navigator[standalone]) {
             $("#setup").hide();
             $("#iphoneapp").val("1");
             if (typeof localStorage === 'object') {
                 if (localStorage.href && localStorage.cookie) {
                     //document.cookie = localStorage.cookie;
                     //document.location.href = localStorage.href;
                 }
             }
         }
     })(document, window.navigator, 'standalone');
</script>
<%}%>