﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="app_UserInfo.aspx.cs" Inherits="eFrameWork.AppMobile.app_UserInfo" %>
<%@ Import Namespace="EKETEAM.FrameWork" %>
<%@ Register src="ToolMenu.ascx" tagname="ToolMenu" tagprefix="uc1" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0,uc-fitscreen=yes" />
     <title><%=appTitle %></title>

    <link href="../Plugins/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet">
    <link href="../Plugins/Theme/default/mobile/base.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" /><!-- 移动端基础样式-->
    <link href="../Plugins/eControls/default/mobile/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" /><!-- eFrameWork控件样式-->
    <link href="../Plugins/Theme/default/mobile/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" /><!--移动端样式-->



    <link href="style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />
    <script src="../Scripts/Init.js"></script>
	
	<script src="../Plugins/layui226/layui.all.js?ver=<%=Common.Version %>"></script>
    <script src="../Plugins/Theme/default/mobile/layout.js?ver=<%=Common.Version %>"></script>
    <script src="js/fastc333lick.js?ver=<%=Common.Version %>"></script>
    <script src="js/common.js?ver=1.0.61"></script>

     

</head>

 <style>
html,body {backg3round-color:#fff;}


.form-button{
    display: block;
    height: 40px;
	line-height: 40px;
    margin-top: 16px;
    text-align: center;
   
    border-radius: 7px;
    background-color: #499BFE;
    font-size: 15px;
    color: white;
    text-decoration: none;
}
</style>
<body>
<div class="eHeader_space"></div> 
<div class="eHeader">
<div class="left"><a href="javascript:;" onclick="history.back();" class="return"></a></div>
<div class="center">基本信息</div>
<div class="right">
    <%
if (model.Action=="view")
{%>
<a href="<%=eParameters.ReplaceAction("edit")%>" class="edit"></a>
 <%}%>
</div>
</div>

	
<div style="margin-top:10px;">
 <asp:Literal id="litBody" runat="server" />
</div>


<uc1:ToolMenu ID="ToolMenu1" runat="server" />
</body>
</html>

