﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="app_Statistics.aspx.cs" Inherits="eFrameWork.AppMobile.app_Statistics" %>
<%@ Register src="ToolMenu.ascx" tagname="ToolMenu" tagprefix="uc1" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><%=appTitle %></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0,uc-fitscreen=yes" />
    <link href="../Plugins/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet">
    <link href="../Plugins/Theme/default/mobile/base.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" /><!-- 移动端基础样式-->
    <link href="../Plugins/eControls/default/mobile/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" /><!-- eFrameWork控件样式-->
    <link href="../Plugins/Theme/default/mobile/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" /><!--移动端样式-->




    <link href="style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery.js"></script>
	<script src="../Plugins/layui226/layui.all.js?ver=<%=Common.Version %>"></script>
    <script src="../Scripts/eketeam.js?ver=<%=Common.Version %>"></script>
  

	<script src="../Plugins/AntV/g2.min.js"></script>
<style>
html,body{ background-color:#EDEDED;}
</style>
</head>
<body>
<div class="eHeader_space"></div> 
<div class="eHeader">
<div class="left"></div>
<div class="center"><%=model.ModelInfo["MC"].ToString()%></div>
<div class="right"></div>
</div>

<div class="bodyitem">
<div style="border-bottom:1px solid #ededed;padding-bottom:6px;">
<div class="item_title">2020年-年度目标完成情况</div>
</div>
<div style="margin:5px;font-size:0.28rem; text-align:center;padding-top:5px;padding-bottom:5px;color:#555;">75%
<div style="width:100%;" class="progress">
<div style="width:75%;"></div> 
</div>
</div>
</div>



<div class="bodyitem">
<div class="eReport" style="width:100%;min-height:380px;margin-top:0px;" ControlType="pie">
<h1>2020年公司费用支出情况统计</h1>
<p>以财务上报数据统计</p>
<div id="demo0" class="eReportItem"></div>
<script>
function getReportHeight(obj)
{
	var rep=obj.parent();
	var height=rep.height();
	if(rep.find("h1").height()){height-=rep.find("h1").height()+14;}
	if(rep.find("p").height()){	height-=rep.find("p").height()+14;}
	return height;
};

var data0 = [{text:"日常",value:15100},{text:"办公",value:32120},{text:"出差",value:25000},{text:"福利",value:16000},{text:"其他",value:7500}];
var allvalue=0;
for(var i=0;i<data0.length;i++){
allvalue += parseFloat(data0[i].value);
}
var chart0 = new G2.Chart({
    container: 'demo0',
forceFit: true,
height: getReportHeight($('#demo0')),
padding: 'auto'});
chart0.source(data0, {
percent: {
formatter: function formatter(val) {
val = (val * 100).toFixed(0) + '%';
return val;
}
}
});

chart0.coord('theta');
chart0.tooltip({
showTitle: false
});
    chart0.intervalStack().position('value').color('text').label('value', {
offset: -40,
formatter: function formatter(text, item) {
var d = item.point;
return  parseFloat((d.value/allvalue) * 100).toFixed(0) + '%';
},
textStyle: {
fill: 'white',
textAlign: 'center',
shadowBlur: 2,
shadowColor: 'rgba(0, 0, 0, .45)'
}
}).tooltip('text*value', function(text, value) {
return {
name: text,
value: value
};
}).style({
lineWidth: 1,
stroke: '#fff'
});
    chart0.render();
</script>
</div>
</div>

<div class="bodyitem">
<div class="eReport" style="width:100%;min-he3ight:380px;margin-top:0px;" ControlType="pie">
<h1>2020年公司营收情况统计</h1>
<p>以财务上报数据统计</p>
<div id="demo1" class="eReportItem"></div>
<script>
  var data1 = [{
    year: '1',
    value: 31000
  }, {
    year: '2',
    value: 27000
  }, {
    year: '3',
    value: 35000
  }, {
    year: '4',
    value: 15100
  }, {
    year: '5',
    value: 49000
  }, {
    year: '6',
    value: 60000
  }, {
    year: '7',
    value: 26000
  }, {
    year: '8',
    value: 51001
  }, {
    year: '9',
    value: 11300
  }
  , {
    year: '10',
    value: 75400
  }
  , {
    year: '11',
    value: 213111
  }
  , {
    year: '12',
    value: 130000
  }
  ];
  var chart = new G2.Chart({
    container: 'demo1',
    forceFit: true,
    height: $("#demo1").height(),
	padding: 'auto'
  });
  chart.source(data1);
  chart.scale('value', {
      min: 0, alias: '收入'
  });
  chart.scale('year', {
    range: [0, 1]
  });
  chart.tooltip({
    crosshairs: {
      type: 'line'
    }
  });
  chart.line().position('year*value');
  chart.point().position('year*value').size(4).shape('circle').style({
    stroke: '#fff',
    lineWidth: 1
  });
  chart.render();
</script>
</div>
</div>

<div class="bodyitem">
<div class="eReport" style="width:100%;min-he4ight:380px;margin-top:0px;" ControlType="pie">
<h1>2020年公司全家经营情况统计</h1>
<p>以财务上报数据统计</p>
<div id="demo2" class="eReportItem"></div>
<script>
 var data2 = [{
    year: '一季度',
    sales: 380000
  }, {
    year: '二季度',
    sales: 520000
  }, {
    year: '三季度',
    sales: 610000
  }, {
    year: '四季度',
    sales: 450000
  }];
  var chart = new G2.Chart({
    container: 'demo2',
    forceFit: true,
    height:$("#demo2").height(),
	padding: 'auto'
  });
  chart.scale('sales', { alias: '收益' });
  chart.source(data2);
  chart.interval().position('year*sales');
  chart.render();
</script>
</div>


</div>
<uc1:ToolMenu ID="ToolMenu1" runat="server" />
</body>    
</html>﻿
<script src="../Plugins/Theme/default/mobile/layout.js?ver=<%=Common.Version %>"></script>
<script src="js/fas333tclick.js?ver=<%=Common.Version %>"></script>	