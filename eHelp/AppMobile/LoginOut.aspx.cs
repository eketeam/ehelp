﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;

namespace eFrameWork.AppMobile
{
    public partial class LoginOut : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            eUser user = new eUser("Application");
            string app = "";
            string iphoneapp = "";
            if (user.Logined)
            {
                app = user["app"].ToString();
                iphoneapp = user["iphoneapp"].ToString();
                eFHelper.UserLoginOutLog(user);//用户退出日志
            }
            user.Remove();
            if (iphoneapp == "1" || app=="true")
            {
                Response.Write("<script>localStorage.removeItem('cookie');localStorage.removeItem('href');document.location='Login.aspx';</script>");
            }
            else
            {
                //Response.Write("退出成功!");
                Response.Redirect("Login.aspx" + (app.Length > 0 ? "?app=" + app : ""), true);
            }           
        }
    }
}