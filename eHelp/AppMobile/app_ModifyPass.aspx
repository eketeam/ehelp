﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="app_ModifyPass.aspx.cs" Inherits="eFrameWork.AppMobile.app_ModifyPass" %>
<%@ Register src="ToolMenu.ascx" tagname="ToolMenu" tagprefix="uc1" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0,uc-fitscreen=yes" />
     <title><%=appTitle %></title>

    <link href="../Plugins/font-awesome-4.7.0/css/font-awesome.css" rel="stylesheet">
    <link href="../Plugins/Theme/default/mobile/base.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" /><!-- 移动端基础样式-->
    <link href="../Plugins/eControls/default/mobile/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" /><!-- eFrameWork控件样式-->
    <link href="../Plugins/Theme/default/mobile/style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" /><!--移动端样式-->



    <link href="style.css?ver=<%=Common.Version %>" rel="stylesheet" type="text/css" />
    <script src="../Scripts/Init.js"></script>
	
	<script src="../Plugins/layui226/layui.all.js?ver=<%=Common.Version %>"></script>
    <script src="../Plugins/Theme/default/mobile/layout.js?ver=<%=Common.Version %>"></script>
    <script src="js/fastc333lick.js?ver=<%=Common.Version %>"></script>




  

</head>

 <style>
html,body {backg3round-color:#fff;}


.form-button{
    display: block;
    height: 40px;
	line-height: 40px;
    margin-top: 16px;
    text-align: center;
   
    border-radius: 7px;
    background-color: #499BFE;
    font-size: 15px;
    color: white;
    text-decoration: none;
}
</style>
<script>
    function modifyPass(frm) {
        var _back = eCheckform(frm);
        if (!_back) { return; }
        $.ajax({
            type: 'post',
            url: $(frm).attr("action"),
            data: "ajax=true&" + $(frm).serialize(),
            dataType: "json",
            success: function (data) {
                if (parseBool(data.success)) {
                    layer.msg(data.message);
                    
                }
                else {
                   layer.msg(data.message);
                }
                $(document).find("input[type='password']").each(function (i, elem) {
                    elem.value = "";
                });
            }
        });
    };
</script>
<body>
<div class="eHeader_space"></div> 
<div class="eHeader">
<div class="left"><a href="javascript:;" onClick="history.back();" class="return"></a></div>
<div class="center">修改密码</div>
<div class="right">
</div>
</div>

	
<div style="padding:10px;">
<form id="frmaddoredit" name="frmaddoredit" method="post" action="">
<input type="hidden" id="act" name="act" value="save">
<input type="hidden" id="fromurl" name="fromurl" value="">
<table class="eDataView" width="100%" cellspacing="0" cellpadding="0" border="0" style="margin:0px;">
<colgroup>
<col width="120">
<col>
</colgroup>
<tbody>  <tr>
      <td width="120" class="title"><font color="#FF0000"> *</font>&nbsp;旧密码：</td>
      <td class="content"><span class="eform"><input name="f1" type="password" class="text" id="f1" value="" fieldname="旧密码" notnull="true" style="width:150px;" /></span></td>
    </tr>
    <tr>
      <td class="title"><font color="#FF0000"> *</font>&nbsp;新密码：</td>
      <td class="content"><span class="eform"><input name="f2" type="password" class="text" id="f2" value="" fieldname="新密码" notnull="true" style="width:150px;" /></span></td>
    </tr>
    <tr>
      <td class="title"><font color="#FF0000"> *</font>&nbsp;确认密码：</td>
      <td class="content"><span class="eform"><input name="f3" type="password" class="text" id="f3" value="" fieldname="确认密码" notnull="true" equal="f2" style="width:150px;" /></span></td>
    </tr>
<tr>
<td colspan="2" class="title" style="border:0px;text-align:left;padding-lef3t:105px;">

<a class="form-button" href="javascript:;" onClick="modifyPass(frmaddoredit);"  onClick2="ajaxSubmit(frmaddoredit);" _onclick="if(form1.onsubmit()!=false){form1.submit();}">
                <span style="color:#fff;">保存修改</span>
            </a>


</td>
</tr>
</tbody>
</table>

</form>
</div>


<uc1:ToolMenu ID="ToolMenu1" runat="server" />
</body>
</html>

