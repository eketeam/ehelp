﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using EKETEAM.UserControl;
using System.Text;

namespace eFrameWork.AppMobile
{
    public partial class app_UserInfo : System.Web.UI.Page
    {
        public string UserArea = "Application";
        private eUser user;
        public string AppItem = eParameters.Request("AppItem");
        public string act = eParameters.Request("act");
        public eModel model;
        public string appTitle = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser(UserArea);
            user.Check();
            appTitle = eConfig.ApplicationTitle(user["SiteID"].ToString());

            string ModelID = "5fcc3772-39be-4c7a-8c6e-9f64781407ae";
            model = new eModel(ModelID, user);
            model.clientMode = "mobile";
            model.Power["view"] = "true"; //给查看权限
            model.Power["edit"] = "true"; //给添加权限
            model.eForm.ID = user.ID;
            model.eForm.onChange += eForm_onChange;
            switch (act)
            {
                case "":
                    model.Action = "view";
                    model.eForm.Action = "view";
                    litBody.Text = model.getActionHTML();
                    break;
                case "edit":
                    model.Action = "edit";
                    model.eForm.Action = "edit";
                    litBody.Text = model.getActionHTML();
                    break;
                case "save":
                    model.Save();
                    break;
            }

        }
        private void eForm_onChange(object sender, eFormTableEventArgs e)
        {
            if (e.eventType == eFormTableEventType.Updated)
            {
                DataTable tb = eBase.DataBase.getDataTable("select * from a_eke_sysUsers where UserID='" + e.ID + "'");
                if (tb.Rows.Count > 0)
                {
                    user["name"] = tb.Rows[0]["xm"].ToString();
                    if (tb.Rows[0]["face"].ToString().Length > 10)
                    {
                        user["face"] = eBase.getVirtualPath() + tb.Rows[0]["face"].ToString();
                    }
                    user.Save();
                }
            }
        }
    }
}