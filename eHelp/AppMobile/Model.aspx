﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="Main.Master" CodeFile="Model.aspx.cs" Inherits="eFrameWork.AppMobile.Model" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">        
<%if (!Ajax && eBase.parseBool(model.ModelInfo["mAutoLayout"]))
  { %>
<div class="eHeader">
<div class="left"><%if (model.Action.Length > 0 && model.Action.ToLower() != "building"){%><a href="<%=model.ListURL %>" class="return"></a><%}else{%><a href="javascript:;" nowait="true" class="menu" _onclick="_showmenu();"></a><%}%></div>
<div class="center"><%=model.ModelInfo["MC"].ToString()%></div>
<div class="right">
<%if (model.Action == "" && model.Conditions.Select("show=1 and ControlType<>'quick' and ControlType<>'searchgroup'").Length > 0)
  {%><a href="javascript:;" class="search"  nowait="true" onclick="switchSearch();"></a><%}%>
<%if(model.Action=="" && model.Power["Add"]){%><a href="<%=model.eForm.getAddURL()%>" class="add"></a><%}%>

<%
      if (model.Action == "view" && model.ModelInfo["viewActionBotton"].ToString() == "True")
{%>
 <% if (model.Power["Edit"] && model.ID.IndexOf(",") == -1)
    {%>
<a href="<%=eParameters.ReplaceAction("edit")%>" class="edit"></a>
 <%}%>
 <% if (model.Power["Del"] && model.ID.IndexOf(",") == -1)
    {%>
<a href="<%=eParameters.ReplaceAction("del")%>" onclick="javascript:return confirm('确认要删除吗？');" class="close"></a>
<%}%>
<%}%>
</div>
</div>
<%} %>
<%= model.StartHTML   %>
<%= model.Tip  %>
<asp:Literal ID="LitBody" runat="server" />
</asp:Content>