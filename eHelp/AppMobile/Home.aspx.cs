﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using EKETEAM.UserControl;
using System.Text;

namespace eFrameWork.AppMobile
{
    public partial class Home : System.Web.UI.Page
    {
        public string UserArea = "Application";
        private eUser user;
        public string AppItem = eParameters.Request("AppItem");
        public eModel model;
        public string appTitle = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser(UserArea);
            user.Check();
            appTitle = eConfig.ApplicationTitle(user["SiteID"].ToString());

            eModelInfo customModel = new eModelInfo(user);
            model = customModel.Model;

            DataTable dt = eBase.a_eke_sysApplicationIcons.Select("ApplicationID='" + model.ApplicationID + "' and show=1", "px,addtime desc").toDataTable();
            if (dt.Rows.Count > 0)
            {
                RepIcons.DataSource = dt;
                RepIcons.DataBind();
            }

            eList list = new eList("AppAds");
            list.Fields.Add("mc,pic,url");
            list.Where.Add("SiteID='" + user["siteid"] + "' and show=1 and deltag=0");
            list.OrderBy.Add("px,addtime desc");
            RepAds.ItemDataBound += RepAds_ItemDataBound;
            if (list.getDataTable().Rows.Count > 0) list.Bind(RepAds);
            

        }
        protected void RepAds_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Control ctrl = e.Item.Controls[0];
                Literal lit = (Literal)ctrl.FindControl("LitPic");
                if (lit != null)
                {
                    string text = "<img src=\"" + eBase.getVirtualPath() + DataBinder.Eval(e.Item.DataItem, "pic").ToString() + "\" />";
                    if (DataBinder.Eval(e.Item.DataItem, "url").ToString().Length > 0)
                    { 
                        text = "<a href=\"" + DataBinder.Eval(e.Item.DataItem, "url").ToString() + "\">" + text + "</a>";
                    }
                    lit.Text = text;
                }
            }
        }
    }
}