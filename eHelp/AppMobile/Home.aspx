﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="eFrameWork.AppMobile.Home" %>
<%@ Import Namespace="EKETEAM.FrameWork" %>

    <style>
html,body{ background-color:#EDEDED;}
</style>
<script>
    function show_body(a) {
        var index = $(a).index();
        var dl = getParent(a, "dl");
        $(dl).find("dt a").removeClass("cur");
        $(dl).find("dt a:eq(" + index + ")").addClass("cur");
        $(dl).find("dd>div").hide();
        $(dl).find("dd>div:eq(" + index + ")").show();
    };
</script>
<div class="notice">
<ul>
<li><a href="javascript:;" _href="Article.aspx?id=2" onClick="layer.msg('暂未实现!');"><i>查看详情</i>公司2021年春节放假通知!</a></li>
<li><a href="javascript:;" _href="Article.aspx?id=1" onClick="layer.msg('暂未实现!');"><i>查看详情</i>员工手册</a></li>
</ul>
</div>
<script type="text/javascript">
	function AutoScroll(obj){
		$(obj).find("ul:first").animate({
			marginTop:"-40px"
		},500,function(){
			$(this).css({marginTop:"0px"}).find("li:first").appendTo(this);
		});
	}
	$(document).ready(function(){
		setInterval('AutoScroll(".notice")',3000);
	});
	</script>


<asp:Repeater id="RepIcons" runat="server">
<HeaderTemplate><div class="icontext" style="background-color:#fff;padding-bottom:10px;"></HeaderTemplate>
<ItemTemplate>
<a href="<%# Eval("url").ToString() %>">
<img src="<%# eBase.getVirtualPath() + Eval("Icon").ToString() %>" />
<p><%# Eval("mc").ToString() %></p>
</a>
</ItemTemplate>
<FooterTemplate></div></FooterTemplate>
</asp:Repeater>

<asp:Repeater id="RepAds" runat="server">
<HeaderTemplate>
<div class="item" style="padding:0px 10px 10px 10px;">
<div id="banner">
<ul class="swiper-wrapper">
</HeaderTemplate>
<ItemTemplate>
<li class="swiper-slide"><asp:Literal ID="LitPic" runat="server" /></li>
</ItemTemplate>
<FooterTemplate>
</ul>
<div class="swiper-pagination"></div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        if ($("#banner li").length > 1) {
            var swiper = new Swiper("#banner", {
                pagination: ".swiper-pagination",
                paginationClickable: true,       //是否切换到当前图片对应的li 添加事件
                spaceBetween: 0,                 // 每个元素之间的 右外边距。 取值为number
                centeredSlides: true,            // 设置元素位置  取值 true:居中 / 取值 false:居左
                autoplay: 2500,                  //　自动轮播的间隔设置 单位 (ms)
                autoplayDisableOnInteraction: false,  //设置滑动元素后是否自动轮播　取值 false 是启动 / 取值true 不启动
                loop: true
            });
        }
    });
</script>
</div>
</FooterTemplate>
</asp:Repeater>




<div class="bodyitem">
<div style="border-bottom:1px solid #ededed;padding-bottom:6px;">
<div class="item_title">2020年-年度目标完成情况</div>
</div>
<div style="margin:5px;font-size:0.28rem; text-align:center;padding-top:5px;padding-bottom:5px;color:#555;">75%
<div style="width:100%;" class="progress">
<div style="width:75%;"></div> 
</div>
</div>
</div>



<div class="bodyitem">
<dl class="dlgroup">
<dt style="border-bottom:0;"><div>2020年-年度经营情况<span><a href="javascript:;" onClick="show_body(this);" class="cur">按月</a><a href="javascript:;" onClick="show_body(this);">按季度</a><span></div></dt>
<dd>
<div>
    <table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#DDDDDD" style="margin-top:0px;">
<colgroup>
 <col />
 <col width="27%" />
 <col width="27%" />
 <col width="27%" />
 </colgroup>

<thead>
  <tr bgcolor="#F9F9F9">
    <td>时间</td>
    <td>收入</td>
    <td>支出</td>
	<td>营利</td>
  </tr>
  </thead>
  <tbody>
  <tr bgcolor="#FFFFFF">
    <td>1月</td>
    <td class="num">56000.00</td>
    <td class="num">38000.00</td>
	<td class="num green">18000.00</td>
  </tr>
  <tr bgcolor="#F9F9F9">
    <td>2月</td>
    <td class="num">45000.00</td>
    <td class="num">55000.00</td>
	<td class="num red">-10000.00</td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td>3月</td>
    <td class="num">120000.00</td>
    <td class="num">43000.00</td>
	<td class="num green">77000.00</td>
  </tr>
   <tr bgcolor="#F9F9F9">
    <td>4月</td>
    <td class="num">55000.00</td>
    <td class="num">53000.00</td>
	<td class="num yellow">2000.00</td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td>5月</td>
    <td class="num">63000.00</td>
    <td class="num">41000.00</td>
	<td class="num green">22000.00</td>
  </tr>
   <tr bgcolor="#F9F9F9">
    <td>6月</td>
    <td class="num">95000.00</td>
    <td class="num">45000.00</td>
	<td class="num green">50000.00</td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td>7月</td>
    <td class="num">21500.00</td>
    <td class="num">43600.00</td>
	<td class="num red">-22100.00</td>
  </tr>
   <tr bgcolor="#F9F9F9">
    <td>8月</td>
    <td class="num">78000.00</td>
    <td class="num">41500.00</td>
	<td class="num green">36500.00</td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td>9月</td>
    <td class="num">80100.00</td>
    <td class="num">47500.00</td>
	<td class="num green">32600.00</td>
  </tr>
   <tr bgcolor="#F9F9F9">
    <td>10月</td>
    <td class="num">31500.00</td>
    <td class="num">43500.00</td>
	<td class="num red">-12000.00</td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td>11月</td>
    <td class="num">20100.00</td>
    <td class="num">42300.00</td>
	<td class="num red">-22200.00</td>
  </tr>
   <tr bgcolor="#F9F9F9">
    <td>12月</td>
    <td class="num">15000.00</td>
    <td class="num">46750.00</td>
	<td class="num red">-31750.00</td>
  </tr>
  </tbody>
</table>
</div>
<div style="display:none;">
    <table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#DDDDDD" style="margin-top:0px;">
<colgroup>
 <col />
 <col width="27%" />
 <col width="27%" />
 <col width="27%" />
 </colgroup>
<thead>
  <tr bgcolor="#F9F9F9">
    <td>时间</td>
    <td>收入</td>
    <td>支出</td>
	<td>营利</td>
  </tr>
  </thead>
  <tbody>
  <tr bgcolor="#FFFFFF">
    <td>一季度</td>
    <td class="num">221000.00</td>
    <td class="num">136000.00</td>
	<td class="num green">85000.00</td>
  </tr>
  <tr bgcolor="#F9F9F9">
    <td>二季度</td>
    <td class="num">213000.00</td>
    <td class="num">139000.00</td>
	<td class="num green">74000.00</td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td>三季度</td>
    <td class="num">179600.00</td>
    <td class="num">132600.00</td>
	<td class="num green">47000.00</td>
  </tr>
   <tr bgcolor="#F9F9F9">
    <td>四季度</td>
    <td class="num">66600.00</td>
    <td class="num">132550.00</td>
	<td class="num red">-65950.00</td>
  </tr>
   <tr bgcolor="#FFFFFF">
    <td><b>合计</b></td>
	<td class="num"><b>680200.00</b></td>
    <td class="num"><b>540150.00</b></td>
	<td class="num green"><b>140050.00</b></td>
  </tr>
  </tbody>
</table>
</div>
</dd>
</dl>

	
</div>

