﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EKETEAM.Data;
using EKETEAM.FrameWork;
using EKETEAM.UserControl;
using System.Text;

namespace eFrameWork.AppMobile
{
    public partial class ToolMenu : System.Web.UI.UserControl
    {
        private eUser user;
        public string AppItem = eParameters.Request("AppItem");
        public DataRow[] topRows;
        public DataRow[] rsSub;
        private DataTable _applicationitems;
        private DataTable ApplicationItems
        {
            get
            {
                if (_applicationitems == null)
                {
                    _applicationitems = eBase.a_eke_sysApplicationMenus.Select("ApplicationID='" + AppID + "'").toDataTable();
                    if (_applicationitems.Rows.Count == 0) _applicationitems = eBase.a_eke_sysApplicationMenus.Clone();
                    for (int i = _applicationitems.Rows.Count - 1; i >= 0; i--)
                    {
                        string cond = _applicationitems.Rows[i]["condDisable"].ToString();
                        if (cond.Length > 0)
                        {
                            cond = eParameters.Replace(cond, null, user);
                            string result = eVsa.Eval(cond);
                            if (result.ToLower() == "true")
                            {
                                _applicationitems.Rows.Remove(_applicationitems.Rows[i]);
                            }
                        }
                    }
                }
                return _applicationitems;
            }
        }
        private string _appid;
        private string AppID
        {
            get
            {
                if (_appid == null)
                {
                    _appid = eBase.a_eke_sysApplicationItems.Select("ApplicationItemID='" + AppItem + "'")[0]["ApplicationID"].ToString();
                }
                return _appid;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            user = new eUser("Application");
            user.Check();
            StringBuilder sb = new StringBuilder();
            rsSub = ApplicationItems.Select("ApplicationID='" + AppID + "' and ParentID is not Null");
            if (rsSub.Length > 0)
            {
                #region 多级菜单
                topRows = ApplicationItems.Select("ApplicationID='" + AppID + "' and Show=1 and ParentID is Null", "px,addTime");
                if (topRows.Length > 0)
                {
                    string width = "";
                    #region 设置菜单宽度
                    switch (topRows.Length)
                    {
                        case 1:
                            width = "100%";
                            break;
                        case 2:
                            width = "50%";
                            break;
                        case 3:
                            width = "33.33%";
                            break;
                        case 4:
                            width = "25%";
                            break;
                        default:
                            width = "20%";
                            break;
                    }
                    #endregion
                    sb.Append("<ul class=\"toolmenu\">\r\n");
                    for (int i = 0; i < topRows.Length && i < 5; i++)
                    {
                        DataRow[] rs = ApplicationItems.Select("ParentID='" + topRows[i]["ApplicationMenuID"].ToString() + "' and len(mc)>0 and len(url)>0 and show=1", "px,addtime");
                        sb.Append("<li style=\"width:" + width + ";\">");
                        if (rs.Length == 0)
                        {
                            string url = topRows[i]["url"].ToString().Length > 0 ? eParameters.Replace(topRows[i]["url"].ToString(), null, user) : "javascript:;";
                            sb.Append("<a href=\"" + url + "\">" + topRows[i]["mc"].ToString() + "</a>");
                        }
                        else
                        {
                            sb.Append("<a href=\"javascript:;\"><span class=\"icon\"></span>" + topRows[i]["mc"].ToString() + "</a>");
                            sb.Append("<ul>\r\n");
                            foreach (DataRow dr in rs)
                            {
                                string url = dr["url"].ToString().Length > 0 ? eParameters.Replace(dr["url"].ToString(), null, user) : "javascript:;";
                                sb.Append("<li><a href=\"" + url + "\" _target=\"_blank\">" + dr["mc"].ToString() + "</a></li>\r\n");
                            }
                            sb.Append("</ul>\r\n");
                        }
                        sb.Append("</li>\r\n");
                    }
                    sb.Append("</ul>\r\n");
                }
                #endregion
            }
            else
            {
                #region 单级菜单
                topRows = ApplicationItems.Select("ApplicationID='" + AppID + "' and Show=1 and ParentID is Null", "px,addTime");
                if (topRows.Length > 0)
                {
                    sb.Append("<div class=\"toolflex\">\r\n");
                    for (int i = 0; i < topRows.Length && i < 5; i++)
                    {
                        bool cur = topRows[i]["url"].ToString().Length > 0 && Request.Url.PathAndQuery.ToLower().IndexOf(topRows[i]["url"].ToString().ToLower()) > -1;
                        if (!cur)
                        {
                            string _appitem = topRows[i]["url"].ToString().getQuery("appitem");
                            if (_appitem.Length > 0)
                            {
                                DataRow[] rows = eBase.a_eke_sysApplicationItems.Select("ApplicationItemID='" + AppItem + "'");
                                if (rows.Length > 0)
                                {
                                    if (_appitem == rows[0]["PackID"].ToString()) cur = true;
                                }
                            }
                        }
                        string url = topRows[i]["url"].ToString().Length > 0 ? eParameters.Replace(topRows[i]["url"].ToString(), null, user) : "javascript:;";
                        sb.Append("<a href=\"" + url + "\"" + (cur ? " class=\"cur\"" : "") + ">");
                        if (topRows[i]["IconHTML"].ToString().Length > 0)
                        {
                            sb.Append(topRows[i]["IconHTML"].ToString());
                        }
                        else
                        {
                            string icon = topRows[i]["icon"].ToString();
                            if (cur && topRows[i]["IconActive"].ToString().Length > 0) icon = topRows[i]["IconActive"].ToString();
                            if (icon.Length == 0) icon = "images/noicon.png";
                            sb.Append("<img src=\"../" + icon + "\">");
                        }
                        sb.Append(topRows[i]["mc"].ToString() + "</a>");
                    }
                    sb.Append("</div>\r\n");
                }
                #endregion
            }
            LitMenu.Text = sb.ToString();
        }
    }
}